---
title: "About Words On Health"
layout: page-sidebar
permalink: "/about.html"
image: "/assets/images/screenshot.jpg"
comments: true
---
The practical health information in Words On Health is based on research conducted either by NIH’s own scientists or by grantees at universities and medical schools around the country. The National Institutes of Health (NIH) plays a major role in finding better ways to diagnose, treat, cure or prevent diseases.

All scientific and medical information is reviewed for accuracy by representatives of the National Institutes of Health. However, personal decisions regarding health, finance, exercise, and other matters should be made only after consultation with the reader’s physician or professional advisor. Opinions expressed here are not necessarily those of wordsonhealth.com

The content of this site is offered as educational material only, not as medical advice. If you have a question about a specific condition or symptom then you need to consult a medical professional.
