---
layout: post 
title: "Protect Your Hearing"
date: 2019-03-10
image: assets/images/protect-your-hearing.webp
excerpt: "Protect your hearing health now, while you still can"
tags: Deafness, Ears, hearing, Hearing Loss, NIDCD, NIHL
draft: false
---

Many years ago, a high school sophomore went to his first rock concert with his buddies. They stayed up late, ecstatic with the excitement of the teeming crowd, overwhelmed by the driving, booming beat. They couldn’t even hear each other over the loud music coming from the stacks of speakers, so they shouted into each other’s ears about how much fun they were having. At one point, the young man felt a pain in his ear, but until the concert was over and they walked outside, he had no idea how much damage he’d done to his hearing in just those couple of hours.

According to the National Institute on Deafness and Other Communication Disorders (NIDCD), even one minute of exposure to a noise over 110 decibels (a measure of loudness) can lead to permanent hearing loss. An amplified rock concert is typically around 140 decibels. In my case — yes, I’m the unfortunate fellow described above — my ears felt so clogged afterward that I could barely hear the people speaking to me. I also heard a constant, high-pitched tone, a condition called tinnitus. While I regained most of my hearing within a couple of days and the tinnitus lessened, I did suffer some permanent hearing damage. When I’m in a quiet room I can still hear the ringing. At night, I use a fan to drown out the sound so I can fall asleep.

**How do we hear?**

![img](/assets/images/35-Protect-Your-Hearing.webp)

The ear has three main parts: the outer, middle and inner ear. Sound enters through the outer ear and reaches the middle ear where it causes the ear drum to vibrate. The vibrations are transmitted through three tiny bones in the middle ear, called the ossicles. These three bones are named the malleus, incus and stapes (and are also known as the hammer, anvil and stirrup). The ear drum and ossicles amplify the vibrations and carry them to the inner ear. The stirrup transmits the amplified vibrations into the fluid that fills the inner ear. The vibrations move through fluid in the snail-shaped hearing part of the inner ear (cochlea) that contains the hair cells. The fluid in the cochlea moves the hair cells, which bring about nerve impulses. These nerve impulses are carried to the brain where they are interpreted as sound. Different sounds move the hair cells in different ways, allowing the brain to hear different sounds.

NIDCD is leading, in collaboration with the National Institute of Occupational Safety and Health, a coalition of 85 national organizations in a campaign to prevent the kind of noise-induced hearing loss (NIHL) I’ve experienced. Called WISE EARS!, the campaign includes events and a battery of materials to help people understand how we hear, how our hearing gets damaged, and how to prevent hearing loss.

**About NIHL**

NIDCD says that 30 million Americans of all ages are exposed to hazardous sound levels on a regular basis. Much of this exposure is at work, around machines and power tools, but people are also exposed to loud noises at play and at home. Sources of loud noises that can cause NIHL include motorcycles, firecrackers and small arms fire, all creating sounds from 120 decibels to 140 decibels. Snowmobiles, go-carts, power horns, cap guns and powered model airplanes can all damage your hearing as well. Harmful noises at home may come from vacuum cleaners, lawn mowers, leaf blowers and hair dryers.

Both the sensitive hair cells of the inner ear and the nerve that senses the sound (the auditory nerve — see diagram) are vulnerable to loud noises. Damage can result from either a brief intense noise like an explosion, or from noises like those produced by woodworking and power tools. The damage from a brief, intense sound may be instantaneous and result in an immediate hearing loss that can be permanent. It may be accompanied by tinnitus, the condition I have — a ringing, buzzing or roaring in the ears. Tinnitus may subside over time, but it also might continue constantly or intermittently for the rest of your life.

The damage that occurs over years of exposure to loud noise can also result in hearing loss or tinnitus. Often the person with the hearing loss does not recognize the gradual loss, but friends or family may. Sounds may gradually become distorted or muffled to you, and you may have difficulty understanding speech. If you suspect a problem, or others suggest you may be having difficulty hearing — for example, because you turn the television up too loud or have difficulty hearing conversations in restaurants — ask your doctor about getting a hearing test.

**Prevention**

Both forms of NIHL can be prevented by using hearing protectors such as ear plugs or ear muffs whenever you’re exposed to loud noise. You can get them at pharmacies, hardware stores, and sporting goods stores.

In a sense, I was lucky because I had a clear symptom of my hearing damage, the tinnitus. I started wearing earplugs to the many concerts I attended in the years that followed, and always make sure to wear them when using power tools or mowing the lawn. When I see people setting off firecrackers, or blasting music directly into their ears with personal stereos, or driving by with their music so loud I can hear it clearly through the closed car windows, I wonder how long it’ll be before they realize they are destroying their hearing. Permanent hearing loss isn’t worth the excitement of a booming bass line or a good guitar lick. Protect your hearing now, while you still can.

***A Word to the Wise...***

Noise-induced hearing loss (NIHL) is preventable. Everyone should understand the hazards of noise and how to prevent NIHL.

Know which noises can cause damage (see How Loud is too Loud? for examples).
Wear ear plugs or other hearing protective devices when involved in a loud activity (hearing protectors are available at pharmacies, hardware stores and sporting good stores).
Be alert to hazardous noise in the environment.
Protect children, especially those who are too young to protect themselves.
Make family, friends and colleagues aware of the hazards of noise.
Have a medical examination by an otolaryngologist, a physician who specializes in diseases of the ears, nose, throat, head and neck, and a hearing test by an audiologist, a health professional trained to identify and measure hearing loss and to help people with hearing impairments.

Source: NIDCD
