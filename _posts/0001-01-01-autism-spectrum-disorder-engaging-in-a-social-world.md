---
layout: post 
title: "Autism Spectrum Disorder Engaging in a Social World"
date: 2022-01-02T19:25:45+01:00
image: assets/images/autism-spectrum-disorder-engaging-in-a-social-world.webp
excerpt: New tools are being developed to help people with autism learn about emotions and interacting with others.
draft: false
---

Being social and making friends isn’t always easy. Relationships have many subtleties. But people with autism spectrum disorder, or ASD, struggle more than most. For them, communicating with others can be very difficult.

Autism is called a “spectrum” disorder because it’s not the same for everyone. Generally, people with autism both have difficulties with social communication and engage in repetitive behaviors.

ASD can also affect learning and problem-solving abilities. But people with ASD range from being very gifted to having severe challenges.

“Some people with ASD need a great deal of support and more extensive services, and some have a milder range of difficulties and need less support,” explains Dr. Lisa Gilotty, an autism expert at NIH.

Scientists don’t know the exact causes of ASD. They’re working hard to understand how different factors contribute. Genes, biology, and environment all can play a role in its development.

Parents of children with ASD may notice their child doesn’t respond to their name, avoids eye contact, or interacts with others only to achieve a specific goal. Kids with autism usually don’t understand how to play or engage with other children.

“Typically developing children who don’t have autism also have to learn how to interact with one another,” explains Dr. Dennis Wall, a child mental health and technology expert at Stanford University. “They’re inherently social and go engage with others. But kids with autism aren’t as social. They need to be brought into that social world.”

Researchers are studying ways to better engage people with autism. They’re developing tools that aid in learning emotions and interacting with others. They’re also looking for ways to identify who’s at risk earlier on. That way kids can get help as early as possible.

### Early Detection

Diagnosing ASD can be difficult. There are no medical tests. Doctors can only look at a child’s behavior as they age.

The symptoms of ASD usually appear around age two. That’s why experts recommend children be screened for autism at their 18- and 24-month well-child visits.

“Children with autism don’t look like they have symptoms in the first year of life,” says Dr. Joseph Piven, a child psychiatrist at the University of North Carolina. “Children at age six months are very social. Typically developing children can play peekaboo, laugh, and engage. The children who go on to develop autism are not distinguishable on the basis of their social behavior.”

Researchers are trying to detect changes in the brain before behavioral symptoms appear. “If you can identify children at risk, you can follow their development more closely and get them into a very early intervention program that can work to hopefully minimize that risk,” Gilotty explains.

Piven’s team is tracking infants’ brain development using brain scans. Their studies follow infants from families that have an older child with ASD.

“We looked at the brain in the first year of life and could predict which of those children are going to get a diagnosis of autism at age two,” Piven says. “We found that parts of the brain’s surface basically expanded more quickly in children who developed autism than in comparison children,” he says.

They also found differences in how brain networks function. His team is now trying to confirm these results in studies with more children. They’re [recruiting infants from families with autism](https://www.ibis-network.org/recruitment-1.html) to participate.

### Innovative Interventions

Getting treatment as early as possible may help change the way a child develops.

“In early infancy, the brain is considered much easier to change and so interventions may have a bigger effect,” Piven says.

Current treatments for autism involve behavioral therapy. “One of the most widely used strategies is called applied behavior analysis, or ABA, therapy,” explains Wall. “This therapy uses tools like flashcards to reinforce an understanding of facial emotion. So the flashcards will have happy faces and sad faces.”

Wall’s team has built a computerized version of this therapy. The system uses a camera on augmented-reality glasses. But instead of faces on flashcards, the camera captures the face of the actual person you’re talking with. Then, it identifies the person’s emotion in the glasses.

“The emotion comes in the form of an emoji, a written word, or color. It also emits a computerized voice saying the emotion through the earpiece,” Wall explains.

Kids with autism who used the technology showed improvements in social behaviors. Wall’s team is now testing a complementary approach in an app to help kids learn to act out emotions. It’s called “[Guess What](https://guesswhat.stanford.edu/).”

The app works as a game. One person holds the phone on their forehead and the other acts out the image displayed, like a surprise face. Then, you say what you think they’re acting out. If you’re right, you get a point and move to the next one.

“These tools are meant to be used in the same developmental windows that typically developing kids are learning emotions,” Wall says. They give kids with autism the extra help they need to understand their social world.

Another group is testing theater techniques for honing social and emotional abilities. People with autism act out a play alongside their peers. The plays have different themes that deal with age-appropriate topics.

“They focus on every aspect of what someone needs to be successful in their social interactions,” says Gilotty. “You have to think about your own character and what you’re projecting with your face, body, voice, and gestures. But you also have to think about the other characters in a play and what they’re thinking and what they’re feeling and what they’re projecting.”

A clinical trial led by Dr. Blythe Corbett at Vanderbilt University found that people with autism who were in the plays improved in their social understanding and interactions with peers.

### Personalized Treatments

Because autism is different for each person, researchers are searching for ways to identify which treatments will work best for whom.

“They’re looking for different markers that are associated with different types of social difficulties,” Gilotty says. “If you can begin to identify those brain differences, then you can develop treatments around them.”

People with ASD will face different challenges as they age. For some, symptoms may improve with age and treatment. But many will still need help as they get older. NIH-funded research also focuses on how to support the transition to adulthood.

You can be diagnosed with ASD at any age. Though symptoms show up in early childhood, they may go unnoticed until later. If you think you or your child may show signs of the condition, talk with your health care provider.

### Wise Choices

#### Signs of Autism

Children or adults with autism may:

- avoid eye contact and want to be alone.
- have difficulty making friends or interacting with peers. 
- miss social cues, such as facial expressions and gestures.
- avoid or resist physical contact.
- not point at objects to show interest or not look at objects when others point at them.
- have trouble talking about their own feelings or understanding others’ feelings.
- insist on familiar routines and get upset by minor changes. 
- be more or less sensitive than others to the way things smell, taste, look, feel, or sound.
