---
layout: post 
title: "Drought in Your Mouth?"
date: 2019-03-08
image: assets/images/drought-in-your-mouth.webp
excerpt: "Dry Mouth Remedies, Causes, Symptoms & Treatment"
tags: Dry Mouth, NIDCR, Salivary glands, Sjögren's Syndrome
draft: false
---

Dry mouth. It doesn’t receive a great deal of attention. But if you’re among the thousands of people who suffer from chronic dryness of the mouth, then you know how unpleasant, annoying and even dangerous it can be.

“It can seriously mar the quality of life,” notes Dr. Bruce Baum, chief of the Gene Therapy and Therapeutics Branch of the National Institute of Dental and Craniofacial Research (NIDCR) and an expert on salivary (saliva-producing) gland disorders. “Dry mouth can lead to cavities, mouth sores and infections; chewing, speaking and swallowing difficulties; and other problems. The good news is that there are therapeutic options that can often successfully deal with this symptom.”

Dry mouth comes about when the salivary glands don’t work properly. It is a common side effect of many medicines, including those for high blood pressure and depression. Long-lasting mouth dryness may also be the result of head and neck radiation treatments, chemotherapy, nerve damage, stress, or diseases such as diabetes, AIDS, Parkinson’s disease and Sjögren’s Syndrome (pronounced “show-grens”), a disorder in which the person’s own immune system targets their moisture-producing glands and causes dryness in the mouth and eyes. Contrary to popular belief, dry mouth is not a normal part of aging.

There is currently no cure for severe salivary gland damage. Dr. Baum and his colleagues have been working on novel approaches, including using gene transfer methods and trying to develop an artificial salivary gland. “Based on the results of our studies thus far,” Dr. Baum says, “I am optimistic that we or others will be able to help these patients in the not too distant future.” However, he wants to make it clear that such therapies are still being tested in the laboratory and not yet ready for patients.

In the meantime, certain treatments may help some people with dry mouth. If you suffer from the condition, talk to your doctor or dentist about possible solutions. See the accompanying side box for some suggestions.

***A Word to the Wise...***

What can you do if you experience chronic dry mouth? Dr. Bruce Baum of the National Institute of Dental and Craniofacial Research advises:

- Chew sugarless gum or candy or use a variety of fluids to keep your mouth moist.
- Talk to your doctor or dentist about the medicines you are taking. Many drugs, such as those used to treat high blood pressure and depression, cause dry mouth. You may need to have your dosage adjusted or your medicine changed.
- Discuss with your doctor or dentist the possibility of using artificial saliva to keep your mouth wet.
- There are prescription medicines that help the salivary glands function better. Your doctor or dentist may be able to prescribe one for you.
- When you eat, sip water or other non-caffeinated drinks to make chewing and swallowing easier.
- Avoid tobacco, alcohol, and caffeine, all of which dry out the mouth.
- Put a humidifier in your bedroom.
