---
layout: post 
title: "Depression in the Elderly"
date: 2019-01-02
image: assets/images/depression-in-the-elderly.webp
excerpt: "Early Intervention by Family, Friends Crucial"
tags: Depression, Elderly, Mental health, Temper
draft: false
---

Lately, Jim has noticed his 73-year-old mother is not herself. She isn’t sleeping through the night, and is often irritable during the day. She doesn’t want to play her weekly bridge game. In fact, she hardly seems interested in any of her usual routine. Her temper is short. Small annoyances set her off. When asked, she assures Jim she’s fine, just a little down these days. Jim wonders if this is just normal for a senior citizen.
It is not normal, according to research supported by the National Institute of Mental Health (NIMH), one of the National Institutes of Health. Jim’s mom is showing signs of depression, a common illness among elders. Depression is not a normal part of aging. After about two weeks of disinterest and insomnia, it is time to insist mom see her doctor, advises Dr. Charles Reynolds, director of the Late-Life Depression Clinic at the University of Pittsburgh, one of three Intervention Research Centers supported by NIMH.

“When an elder begins to have problems functioning, when he or she can no longer do his or her regular routine, that’s when the problem has moved beyond normal toward a diagnosis of clinical depression,” Dr. Reynolds says. “Typically, when an older person no longer feels engaged and seems to stop enjoying life for longer than two weeks, it’s time to seek medical help.”

Warning signals family and friends should look for include chronic sleep problems and inability to rest, excessive worrying, increasing dependency, withdrawal from friends and/or normal activities, hypochondria and complaints of chronic aches or pains that cannot be attributed to other disorders.

“Family members are key in getting elders into treatment, because too often elders don’t recognize depression in themselves,” Dr. Reynolds points out. In addition, he said, many senior citizens will not discuss a mental health problem with their doctors because they don’t want the term “depression” associated with them. Also, Dr. Reynolds continues, many doctors may be too pressed for time to ask about a person’s emotional state, and many of depression’s symptoms are often mistakenly attributed to some other disorder.

“Another problem is the stigma of mental illness that is prevalent in the current generation of older persons,” notes Dr. Barry Lebowitz, chief of the Adult/Geriatric Treatment and Prevention Research Branch, NIMH. Fortunately, negative perceptions traditionally associated with depression and other mental health issues are expected to decrease as Baby Boom-age Americans become senior citizens. “Future generations are thought to be more comfortable with the idea of mental illness as real illness,” Dr. Lebowitz says.

Only one in six elders with clinical depression get diagnosed and treated for the illness, according to a 1997-updated NIH consensus development statement. Although research suggests that rates of depression decrease with age, Dr. Lebowitz says some researchers predict that “when the Boomer generation reaches older ages their rates of depression will be higher than the current generation of older persons. And, depressive symptoms — ones that are not as severe or ones that have not lasted long enough yet to qualify as major depression — do increase with age.”

The actual number of older people with clinical depression may depend on where they are living, says Dr. Reynolds. An NIMH study found that 2 to 3 percent of elderly people living in the general community — not in hospitals or nursing homes — may be clinically depressed.
However, eight to 10 percent of seniors who visit primary care clinics may fit the diagnosis for clinical depression, between 20 and 25 percent of older people in hospitals have depression and one in three senior citizens living in nursing homes may be suffering from the illness.

“This information tells us we need to look in the medical settings where the elderly are,” notes Dr. Reynolds. “It’s important for family and caregivers to be particularly vigilant in recognizing the signs, and seeking the early intervention that can prevent full-blown depression.” Especially susceptible to clinical depression are elders who are recently bereaved, and those who have had other serious health problems such as a stroke, a heart attack or a cancer diagnosis.

Finally, both doctors issue a warning to elders and their families about trying to self-treat depression with dietary supplements or other products they find at the drug store.

“I tell people to do some research on alternative therapies as an adjunct to — not a substitute for the care they receive from their physician,” advises Dr. Reynolds. It’s important that seniors or their caregivers inform all their doctors and pharmacists about all the products they are using, he says, so that adverse drug interactions can be avoided.

Dr. Lebowitz says he routinely dispenses the so-called brown-paper-bag advice: “Put everything you take — prescription drugs, over-the-counter medicines, vitamins and any kind of supplements — in a bag and bring them to the doctor. Because older people take so many drugs for so many conditions, the best advice is to be very careful adding another and make sure your primary care doctor is informed about all drugs being taken.”

The steps to handling depression are easy to remember, for both seniors and their loved ones:

Recognize that depression is not a normal part of aging and take symptoms seriously.
Seek information from individuals and organizations experienced in helping the elderly.
Mobilize social support from relatives and friends.
Most importantly, get treated promptly.

What is important to remember is that depression is a chronic disease with a very poor prognosis — especially for those experiencing a first episode of depression late in life, concludes Dr. Lebowitz. “Long-term treatment might be necessary to prevent relapse or recurrence. Getting well is only the first step — staying well is the real challenge.”

***A Word to the Wise...***

***Recognize the Signs of Depression***

It’s important that family members identify the signs of depression in their elder relatives, according to Dr. Charles Reynolds, a medical researcher who focuses on mental health in the older population. Often senior citizens do not realize they are depressed, or do not seek treatment for emotional problems. If the following symptoms continue for more than two weeks, you need to seek medical help from a geriatric specialist, who is trained to treat elders:

- Insomnia
- Excessive worry
- Inability to complete normal routine
- Irritability, short-temperedness
- Hypochondria
- Withdrawal from family, friends and/or normal activities
- Chronic aches or pains that cannot be attributed to other disorders In addition, if a senior citizen begins to show signs of hopelessness or expresses concern that he or she has become a burden, help should be sought immediately.
- Increased dependability on family
