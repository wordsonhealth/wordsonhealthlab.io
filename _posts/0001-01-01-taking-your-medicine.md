---
layout: post 
title: "Taking Your Medicine"
date: 2019-02-12
image: assets/images/taking-your-medicine.webp
excerpt: "The Importance of Taking Your Medications Correctly"
tags: Medication regime, Medications, Medicine, Regimen
draft: false
---

The power of our memory often goes down with age. At the same time, aging often brings complicated medication regimens involving several drugs that need to be taken on different schedules throughout the day. People over 65 consume more prescription and over-the-counter drugs than any other age group, and following a regimen properly can mean the difference between life and death. Dr. Denise Park, an NIH grantee who has dedicated her career to studying the aging mind, says there are many things older people can do to help them follow a complicated medication regimen. She points out that even people who aren’t elderly often have trouble sticking to a regimen.

**Brain Changes with Age**

In looking for ways to help people adhere to their medication regimens, Dr. Park, a professor of psychology and senior research scientist at the University of Michigan in Ann Arbor, has thought a lot about how the brain changes with age. It is well established that our working memory — the ability to process, store and retrieve information — gets slower with age. We have less capacity to absorb new information, and we sometimes have difficulty focusing on the most important aspects of new information we receive. Most of these functions are controlled by the frontal lobes, an area of the brain that shrinks disproportionately with age and operates less efficiently as we get older.

But some things in the brain can improve with age: those things that draw on the knowledge and wisdom that come from experience. Dr. Park calls these world knowledge tasks. “World knowledge tasks go up modestly or remain constant well into the 70’s,” Dr. Park explains. Older people bring more knowledge, background information, and often more familiarity with health care into a doctor’s office. While older people don’t tend to process as much information as quickly, research suggests that they can still make medical decisions equal in quality to those of younger people.

**Taking Medications**

When it comes to following a complicated medication regimen, there are several things you need to be able to do. First, you have to understand the instructions: when to take the medicines and what kinds of restrictions there are on each one, such as taking it with food or avoiding alcohol. You need to be able to plan a schedule, which can be complicated when several medicines are involved, and to understand and remember that schedule. Finally, you need to be able to remember to take each dose. All of these steps are affected in the aging mind.

But just because your memory is slowing down doesn’t necessarily mean you’ll have trouble taking your medicine. “Older adults do suffer declines in cognitive function,” Dr. Park says, “but they have plenty remaining to remember to take a pill in the morning with breakfast.” Aging isn’t always the best predictor of whether or not someone is going to take their medicines properly, Dr. Park says. She has found that, indeed, people over 78 years old were the least adherent in taking medicines. But those between 60 and 77 followed their medication regimes the best. The big surprise was that middle-aged people did much worse than 60-77 year olds.

The first step in understanding how to help people take their medicines properly is to figure out why these groups have different adherence patterns. Dr. Park believes that the 60-77 year old people feel vulnerable enough about their health to take their medications very seriously, and can still follow complicated regimens. Many people over 78 may have trouble understanding and remembering their regimens, accounting for this group’s poor adherence. As for the middle-aged people, they were perfectly capable of following their regimens. But, Dr. Park explains, “We found that people who reported being very busy and having a lot of things going on in their lives were the least adherent, and these were middle-aged people. Their working memory was full of tasks and information, and they often were too distracted to remember to take their medicines.”

Dr. Park concludes that people with more stable, less hectic lives follow their medication regimens the best. And older people tend to have more stable, less hectic lives than middle-aged people.

**Helping a family member follow a medication regime**

Some people aren’t able to understand why they’re taking their medicines and aren’t capable of making up a daily plan for when to take each one. These people need a health professional or family member to draw up a plan for them and to figure out a system of reminders. If you have a friend or family member in this situation, try to get them as involved in the process as possible.

In designing a schedule and a reminder system, be creative and keep working with your relative to figure out what will work best for them (see main story for some suggestions). The more they understand about why they are taking these medicines, the more likely they will be able to follow the regimen.

Remember that it’s absolutely crucial for pill organizers to be loaded properly. All the planning in the world won’t help your relative take their medicines properly if their organizer isn’t loaded correctly.

**Medication Aids**

![img](/assets/images/21-pill-organizer.webp)

There are many things you can do to help you follow a medication regimen. Dr. Park has experimented with bottle tops that beep when you need to take your medicine and found them very effective. But until such things are widely available, a programmable wristwatch can be a good reminder. A phone service or computer scheduling program might be a good substitute if you are usually at home when you need your medications. However, for people taking medicines on different schedules, Dr. Park is reluctant to recommend these simple time reminders without something else to tell them which medicine they’re supposed to take each time. Pill organizers with a different compartment for various times of the day can help people stick to a schedule.

But often the most difficult part of taking several medicines for an elderly person is simply figuring out what the regimen is supposed to be in the first place. “I think it would be really useful for a health professional to sit down and ask the person to write out a medication plan,” Dr. Park says. It’s best for the patient to produce the plan, drawing out a day by day, hour by hour schedule of when they have to take all their medications. It could be a grid with dates and times detailing when to take each medicine, along with any restrictions on them. A plan can be in the form of a poster, a booklet, or just a sheet of paper. Checking off each medicine as it is taken can help you make sure you are following the regimen properly.

“Understand as much about why you’re taking these things as you can,” Dr. Park advises. “That helps adherence.” On drugs that are crucial for a person’s health, she says, people are generally very adherent, particularly older people. “One of the reasons is that people’s lives depend on this, people’s health depends on this, and they know it.”

The last advice Dr. Park has is to build a consistent, structured schedule for taking your medications into your daily life. “Behaviors become automatic and almost unconsciously performed over time,” she says. “For example, you get to your office and realize you have no memory of how you got there. Taking medications can similarly become just as automatic…. Having daily routines that are highly structured leads to greater adherence.” For example, you might decide to take one medication after you brush your teeth every morning. Dr. Park thinks that people who have a sudden-onset medical condition like a heart attack tend to have a harder time following a complicated regimen than those whose regimens gradually build in complexity. The latter have had time to slowly build these things into their lifestyles and incorporate them into their daily schedules.

**A Complicated Problem**

There’s no simple solution to helping people follow their medication regimens. Dr. Park says that a number of inventors have investigated designing devices to dispense medications. But the task is extremely difficult because of all the different pill sizes and complicated schedules. “It would need a lot of programming,” she says. There are also instructions that might be lost with such a device — to take the medicine with food or drink, for example — as well as warnings, like not using alcohol while taking the drug.

Dr. Park believes that in order to help people follow their medication regimens it’s critical to understand how changes in the aging mind affect reasoning, learning and memory. People will be healthier if they understand their own health problems and how to manage them. A spoonful of sugar may help the medicine go down, but if you can’t remember which of your medicines to take and when to take them, the sugar’s not much help.

***A Word to the Wise...***

- DO keep a daily checklist of all the medicines you take. Include both prescription and OTC medicines. Note the name of each medicine, the doctor who prescribed it, the amount you take, and the times of day you take it. Keep a copy in your medicine cabinet and one in your wallet or pocketbook.
  pill graphic
- DO read and save any written information that comes with the medicine.
  pill graphic
- DO check the label on your medicine before taking it to make sure that it is for the correct person — you — with the correct directions prescribed for you by your doctor.
  pill graphic
- DO take your medicine in the exact amount and precise schedule your doctor prescribes.
  pill graphic
- DO check the expiration dates on your medicine bottles and throw away medicine that has expired.
  pill graphic
- DO call your doctor right away if you have any problems with your medicines or if you are worried that the medicine might be doing more harm than good. He or she may be able to change your medicine to another one that will work just as well.
  pill graphic
- DO NOT take medicines prescribed for another person or give yours to someone else.
  pill graphic
- DO NOT stop taking a prescription drug unless your doctor says it’s okay — even if you are feeling better.
  pill graphic
- DO NOT take more or less than the prescribed amount of any medicine.
  pill graphic
- DO NOT use alcohol while taking a medicine unless your doctor says it’s okay. Some medicines may not work well or may make you sick if you drink alcohol
