---
layout: post 
title: "Colorectal Exams"
date: 2019-02-24
image: assets/images/colorectal-exams.webp
excerpt: "What can you do to decrease your risk for developing cancer of the colon or rectum?"
tags: colon, Colonoscopy, Colorectal Cancer, NCI, rectum
draft: false
---

Your blood pressure is normal, your blood cholesterol values are good, and you’ve even shed a few of those extra pounds. But then your doctor asks if you’ve ever had a colorectal exam. If you’re like most people, your face stiffens and a bead of sweat breaks out on your forehead. You’ve never had one, and the idea isn’t exactly appealing.

Now’s the time to give it some serious thought. Why? The facts are plain and simple: a colorectal exam may save your life. Cancers of the colon and rectum can be successfully treated — if they’re caught early. Moreover, in some cases an exam can help you prevent colorectal cancer altogether.

Cancers of the colon or rectum occur in both men and women and are found most often in those over the age of 50. Colorectal cancer is the fourth most common cancer in the United States and the second leading cause of cancer deaths here, according to NIH’s National Cancer Institute (NCI). Each year, more than 135,000 new cases of colorectal cancer are diagnosed, and these cancers will take the lives of over 56,000 Americans. But colorectal cancer death rates have been declining since the mid-1980s because of more effective treatments, earlier detection of tumors, and better prevention via removal of precancerous growths (polyps).

**Colorectal Cancer – A Preventable Killer**

While everyone can benefit from regular screening, if you have a family history of colorectal cancer or another bowel disorder, you should be even more diligent. Ask your doctor how often you should be screened and which colorectal tests you should undergo. In the meantime, there are other things you can do.

DIET: Diet plays a large role in colorectal cancers. Migrants to the United States from Japan and other countries where rates of colorectal cancer are lower than in the U.S. have higher rates of colorectal cancers than do those who remain in their native country. Children of these migrants develop colorectal cancers at rates equal to or even higher than those of the United States white population. Studies have shown that people who eat a lot of well-done, fried, or barbequed meats have an increased risk of developing colorectal cancer. For prevention, NCI encourages people to eat five or more servings of vegetables and fruits each day as part of a low-fat, high-fiber diet and to follow the Dietary Guidelines for Americans.

EXERCISE: Two words: Get moving. Exercise may lower a person’s risk for colorectal cancer, NIDDK experts contend, because it speeds the amount of time it takes for wastes to leave your body.

SMOKING: Do all you can to quit. In addition to smoking’s well-established link to cancers of the lung and head and neck, long-term smoking that begins before age 30 also increases the risk of developing colorectal cancer as well as precancerous polyps.

ALCOHOL: Excessive alcohol consumption — drinking two or more alcoholic drinks a night — has been associated with an increased prevalence of rectal cancer.

**What is it?**

![img](/assets/images/28-Colorectal-Exams-1.webp)

The colon and rectum are the parts of the body’s digestive system that remove nutrients from food, turning the rest into waste and then passing it out of the body. Colorectal cancer is a disease in which cells in the colon or rectum become abnormal and start dividing without control, forming a mass known as a benign or malignant tumor. If malignant, or cancerous, these cells can invade and destroy the tissue around them, and can also break away from the tumor and spread to form new tumors in other parts of the body.

But prevention goes a long way. Some methods of screening for colorectal cancer can detect early growths (i.e., intestinal polyps) that may be precursors to rectal or colon cancer. Early detection and removal of these polyps may help prevent colorectal cancer.

There are several tests doctors can perform to screen for cancer of the rectum and colon. Your doctor can first check to see if “hidden” blood is present in your stool using a fecal occult blood test. The patient places very small samples of stools on specially coated cards that are then mailed into a laboratory for analysis.

Studies show that checking for blood using these procedures can lead to detecting cancer at an early stage and thus improve survival. These checks should be done periodically, starting by age 50 unless there are risk factors (e.g., family history of colon cancer or long-standing inflammatory bowel disease, such as ulcerative colitis or Crohn’s disease), in which case the regular exams should probably be started sooner. Blood in the stool does not always indicate cancer or even disease, but it can be an early sign of colorectal cancer or another intestinal disorder and signals the need for a test to view the colon and rectum to determine the source of the blood.

One of these viewing tests is sigmoidoscopy, which takes 10-20 minutes, during which the doctor uses a thin, flexible tube with a light to look inside the rectum and lower intestine or colon. With this procedure, the physician can see if there is any bleeding, inflammation, abnormal growths, or ulcers. Sigmoidoscopy enables the doctor to see the bottom one-third of the colon. Another procedure, colonoscopy, enables the doctor to see the entire colon. For this test, which also involves the use of a lighted tool, a patient is mildly sedated to keep her or him relaxed and comfortable.

During colonoscopy as well as sigmoidoscopy, should the doctor see benign growths known as polyps (that could later become malignant) he or she can do a biopsy (take a sample of the tissue) and/or remove the polyp on the spot. Patients are not allowed to drive home after a colonoscopy because of lingering drowsiness, so another driver must accompany them to the visit.

Also in the works is the “virtual colonoscopy”, in which doctors use 3-D computer graphics of X rays (essentially a CAT scan) to view the entire colon. This state-of-the-art technique could reduce the need for inserting a sigmoidoscope or colonoscope into the rectum and colon to find polyps or growths, says NCI, and would undoubtedly increase the number of patients willing to undergo screening. If growths are found, however, a traditional colonoscopy would be necessary to remove them. The procedure should become available within the next few years. These signaling molecules from the immune system can also activate the part of the brain that controls the stress response, the hypothalamus. Through a cascade of hormones released from the pituitary and adrenal glands, the hypothalamus causes blood levels of the hormone cortisol to rise. Cortisol is the major steroid hormone produced by our bodies to help us get through stressful situations. The related compound known as cortisone is widely used as an anti-inflammatory drug in creams to treat rashes and in nasal sprays to treat sinusitis and asthma. But it wasn’t until very recently that scientists realized the brain also uses cortisol to suppress the immune system and tone down inflammation within the body.

**Fear Shouldn’t Get the Better of You**

Why are we so fearful or shy about having a colorectal exam performed? “Certainly one of the reasons is that most of us do have some hesitancy about being touched in that area,” explains Dr. Frank Hamilton, chief of the Digestive Diseases Program of the National Institute of Diabetes, Digestive and Kidney Diseases (NIDDK). He added that a fear of what might be found through testing keeps some people from doing what they should. “But,” he says “about 65 million people over age 50 are at risk for colorectal cancer, so these are very critical exams.”

So don’t wait. Talk to your doctor now about being screened for colorectal cancer. It could save your life.

***A Word to the Wise...***

*Intestinal Do’s and Don’ts*

**DO:**

…get regular colorectal screenings — annual fecal occult tests for blood in the stool and/or sigmoidoscopy or colonoscopy, as recommended by your doctor.

…eat a low-fat, high-fiber diet with plenty of fruits, vegetables, and grains. Drink plenty of liquids, especially water.

…get regular exercise.

…report unusual intestinal symptoms to your doctor — for example, chronic diarrhea or constipation, blood in the stool, vomiting or persistent stomach discomfort.

**DON’T:**

…drink alcohol excessively (more than two alcoholic drinks per day).

…smoke.

…overcook your foods (but don’t undercook meats either).

…put off getting a colorectal exam.
