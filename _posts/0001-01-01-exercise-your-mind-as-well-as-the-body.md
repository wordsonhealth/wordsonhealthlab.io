---
layout: post 
title: "Exercise Your Mind as Well as the Body"
date: 2019-02-08
image: assets/images/exercise-your-mind-as-well-as-the-body.webp
excerpt: "Getting Older Doesn’t Have to Mean Becoming Forgetful"
tags: Alzheimer, Alzheimer's, Memory loss, Mind
draft: false
---

Everybody’s memory fades as they get older, right? Not so, according to a research funded by the National Institute on Aging (NIA), a component of the National Institutes of Health. These studies suggest that memory loss is not a normal part of aging, and that keeping your mind active is the key to maintaining brain function.

“Use it or lose it seems to be the direction the recent research is leading,” says Dr. Andrew Monjan, chief of NIA’s Neurobiology of Aging Branch, which funds studies in this area at universities and medical schools around the country. “We’re changing from the old dogma that when you get old you’ll be senile. We’re learning that dementia and memory loss commonly associated with old age are not in fact part of the normal aging process.”

Why then do some older people but not others seem to experience memory loss? Various diseases that can have negative effects on brain cells are more likely than normal aging to cause cognitive impairment, Dr. Monjan says. Studies conducted in the last couple of years suggest most brain-function loss can be attributed to some kind of pathological event not directly tied to old age, he says. Medical reasons that can cause brain cells to malfunction or die include viral infections, unhealthy behaviors such as chronic alcoholism and drug abuse, and brain or nervous system disorders such as Alzheimer’s disease or a history of mini-strokes.

**NIA Launches Memory Deficit Treatment Study**

The National Institute on Aging (NIA) has launched a nationwide treatment study targeting individuals with mild cognitive impairment (MCI), a condition characterized by significant memory deficit, but not dementia. The Memory Impairment Study is the first such Alzheimer’s disease (AD) prevention clinical trial carried out by NIH, and is being conducted at 65-80 medical research centers located in the United States and Canada. This study is testing the usefulness of two drugs to slow or stop the conversion from MCI to AD. The trial is evaluating placebo, vitamin E, and donepezil, a drug approved by the Food and Drug Administration for treatment of AD. Each center will enroll 12 to 36 participants. It is important to note that for every participant accepted into the study, many more volunteers will need to be screened.

In addition, Dr. Monjan says, new evidence suggests that some brain cell loss—especially loss due to disease—may be reversible. Brain cells may even be capable of repair and regeneration with proper medical treatment, he says.

Other myths about memory, aging and the brain are being disproven as well, says Dr. Molly Wagster, director of NIA’s grants program in the neuropsychology of aging.

Contrary to commonly held beliefs, people continue to develop new brain cells throughout their lifetimes. “It was once thought that at birth, you had all the brain cells you would ever have,” she points out, “but new research on stem cells tells us that brain cells are still being born, even into older adulthood.”

Better imaging technologies—improvements in what is known as functional magnetic resonance imaging or fMRI, and positron emission tomography or PET—are also allowing researchers to see for the first time what is happening in the brain while the individual is performing tasks or processing information. Dr. Wagster says such relatively new technological capabilities are helping scientists specify which regions of the brain are responsible for certain activities. These revolutions in imaging are also shedding light on how we learn and remember. “Several of our investigators are currently examining how the older person’s brain memorizes and retrieves information compared to the ways a younger person’s brain completes the tasks,” Dr. Wagster explains. “This is definitely one of the most exciting new areas of research, and we are reaping the benefits for the older population.”

Finally, both Drs. Monjan and Wagster caution that the popular notion that one can improve memory by taking dietary supplements such as gingko biloba and vitamins is currently more a part of our society’s trend to find a quick fix than proven fact. Gingko has been shown to increase blood circulation, which in turn has been shown to aid brain function, Dr. Monjan points out. However, he says, the exact role of using gingko—beyond some placebo effects—is not known yet. NIH—through its National Center for Complementary and Alternative Medicine and NIA—has just launched a clinical study of the effect ginkgo has on memory. “I think it’s premature yet to add these things to your diet, thinking that you’ll increase your memory,” advises Dr. Monjan. “The real secret to optimal aging and mind function is what our mothers always told us,” says Dr. Monjan. “Eat well. Get plenty of rest. And, exercise the mind as well as the body.”

***A Word to the Wise...***

*A Healthy Body Leads to a Healthy Mind*

There is so far no magic pill to help you ensure optimal memory as you get older, but maintaining a healthy body will go a long way toward maintaining a healthy mind, says Dr. Andrew Monjan of the National Institute on Aging at the National Institutes of Health.

Eat a healthy diet. A balanced diet will protect you from many diseases that can cause brain cells to malfunction or die.

Exercise regularly. Increased blood circulation has been found to aid brain function and memory.
Get a good night’s sleep. Chronic sleep problems, caused by sleep apnea*, in which a person has brief interruptions of breathing during sleep, may lead to diminished or damaged brain cells as well as other serious health problems.

Keep your mind active. Brain stimulation is important for people of all ages. Try taking up a new activity or learning a new skill to exercise “the little gray cells.”

Watch your medications carefully and discuss any questions on prescriptions with your doctor. Drug interactions may also lead to feelings of disorientation or may mimic symptoms of dementia. Be sure your doctor is aware of all of the prescription and over-the-counter medicines, vitamins, and supplements you are taking regularly.
