---
layout: post 
title: "Have Your Baby S Hearing Tested"
date: 2019-01-24
image: assets/images/have-your-baby-s-hearing-tested.webp
excerpt: "Early Detection Important for Speech and Language Development"
tags: Baby's Hearing, Communication Disorders, Deafness
draft: false
---

How early should your child’s hearing be tested? Soon after birth, according to the nation’s hearing and communication experts. If that answer surprises you, consider this: Every year, about 12,000 babies in this country are born with hearing problems. Many of these children will not be diagnosed and treated until after they are 2 years old. By then, they will have missed out on key years of stimulation of the hearing centers in their brains. As a result, they could have problems developing speech and language skills that could affect them the rest of their lives, especially in school or at work. Early detection, follow-up and treatment of hearing problems in newborns, however, can prevent many of these communication problems, according to an expert panel convened by the National Institute on Deafness and Other Communication Disorders (NIDCD), one component of the National Institutes of Health.

Now that advances in technology allow more precise and more accurate testing of infants’ hearing, the panel said, all newborns should be screened–universal screening–in the hospital or birthing center before they are discharged. Ideally, infants should be tested before they are three months old, so that any necessary treatment can begin before 6 months of age–the crucial period in a baby’s life for speech and language development.

In response to the panel’s recommendations, NIDCD launched “Silence Isn’t Always Golden,” a nationwide campaign to increase the awareness of detecting early hearing loss. NIDCD collaborated with NIH’s Office of Research on Minority Health and the National Medical Association.

The panel recommended several low-cost methods for universal newborn screening, including: Auditory Brainstem Responses (ABR) and Otoacoustic Emissions (OAE). Both tests are painless and take only minutes to administer.

In the ABR test, sound is introduced to the baby’s ears through tiny headphones while the baby is sleeping. Three small discs placed usually on the baby’s head measure whether the baby’s brain is detecting the sounds. The whole process takes about 5 minutes if the baby is quiet and cooperative.

The OAE test works differently. OAEs are very faint, but detectable sounds produced by most normal ears. Although a person cannot hear his or her own emissions, tiny sensitive microphones placed in the ear canal can measure the sounds. During the OAE screeing, sound is introduced through a small flexible probe inserted into the sleeping baby’s ear. A microphone inside the probe detects the emissions produced by the baby’s ear in response to the sound. No emissions are detected from an infant who cannot hear.

“Silence Isn’t Always Golden” has handy checklists to help parents monitor their child’s hearing and reactions to sound from birth to 36 months of age, along with a glossary of terms doctors may use when talking to parents about a child’s hearing.

**How early should I have my baby’s hearing screened?**
Your baby should have a hearing screening within the first month of life. If hearing loss is suspected, make sure a hearing expert (called an audiologist) tests your baby’s hearing by three months of age. If hearing loss is confirmed, it’s important to consider the use of hearing devices and other communication options by six months of age.

**Where can my baby’s hearing be screened?**
Ask your doctor or hospital if they plan to do the test on your newborn. Many hospitals automatically screen all newborns for hearing loss. Some normally screen only those at high risk for hearing loss, such as babies with a family history of deafness or hearing problems, low birth weight, or certain other medical conditions. Even if your baby doesn’t have risk factors, being screened is important, because many children with no risk factors have hearing loss.

**How will my baby’s hearing be screened?**
Two hearing tests are used to screen babies. In both tests, no activity is required from your child other than lying still. **Otoacoustic emissions (OAE)** tests can show whether parts of the ear respond properly to sound. **Auditory brain stem response (ABR)** tests check how the brain stem (the part of the nerve that carries sound from the ear to the brain) and the brain respond to sound. If your child doesn’t respond consistently to the sounds presented during either of these tests, your doctor may suggest a follow up hearing screening and a referral to an audiologist for a more comprehensive hearing evaluation.

**How can I recognize hearing loss during early childhood?**
Even though screening is designed to detect hearing loss as early as possible, some children don’t develop hearing loss until later in life. Even if you’ve had your baby’s hearing tested, you should look for signs that your baby is hearing well.

For example, during the first year, notice whether your baby reacts to loud noises, imitates sounds, and begins to respond to his or her name. At age two, ask yourself whether or not your toddler imitates simple words and enjoys games like peek-a-boo and pat-a-cake. Is he or she using two-word sentences to talk about and ask for things? At age three, notice whether or not he or she begins to understand “not now” and “no more” and follows simple directions. If for any reason you think your child is not hearing well, talk to your doctor.

**If my child has a hearing loss, can hearing be improved?**
A variety of devices and strategies are helpful for children who are hard-of-hearing. An audiologist can help you to decide whether these or other devices can help your child:

**Hearing aids** are instruments that make sounds louder. They are worn in or behind the ear and come in several different shapes and sizes. Hearing aids can be used for varying degrees of hearing loss. An audiologist will fit a hearing aid that will work best for your child’s hearing loss. Hearing aids can be expensive, so you’ll want to find out whether they have a warranty or trial period. You’ll also want to talk with your insurance provider to understand what is covered and what isn’t.

**Cochlear implants** have three parts: a headpiece, a speech processor, and a receiver. The headpiece is worn just behind the ear where it picks up sound and sends it to the speech processor. The speech processor, a beeper-sized device that can fit in a pocket or on a belt, converts the sound into a special signal that is sent to the receiver. The receiver, a small round disc about the size of a quarter that a surgeon has placed under the skin behind one ear, sends a sound signal to the brain.

Not all children who have hearing loss should get cochlear implants. Doctors and hearing experts think they’re best for children who have a profound hearing loss and won’t benefit from hearing aids.

**How can I help my child communicate?**
There are a variety of ways to help children with hearing loss express themselves and interact with others. The option you choose will depend on how you want your child to learn and communicate. Find out about all of the choices and talk to lots of experts:

**Oral/Auditory Options** combine hearing, lip-reading, and hearing devices such as hearing aids and cochlear implants. The goals of oral/auditory options are to help children develop speech and English-language skills.

**American Sign Language (ASL)** is a visual language used by some deaf children and their families. ASL consists of hand signs, body movements, facial expressions, and gestures. It’s a language with its own grammar and syntax, which are different from English. ASL has no written form.

**Signed English** is similar to ASL, using the same visual vocabulary of signs, but it adheres more strictly to the sentence structures of spoken and written English.

**Cued Speech** is a system that uses handshapes in different locations along with the natural mouth movements to represent speech sounds. Watching the mouth movements and the handshapes can help some children learn to speech-read English; this is especially important in discriminating between sounds that sound different but look the same on the lips.

**Combined Options** use portions of the various methods listed above. For example, some deaf children who use oral/auditory options also learn signed English. Children who use ASL also learn to read and write in English. Combined options can expose children who are deaf or hard of hearing to many different ways to communicate and express themselves.

***A Word to the Wise...***

**Check your baby’s hearing:**

Find your child’s age. Check yes or no for every item. After you complete the checklist, show it to your child’s doctor. Ask the doctor questions. Talk about the items checked “no.” If you think your child has trouble hearing, tell the doctor right away.

- ***Birth to 3 Months*** (YES or NO)
  Reacts to loud sounds
  Is soothed by your voice
  Turns head to you when you speak
  Is awakened by loud voices and sounds
  Smiles when spoken to
  Seems to know your voice and quiets down if crying
- ***3 to 6 Months***
  Looks upward or turns toward a new sound
  Responds to “no” and changes in tone of voice
  Imitates his/her own voice
  Enjoys rattles and other toys that make sounds
  Begins to repeat sounds (such as ooh, aah, and ba-ba)
  Becomes scared by a loud voice
- ***6 to 10 Months***
  Responds to his/her own name, telephone ringing, someone’s voice, even when not loud
  Knows words for common things (cup, shoe) and sayings (“bye-bye”)
  Makes babbling sounds, even when alone
  Starts to respond to requests such as “come here”
  Looks at objects or pictures when someone talks about them
- ***10 to 15 Months***
  Plays with own voice, enjoying the sound and feel of it
  Points to or looks at familiar objects or people when asked to do so
  Imitates simple words and sounds; may use a few single words meaningfully
  Enjoys games like peek-a-boo and pat-a-cake
- ***15 to 18 Months***
  Follows simple directions, such as “give me the ball”
  Uses words he/she has learned often
  Uses 2-3 word sentences to talk about and ask for things
  Knows 10 to 20 words
- ***18 to 24 Months***
  Understands simple “yes-no” questions (Are you hungry?)
  Understands simple phrases (“in the cup” “on the table”)
  Enjoys being read to
  Points to pictures when asked
- ***24 to 36 Months\***
  Understands “not now” and “no more”
  Chooses things by size (big, little)
  Follows simple directions such as “get your shoes” and “drink your milk”
  Understands many action words (run, jump)
- ***For All Babies and Young Children***
  Do others in the family, including brothers or sisters, have a hearing problem?
  The child’s mother had medical problems in pregnancy or delivery (serious illness or injury, drugs or medications)
  The baby was born early (premature). Weight at birth:_______
  The baby had physical problems at birth.
  The child rubs or pulls on ear(s) often.
  The child had scarlet fever.
  The child had meningitis.
  The child had ___________ ear infections in the past year.
  The child has colds, allergies, and ear infections, once a month_______ more often___________
