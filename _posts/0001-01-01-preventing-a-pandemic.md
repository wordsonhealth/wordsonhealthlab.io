---
layout: post 
title: "Preventing a Pandemic"
date: 2019-06-26
image: assets/images/preventing-a-pandemic.webp
excerpt: "Preparing for the Next Deadly Flu Outbreak"
tags: Corona virus, Coronavirus, Flu Outbreak, Influenza, NIAID, Pandemic, Seasonal flu
draft: false
---

Researchers are racing to prevent the next deadly flu outbreak. Outbreaks in 1918, 1957 and 1968 killed millions worldwide. No one knows how fatal the next one could be, so researchers are taking as many different approaches as they can to prepare us to fight it if it comes.

Seasonal flu, or influenza, viruses change slightly over time, allowing the virus to evade our immune systems. That’s why there’s a new seasonal flu shot every year: to protect you from the newest strains, or varieties, of virus. Healthy people, even if they don’t get a flu shot, aren’t usually seriously threatened by seasonal flu. But it can be severe for the very young, the elderly and those with a weakened immune system. Seasonal flu causes 36,000 deaths and more than 200,000 hospitalizations every year in the U.S.

![img](/assets/images/88-Preventing-a-Pandemic.webp)

Every so often, a flu virus emerges that’s so different, human immune systems aren’t at all prepared to fight it. Historically, they’ve infected people by “jumping” from animals, most commonly birds. People can become infected when they’re in close contact with infected birds, such as while slaughtering or preparing poultry, drinking water contaminated with droppings of infected birds or when they allow infected birds to roam freely in their living areas. Animal and human viruses can also mix in the body when someone’s infected with both to produce other new viruses.

You’ve probably heard about a class of bird, or avian, flu viruses called H5N1. “H” and “N” stand for particular proteins on the surface of the influenza virus that change frequently. H5N1 viruses have infected birds throughout Southeast Asia and are now spreading into Central Asia, Africa and Europe. When people have become infected, the results have often been fatal. By December 2006, the World Health Organization confirmed over 250 human cases of H5N1 and more than 150 deaths.

NIH researchers were able to develop a vaccine that protects against multiple strains of the flu subtype called H1N1. The vaccine includes a piece of DNA that makes a specific viral protein. They vaccinated mice, ferrets and monkeys. Some of the animals later received a booster shot of a seasonal flu vaccine.

Animals given both the DNA and the boost vaccines produced antibodies that blocked several H1N1 strains. The antibodies also blocked other flu subtypes, including H5N1.

Since we can’t predict when the next influenza pandemic will occur or which strain of the virus will cause it, NIH’s National Institute of Allergy and Infectious Diseases (NIAID) is funding many different approaches to get us ready.

If health officials could quickly figure out which flu virus they’re dealing with, they would have a better chance of preventing a pandemic. A good diagnostic test would allow a doctor to distinguish between a routine case of seasonal flu and avian flu. That’s why researchers have been working hard to develop a test that would allow labs across the country to rapidly diagnose flu infections.

This summer, NIAID-funded scientists from the University of Colorado worked with the Centers for Disease Control and Prevention to develop a test called the FluChip. It can distinguish among several seasonal and avian flu strains in less than 12 hours. More recently, the same team developed the MChip, a simplified test that distinguishes among flu strains by focusing on the “M” virus protein, which doesn’t change as often as the H and N proteins. Plans are now under way to commercialize the MChip.

When it comes to fighting a pandemic, the first thing that may come to mind is a vaccine. Results from earlier this year show that researchers may be on the path to developing a vaccine to protect people.

Researchers supported by NIAID designed an experimental vaccine based on an H5N1 virus originally isolated in Southeast Asia in 2004. In a series of studies, they gave people different doses of the vaccine and tested to see how much antibody, a virus-fighting molecule, their bodies made. They found that the higher the dose of vaccine people received, the more antibodies they produced. Researchers are continuing to improve on this vaccine and are now testing it in children and the elderly.

Several ways to make more effective avian flu vaccines are being tested. One method is to use immune-boosting compounds called adjuvants. Back in 2004, NIAID asked Novartis Vaccines and Diagnostics to use an adjuvant to produce a vaccine against H9N2, an avian flu that infected two children in Hong Kong in 1999. In a preliminary trial, the vaccine with adjuvant raised antibody levels to ranges that may be able to protect people against infection.

Researchers are also making progress using animals to test new strategies. In a study done under an agreement between NIAID’s Laboratory of Infectious Diseases and MedImmune Inc., researchers showed that a vaccine based on one strain of H5N1 could protect mice and ferrets from infection by several naturally occurring H5N1 viruses. That’s promising news for researchers hoping to develop a vaccine against unforeseen H5N1 strains. A preliminary trial was launched last June to start testing a similar vaccine in humans.

Other ways to combat flu viruses are being explored, too. Researchers are testing many compounds designed to slow the spread of avian flu viruses within the body once a person’s been infected.

A better understanding of a virus that caused a past pandemic has recently given researchers ideas about another possible way to fight flu. A study of mice infected by the virus that caused the 1918 pandemic showed that the virus provoked their immune systems to overreact. That may be why the virus was so fatal to humans. So, in addition to developing methods to target the viruses themselves, researchers may be able to develop ways to dampen the human immune system’s response to an infection.

Researchers continue to work on these and other approaches to fighting influenza. Experts don’t know when or where a deadly new flu might emerge, but they hope the tools being developed now will prevent another pandemic and help us fight one if it does occur.

*Definitions:*

**Antibody:**
A virus-fighting molecule made by the immune system.

**Immune System:**
The system that protects your body from invading viruses, bacteria and other microscopic threats.

**Pandemic:**
An outbreak of disease that spreads to people in many different parts of the world.

***A Word to the Wise...***

*Avoid the Flu*

- Get a yearly flu vaccine if your doctor recommends it. But keep in mind that the vaccines currently available don’t protect you from bird (avian) flu.
- Stay home when you’re sick and avoid contact with others who appear to be sick.
- Wash your hands before eating and before touching your eyes, nose or mouth.
- If you travel anywhere that bird flu has been found, use caution when handling and cooking poultry, as it may be infected. If you interact with any birds while in these countries, wash your hands and clean the bottom of your shoes or any other items that may have been in contact with birds or bird droppings.
