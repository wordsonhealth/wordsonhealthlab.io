---
layout: post 
title: "Detecting Alzheimer's Disease"
date: 2019-05-12
image: assets/images/detecting-alzheimers-disease.webp
excerpt: "Research Aims for Earlier Diagnosis"
tags: Alzheimer, Alzheimer's, dementia, NIA
draft: false
---

Do you ever forget where you put your car keys or what you were supposed to pick up at the grocery store? You might worry that these memory lapses, or “senior moments,” could be an early sign of Alzheimer’s disease (AD), an irreversible brain illness. AD is the most common cause of dementia, which involves memory loss, loss of the ability to solve problems, personality changes and behavioral problems severe enough to interfere with normal activities and relationships.
If you’re concerned about changes in what you can and can’t remember, a neurologist or other expert in brain function can test you to find out whether these changes are normal or an early sign of AD. However, doctors can only diagnose AD once symptoms develop. For researchers hoping to slow the progression of AD or prevent it altogether, these clinical tests come very late in the game.

What if there were ways to detect AD well before the signs of memory loss appeared in everyday life? Earlier diagnosis would allow researchers to test possible drugs more quickly for better and earlier treatments.

![img](/assets/images/66-Detecting-Alzheimer-Disease.webp)

“There are a number of new drugs under development that we hope will interfere with the progression of Alzheimer’s,” notes Dr. Susan Molchan of the AD Clinical Trials Program at NIH’s National Institute on Aging (NIA). “Ultimately, what we want to be able to do is to test and provide medications that can preserve memory for people at risk.”

But doctors often can’t tell who those people are until the disease has already developed. Several lines of research funded by NIH are now aiming to uncover AD as early in the disease process as possible.

More than 90% of AD cases develop in people older than 65. AD and dementia, however, are not a normal part of aging.

AD comes about when nerve cells in the brain stop working, lose connections with other nerve cells and die. If you picture a brain as a giant network of highways that relay information, it’s as if some cars break down and stop working. Even cars that still work eventually get caught in traffic jams so they can’t get to their destinations.

The first breakdowns in AD occur in those parts of the brain that control memory. As these nerve cells stop working correctly, short-term memory fails and the ability to do familiar tasks can begin to decline. Damage follows in those parts of the brain responsible for language and reasoning. Over time, even more areas become entangled, leading to behavior and personality changes as well as problems with decision-making and thinking. In the end, a person with AD is bedridden and unable to recognize even close family members. It is a slow process, usually lasting 8-10 years or more.

Researchers have discovered some of the key culprits behind AD. Protein fragments called beta-amyloid form clumps in the spaces between the brain’s nerve cells. Researchers think these “plaques” may function like road blocks that keep signals from passing between nerve cells. Another protein called tau forms “tangles,” masses of twisted protein threads, inside nerve cells. These tangles, researchers believe, affect communication between nerve cells and eventually cause the cells to die. As more and more cells die, those brain areas begin to shrink.

No treatment can currently stop AD. However, several medications are available for people in the early and middle stages of the disease that may keep some symptoms from becoming worse for a limited time.

Researchers believe that potential therapies for AD will prove most effective if used early in the course of the disease, before symptoms become apparent. One way to identify people at risk is to identify genes associated with overall AD risk or with particular aspects of the disease, such as the age at which it begins. Four genes have been shown to affect AD development, and scientists estimate there may be several more.

Some tests of memory, problem solving, attention, counting and language skills can identify people with a higher risk of developing AD. As scientists learn more about changes in the brain during AD, they are developing methods of detecting AD even earlier. Changes in levels of certain key proteins such as beta-amyloid and tau in cerebrospinal fluid (the liquid that bathes the brain and spinal cord), blood or urine may be an early signal that someone has AD.

Another possible approach may be to use advanced imaging techniques. Doctors can now see what parts of the brain are active as a person does a mental task. They can also measure the size of parts of the brain to see if any areas are shrinking over time. The Alzheimer’s Disease Neuroimaging Initiative is a major research study sponsored by NIH to determine which brain scans, blood tests and other tests best track AD. Researchers are now looking for volunteers between 55 and 90 years old—with and without memory problems—.

As methods to detect AD continue to improve, scientists supported by NIA are also testing a number of treatments to see if they can prevent AD, slow the disease or help reduce its symptoms.

Doctors at specialized centers can now diagnose AD correctly up to 90% of the time. As promising treatments become available, earlier and more accurate diagnosis of AD will give doctors a better chance to treat its symptoms. An earlier diagnosis will also help people with AD and their families discuss plans for the future while the patient is able to play a role in decision making.

*Definitions:*

**Nerve Cells:**
Cells that send and receive signals throughout the brain, creating our thoughts, actions and personalities.

**Genes:**
Stretches of DNA, a substance you inherit from your parents, that define characteristics you inherit like height, eye color and how likely you are to get certain diseases.

***A Word to the Wise...***

*Risk Factors for Alzheimer’s Disease*

There’s no proven way to prevent Alzheimer’s disease (AD), but researchers think certain factors may affect your risk of getting the disease.

- High blood levels of both cholesterol and the amino acid homocysteine may increase your risk of getting AD. Research suggests that you can significantly lower levels of both these compounds by eating a diet rich in green leafy vegetables, low-fat dairy products, citrus fruits and juices, whole wheat bread and beans.
- High blood pressure creates a higher risk for mental decline, one more reason to try to maintain a healthy blood pressure.
- In one study, diabetes was linked to a 65% increased risk of developing AD. If you have prediabetes, act now to prevent diabetes. If you have diabetes, keep it under control.
- Studies have shown that intellectually stimulating activities may lower your risk of getting AD, so try to keep your mind active by doing things such as listening to the radio, reading newspapers and magazines, playing puzzle games and taking classes. Remaining socially engaged may also help reduce the risk of dementia.
- Research has shown that both physical and mental function can improve with aerobic fitness, so exercise regularly.
