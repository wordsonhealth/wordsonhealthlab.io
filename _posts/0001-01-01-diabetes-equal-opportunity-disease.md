---
layout: post 
title: "Diabetes Equal Opportunity Disease"
date: 2019-05-06
image: assets/images/diabetes-equal-opportunity-disease.webp
excerpt: "Confronting Diabetes in High-Risk Populations"
tags: Diabetes, Diabetes Control, Diabetes type 2, obesity, physical inactivity
draft: false
---

Some populations have higher rates of diabetes. African Americans, Hispanic Americans, American Indians, Asian Americans and Pacific Islanders develop type 2 diabetes more often than white Americans. But obesity and physical inactivity may be greater risk factors than a person’s genes.
Diabetes is a disease in which the body has problems producing or using insulin, a hormone needed to convert sugar, starches and other food into energy. For people living a Western lifestyle—with low physical activity levels and a high-fat, high-sugar, low-fiber diet—a family history of type 2 diabetes is one of the strongest risk factors for getting the disease. But people living in non-Westernized areas appear to get less type 2 diabetes regardless of their genetic risk.

![img](/assets/images/63-Diabetes-Equal-Opportunity-Disease.webp)

“We know that there are genetic factors involved, but it’s clear that lifestyle, food habits and the amount of physical activity play very important roles in the development of type 2 diabetes,” Dr. Saul Malozowski of NIH’s National Institute of Diabetes and Digestive and Kidney Diseases explained.

Among Asian Americans and Pacific Islanders, for example, traditional plant- and fish-based diets are being replaced with more animal protein, animal fats and processed carbohydrates. This could be a reason why diabetes is a growing problem among those populations in the United States. People who are inactive and eat a diet high in fat and low in fruits and vegetables are at higher risk for developing type 2 diabetes.

“Being overweight and sedentary is an unhealthy combination,” said Dr. James Gavin, III, past chair of the National Diabetes Education Program and clinical professor of medicine at Emory University’s School of Medicine. “It becomes even more risky when you add a genetic susceptibility to type 2 diabetes.”

A crucial fact revealed by diabetes research is that, whatever your ethnic background, if you are at high risk for developing diabetes, you can still lower your chance of getting it by losing a modest amount of weight, lowering the fat and calories in your diet, and increasing your physical activity to 30 minutes a day, 5 days a week.

*Definitions*

**Diabetes:**
A disease in which the body has problems producing or using insulin, a hormone needed to convert sugar, starches and other food into energy. In time, diabetes can lead to serious problems including heart disease, blindness, kidney failure and nerve damage.

**Genes:**
Stretches of DNA, a substance you inherit from your parents, that define characteristics you inherit like height and eye color. They also affect how likely you are to get certain diseases like diabetes.

**Type 2 Diabetes :**
Formerly called adult-onset diabetes, this is the most common form of diabetes. People can develop it at any age. Being overweight and inactive increase the chances of developing type 2 diabetes.

***A Word to the Wise...***

*Are You at Risk for Diabetes and Prediabetes?*

If you are 45 or older, especially if you’re overweight, talk to your health care provider about testing for diabetes and prediabetes. If you’re under 45, ask about your risk for prediabetes or diabetes and whether you should get tested if:

- You’ve had diabetes while pregnant or gave birth to a baby weighing 9 pounds or more.
- You’ve been told that your blood glucose or cholesterol (lipid) levels are not normal.
- Your blood pressure is 140/90 or higher or have been told that you have high blood pressure.
- You’re fairly inactive, doing physical activity less than 3 times a week.
- You have a parent, brother or sister with diabetes.
- You’re overweight.
- Your family background is African American, Hispanic/Latino, American Indian, Asian American or Pacific Islander.
