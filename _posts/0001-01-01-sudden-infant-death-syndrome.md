---
layout: post 
title: "Sudden Infant Death Syndrome"
date: 2019-03-16
image: assets/images/sudden-infant-death-syndrome.webp
excerpt: "What You Can Do To Lower the Risk of SIDS"
tags: Baby, NICHD, SIDS, Sudden Infant Death Syndrome
draft: false
---

If you’re a new parent, you want to do everything you can to keep your baby healthy. You probably already know that putting your baby to sleep on his or her back will reduce the risk of Sudden Infant Death Syndrome (SIDS), the sudden and unexplained death of a baby under one year of age. But did you know that placing an infant to sleep on his back can be good for your baby’s health in other ways, too? New research findings go beyond earlier studies that showed there were no adverse effects from placing babies to sleep on their backs.

“Placing infants to sleep on their backs not only reduces their risk of SIDS, but also appears to reduce the risk for fever, stuffy nose, and ear infections,” says Dr. Duane Alexander, director of NIH’s National Institute of Child Health and Human Development (NICHD). That’s good news for parents who may still have reservations about placing infants to sleep on their backs.

**Unexplained Infant Deaths**

SIDS deaths are associated with sleep, but no one knows the cause of these unpredictable and sudden deaths. Approximately 3,000 infants die each year of SIDS in the United States.

Before research identified a link between sleep position and SIDS, it was common practice to place babies on their stomachs to sleep. Some parents feared that an infant sleeping on his or her back is more likely to choke or vomit. Others believe that infants sleep better on their stomachs. Until a few years ago, many doctors advised parents to place their babies on their stomachs to sleep, because it was believed to be less risky than other sleep positions. Some studies estimated that in 1992, up to 70% of infants were being placed to sleep on their stomachs.

But research has since found that back-sleeping is the safest position. In the early 1990s, several studies showed that babies placed on their stomachs were much more likely to die of SIDS than babies placed on their back or side. Although babies placed on their sides often roll onto their backs, some roll onto their stomachs. Based on research studies, the American Academy of Pediatrics now recommends back-sleeping for infants under one year of age to reduce the risk of SIDS.

“A number of studies have shown the importance of educating parents and caregivers about continuing to put infants to sleep on their back throughout the first year of life,” Dr. Alexander says.

In 1994, NICHD launched the Back to Sleep campaign to do just that. By the late 1990s, only 17% of babies were being placed on their stomachs. Since the campaign began, SIDS has decreased by 50%.

**Risk Factors**

Researchers have found several other risk factors that place certain people at higher risk for SIDS than others.

African American babies, for example, are more than twice as likely to die from SIDS as white babies. Researchers think this could be due, at least in part, to the fact that African American infants are placed to sleep on their stomachs more often than other infants. According to previous studies, over 40% of black infants sleep on their stomachs, compared with about 25% of babies overall. One NICHD-funded study found that African Americans are less likely than other groups to be advised by their doctors to avoid placing their infants on their stomachs.

American Indian babies are more than three times as likely to die from SIDS as white babies. One study of Northern Plains Indians funded by NICHD and other Federal agencies associated maternal alcohol use during the first trimester with increased rates of SIDS, and also found that infants were more likely to die of SIDS if they wore two or more layers of clothing while they slept. Other studies also support the finding that keeping a sleeping child too warm may increase the risk of SIDS.

According to NIH researchers, infants who share a bed with other children are at higher risk for SIDS. Other studies have found that infants who sleep on soft bedding, such as sheep skins, pillow, quilts, or soft mattresses are five times more likely to die of SIDS than infants who sleep on firm bedding in their own sleep area, such as a crib. Sleeping on a sofa appears to be especially dangerous, but researchers don’t know why.

When two or more risk factors are present, the risk can go up even more dramatically. Babies who are put to sleep on their stomachs and on soft bedding can be as much as 21 times more likely to die of SIDS.

Because many of the known risk factors for SIDS can be avoided , families need to learn how to reduce the risk of SIDS. To learn more, read the accompanying box What You Can Do To Lower the Risk of SIDS.

***Did you Know That…***

- SIDS is the leading cause of death in babies after 1 month of age.
- Most SIDS deaths happen in babies between 2 and 4 months old.
- More SIDS deaths happen in colder months.
- Babies placed to sleep on their stomachs are much more likely to die of SIDS than babies placed on their backs to sleep.
- American Indian babies are over three times as likely to dies of SIDS than Caucasian babies.
- African American babies are more than twice as likely to die of SIDS than Caucasian babies.

**Source:** National Institute of Child health and Human Development and the Centers for Disease Control and Prevention’s National Vital Statistics Reports.

**Other Benefits**

![img](/assets/images/38Reduce-the-Risk-of-SIDS.webp)

If parents want more proof about the benefits of back-sleeping, consider the fact that babies placed to sleep on their backs also are less likely to develop fevers, stuffy noses, or ear infections. This finding is the result of research funded by both NICHD and NIH’s National Institute on Deafness and Other Communication Disorders (NIDCD). Researchers found that at one month of age, infants sleeping on their backs were less likely to come down with a fever, and at six months of age were less likely to develop stuffy noses than stomach sleepers. At both three and six months, back sleepers needed to visit the doctor less often for ear infections than stomach sleepers.

“Ear infections are quite common in infants and young children, and as many parents know, can be a cause of concern, especially when they occur during the first few months of life,” says Howard Hoffman, director of NIDCD’s Epidemiology and Biostatistics program.

Infants who have ear infections early in life are more likely to have chronic infections, which can result in hearing loss. As several studies have now shown an association between sleep position and ear infections, Hoffmann says, “This is more evidence for parents to put infants to sleep on their backs.”

***A Word to the Wise...***

Although there is no way to know which babies might die of SIDS, there are some things that you can do to make your baby safer.

- Always place your baby on his or her back to sleep, even for naps.
- Place your baby on a firm mattress, such as in a safety-approved crib.
- Remove soft, fluffy and loose bedding and stuffed toys from your baby’s sleep area.
- Make sure your baby’s face and head stay uncovered during sleep.
- Do not allow smoking around your baby.
- Don’t let your baby get too warm during sleep.
- Make sure everyone who cares for your baby knows to place your baby on his or her back to sleep.

**Source:** National Institute of Child Health and Human Development, Centers for Disease Control and Prevention’s National Vital Statistics Reports
