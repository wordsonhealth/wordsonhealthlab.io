---
layout: post 
title: "Genes or Environment"
date: 2019-05-18
image: assets/images/genes-or-environment.webp
excerpt: "Epigenetics Sheds Light on Debate"
tags: DNA, Epigenetics, Genes, NIEHS
draft: false
---

Which is more important in shaping who we are and what we will become—our genes or the environment around us? For centuries, people have debated whether nature or nurture decides how we look and act. Now, a field of research called epigenetics is showing that we can’t really separate one from the other.

“We can no longer argue whether genetics or environment has a greater impact on our health and development,” argues Dr. Randy Jirtle, a genetics researcher in Duke University’s Department of Radiation Oncology, “because both are inextricably linked.” Jirtle recently co-chaired a conference on epigenetics that was co-sponsored by Duke and NIH’s National Institute of Environmental Health Sciences (NIEHS).

![img](/assets/images/69-Genes-or-Environment.webp)

Your body “reads” genes and follows their instructions to make you who you are. Epigenetics is the study of factors that can change the way those genes are read without changing the genetic code itself. According to Jirtle, nutrients, toxins or other things in the environment can cause such “epigenetic” changes. They are one reason, for example, why identical twins who share the same set of genes can be so different.

Epigenetics can have long-lasting effects. Even poor nutrition during pregnancy can cause epigenetic changes that affect a child’s risk of getting many diseases later in life. Dr. David Barker of Oregon Health and Sciences University in Portland, Oregon says, “Studies around the world have shown that people with low birth weight have significantly increased risk of coronary heart disease, stroke, type 2 diabetes, hypertension and osteoporosis.”

Research shows that these epigenetic changes can even be passed down from one generation to the next.

Scientists are now discovering exactly how environmental factors produce these lasting changes. A major way genes can be altered is by something called methylation. That’s when a bunch of atoms scientists call a methyl group gets chemically attached to a gene.

Dr. Frederick Tyson, a program administrator with NIEHS and co-organizer of the conference, explains, “The addition of the methyl group ‘silences’ the gene….” In other words, when a gene’s had a methyl group attached to it, your body can’t read it anymore, even though the genetic code hasn’t changed one bit.

Studies by Jirtle’s research team have shown that epigenetic changes like this can change the course of development. In one set of experiments, pregnant mice eating a diet with four common nutritional supplements gave birth to mice with brown or black fur. Pregnant mice eating the same diet without the supplements gave birth to mice with yellow coats.

Even more important, the mice whose mothers received the supplements had less obesity, diabetes and cancer as adults. The researchers went on to discover that the nutrients affected a gene called agouti, leading to both the changes in coat color and susceptibility to disease.

NIEHS’s research programs now aim to learn more about how environmental factors influence genes and affect your risk for disease. Investigators are looking
at a wide range of substances in the environment, including pesticides and heavy metals, to see which can alter genes and which genes they’re altering.

At least a dozen drugs targeting epigenetic changes are already being tested, and more are in development. Last May, the Food and Drug Administration approved a drug that affects methylation to treat a rare bone marrow disorder that can lead to leukemia. As researchers learn more about how genes and environmental factors interact, they’ll be able to develop new approaches for disease prevention and treatment.

*Definitions*

**Epigenetics:** The study of factors that affect genes in a lasting way without modifying the genetic code itself.

**Genes:** Stretches of DNA, a substance you inherit from your parents, that define characteristics like height and eye color.
