---
layout: post 
title: "Aches in Your Legs"
date: 2019-06-20
image: assets/images/aches-in-your-legs.webp
excerpt: "Understanding Peripheral Arterial Disease"
tags: Aches Legs, Clogged arteries, Legs, NHLBI, PAD, Peripheral Arterial Disease
draft: false
---

If you’re past age 50, you may have resigned yourself to feeling a few more aches these days. However, if you’ve had pain or cramping in your legs when you’re walking that goes away when you stop, don’t shrug it off. It might be an early warning signal of a serious and sometimes-silent disorder called peripheral arterial disease (PAD).

Just like arteries in the heart, those in the lower legs can be become clogged with fatty deposits. Imagine your arteries are a complex highway system. Fatty deposits, also known as plaque, are the traffic jams that limit blood flow. Clogged arteries—blood-flow traffic jams—anywhere in the body increase the risk of heart attack and stroke.

![img](/assets/images/85-Aches-in-Your-Legs.webp)

Between 8 million and 12 million people over age 50 have PAD. Many never notice any symptoms. PAD symptoms include heaviness in your legs, awakening at night with pain in your lower legs, and pain or cramping in the legs when you’re walking that seems to lessen with rest. A lot of people who have these symptoms don’t tell their doctors. They simply accept the discomfort as part of growing older. Another sign of PAD that people may notice but dismiss is a change in the color of their feet.

Whether painful or silent, undiagnosed PAD is too dangerous to ignore. That’s why NIH’s National Heart, Lung, and Blood Institute (NHLBI) recently started a campaign to tell more people about the disorder, joining with 40 other organizations across the U.S. to raise awareness about PAD. Campaign organizers want people to recognize the signs and alert their doctor if they notice the symptoms in themselves.

Those most at risk for PAD are people over age 50, especially African Americans. Smokers and former smokers, and people who have diabetes, high cholesterol or high blood pressure are also at risk. Those who have had vascular disease, heart attack or stroke, or have a family history of those disorders should also be on the lookout for PAD.

If you’re over 50 or otherwise at risk, ask your doctor about being tested for PAD. A simple test called the ankle brachial index (ABI) can identify the problem. The ABI compares the blood pressure in your arm with blood pressure in your legs. Reduced blood flow in the legs could signal artery disease.

Once PAD is detected, your doctor will offer several treatments to help clear out the blockages before they lead to more serious problems. Your doctor may tell you to get more exercise, if you don’t have an active lifestyle. Recent results from a study of people with PAD showed that daily physical activity improves survival rates. Your doctor may also recommend changes to your diet and other efforts to lower high cholesterol and high blood pressure. Medications and surgery are also treatment options that can improve blood flow in the vessels.

What’s most important is to take those aches seriously and seek help from your doctor.

*Definitions:*

**Arterial:**
Related to arteries, the series of tubes that carry blood from the heart throughout the body.

**Vascular Disease:**
A disease of the vascular system, the system of vessels that circulate blood throughout the body.

***A Word to the Wise...***

*Questions to Ask Your Health Care Provider*

- Am I at risk for PAD?
- How can I lower my risk?
- Which screening tests or exams are right for me?
- What is my blood sugar level? If it’s too high or if I have diabetes, what should I do about it?
- What is my blood pressure? Do I need to do anything about it?
- What are my cholesterol numbers? Do I need to do anything about them?
- If I have PAD, what steps should I take to treat it?
- Will PAD increase my risk for other conditions?
