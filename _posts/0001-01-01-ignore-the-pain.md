---
layout: post 
title: "Ignore the Pain?"
date: 2019-06-02
image: assets/images/ignore-the-pain.webp
excerpt: "Innovative Pain Management Ideas"
tags: Controlling pain, Pain, Pain control, Pain Management
draft: false
---

Two people fall and suffer seemingly similar injuries. Six months later, one has completely recovered but the other still has debilitating pain. How can different people seem to experience pain so differently? Through carefully controlled experiments using advanced brain imaging techniques, researchers are discovering that people’s brains can process the same pain signals from their bodies very differently. These insights are leading to surprising new strategies for controlling pain.

Advances in pain research were the focus of the first annual symposium of the NIH Pain Consortium. NIH created the consortium to enhance pain research and promote collaboration among pain researchers across the many NIH institutes and centers involved with pain research. Scientists at the inaugural symposium described their investigations into the genes involved in pain, how nerves transmit pain signals from the body to the brain, and new medications and other therapies under development.

![img](/assets/images/76-Ignore-the-Pain.webp)

Dr. Robert Coghill of Wake Forest University explained that there are significant differences in the way people experience pain. When people had the same level of heat applied to the backs of their legs, Coghill recounted, the intensity of pain they reported was “all over the place”—from someone who said it didn’t hurt at all to someone who said the pain was so intense they almost withdrew from the study.

Coghill’s team wanted to see whether these ratings represent a true difference in the way people experience pain or differences in how they explain what they feel. To answer this, they examined the brain activity of their subjects using an MRI (magnetic resonance imaging) machine while they applied different levels of heat. They found that those who reported feeling more pain had stronger and more frequent activation in a number of brain areas, particularly a region called the primary somatosensory cortex.

These people were all getting a generally similar input delivered to their brains, but once the signal got into their brains, it seemed to be processed differently in different people. To see if they could manipulate that processing, the researchers trained people to associate different levels of painful stimulus with different tones. They then tested the impact of expectation by signaling a moderately painful stimulus but then delivering an intensely painful one.

“When we look at their pain intensity ratings, they decrease significantly,” Coghill said. “The bigger the expectation people had that the pain was going to go down, the more the pain in fact went down. Changes in expectation accounted for 88% of the variability in the pain people said they felt.”

What’s the possibility that the subjects were only telling researchers what they thought they wanted to hear? MRI showed that their brain activation matched what they were saying they felt.

“These people really were experiencing less pain than they would normally when they were correctly expecting the stimulus,” Coghill said. Expectation has a widespread impact on how the brain processes pain.

“The final word is always look on the bright side of life,” Dr. Coghill concluded. “Try to think positively. That can really change the way you experience pain.”

Dr. M. Catherine Bushnell of McGill University said that researchers have known for years that people feel more pain when they’re focusing on it than when they’re paying attention to something else. The problem in manipulating attention, however, is that mood and emotions play such a large role. But maybe manipulating mood, she thought, can have an effect on pain.

Odors have a strong emotional impact on people. By finding an odor someone likes and one they don’t, Bushnell’s group found that you can manipulate their mood. They went on to show that odor-induced mood affects how people rate pain when researchers apply heat to their arms.

“We found that, in fact, if you correlate the rating of pain unpleasantness with all these different factors…,” Bushnell said, “the only factor that predicts the ratings of pain unpleasantness is mood.”

Brain-imaging experiments showed that odor had a widespread effect on pain processing that involved many areas of the brain. Like the people in Coghill’s experiments, those in Bushnell’s really were experiencing different levels of pain.

Dr. Christopher DeCharms of Omneuron wanted to see if people could learn how to manipulate their brain activity themselves to affect how much pain they feel. It’s already known that stimulating certain brain regions electrically or with certain medications can have an impact on pain, DeCharms explained. “What if you can train the patient to cognitively modulate that same brain region without surgery or pharmacology?” he asked.

He and his colleagues developed a way, using an advanced MRI machine, to acquire brain activity data in real time and then feed that information back to the patient. The subjects had a device on their non-dominant hand to generate a painful heat stimulus. A scrolling line graph or a graphic of a fire going up and down showed them in real time the activity in a brain region called the anterior cingulate cortex, an area involved in pain perception and regulation.

The subjects successfully learned to manipulate activity not only in that region of the brain, DeCharms said, but also in other regions in the pain processing network as well.

“Through the course of training, they showed greater and greater control over their pain perception,” DeCharms explained. “Control over brain and control over pain mirrored each other very closely.”

The group next wanted to see if it was possible to use this approach on chronic pain. They performed a similar procedure in chronic pain patients from the Stanford Pain Management Center, except without applying any external pain.

The subjects reported a substantial decrease in their chronic pain after just one session. They said they felt like they had a greater sense of control over their pain and that they felt they’d learned what they needed to do to control it. While the experiment had only 8 subjects, DeCharms says he is planning to test the technique with larger groups.

However effective this particular method proves, one thing does seem indisputable: psychological factors like mood and attention can affect how much pain you feel.

***A Word to the Wise...***

*Help Your Doctor Treat Your Pain*

Doctors can prescribe several different medications and treatments for pain relief. To help them figure out how best to help you manage your pain, be prepared to talk about the following (a family member or caregiver can help someone with a communication or thinking impairment):

- **Pain**. Describe the pain-when it started, how long it lasts and whether it’s worse during certain times of the day or night.
- **Location**. Show exactly where the pain is on your body or on a drawing of a body.
- **Intensity or severity**. How bad is the pain?
- **Other factors**. What, if anything, increases or decreases the pain?
- **Personal response to pain**. Fear, confusion or hopelessness about the causes of pain can affect how you respond to and describe pain. Don’t be shy talking about things that are bothering you. Let your doctor know what you’re going through.
- **Goals for pain control**. How much pain are you willing to put up with?

Other signs of pain. Family, friends and caregivers may note behaviors that signal pain, too.

Source: NIH, National Cancer Institute
