---
layout: post 
title: "Make the Kidney Connection"
date: 2019-05-22
image: assets/images/make-the-kidney-connection.webp
excerpt: "Millions at Risk for Kidney Disease"
tags: Diabetes, High Blood Pressure, Kidney disease, NIDDK
draft: false
---

Do you know the main causes of kidney disease? If not, you’re not alone. Diabetes and high blood pressure are the leading risk factors for chronic kidney disease, which affects nearly 20 million Americans. Yet many people with diabetes and high blood pressure haven’t made “the kidney connection” and aren’t aware of their risk.
Dr. Josephine P. Briggs, a kidney specialist with NIH’s National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK) says, “It’s important for people at risk to get their kidneys tested and take steps to protect their kidney function.”

![img](/assets/images/71-Make-the-Kidney-Connection.webp)

March is National Kidney Month, and NIDDK’s National Kidney Disease Education Program wants people at risk for kidney disease to know about the importance of regular testing and the availability of treatments to prevent or slow kidney failure. If you have diabetes or high blood pressure, you should talk to your doctor about getting tested. Untreated, kidney disease can lead to kidney failure. Then, your only options are dialysis or a kidney transplant.

“Don’t wait for symptoms,” Briggs says. “Kidney disease strikes without warning. It often has no symptoms until just before the kidneys fail. People find themselves in the emergency room or on dialysis before they even know they have a problem.”

Your kidneys are bean-shaped organs located near the center of your back. Tiny blood vessels in your kidneys work to filter your blood to remove wastes. Diabetes and high blood pressure can damage these blood vessels, causing wastes to build up in your body. The damage can happen very slowly, without you knowing it’s going on.

Kidney failure affects both men and women from all racial and ethnic groups. However, African Americans, Hispanics and Native Americans have higher rates of diabetes and high blood pressure, putting them at greater risk for developing kidney disease.

However, kidney failure can be prevented or delayed. If a test shows that you have kidney disease, medicines called ACE inhibitors and ARBs can help keep your kidneys healthy. Those with diabetes and high blood pressure should also continue to control these conditions.

“Ten years ago, dialysis was inevitable for people with kidney disease,” says Briggs. “But because of treatments available today, it’s a different story. That’s why it’s so important for those at risk to get tested.”

***A Word to the Wise...***

*Protect Your Kidneys*

Here’s what you can do to protect your kidneys:

- **Control your diabetes and high blood pressure.** Continuing to control these conditions can help reduce the stress on your heart and blood vessels, which contributes to kidney disease.
- **Talk to your health care provider.** Discuss your risk for kidney disease, testing and how you can keep your kidneys healthy.
- **Get tested.** Ask your doctor to test your blood and urine for signs of kidney disease. The tests are the only way to know for sure if you have kidney disease.
- **Get treated.** If tests show that you have kidney disease, medicines are available to help slow its progression or prevent kidney failure altogether. Your doctor may also want you to see a kidney specialist.
