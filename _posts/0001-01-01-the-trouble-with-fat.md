---
layout: post 
title: "The Trouble With Fat"
date: 2019-05-24
image: assets/images/the-trouble-with-fat.webp
excerpt: "Insights into Low-Fat Eating"
tags: Colorectal Cancer, diet, Fat, Heart Disease, Low-fat diet, poor diet, WHI
draft: false
---

For years you’ve heard you should eat less fat. In a recent major study, however, a low-fat diet didn’t lower the risk of breast cancer, **colorectal cancer** or heart disease in women past menopause. But don’t pile on the butter and fried foods just yet. There’s still plenty of evidence that a low-fat diet full of fruits, vegetables and whole grains can help you live a long, healthy life.
The major study is the Women’s Health Initiative (WHI), one of the largest medical studies ever. One part of the study looked into the effects of a low-fat diet high in fruits, vegetables and grains. Previous studies had suggested, but not proven, that such a diet might reduce the risk of breast cancer, colorectal cancer and heart disease.

“Some of the media reports read as though everybody knew that a low-fat diet would prevent breast cancer, but in the scientific community this was a very open question,” Dr. Ross Prentice explained at a recent conference about the WHI. Prentice, a professor at the University of Washington, was involved in the study’s design.

![img](/assets/images/72-Insights-into-Low-Fat-Eating.webp)

Women were randomly assigned to follow either a low-fat eating plan or, for comparison, to continue eating as they had been. Those in the low-fat group aimed to reduce their fat intake to 20% of **calories** (Americans at the time were averaging 34%), increase their fruits and vegetables to 5 or more servings a day and increase their grain servings to 6 or more a day.

Almost 49,000 women were followed for an average of 8.1 years. They had physical exams, filled out questionnaires about their diets and sent in updates about diseases such as cancer and heart disease, among other things.
The study found no major differences in colorectal cancer, heart disease or stroke between the two groups. Although there was a 9% lower risk of breast cancer among the women who reduced the amount of fat they were eating, that difference wasn’t enough to prove it might not just be due to chance.

Prentice still thinks the result is encouraging. He also said he saw some other promising trends in the massive amount of data collected during this trial. For example, women who started at the highest levels of fat decreased their fat intake more and had a reduced risk of breast cancer. Women who had the greatest decrease in the **saturated and trans fats** they ate saw improvements in their rates of heart disease. These fats weren’t the focus of the study when it was designed, but we know they’re more strongly related to heart disease than the total amount of fat you eat.

Women on the low-fat diet also weighed about 5 pounds less than the comparison group after a year. After 9 years of follow-up, they still weighed about a pound less on average. Those on the low-fat diet had modest improvements in other measures that are proven risk factors for heart disease as well.

But to prove all these connections, Prentice explained, “We need longer follow-ups for a more definitive evaluation.”

To put these results in perspective, what scientists call a negative result—one that fails to prove something—doesn’t prove an idea is wrong. For example, low-fat eating didn’t turn out to be a magic bullet to prevent breast and colorectal cancer, but there still may be a connection. Cancer takes years to develop, and this study may simply not have been long enough to expose it.

Also, there are many things that could have confused the results of this study. Because of recent eating trends, women in the comparison group ate less fat than the researchers expected, so there wasn’t as much difference between the groups as they’d planned for. The women in the intervention group didn’t meet their goals for eating grains, either, and toward the end the difference between them and the comparison group had narrowed. In addition, research shows that people often underestimate how much they eat; that could definitely affect the results. Things might also have turned out differently if the women had been followed for longer—or if younger women or even men had been studied.
So what should you take away from this study? Maybe it’s best not to try thinking about eating and health in such simple terms. Most researchers believe that it’s a lifetime of healthy eating habits that makes the real difference. Dr. Linda Van Horn of Northwestern University pointed out at the conference, “I’ve never seen a study yet that says fruits and vegetables are bad for you.”

In another arm of the WHI, a study in which researchers tracked nearly 94,000 women for an average of 7 years, women who weighed less and exercised more had a lower risk of developing breast cancer. These two factors—controlling your weight and getting physical activity—turn up time and time again in studies looking into heart disease, cancers and a whole host of other health issues.

Dr. Elizabeth Nabel, director of NIH’s National Heart, Lung, and Blood Institute, which supported the WHI, commented, “The results of this study really do not change any established recommendations for women in terms of disease prevention.”

So while simply lowering the amount of fat you eat may not lower your risk of breast or colorectal cancer, eating well and exercising over the course of a lifetime will help your chances of living a long, healthier life.

*Definitions:*

**Calories:**
The amount of energy stored in food. When you eat more calories than your body can use, it stores that extra energy as fat.

**Colorectal Cancer:**
Cancer of the colon or rectum, parts of the large intestine.

**Saturated and Trans Fats:**
Types of fat in food that raise the level of cholesterol in your blood. The extra cholesterol can clog your arteries and put you at greater risk for having a heart attack or stroke.

***A Word to the Wise...***

*You’re the Key*

While a low-fat diet may not lower your risk of breast cancer, colorectal cancer or heart disease, there are other proven ways to lower your risk:

- **Breast Cancer:** Take steps, such as having regular mammograms and breast examinations, to catch breast cancer early. See www.nci.nih.gov/cancerinfo/types/breast for more information.
- **Colorectal Cancer:** Talk to your health care provider about when to begin tests, which ones to have and how often to schedule appointments. See www.nci.nih.gov/cancerinfo/types/colon-and-rectal for more information.
- **Heart Disease:**
  • **Don’t smoke**, and if you do, quit.
  • **Aim for a healthy weight.** 
  • **Get moving.** Aim for 30 minutes of moderate-intensity physical activity on most, if not all, days of the week.
  • **Eat for heart health.** Choose a diet that’s low in saturated fat, trans fat and cholesterol. Be sure to include whole grains, vegetables and fruits.
