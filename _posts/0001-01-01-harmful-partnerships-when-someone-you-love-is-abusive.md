---
layout: post 
title: "Harmful Partnerships When Someone You Love Is Abusive"
date: 2022-01-03T00:56:07+01:00
image: assets/images/harmful-partnerships-when-someone-you-love-is-abusive.webp
excerpt: Do you know the signs of an unhealthy relationship? Abuse isn't always easy to recognize when it's your own relationship.
draft: false
---

Abuse can be difficult to see, especially when it’s in your own relationship. It can start slowly, and it’s not always physical. You may not realize that the small comments a loved one makes to you are doing harm. You may even make excuses for them. They’re stressed from work or the pandemic.

But abuse takes many forms. It can be physical, like slapping, punching, or kicking. It can involve sexual violence. For many, it’s psychological—making someone feel worthless or isolating them from friends and family. Sometimes it includes stalking.

All these behaviors are forms of domestic violence. This is sometimes called intimate partner violence. At least 25% of women and 10% of men are estimated to have experienced intimate partner violence.

“Intimate partner violence is about power and control,” says Dr. Eve Valera, an expert on domestic violence and brain injury at Harvard Medical School and Massachusetts General Hospital. “It’s any type of physical, emotional, or psychological violence from a partner or spouse, husband, a wife—or even an ex.”

Some people experience one act of partner violence. Others suffer repeated abuse over years. The effects from either can be long-lasting.

Researchers are working to understand and prevent intimate partner violence and learn how to help those who have been affected.

### Signs of Partner Abuse

It’s important to recognize the signs of an abusive relationship. Controlling behavior is a common sign. Your partner may monitor where you are and how much you see friends and family. They might decide what you wear or eat or how you spend your money.

Verbal threats are also common. Other warning signs include name calling, humiliating someone in front of others, or blaming someone else for their own violent outburst.

Intimate partner violence affects the whole family. Children in homes where a parent is abused may be fearful and anxious. They can be afraid of conflict. They may also often be on guard, waiting for violence to break out.

“The repercussions of intimate partner violence are huge. It’s just devastating to homes,” says Dr. Ted George, an NIH expert on alcohol and violence.

Heavy drinking is one risk factor for intimate partner violence. Studies show that partner abuse is much more likely on days of heavy alcohol use.

George studies the brain areas involved in loss of control and violence. His work has found that some people who commit domestic violence have fewer connections in the brain that slow down the fight response.

Interventions that help people pause before they react may help prevent violence, says George. His findings also suggest that drinking may increase the risk of partner violence by affecting these same brain areas.

Other factors are also linked to committing partner violence. These include harmful use of drugs, having a personality disorder, and having abused a previous partner.

### Understanding the Harms

The harm from domestic violence isn’t always visible. Experiencing it puts you at higher risk for a range of health conditions. These include heart disease, high blood pressure, digestive problems, and reproductive issues.

Intimate partner violence is linked to several mental health conditions like depression, post-traumatic stress disorder, and suicide. People who experience intimate partner violence are also more likely to binge drink and misuse other drugs.

Studies show that many people may suffer brain injuries as a result of physical abuse. Valera’s research suggests that traumatic brain injuries are common. This is especially true for mild ones called concussions. They can have long-lasting effects on brain function.

Her team uses interviews, brain scans, and lab tests to look at abuse-related brain injury. They’ve found a relationship between the number of brain injuries and brain function. Brain injuries were linked with learning and memory problems and mental distress.

Valera notes that there are likely more women who sustain traumatic brain injuries from their partners than those with brain injuries from being an athlete or in the military. But these injuries usually go undiagnosed.

“The repetitive traumatic brain injuries that women often receive may not even be recognized as brain injuries, and are certainly not given appropriate care or treatment,” she says.

Domestic violence can escalate with tragic results. In the U.S., women are more likely to be killed by a current or former intimate partner than by someone else.

If you’re experiencing intimate partner violence, help is available. The Wise Choices box shows some of your options.

### Preventing Partner Violence

So what’s the best way to prevent intimate partner violence? Learn what to look for in a healthy relationship and how to build healthy relationship skills. It’s important to start early.

People who have violent relationships as teens are more likely to have them as adults. Dr. Jeff Temple, an expert on teen dating violence at the University of Texas Medical Branch, teaches youth about building healthy relationships in a school-based program. He’s been studying how well the program works.

In the program, students build relationship skills through role playing. They practice how to handle real-life situations, like apologizing or breaking up.

“Practice is huge for when they get into that situation in real life,” Temple says. “What the research tells us is that kids who are able to resolve conflicts and manage their emotions are less likely to be in violent relationships later on.”

He notes that no one is really taught how to be in a healthy relationship, even though it’s a basic part of being human. We practice reading, writing, sports—everything except relationships.

“So we learn about relationships through friends, which is sometimes okay, oftentimes poor. We learn from the media, which is not that great,” Temple says.

Ultimately, most of us learn about relationships through trial and error. Programs like the one Temple is studying can teach teens to build healthy, happy relationships.

### Wise Choices

#### Getting Help

If there is immediate danger, call the police. If there’s no immediate danger, consider these options:

- **Get medical care.** If you have been injured or sexually assaulted, go to a local hospital emergency room or urgent care center. 
- **Find out where to get help in your community.** 
- **Talk to someone.** Reach out to someone you trust for emotional support. 
