---
layout: post 
title: "The End of One-Size-Fits-All Medicine?"
date: 2019-04-12
image: assets/images/the-end-of-one-size-fits-all-medicine.webp
excerpt: "Personalized Medicine Showing Promise"
tags: diagnosis, Medicine, Pharmacogenomics, symptoms
draft: false
---

Most of the time, doctors can be pretty sure that the medicine they give you is going to help. But sometimes they have to just wait and see. The fact is, people don’t all respond the same way to medications. A new field of research is trying to take out the guesswork and help doctors predict which medicines will be right for you.

Because each of us has a unique set of genes , we all have tiny differences in our bodies that can affect the way medicines do their jobs. While typical doses of medicines work well for most people, in others they might not work at all or could cause unwanted side effects. The study of how our genes affect the way we respond to medicines is called pharmacogenomics. The ultimate goal of this research is to tailor medicines to people’s unique genetic make-ups, making drugs safer and more effective for everyone in the end.

For example, some people don’t process certain cancer medicines as fast as others. A normal dose for most people could be a dangerous overdose for them. Dr. Howard L. McLeod’s NIH-funded research group at Washington University in St. Louis has found specific differences that can predict whether certain cancer drugs will be toxic to a patient. This knowledge can be used to design a test to help doctors know which patients shouldn’t take these medicines.

“Patients will have more of a say in their therapy,” McLeod explained at a recent scientific meeting. “It’ll be their genes guiding decisions.”

Anti-cancer drugs aren’t the only ones that work differently depending on our genes. Dr. Stephen Liggett at the University of Cincinnati Medical Center studies the effects of medicines called beta blockers in people with chronic heart failure.

“These medicines can be very effective in some people,” he told the audience, “but the variability in response is enormous.” Liggett’s group, with funding from NIH, has pinpointed a single genetic difference that determines how people with chronic heart failure will respond to a beta blocker called bucindolol. In the future, tests may be able to tell doctors which patients this medicine will be able to help.

![img](/assets/images/51-inline-genome-sequencing.webp)

Pharmacogenomics is affecting many other fields of medicine as well. Researchers supported by NIH have found variations in genes that affect how people with asthma respond to a type of inhaled medicine. These genes might one day tell doctors which medicines will work best on a given patient. Researchers are also using pharmacogenomics to understand why people react differently to medicines for mood and anxiety disorders. And the list doesn’t end there.

*Definitions:*

**Genes**: Stretches of DNA, a substance you inherit from your parents, that define characteristics like height and eye color—and how you respond to medicines.

So when might we see practical changes from these advances in our health care? “Health care systems will gradually change,” predicts Dr. Rochelle Long of NIH’s National Institute of General Medical Sciences, who directs the Pharmacogenetics Research Network, a nationwide research effort sponsored by NIH to drive this new field forward. “Some changes are happening now, and within 5 to 10 years, medicine choices and doses will become much more tailored.”

Tests to predict how patients will respond to some cancer medicines are already on the market. The Food and Drug Administration, NIH’s sister agency, has also begun including pharmacogenomic information on some drug labels.

**Questions for Your Doctor:**

- How do I know if this medicine is working?
- What kinds of side effects should I be aware of when taking this medicine?
- What should I do if I get these side effects?

Ultimately, with the right privacy laws and security measures in place, we might all have cards in our wallets and doctors’ offices containing our genetic information. Our doctors will use this information to predict which medicines will work best in our bodies. But Long cautions, “How to consider the sum total of all this research information is still a considerable challenge. That world is still some distance off in the future.”

*Statistics:*

**Everyone’s Different**

- Nearly 3 million people in the U.S. are at risk for overdose when given the standard amount of a medicine used to prevent blood clots.
- Some people get no pain relief from certain prescription painkillers.
- Certain allergy and asthma medicines work well for some but not at all for others.

Source: National Institute of General Medical Sciences.

***A Word to the Wise...***

*Make Your Own Personalized Medical Plan*

Pharmacogenomics isn’t the only way to practice personalized medicine. Start your own personalized medical plan now:

- Gather your family’s medical history to find out what risks you might have inherited. (See the accompanying story in this issue for a free tool to help you gather this information). Then talk to your doctor about what you can do to lower these risks.
- Start moving. An active lifestyle can help ward off health problems. Walk, run, bike, swim, garden, or do any other physical activity you enjoy. Talk to your doctor about an exercise program that’s right for you.
- Develop good eating habits and control your weight., Good nutrition is a big part of staying healthy.
- If you smoke, quit. Smoking is the third leading cause of death in the U.S.
