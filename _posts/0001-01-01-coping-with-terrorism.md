---
layout: post 
title: "Coping With Terrorism"
date: 2019-02-16
image: assets/images/coping-with-terrorism.webp
excerpt: "How to Deal with the Anxiety and Fear Terrorism Fosters?"
tags: Anxiety, Fear, NIMH, Terrorism, Terrorist attack
draft: false
---

Most people may not think of them this way, but emotions are critical for human survival. Among other things, they help us learn from our bad experiences so that we can avoid harmful situations in the future. Terrorism turns this survival mechanism against us.

Terrorists exploit two very natural aspects of our brain, according to Dr. Steven E. Hyman, the outgoing director of NIH’s National Institute of Mental Health (NIMH). In a recent talk on the NIH campus, Dr. Hyman explained, “The first is that novel and uncontrolled situations are extremely stressful.” From animal studies, he said, we know that these situations can cause severe physical effects.

“Secondly,” he said, “we normally empathize with others. When we face danger, we don’t turn to the rational part of our brain and begin to do a statistically-based risk analysis. Instead, we see these pictures…or hear stories in the media…and we begin to identify with the victims.” This is a natural reaction, and has helped people survive throughout history by warning others of danger. “Fear is contagious from person to person,” Dr. Hyman said, “even if anthrax is not.”

Fear can help us consolidate memories, Dr. Hyman said. “All of us will remember where we were on September eleventh when we found out about the World Trade Center and the Pentagon.” This kind of learning is meant to be permanent, he said. “The idea is not to put yourself in harm’s way twice.”

But in the case of terrorist attacks, these mechanisms in the brain that are designed to ensure our survival can cause mental disorders that get in the way of normal life. And conditions like post-traumatic stress disorder (PTSD), depression, and anxiety disorders can plague not only the people who were directly in the line of a terrorist attack but also those watching events unfold in the media.

**Coping**

We are all prone to some fear and anxiety about terrorism. Many people have difficulty concentrating, unwanted memories, fatigue, difficulty sleeping, and a racing heartbeat. Other reactions can include emotional numbing or dissociation — the feeling that you are in a dream, that you’re having difficulty knowing what is going on around you.

These symptoms can lead to more serious mental disorders like PTSD. However, according to Dr. Farris Tuma, Chief of NIMH’s Traumatic Stress Program, people are very resilient. “The vast majority of victims,” he says, “do not develop a psychiatric disorder. With support and reassurance, most will resume normal and healthy lives.”

In order to cope with the stress and anxiety of terrorism, these experts both recommend that we turn to our social networks, including friends, family, and coworkers. Dr. Hyman pointed out that it’s important not to spin nightmare scenarios together but to come up with coping strategies. Examples are exercising or doing any of the other social things you usually do together. Restore your normal patterns of sleep and eating. Restore your normal work patterns. Get involved in volunteer activities that give you a sense of doing good for others.

If you have children, increase your family time together. Find out what children understand and talk about their fears. Dr. Hyman said that it’s important to reassure them that the adults are doing everything possible to ensure their safety. “It’s important that we don’t communicate fear to children through our own behavior,” Dr. Hyman said. “Remember, fear is contagious. Kids will pick up the non-verbal signals.”

It’s also a good idea to limit traumatizing exposure to television. Dr. Tuma says that, after the Oklahoma City bombing, PTSD symptoms in children were related to the amount of time they watched TV shows about the bombing. “We should be very concerned about watching these scenes over and over,” Dr. Tuma says, “especially by those who might be particularly vulnerable — including children and those with existing mental health concerns.”

One of the most important things a friend, family member, or co-worker can do for someone who’s been in a disaster or other trauma is to be a supportive, active listener, Dr. Tuma says. “It’s important to realize that it takes weeks, months, and sometimes years before a survivor of trauma is able to put the disaster behind him or her.”

**When to Get Help**

If your symptoms persist or are severe enough to keep you from doing and enjoying everyday things, it’s time to get professional help. Talk about suicide, excessive guilt or anxiety, and drug or alcohol abuse are all warning signals that require immediate professional attention. Talk to your doctor about your experience and your feelings, including scary memories, depression, trouble sleeping, or anger.

“The goal of terrorism,” Dr. Hyman said, “is really to undercut coping and leave us…with disorganization and panic.” To combat this effect, we need to try to be rational about the real dangers we face and to do everything we can to cope with our very natural anxiety.

***A Word to the Wise...***

*Post-Traumatic Stress Disorder*

Post-Traumatic Stress Disorder (PTSD) usually shows itself within three months after an event, but for some people signs don’t show up until years later. “It’s important that we make sure that people who actually have PTSD get professional help,” Dr. Hyman says. If you have PTSD, there are good treatments involving both psychotherapy and medication. Read the questions below to see if you might have PTSD.

- I feel like the terrible event is happening all over again. This feeling often comes without warning.
- I have nightmares, flashbacks and scary memories of the terrifying event.
- I stay away from places that remind me of the event.
- I jump and feel very upset when something happens without warning.
- I have a hard time trusting or feeling close to other people.
- I get mad very easily.
- I feel guilty because others died and I lived.
- I have trouble sleeping, and my muscles are tense.

If you have some of these problems, it’s time to see a health professional. Source: NIMH
