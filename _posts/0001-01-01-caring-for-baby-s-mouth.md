---
layout: post 
title: "Caring for Baby’s Mouth"
date: 2019-02-16
image: assets/images/caring-for-baby-s-mouth.webp
excerpt: "How to care for your baby's gums and emerging teeth?"
tags: Baby’s Mouth, Child’s teeth, Newborn, NIDCR, Teeth
draft: false
---

Do you have an infant? If so, the experts at the National Institute of Dental and Craniofacial Research (NIDCR), a component of the National Institutes of Health (NIH), have published an easy-to-read booklet, A Healthy Mouth for Your Baby, full of sound advice on how to help your baby have a healthy mouth even before the first tooth appears.

The booklet points out how good nutrition during pregnancy will help you and your growing child stay healthy. Ask your doctor for advice on eating the right foods and taking vitamins. Remember that breast-feeding is the optimal choice for feeding your newborn. Mother’s milk contains important protective antibodies that protect your infant against bacteria and viruses.

When should you begin cleaning your baby’s mouth and teeth? The sooner the better. Starting at birth, clean your baby’s gums with a clean, damp washcloth. As soon as teeth erupt, begin cleaning them — teeth can begin to decay as soon as they come in. Again, use a moistened cloth or a small, soft-bristled toothbrush at least once a day, preferably just before bedtime. Healthy teeth should be all one color. If you see spots or stains on the teeth, take your baby to a dentist. When most of your child’s teeth are in, at about the age of 2, you can add a small drop of toothpaste to the brush. Also, ask your doctor or dentist if you need to use fluoride drops for your baby’s teeth. Well known for its use in preventing tooth decay, fluoride may not be added to your town’s water supply.

Around the time of your baby’s first birthday, call your dentist and ask when you should come in for the first checkup. Pediatric dentists usually want to see a child between the ages of 1 and 2 and then twice a year thereafter. At this first visit, the dentist will quickly check your baby’s teeth and answer any questions about your child’s oral health.

***A Word to the Wise...***

- If you put your baby to bed with a bottle, fill it only with water. Most other liquids, including milk, have sugar in them and can cause tooth decay when your child sucks them.
- If your baby uses a pacifier, do not dip it in anything sweet like sugar or honey. Sucking on a sweetened pacifier has the same effect on your child’s teeth as sucking a bottle filled with a liquid containing sugar.
- Avoid feeding your child foods containing a lot of sugar like candy and cookies. Fruits and vegetables are more nutritious and much healthier choices.
- As your child gets older, let him use his own toothbrush. Until he’s about 6, though, you should put the toothpaste on the brush.
- Have your child spit out the extra toothpaste after she brushes.
- Until your child reaches 7 years old, you should brush his teeth after he does. Very young children cannot get their teeth clean by themselves.
