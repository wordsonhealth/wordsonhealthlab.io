---
layout: post 
title: "Caring for Concussions More Than a Bump on the Head"
date: 2022-01-02T19:28:21+01:00
image: assets/images/caring-for-concussions-more-than-a-bump-on-the-head.webp
excerpt: Recognizing the causes and symptoms of a concussion can help you reduce your risk of getting one and know what to do if you have one.
tags: [sticky]
draft: false
---

Accidents happen. A fender bender. A collision while playing sports. Or a fall at home. Many types of accidents can cause a bang to the brain. Each year, more than 2 million people in the U.S. visit an emergency room for a traumatic brain injury. Many others experience a blow to the head but never see a doctor.

Brain injuries can range from mild to severe. The most common type of mild brain injury is a concussion. Concussions can be caused by an impact to your head or whiplash motion to your body that makes your brain bounce or twist inside your skull. That can stretch your brain cells and cause harmful chemical changes that interfere with brain activity.

Even though they’re called “mild,” concussions should be taken seriously, explains Dr. Geoffrey Manley, an NIH-funded traumatic brain injury researcher at the University of California, San Francisco.

Most people heal from a concussion relatively quickly. But some will experience long-term effects on their thinking, mood, balance, and more. “If you’re still feeling effects a year after your injury, there’s nothing mild about it,” he says.

Concussions are particularly common among children and young adults. Older adults—age 75 and up—also have a higher than average risk of concussion because they’re at higher risk for falls.

Being able to recognize the causes and symptoms of a concussion is important so you can reduce the risk of getting one and seek immediate treatment if you have one.

### Seek Treatment

Some people may briefly lose consciousness right after a jolt to the head. Later symptoms can include headache, nausea, confusion, dizziness, or memory problems. Some people may also have sensitivity to light or noise, feel groggy or slow, or have changes to their sleep patterns.

“No two brain injuries are exactly the same,” says Dr. Dorian McGavern, a brain-injury researcher at NIH. Every person’s brain is a little different, and each impact can affect your brain differently, he explains.

Doctors have become much better at diagnosing concussions over the last decade, says Dr. Christina Master, an NIH-funded researcher studying children with traumatic brain injury at the Children’s Hospital of Philadelphia.

To diagnose a concussion, your doctor will ask how the injury happened and about your symptoms. They may also evaluate your balance, vision, and eye movements.

Most people with a suspected concussion don’t need additional tests. But those with more serious concussions may have bleeding or other damage in the brain. They may need an imaging test, like a CT scan, to detect these issues. A recently approved blood test can help doctors identify adults who need an imaging test.

Manley and other researchers are studying whether blood tests can better, and more quickly, diagnose concussions. “If we can identify who has a brain injury, we can treat them better, and make sure they don’t fall through the cracks,” he explains.

People with a concussion also need follow-up care. But Manley and others found in a recent study that fewer than half of people diagnosed with concussion in the ER receive such care. This lack of follow-up can prevent people from getting treatment that could improve their quality of life, Manley explains.

Drugs don’t yet exist to treat concussion itself. “But we have plenty of drugs and interventions for the side effects of traumatic brain injury,” he says. These include medications to help with chronic headache, depression, and sleep problems.

### Avoid a Second Blow

The brain is more vulnerable to a second blow after a concussion. That’s because a concussion can damage nerve cells in the brain. It can also cause the blood vessels that feed the brain to become leaky.

The body usually repairs these blood vessels over a period of several weeks. But in some people, they don’t heal that quickly—or at all.

“Some vessels will still have cracks in the seals. This lets material from the blood enter into the brain,” McGavern says. “These leaks can continue for sometimes months or more after the initial injury.”

Material from the blood is not supposed to get into the brain. Its presence can trigger **inflammation** and other types of damage. This damage may help explain the long-term symptoms some people experience after a concussion, McGavern says.

It may also explain why another blow to the head soon after the first can be so dangerous. McGavern’s team found that blood vessels in mice that had a second brain injury within a day had difficulty healing. In contrast, blood vessels in mice that experienced a second injury later repaired themselves normally.

### Take Time to Heal

After a concussion, the brain needs some rest. Research has shown that both children and adults benefit from reducing their mental and physical activity for a short time and should return to those activities gradually.

“Visual and balance problems can make it hard to do things like read, write, use an electronic device, or navigate a busy hallway,” Master explains. “Early recognition and accommodations for those problems can make a huge difference in the quality of life for people as they slowly return to work or school.”

People who have experienced a head injury shouldn’t feel frustrated if it takes up to a month to feel normal, she adds. “Rest” for someone with a concussion doesn’t mean doing nothing at all, Master says. Doing light physical activity and using your brain in ways that don’t make concussion symptoms worse can help you heal faster.

Physical rehabilitation programs can also help those with longer-lasting symptoms, explains Master. Talk with your doctor about how to return to your normal activity after a concussion. If symptoms persist, ask about physical therapy.

As scientists have come to understand the importance of protecting the brain after a concussion, new safety rules have followed, Master says. “In the last decade, every state in the nation has passed ‘return to play’ legislation,” she explains.

These rules, designed to protect youth athletes playing school sports, mandate a period of recovery for young athletes after a brain injury. It also requires a doctor-supervised period of gradual physical activity before returning to sports.

“These rules make sure that we don’t return athletes back to play before they are recovered, where another injury could cause more significant problems,” Master says.

Everyone can take steps to decrease their risk of concussion. See the Wise Choices box for tips.

### Wise Choices

#### Lower Your Risk of Concussion

Many head injuries can be avoided. Tips to stay safe include:

- Wear a seatbelt when you drive or ride in a car.
- Wear the correct helmet and make sure it fits properly when riding a bicycle, skateboarding, skiing or snowboarding, and playing sports like hockey and football.
- Install window guards and stair safety gates at home for young children.
- Improve lighting and remove area rugs, clutter, and other trip hazards in the hallway.
- Use nonslip mats and install grab bars next to the toilet and in the tub or shower for older adults.
- Install handrails on stairways.
- Improve balance and strength with regular physical activity.
