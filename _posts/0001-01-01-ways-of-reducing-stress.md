---
layout: post 
title: "Ways of Reducing Stress"
date: 2019-06-30
image: assets/images/ways-of-reducing-stress.webp
excerpt: "Stress Affects Both Body and Mind"
tags: Stress, Stress Control, Stressed out
draft: false
---

Maybe it’s money trouble or the burden of caring for a sick relative. Maybe it’s your job. Maybe it’s the traffic. Whatever the cause, everyone seems stressed out these days. People once hotly debated the idea that stress can affect your body, but we now know that stress can cause both short- and long-term changes to your body and mind. The more we understand how stress affects us, the more we learn about how to cope better.

Long before we humans learned how to drive cars to work and check in with the office on handheld computers, our bodies evolved to be finely attuned to a predator’s attack. When we sense danger, our bodies quickly release hormones like adrenaline into our bloodstream that increase our heart rate, focus our attention and cause other changes to quickly prepare us for coming danger. Stress was—and still is—crucial to our survival.

![img](/assets/images/89-Ways-of-Reducing-Stress.webp)

The stress that we’re adapted to deal with, however, is the short, intense kind—like running away before a bear can make a lunch of us. Modern life frequently gives us little time between periods of stress for our body to recuperate. This chronic stress eventually takes both a mental and physical toll.

It’s long been known that blood pressure and cholesterol levels go up in people who are stressed. Studies have now linked chronic stress with cardiovascular problems like hypertension, coronary heart disease and stroke.

The immune system is also affected by stress. Dr. Esther M. Sternberg at NIH’s National Institute of Mental Health says it makes sense for the immune system to gear up and get ready to heal potential wounds. But chronic stress can cause the system to backfire. Research has shown that wounds in people under chronic stress heal more slowly. Caregivers of people with Alzheimer’s disease, who are often under great stress, are more likely to get the flu or a cold—and when they take vaccines to protect their loved ones from getting flu, their bodies don’t respond as well.

Certain hormones that are released when you’re stressed out, such as cortisol and catecholamines, have been tied to these long-term effects of stress. Sternberg says, “If you’re pumping out a lot of cortisol and your immune cells are bathed in high levels of stress hormones, they’re going to be tuned down.”

Animal studies and brain imaging studies in people have shown that chronic stress can have a similar effect on the brain. Dr. Bruce S. McEwen of Rockefeller University explains, “Hyperactivity of the stress response results in changes over time in the circuitry of the brain.”

Brain cells bombarded by stress signals have little recovery time and eventually start to shrink and cut connections to other brain cells. The network that coordinates our thoughts, emotions and reactions thus starts to rearrange. Over time, entire regions of the brain can grow or shrink. That may explain why studies have linked higher levels of stress hormones with lower memory, focus and problem-solving skills.

Not everyone deals with stress the same way, however, and why some people seem to cope better is a major area of research. McEwen says studies in animals show that early life experiences and the quality of maternal care affect how curious an animal is when it’s older and how stressed it gets in a new environment.

Dr. Teresa Seeman of the University of California at Los Angeles School of Medicine points out that studies have also linked poverty and deprivation in childhood with how well people deal with stress. “There does appear to be a lingering impact,” Seeman says, but adds that it’s difficult to know the exact cause.

Two things that affect how much stress people feel are self-esteem and a sense of control. Workers who feel more in control at their jobs tend to feel less stress. People with low self-esteem produce more cortisol when they’re asked to do something that’s not easy for them, like speak in front of other people. They also don’t become accustomed to the stress even after doing something several times and continue to produce high levels of cortisol.

It’s not easy to change things like self-esteem and your sense of control at work, but there are things you can do to help you cope with the stresses of modern life.

“Sleep deprivation is a major issue,” McEwen says. People who are stressed out tend to get less quality sleep. And sleep deprivation affects your ability to control your mood and make good decisions. It also throws the stress hormones in your body off balance.

“If you’re sleep deprived,” McEwen explains, “blood pressure and cortisol don’t go down at night like they should.” McEwen sees people who work night shifts as a window into what chronic stress does to the body over time. “They’re more likely to become obese and to have diabetes, cardiovascular disease and depression,” he says.

People who are stressed out tend to do other things that make their body less healthy and more vulnerable to the effects of stress. Many eat more fatty comfort foods, which can lead to obesity and diabetes. They may smoke or drink more, raising the risk for cancer and other diseases. And they often feel they’re just too busy to exercise.

Seeman says, “Being physically active helps keep the body’s systems in better shape and thus better able to deal with any demands from other stressful conditions.”

Another factor affecting how we deal with stress is the isolation of modern life. Sometimes it seems like the only time we interact with our family or co-workers is when we’re having a conflict. Seeman says it’s important to develop a network of people you can go to and talk with when you’re confronted with difficulties in your life.

“Large studies have clearly shown,” she says, “that people who have more social relationships, a larger network of people they interact with on a regular basis, live longer. Research suggests they’re less likely to show declines as they’re older.”

All this research highlights the fact that healthy practices can complement mainstream medicine to help treat and prevent disease. Do things that make you feel good about yourself, mentally and physically. Get enough sleep. Eat a healthy diet and exercise regularly. Develop a network of people you can turn to in difficult times.

If you still find yourself too stressed out, talk to your health care professional. There are many therapies they may recommend to help you deal with stress and its consequences. The effects of being chronically stressed are too serious to simply accept as a fact of modern life.

***A Word to the Wise...***

*Ways of Reducing Stress*

- Get enough sleep.
- Exercise and control your diet.
- Build a social support network.
- Create peaceful times in your day.
- Try different relaxation methods until you find one that works for you.
- Don’t smoke.
- Don’t drink too much or abuse any other substances.
