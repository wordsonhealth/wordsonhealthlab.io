---
layout: post 
title: "The Future of Genetic Testing"
date: 2019-05-16
image: assets/images/the-future-of-genetic-testing.webp
excerpt: "Telling Science Fact from Science Fiction"
tags: DNA, Genetic diseases, Genetic Testing, NHGRI
draft: false
---

Are you likely to get heart disease? Is obesity in your future? Your risk of developing many diseases and health conditions is partly written in your genes. One day soon you’ll be able to visit the doctor, have some blood drawn and find out more about your health risks for the next 5 or 10 years through a method called genetic testing. But we still have many things to learn about genes before that vision becomes a reality.

First, the facts: The idea of genetic testing isn’t new. As early as the 1960s, doctors began urging that newborn babies be tested for rare diseases they could inherit from their parents. Such diseases are called genetic diseases because the illnesses are contained in genes. Testing began for phenylketonuria (PKU), a rare disease that causes mental retardation. PKU can be prevented with a special diet if it’s detected early. The test for PKU and other rare but treatable diseases are now routinely done in the hospital soon after a baby is born.

In the 1970s, researchers developed genetic tests that could be done before a baby is born. Scientists found that if a mother and father both have the gene for a certain illness, their child has a high possibility of being born with that disease. Soon, couples thinking of having children could get tests for genes that put their children at risk for developing sickle cell disease and Tay Sach’s. By the 1980s, tests called prenatal genetic tests were regularly done before a baby was born to find out the risk of a genetic disease. They’re now a routine part of health care in most states.

In the 1990s, researchers were able to identify genes that showed a person’s chance of developing breast cancer and colon cancer. People with certain forms of a gene called BRCA-1, for example, are more likely to develop breast cancer. Genetic testing of this type can help you find out whether you have a “genetic predisposition” to some diseases—in other words, whether certain gene variations you carry make you more likely to develop a disease. That doesn’t mean that you’re destined to develop it; it only means you’re more likely than those with other versions of the gene.

![img](/assets/images/68-The-Future-of-Genetic-Testing.webp)

“It’s important to realize that nearly every disease has both a genetic component and an environmental component,” Dr. Benjamin Wilfond of NIH’s National Human Genome Research Institute explains. “You can’t modify your genes, but you can modify your environment to help prevent some diseases.”

Consider the common cold virus, for example. Anyone who comes in contact with the virus could become infected. However, some people have a genetic predisposition that makes them more likely to get sick. If you know this ahead of time, you can take measures like steering clear of places where cold germs are likely to be and taking extra care to wash your hands when visiting public places.

Genetic testing can give us a similar head start on fighting many diseases. People who learn early that they’re genetically predisposed to a disease can benefit by knowing what symptoms to look for and potentially finding the disease in its early stages. They may also be able to change things in their lifestyle—their environment—to prevent the disease.

By the early 2000s, computers and other technology had advanced to the point where large collections of a person’s genes could be looked at, a method called genetic profiling. In the future, some technology companies may be able to offer genetic testing to map your whole genome—all the genes in your body at once! That sounds like a dream come true for preventing disease, but the reality is that researchers haven’t yet learned enough to help predict your health.

“The marketing for some testing may be premature,” Dr. Wilfond warns. “We know so little about some of these genes, and they may be small contributors to actual disease. There is a danger of misunderstanding or misinterpreting the results. These tests are giving you just small amounts of information.”

Wilfond advises people to think carefully about offers for testing. “Ask yourself a couple of questions before you decide,” he says. “Is this test part of routine medical care that will help me, or is this test something that’s only going to make somebody a lot of money?”

In the meantime, researchers are learning more about genetics and disease every day. “We now understand how to look at a person’s genes for their risks of more common diseases such as heart disease, asthma and Parkinson’s disease,” Wilfond points out. As scientists learn more about how environmental factors affect genes and how illnesses develop, more types of genetic testing will become part of routine health care.

Television and movies—and even some marketing—often make the science fiction of genetic testing seem like science fact. It’s true that health researchers are always looking far ahead to a day in science future. Part of the challenge of genetic testing is figuring out what to tell people until that day comes.

“There is a process for accepting new things in medicine that happens to be gradual and that’s good,” Wilfond observes, “because as we wait longer, we learn more.”

*Definitions:*

**Genes:** Stretches of DNA, a substance you inherit from your parents, that define characteristics like height and eye color, along with how likely you are to get certain diseases.

**Genetic Testing:** Test that involves taking a sample from someone’s blood, hair, skin or other body part, then examining the person’s genes to look for signs they might be at high risk for certain diseases or disorders.

**Genome:** All the genes in your body.

***A Word to the Wise...***

*Should You Get a Genetic Test?*

Considering whether to have a genetic test is a lot like deciding whether to have any other medical test, according to Dr. Benjamin Wilfond of NIH’s National Human Genome Research Institute. You should ask yourself—and then your doctor—a couple of questions first:

- Is this test a routine part of medical care? A test that has already become part of regular health care practice is well known to doctors and other people on your medical team.
- What will the results tell me? As with any test, it’s important to have the results reviewed by experts who understand them. Doctors will often recommend you consult a genetic counselor, a type of health professional with expertise in medical genetics and counseling, to help you make decisions about your health based on the test results.
