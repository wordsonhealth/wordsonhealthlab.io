---
layout: post 
title: "Successful Parenting"
date: 2019-02-20
image: assets/images/successful-parenting.webp
excerpt: "NIH Booklet Offers Guidance Based on Research"
tags: Children, NICHD, NIH Booklet, Parenting, RPM3
draft: false
---

Being a parent is an adventure. It’s one of the most fulfilling and rewarding experiences in life – and one of the toughest. Even the most conscientious parent can feel overwhelmed when faced with unexpected challenges. New help is available in the form of an easy-to-read booklet from the NIH’s National Institute of Child Health and Human Development (NICHD).

Developed from over three decades of research by NICHD on parenting and child development, Adventures in Parenting: How Responding, Preventing, Monitoring, Mentoring, and Modeling Can Help You Be a Successful Parent suggests ways to include these sound parenting practices in your day-to-day parenting. The booklet is intended for people of every background who are raising children, from first-time parents to grandparents.

“Parenting is the most important job in the world,” says Dr. Duane Alexander, director of NICHD. “Adventures in Parenting explains what we have learned from parenting research and shows parents how to put this research into practice.”

The publication’s basic premise is that parents really matter. That’s why parents need to take an active approach to child raising. You do not have to be perfect to do a good job. Successes and mistakes are a part of being a parent. But there are some general principles you can follow to become a more effective, consistent and attentive parent.

**The RPM3 Method**

Adventures in Parenting encourages parents to use the RPM3 method – a “no-frills” approach to parenting. This information can help you to shape your own parenting practices. RPM3 stands for:

**R**esponding to your child in an appropriate manner
**P**reventing risky behavior or problems before they arise
**M**onitoring your child’s contact with his or her surrounding world
**M**entoring your child to support and encourage desired behaviors
**M**odeling your own behavior to provide a consistent and positive example for your child

RPM3 is not a how-to manual telling parents what they should do. These are guidelines that parents can use to make decisions when different situations arise.

At first glance, some of the RPM3 concepts might seem obvious. But there’s more to RPM3 than meets the eye. For example, responding to a child isn’t the same as reacting. Reacting is answering with the first word, feeling, or action that comes to mind – a normal thing to do, especially with all the other things you do every day. Responding to your child means taking a moment to think about what is really going on before you say or do something. That time will help you choose the best way to get from the current situation to the outcome you want in the long-run.

To help parents apply these concepts, the booklet also provides examples of how some parents have used RPM3 with children in three age groups: three and under, between the ages of four and ten, and between eleven and fourteen. The examples are intended to help you learn what you need and adapt it to your life with your children.

If you have a gifted child or a child with a disability or other special needs, RPM3 can be useful too. All children can benefit from good parenting practices, regardless of family circumstances, family size, living arrangement, and economic status.

**Plan Your Way to Better Parenting**

Why should you use RPM3? You wouldn’t think of not having a plan to help you meet your financial needs or to keep your career on track. Why not have a plan for the most important job of all – being a parent?

“A plan is particularly important in getting through the trying times,” says Dr. Alexander. “A plan can help parents prevent small problems from becoming bigger ones. And having a long-term strategy can help parents turn short-term ‘failures’ into long-term successes.”

If using RPM3 sounds like too much work, consider that you’re probably already using many of the concepts daily as a parent. For example, acting as a “mentor” can mean simply spending time with your child on a regular basis – something most parents do anyway.

According to the NICHD, RPM3 can help parents develop a healthy relationship with their children that can last into adulthood. Children gain a sense of security when they know they can count on their parents, and a good relationship can help both of you get through the difficult times.

**Solving Difficult Problems**

Adventures in Parenting also talks about how to solve particularly difficult parenting problems. Don’t face it alone, the booklet stresses. Talk to other parents or a trusted friend or relative about the problem. Some of them might be dealing with or have dealt with similar things with their children. They may have ideas on how to solve a problem in a way you haven’t thought of. Or they might share your feelings, which can also be a comfort.

Some problems are just too big to handle alone, not because you’re a “bad” parent, but simply because of the nature of the problem. It’s important to get outside help if needed. Use all the resources you have to solve a problem. Remember: It’s not important how a problem is solved, just that it is.

There is no magic formula for good parenting. We can all use some help at times. There’s a lot of advice available to parents out there, but keep in mind that RPM3 is based on decades of research. Dr. Alexander says, “The guidelines provide insights about what children respond to, from their parents and their environment.”

The adventure of parenting may have its struggle and trials, but it has great rewards as well. RPM3 can help you become a more effective, consistent, active, and attentive parent.

***A Word to the Wise...***

*Three Central Principles of Parenting*

The M3 in RPM3 describes three complex, but central principles of parenting: monitoring, mentoring, and modeling. Many people are confused by these words because they seem similar, but they are really very different. It might be easier to understand these ideas if you think of them this way:
Being a monitor means that you pay careful attention to your child and his or her surroundings, especially his or her groups of friends and peers at school.
Being a mentor means that you actively help your child learn more about himself or herself, how the world works, and his or her role in that world. As a mentor, you will also support your child as he or she learns.
Being a model means that you use your own words and actions as examples that show your beliefs, values, and attitudes in action for your child on a daily basis.

*Where can I go for parenting help?*

- Other parents
- Family members and relatives
- Friends
- Pediatricians
- School nurses and counselors
- Social workers and agencies
- Psychologists and psychiatrists
- Community groups
- Support and self-help groups
