---
layout: post 
title: "Communication Breakdown How Aphasia Affects Language"
date: 2022-01-02T19:32:01+01:00
image: assets/images/communication-breakdown-how-aphasia-affects-language.webp
excerpt: Even if your loved one has difficulty communicating, they can still be part of the conversation.
draft: false
---

Language sets humans apart from other species. We use it constantly to tell each other what we feel, think, and need.

Losing the ability to communicate can be devastating. But that’s what happens in a condition called aphasia. Aphasia occurs when a part of the brain that helps process language is damaged.

The most common cause of aphasia is **stroke**. But a head injury, infection, brain tumor, and other brain disorders can also cause the condition. Almost 180,000 people in the U.S. develop aphasia every year.

Different types of aphasia affect language in different ways. For example, people with Wernicke’s aphasia can still speak. But they produce long sentences that don’t make sense and often aren’t aware of their mistakes. People with Broca’s aphasia understand most language and know what they want to say, but struggle to produce even a few words.

Other types of aphasia affect reading, writing, and other aspects of expressing and understanding language. The type depends on which area of the brain is damaged.

“Language is not located in just one place in the brain. It’s really distributed,” says Dr. Leora Cherney, an NIH-funded aphasia researcher at the Shirley Ryan AbilityLab.

That’s helpful for recovery because the brain can often be trained to use different parts to process language in new ways. “You can think of language as an electrical circuit,” Cherney explains. “If you break one part of the circuit, you can create pathways to reconnect it.”

Health care professionals called speech-language pathologists can help people with speech, language, and related problems to retrain their brains. Therapy may start very soon after the loss of language.

“For the best outcomes, we want to jump in straight away to work with the person who has aphasia,” says Cherney.

Some types of therapy for aphasia focus on re-learning one word at a time. Cherney and other researchers have been testing a different type of speech therapy called script training. This technique involves repeating sentences or even whole conversations over and over.

“We apply this training very intensively, for hours a day,” says Cherney. “There’s a growing amount of research that shows intensive practice is important for changing the brain.”

Researchers, including Cherney, are also testing the use of brain stimulation in addition to speech-language therapy. They use magnets or electrodes placed on the head to temporarily alter brain activity.

Even with treatment, some people with aphasia won’t be able to gain back their language skills. But they may benefit from learning other communication strategies, such as using gestures or drawing, Cherney says.

Technology can also help. “There’s so much just on a smartphone that can help facilitate communication,” Cherney explains. “For example, you can take a picture on your phone. That can help people understand the topic that you want to communicate.”

Though aphasia affects language, Cherney stresses that it doesn’t impact intelligence. Even though your loved one might have difficulty communicating, they can still be part of the conversation. See the Wise Choices box for tips.

### Wise Choices

#### Helping Those With Aphasia Be Heard

To help someone with aphasia feel more comfortable communicating:

- Ask for and value the opinion of the person with aphasia.
- Minimize distractions, such as a loud radio or TV.
- Use short, uncomplicated sentences when speaking.
- Write down key words to clarify meaning as needed.
- Avoid correcting their speech.
- Allow them plenty of time to talk.
- Encourage any kind of communication, including speaking, gesturing, pointing, or drawing.
