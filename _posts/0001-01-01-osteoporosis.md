---
layout: post 
title: "Osteoporosis"
date: 2019-03-20
image: assets/images/osteoporosis.webp
excerpt: "Men Can Get Osteoporosis Too"
tags: Bones, calcium, NIA, NIAMS, Osteoporosis, vitamin D
draft: false
---

Most people don’t think that men develop osteoporosis. This disease, in which bone becomes thin and fragile and can fracture easily, is mostly associated with women. Doctors don’t often discuss the issue with their male patients. But men can get the hip and other bone fractures that come with osteoporosis, too, and it’s no less painful or debilitating for them than it is for women.

Men are usually diagnosed with osteoporosis only when they have fractured a bone. Men don’t generally experience the rapid bone loss in their 50’s that women do, but by age 65 or 70, they are losing bone mass at the same rate as women. Hip fractures occur at older ages in men, which might explain why men who break a hip are more likely to die of complications than women. More than half of all men who suffer a hip fracture go from the hospital to a nursing home, and 79 percent of those who survive for one year still live in nursing homes or intermediate care facilities.

![img](/assets/images/40-Osteoporosis-bone.webp)

**Studying Osteoporosis in Men**
Scientists are trying to learn more about the causes, diagnosis, treatment and prevention of osteoporosis and its related fractures in men as well as in women. In 1999, NIH’s National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS), and two other NIH components, the National Institute on Aging (NIA) and the National Cancer Institute (NCI), launched a seven-year study that is following 5,700 men, age 65 or older. “Mr. OS,” as it is called, aims to determine the extent to which the risk of fractures in men is related to bone mass and other factors such as their bone structure, lifestyle and tendency to fall.

In the NCI component of “Mr. OS,” scientists are trying to answer the question of whether having a high bone mass is associated with an increased risk of prostate cancer. In women, if you have a high bone mass, you have a higher risk of getting breast cancer. Both types of cancers are thought to be associated with your whole lifetime exposure to the sex hormones your own body makes.

NIH’s National Heart, Lung, and Blood Institute (NHLBI) is also supporting a part of “Mr. OS” that is looking at the role of sleep in the health of older men.

*Progressive Spinal Deformity in Osteoporosis*

![img](/assets/images/40-Osteoporosis-spinal.webp)

**Treatment and Prevention**
Men are more likely than women to have a high risk of fracture due to secondary causes, like a specific disease (such as celiac disease, in which a person’s intolerance to a protein found in wheat and other grains interferes with their intestinal absorption of calcium) or taking medications that can affect bone mass (like the steroids used to treat asthma, rheumatoid arthritis and other diseases). Knowledge of the diseases and conditions that can affect bone mass can help to prevent men as well as women from reaching the point of fracture before diagnosis.

Getting enough calcium is very important for preventing osteoporosis. Adults 19 – 50 years old need 1,000 milligrams (mg) of calcium every day; those over 50 need 1,200 mg. The best way to get enough calcium is through your diet. Buy fortified orange juice and cereals, and eat lots of green leafy vegetables and low-fat dairy products like cheese, milk, ice cream and yogurt.

![img](/assets/images/40-Osteoporosis-food.webp)

You should also get enough vitamin D. If you spend 15 minutes outside in the sun each day, your body should make enough on its own. If you have limited sun exposure, scientists currently recommend 200 to 400 international units (IU) if you are under age 70 and 600 if you are over.

It’s also important to do regular weight-bearing exercise, such as walking, jogging, stair-climbing, tennis, weight-training and dancing. These exercises may strengthen your bones and may also help with your balance. That will reduce your risk of falling and thus reduce your chances of breaking a bone.

![img](/assets/images/40-osteoporosis-exercises-1.webp)

If you already have osteoporosis, doctors are prescribing most of the same medications that they are giving to women. Alendronate (brand name Fosamax®) and risedronate (brand name Actonel®) both now come in a once-a-week pill. But they can cause problems with your stomach or esophagus (the tube that connects the mouth with the stomach) if not taken exactly as directed. The Food and Drug Administration has approved teriparatide (brand name Forteo®) only for those who are at high risk of fracture; the drug must be injected daily for no longer than two years. Be sure to talk with your doctor about your options.
