---
layout: post 
title: "Oral Complications of Cancer Treatment"
date: 2019-02-06
image: assets/images/oral-complications-of-cancer-treatment.webp
excerpt: "Good Oral Health Care Can Ease Complications of Cancer Treatment"
tags: Cancer Treatment, Oral complications, Oral Health
draft: false
---

Many people are aware that cancer treatment often causes unavoidable side effects like nausea and hair loss. But many of us don’t realize that up to one-third of people who get chemotherapy and head and neck radiation for cancer also develop complications that affect the mouth. These problems can be serious and may interfere with cancer treatment, dramatically affect quality of life, and may even last a lifetime.

The NIH’s National Institute of Dental and Craniofacial Research (NIDCR) wants cancer patients and health professionals to know that proper mouth care can prevent or minimize oral complications and enhance both survival and quality of life. NIDCR, through its National Oral Health Information Clearinghouse (NOHIC), has launched a national awareness campaign about oral complications of cancer treatment, called Oral Health, Cancer Care and You: Fitting the Pieces Together, that provides free materials to patients and health professionals about how to prevent or reduce these potentially serious oral complications.

Of the 1.2 million Americans diagnosed with cancer each year, more than 400,000 will develop oral complications from their treatment. Complications of the mouth are most common in patients receiving head and neck radiation, bone marrow transplants, or certain types of chemotherapy.

**Did You Know That…**

- Many cancer patients develop mouth problems during treatment.
- Side effects in the mouth can not only make it hard to eat, talk, and swallow but also can interfere with your treatment.
- Oral complications of cancer treatment can be short-term or linger for years.
- Proper oral health care can prevent or reduce the severity of many mouth problems.

Radiation and chemotherapy kill cancer cells but they can also kill normal cells, including those in the mouth. These treatments can cause sore, inflamed gums; bleeding; mouth ulcers; infections; and dry mouth due to damaged salivary glands. Long-term side effects that may occur or continue after cancer treatment include chronic dry mouth and rampant tooth decay, jaw stiffness, inability to heal, permanent jaw bone damage, and in children, growth and developmental abnormalities of the bones and teeth.

These conditions can be so bad that patients may only be able to tolerate lower, less effective doses of anticancer medications, may postpone treatments, or may stop treatment entirely. Oral side effects can also be the source of infections that can interfere with cancer treatment and even threaten survival.

But cancer patients, working with their health care providers, can do a lot to prevent these problems or to reduce their severity, if they occur.

NIDCR’s materials provide patients with the tools they need to take charge of their oral health care. They explain the importance of seeing a dentist to ensure a healthy mouth before cancer treatment begins and how to care for the mouth during and after radiation or chemotherapy to help prevent complications. Written in an easy-to-read style, the materials provide practical self-care tips on such problems as keeping the mouth moist, brushing and flossing when the mouth is sore, and avoiding foods that could cause injury and pain.

***A Word to the Wise...***

- If you are scheduled to have cancer treatment, see your dentist at least 2 weeks before your treatment starts.
- Take good care of your mouth during treatment; keep it moist and clean.
- Check your mouth every day for sores or other changes.
- Talk regularly with your cancer doctor and dentist about any mouth problems you have.
- Avoid using alcohol, tobacco, and toothpicks while undergoing cancer treatment.
