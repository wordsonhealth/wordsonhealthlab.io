---
layout: post 
title: "Shattering Myths About Kidney Stones"
date: 2019-03-04
image: assets/images/shattering-myths-about-kidney-stones.webp
excerpt: "What causes kidney stones? Signs, symptoms and treatment"
tags: Bladder, Kidney, Kidney Stones, NIDDK, The Urinary Tract, Ureter
draft: false
---

Kidney stones. Just mention them and people cringe — and for good reason. They are one of the most painful conditions a person can have. Chances are you know someone who’s had them. More people are developing kidney stones, and researchers are trying to find out why and what can be done to prevent them.

But what exactly is a kidney stone? Kidney stones are hard masses developed from crystals that build up in the kidney, the organ that makes urine. These crystals can contain various combinations of chemicals, but are most often made of calcium in combination with either oxalate or phosphate.

![img](/assets/images/32-Shattering-Myths-about-Kidney-Stones.webp)

These chemicals are part of a normal diet and make up important parts of the body, such as bones and muscles. The crystals normally remain tiny enough to travel through the urinary tract and pass out of the body in the urine without being noticed. But in some people, the crystals stick together and continue to build up to form kidney stones.

Many people have misconceptions about kidney stones. Some think that stones occur only rarely and mainly in people who are already ill. But now we know that’s changing. Get the facts about how research is offering new hope to those who suffer from this increasingly common condition.

*Myth: Kidney stones are rare.*
**Fact: Kidney stones are one of the most common disorders of the urinary tract.**

NIH’s National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK) estimates that up to ten percent of Americans will have a kidney stone at some point in their lives, and most will be between the ages of 20 and 40.

Over the past 20 years, the number of people with kidney stones has been increasing. Scientists don’t have an explanation for this, but they think it may be related to diet and lifestyle. “Kidney stones are becoming a common occurrence in adults, and it looks like a combination of environmental and genetic factors may be responsible,” says Dr. Thomas Hostetter, director of NIDDK’s National Kidney Disease Education Program.

*Myth: Only men get kidney stones.*
**Fact: Women get kidney stones too. Even children can get them.**

Although men tend to develop kidney stones more frequently than women, the number of women with kidney stones has been increasing. Even children can have kidney stones, although less often than adults.

“Most children with kidney stones usually have a genetic or metabolic disease that makes them more likely to form stones,” explains Dr. Hostetter.

If you have a family history of kidney stones, you are also more likely to develop them. Some people with a family history are believed to have a defective gene that results in excess calcium in their urine. NIDDK researchers are attempting to find out more about the genes responsible for this disorder, and to see if there are any other unusual factors that make these families more susceptible to kidney stones.

**Did You Know That…**

- People who have a family history of stones or who had more than one stone are likely to develop another.
- If a person is at risk for developing stones, the doctor may perform certain blood and urine tests. These tests will determine which factors can be best altered to reduce that risk.
- Some patients will need medicines to prevent stones from forming.
- People with chronic urinary tract infections and stones will often need the stone removed if the doctor determines that the infection results from the stone’s presence. They should receive careful follow-up to be sure the infection has cleared.

Source: NIDDK

*Myth: Eating certain foods will cause kidney stones to develop.*
**Fact: Not usually.**

In general, scientists don’t think that eating any specific foods causes stones to form in people who are not already susceptible. Dr. Hostetter says that, in some people, a diet high in protein may lead to kidney stones because extra protein causes calcium to be excreted from the body, raising calcium levels in the urine. For a person without any history of kidney stones, a diet with moderate amounts of protein should be followed, he advises.

People with urinary tract infections, kidney disorders such as cystic kidney diseases, and certain rare, inherited metabolic disorders are also more likely to develop kidney stones. In some of these susceptible people, the foods they eat can have an influence on the development of their kidney stones. For example, if you are at higher risk for kidney stones, your doctor may tell you to limit or avoid foods containing higher levels of oxalate, which include chocolate, coffee, beer, dairy products, and some fruits and vegetables.

*Myth: Most kidney stones are formed from calcium, so calcium in the diet should be reduced.*
**Fact: That used to be what doctors thought, but no longer.**

For years, doctors thought a low-calcium diet was the best way to prevent kidney stones, especially in those who already had stones. But recent research has reversed that thinking. Dr. Hostetter explains that several studies have shown that low-calcium diets are not effective, and may actually be harmful, since they tend to increase the likelihood of low-bone density and osteoporosis.

Researchers now believe that more rather than less calcium is better. Recent studies have shown that a diet with normal amounts of calcium is probably best. Over a period of five years, scientists studied a group of men with recurrent kidney stones and found that those who had normal calcium levels in their diet were less likely to form new stones than men who were on a low-calcium diet.

Dr. Hostetter explains that calcium is important because it binds to oxalate and removes it from the body. He adds, “The men also restricted their intake of animal protein and salt, which may explain why they had fewer stones.”

Researchers are continuing to study the benefits of this diet. But for now, drinking that glass of milk and cutting back on the hamburgers and chips may help reduce your risk of kidney stones.

**The Urinary Tract**

![img](/assets/images/32-Shattering-Myths-about-Kidney-Stone.webp)

The urinary tract, or system, consists of the kidneys, ureters, bladder, and urethra. The kidneys are two bean-shaped organs located below the ribs toward the middle of the back. The kidneys remove extra water and wastes from the blood, converting it to urine. They also keep a stable balance of salts and other substances in the blood. The kidneys produce hormones that help build strong bones and help form red blood cells.

Narrow tubes called ureters carry urine from the kidneys to the bladder, a triangle-shaped chamber in the lower abdomen. Like a balloon, the bladder’s elastic walls stretch and expand to store urine. They flatten together when urine is emptied through the urethra to outside the body.

Source: NIDDK

*Myth: Most people with kidney stones have to undergo surgery.*
**Fact: Thankfully, this isn’t true any longer.**

The good news is that most kidney stones pass out of the body without any help. For those that require treatment, there are now a number of options that can be tried before surgery is considered. Major surgery is now usually the last resort for treating kidney stones.

For some people, drinking plenty of water may be all that’s needed to help kidney stones pass easily from the urinary tract. Sometimes pain killers and diuretics, medicines that increase the flow of urine, are given as well. Depending on the type of stone you have, your doctor may recommend a special diet to help reduce or eliminate substances in the urine that can lead to kidney stones. Your doctor may also prescribe medications that can control the amount of calcium and other chemicals in your urine that can form kidney stones. NIH researchers are currently working to develop new drugs with fewer side effects.

If your kidney stones do not pass, you may need extracorporeal shock wave lithotripsy, commonly known as lithotripsy, a technique that uses shock waves produced outside the body to hit and break up the stones so they can pass out of the body. In some cases where stones are quite large or their location will not allow for lithotripsy, you may need surgery or urethroscopy, where a small fiberoptic instrument is placed into the ureter to remove the stones. Today, even with surgery, treatment is so improved that most stones can be removed without a long recovery time.

![img](/assets/images/32-Shattering-Myths-about-Kidney-Stone-.webp)

**When Should You Call the Doctor?**

You should call your doctor when you have:

1. extreme pain in your back or side that will not go away
2. blood in your urine (urine will appear pink)
3. vomiting
4. urine that smells bad or looks cloudy
5. a burning feeling when you urinate

These may be signs of a kidney stone that needs a doctor’s care.

*Myth: Once as person has kidney stones, there’s nothing that can be done to prevent future stones.*
**Fact: Most people can take action to reduce their risk of getting more stones.**

It’s true that once you have a kidney stone, you are more likely to have others down the road, but there are some things that you can do to help prevent this from happening.

As with other chronic conditions, the best treatment for kidney stones is prevention. Drinking plenty of water throughout the day is one of the best ways to prevent future stones from forming. Your doctor may also want to run some laboratory tests and take a medical history to determine the factors that need to be changed to reduce the risk of future stones. A special diet and /or medicines may also be prescribed for you.

The most important thing you can do is to ask your doctor what you can do to prevent kidney stones from recurring.

*Myth: If a person has kidney stones, he or she is more likely to have gallstones.*
**Fact: Not even close.**

Gallstones and kidney stones are not related at all. They form in different areas of the body. Typically, those at risk for developing gallstones are a different group from those who have kidney stones. Women, Native and Mexican Americans, people over 60 and those on frequent diets are more likely to have gallstones.

Health experts say that by taking an active role in your health you may be able to reduce your risk of getting kidney stones. Dr. Hostetter concludes, “Maintaining a healthy diet with normal amounts of calcium, drinking adequate amounts of fluids, and seeing your doctor for regular check-ups are a good prescription for all of us.”

***A Word to the Wise...***

*Facts about Kidney Stones*

1. Most stones will pass out of the body without medical care.
2. See your doctor if you have symptoms that could be caused by a kidney stone.
3. Drink plenty of water throughout the day to keep more stones from forming.
4. When you pass a stone, try to catch it in a strainer to show your doctor.
5. Talk to your doctor about how to avoid kidney stones.
