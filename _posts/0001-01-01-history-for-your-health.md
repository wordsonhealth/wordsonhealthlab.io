---
layout: post 
title: "History for Your Health"
date: 2019-04-14
image: assets/images/history-for-your-health.webp
excerpt: "Collecting Family History to Prevent Disease"
tags: Family Health, Family history, Heart Disease, NHGRI
draft: false
---

Many people collect their family history for a hobby. Did you know it might help save your life, too? Your doctor can use your family’s health history to help figure out your risk of developing cancer, heart disease, asthma, diabetes, depression, and many other diseases and conditions. If you don’t know your family’s health history, now’s the time to start collecting it. A free tool called “My Family Health Portrait” can help by organizing your important health information into a printout you can take to your doctor and put in your medical record.

“The bottom line is that knowing your family history can save your life,” U.S. Surgeon General Dr. Richard H. Carmona explained in launching a campaign last year, called the Family History Initiative, to encourage all American families to learn more about their family health history. “Millions of dollars in medical research, equipment, and knowledge can’t give us the information that this simple tool can.”

That family history is important isn’t new. Every young doctor learns that it’s a valuable tool to help figure out which diseases to watch for in patients. Now that the human genome has been sequenced as a result of the Human Genome Project, we can look forward to a future where we’ll be able to identify glitches in our genes that can lead to illness. But it’ll be years before we understand what all these genes do and how they interact with our environment to cause disease. Until then, tracking illnesses from one generation of a family to the next is a powerful tool for doctors. It can help them figure out what their patients are at risk for and guide them in creating personalized disease-prevention plans.

“Family history’s not going to go out of style just because we’ve learned how to sequence the genome,” Dr. Francis S. Collins, director of NIH’s National Human Genome Research Institute, explained in a speech recently. “It’s still going to be very valuable.” NHGRI is one of many federal agencies involved in this project.

Gathering enough family history information to make useful predictions, however, isn’t always easy. Health care providers are often pressed for time and patients don’t know the details of what diseases run in their families. “My Family Health Portrait” can help you gather and record important health information before your medical appointments.

*Definitions:*

**Genome:** Full set of genes (in a person or any other living thing).

**Human Genome Project :** An international research effort to decipher the order, or “sequence,” of DNA in all the genes we carry.

**Genes:** Stretches of DNA, a substance you inherit from your parents, that define characteristics like height and eye color.

Statistics:

- Americans who believe knowing their family’s health history is important to their health: 96.3%
- Americans who have ever tried to gather and organize their family’s health history: 29.8 %

Source: U.S. Centers for Disease Control and Prevention
