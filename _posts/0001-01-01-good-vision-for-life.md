---
layout: post 
title: "Good Vision for Life"
date: 2019-06-04
image: assets/images/good-vision-for-life.webp
excerpt: "Millions Don't See as Well as They Could"
tags: Eyeglasses, Low Vision, Vision, Vision Education, Visual impairment
draft: false
---

Until now, there hasn’t been a national survey on vision since the mid-1970s. A new report has found that although 94% of Americans 12 years old and older have good vision, the remaining 6%—14 million people—are visually impaired. Of these, more than 11 million could improve their vision with eyeglasses or contact lenses.

The new study, which was designed and supported by NIH’s National Eye Institute, was part of the National Health and Nutrition Examination Survey, an ongoing survey by the National Center for Health Statistics of the Centers for Disease Control and Prevention. More than 15,000 people participated in the survey from 1999 to 2002. They were interviewed in their homes and invited to undergo a comprehensive health examination in a mobile examination center (MEC). More than 14,000 reported to a MEC, and more than 13,000 completed vision tests.

![img](/assets/images/77-Good-Vision-For-Life.webp)

The study found that teenagers, people with diabetes, Hispanics and people who are economically disadvantaged have higher rates of visual impairment and can most benefit from corrective lenses. These findings will help policy planners and health care workers focus their efforts where they can do the most good.

Regular eye exams should be part of everyone’s routine health care. Even if your vision seems fine, it’s a good idea to see an eye care professional regularly for routine vision screenings and eye examinations.

“This study found that most people who have a visual impairment could achieve good vision with proper eyeglasses or contact lenses,” Dr. Paul A. Sieving, director of vision research at NIH, said. “So, if you have trouble seeing, you should get your eyes examined as soon as possible. It may be that corrective lenses will improve your vision. But, if you do have an eye disease, the sooner it is found, the more likely it is that treatment can help preserve your vision.”

*Statistics:*

People with visual impairment that can be corrected with glasses or contact lenses:

**RACE/ETHNICITY**
Hispanic: 88.2% Black: 83.7% White: 83.6% Other: 88.6%

**AGE (YEARS)**
12-19: 93.1%  20-39: 90.0%  40-59: 92.4%  60+: 59.5%

***A Word to the Wise...***

*Check Your Vision*

There are many signs that can signal vision loss. Even with your regular glasses, do you have difficulty…

- recognizing faces of friends and relatives?
- doing things that require you to see well up close, like reading, cooking, sewing or fixing things around the house?
- picking out and matching the color of your clothes?
- doing things at work or home because lights seem dimmer than they used to?
- reading street and bus signs or the names of stores?

Vision changes like these could be early warning signs of eye disease. If you answered “yes” to any of these questions, see an eye-care professional as soon as possible. The earlier your problem is diagnosed, the better your chance of keeping your remaining vision.
