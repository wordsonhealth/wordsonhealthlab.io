---
layout: post 
title: "Winning at Losing"
date: 2019-05-26
image: assets/images/winning-at-losing.webp
excerpt: "How to Keep that Weight Off"
tags: Losing weight, NWCR, Overweight, Weight Control, Weight Loss
draft: false
---

Getting your weight under control can help you avoid many health problems. While there are many ways to successfully lose weight, most people regain it over time. Ongoing research is now giving us insights into how to keep that weight off.

Dr. Rena Wing of Brown Medical School and The Miriam Hospital spoke at NIH recently about the latest research in weight control. She explained how the National Weight Control Registry (NWCR), an effort funded in part by NIH, is helping researchers find out not only how people lose weight but how they can maintain their weight loss.

![img](/assets/images/73-Winning-at-losing.webp)

The NWCR is a different kind of study. Rather than randomly putting people into groups and testing different methods, the researchers set up a registry that anyone can join if they’ve lost at least 30 pounds and kept it off at least a year. Those who enroll fill out questionnaires about how they lost weight, how they’re trying to keep it off and other aspects of their health. There are now over 6,000 people in the study. They’ve lost an average of about 70 pounds and have maintained their weight loss for an average of 5.7 years.

Wing said that the methods people in the registry used to lose weight varied. They included cutting down on certain foods, eating less overall, liquid diets and many others.

In contrast, there isn’t a whole lot of difference in how they maintain their weight. People who successfully control their weight, Wing explained, tend to eat a low-fat diet, watch their total calories and do a lot of physical activity.

Can the lessons learned from the NWCR teach others how to control their weight? Wing just completed an NIH-funded study called STOP-Regain to find out. She and her research team enrolled 314 people who’d recently lost at least 10% of their body weight. They were randomly divided into 3 groups. One group had meetings and weight-control lessons in person. A second group got their lessons over the Internet and met in on-line chat rooms. For comparison, the third group got only quarterly newsletters.

The researchers devised a color-coded system for the in-person and Internet groups. People in the green zone (less than 2 pounds over their starting weight) got gifts. Those 3-4 pounds above their starting weight were in the yellow zone, where they were encouraged to be careful and figure out why they were gaining weight. Those 5 or more pounds above were in the red zone; they got individual counseling to help them start losing again.

The study confirmed that people who’ve recently lost weight are at high risk of regaining it. Almost 75% of those in the newsletter group had regained 5 pounds or more by the end of 18 months. The lessons and meetings, however—in person or by Internet—helped reduce the amount of weight people regained.

Those who succeeded in keeping their weight off, Wing explained, got on the scale every day and took action immediately if they saw changes they didn’t like. “People in the [in-person and Internet] groups have learned to use that information from the scale to self-regulate their eating and exercise behaviors,” she said.

There are several proven ways to lose weight. With a little change in your thinking, you can keep it off.

***A Word to the Wise...***

*Keeping Weight Off*

The National Weight Control Registry continues to give us clues about how to keep weight off:

- **Keep eating fewer calories.**
- **Exercise regularly.** Over 90% of those who’ve kept their weight off use physical activity as part of their weight control program.
- **Eat a healthy breakfast.**
- **Weigh yourself daily.** Plan for how to get back on track if your weight begins to creep up.
- **Watch the fast food.** People in the registry eat fast food less than once a week, and eat out no more than 3 times a week.
- **Don’t be a couch potato.** Almost 2 of every 3 people in the registry watch less than 10 hours of TV per week—much less than average. Try to exercise instead of eating while you watch.
- **Stay consistent.** Those who “go off their diet” on weekends, vacations or holidays have a harder time keeping weight off.
