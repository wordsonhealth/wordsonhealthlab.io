---
layout: post 
title: "Shingles Treatment and Research"
date: 2019-01-26
image: assets/images/shingles-treatment-and-research.webp
excerpt: "Is chickenpox really a problem among adults?"
tags: chickenpox, Shingles, Shingles Vaccine, varicella-zoster
draft: false
---

It is sometimes referred to as “chickenpox the second time around.” Nearly one out of every five people reading this story will some day be affected for a second time by the varicella-zoster virus — the same virus that causes chickenpox in children. Only this time, the symptoms may be worse and longer-lasting, a condition known as shingles.

So, why the replay in later life? After you recover from chickenpox, you are immune from ever getting that disease again. However, the virus hides harmlessly in your body and can reemerge to cause shingles, triggered by such things as an immune disorder or the use of drugs that lower immunity (e.g., anti-cancer drugs).

Up to one million Americans are affected by shingles each year, according to the National Institute of Allergy and Infectious Diseases (NIAID), a component of the National Institutes of Health (NIH). Generally speaking, older people are more susceptible to shingles because their immune system’s memory of the varicella virus decreases with age. More than half of all people affected are age 60 or older, though occasionally shingles may strike a young adult.

In doing its damage, the virus typically travels down a nerve and breaks out on the skin, leaving a rash or cluster of red dots that may initially be very itchy, or you may have burning, tingling, or shooting pain (early symptoms may also include fever or malaise — feeling under the weather). Shortly, these red dots develop into painful, fluid-filled blisters that normally appear only on one side of the body, or sometimes on the face. In time these blisters become crusty. After the crustiness disappears, you may be left with considerable, lingering pain where the blisters have been (called post-herpetic neuralgia) and this can last for months or even years. Additional problems may develop, including sleep disturbances, anxiety, depression, ear or eye damage, and partial, temporary facial paralysis. In a small percentage of cases, inflammation of the brain (encephalitis) could occur. Symptoms normally last three to five weeks, occasionally longer, and can be debilitating.

**Shingles: What Helps?**

Several treatments can help shorten the length of time you have shingles and/or curb the discomfort, according to Dr. Mitchell Max, a neurologist and pain specialist with the National Institute of Dental and Craniofacial Research (NIDCR), a component of the NIH. But treatment must be started early to be effective. Topical ointments, cool compresses, or even calamine lotion may provide some symptomatic relief.

If these are not enough, however, there are several things your doctor can prescribe for you:

- Acyclovir or other antiviral drugs to combat the virus.
- Opiate drugs to counter the pain.
- Steroid drugs such as prednisone to reduce skin inflammation.
- A topical anesthetic patch to reduce the pain.
- Antidepressants to reduce the chronic pain that may develop.
- In cases of extreme, non-relenting discomfort, a nerve block to provide relief for days or even weeks.

**Testing a Shingles Vaccine**

A five year clinical study of an experimental shingles vaccine is being conducted by the Department of Veterans Affairs in scientific collaboration with NIAID scientists and Merck and Company, which developed the vaccine. Researchers are recruiting generally healthy subjects, age 60 and older, with no prior history of shingles. To date, about 1,000 men and women have already participated at the NIH’s Clinical Center in Bethesda, Maryland.

Half of the people in the trial are receiving the vaccine — a more potent form of the chickenpox vaccine routinely given to children — while the remaining half are injected with a placebo. Neither the participants nor the researchers know which substance an individual is getting.

“We believe that by stimulating the immune system with a live but weakened varicella virus vaccine, shingles and its sometimes painful aftermath, post-herpetic neuralgia, may be prevented,” says NIAID’s Dr. Norberto Soto, lead investigator at the NIH Clinical Center study site.

Following a one-time visit to NIH for the shot, all participants are asked to call a toll-free study number to report whether they actually develop any symptoms of shingles. As Dr. Soto points out, ideally the shots will produce a vaccine-type effect that protects against the development of the disease. Should a study participant come down with shingles, he or she will receive free treatment at NIH.

Researchers at NIH and elsewhere are presently seeking additional participants. Chronic conditions, such as diabetes and high blood pressure that are under control, would not exclude anyone from volunteering for the trial. However, anyone who has had shingles is not eligible.

***A Word to the Wise...***

- Anyone who has had chickenpox is at risk for developing shingles later in life.
- You can’t catch shingles from someone who has it, but if you haven’t had chickenpox and are exposed to someone with shingles, you can catch chickenpox.
- Other rashes can be similar to shingles, so see your doctor if you have a rash with pain or flu-like symptoms.
- If you get shingles, the skin must be kept clean to guard against the development of a secondary bacterial infection.
- If you get shingles on your face and the cornea of your eye becomes infected, you could become temporarily or permanently blind.
- Most people do not get shingles more than once.
