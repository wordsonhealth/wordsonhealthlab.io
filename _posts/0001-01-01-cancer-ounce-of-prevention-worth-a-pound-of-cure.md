---
layout: post 
title: "Cancer Ounce of Prevention Worth a Pound of Cure"
date: 2019-03-12
image: assets/images/cancer-ounce-of-prevention-worth-a-pound-of-cure.webp
excerpt: "Ten steps you can take to reduce your risk of getting cancer"
tags: cancer, Cancer Risk, Phytochemicals, Tobacco
draft: false
---

Cancer — just hearing the word can make you anxious. Chances are you know someone with it, and worry about getting it. The statistics are frightening; one of every four deaths in the United States is due to cancer, and this year more than 1,500 people will die from it each day.

However, about a third of these deaths will be due to cigarette smoking. Another third will be related to lifestyle factors such as nutrition, physical activity, and obesity. Added together, the numbers reveal a clear message: By making simple changes in some of your daily habits, you can greatly reduce the odds of being among the grim statistics.

**What Causes Cancer?**

![img](/assets/images/36-Cancer-Ounce-of-Prevention-Worth-a-Pound-of-Cure.webp)

Changes in genes that control cell growth and death are the underlying causes of cancer. Genes, the basic physical and functional units of heredity, are specific pieces of DNA that contain information to make proteins. A misspelling (or mutation) in certain genes can increase the risk of getting some cancers. Our bodies have a repair system that normally recognizes damaged DNA and fixes it, like an automatic spell-checker. However, some DNA misspellings can go uncorrected and, as mutations accumulate, the chance of a cell losing control of its growth goes up.

Cells are the building blocks of the body, and the majority of cells continually divide and grow — which is, for example, why you need to trim your toenails and cut your hair. When you break a bone or cut yourself, new cells are made to heal the fracture or wound. However, if new cells are made when they aren’t needed, or old cells don’t die when they should, the result could be cancer. The extra cells can form a mass, called a growth or tumor. Tumors can secrete chemicals that interfere with other processes in the body and destroy normal tissues.

In benign tumors, the cells don’t spread; often the tumor is removed, and it’s not considered cancer. In malignant tumors, the abnormal cells divide and multiply like rabbits. They can invade surrounding tissues and travel to other parts of the body to form new tumors. This process, termed metastasis, usually occurs over many years.

Cancer can run in a family when an abnormal gene is passed from one generation to the next. But having a family history of cancer (for example, an aunt and sister that have been diagnosed with breast cancer) doesn’t necessarily mean you’re next in line to get it. It’s just one factor that can interact with others such as age and lifestyle habits to influence your likelihood of getting it.

While the link between genetics and cancer has received a lot of attention, it’s important to remember that only about 5-10% of cancers are inherited. It’s the interaction of genes with the environment and lifestyle that typically causes cancer to develop. You have the power to alter your risk of getting cancer by the lifestyle choices you make.

**Increasing Your Cancer Risk**

If you’d like to increase your odds of getting cancer, sit in the sun and puff on a cigarette while sipping cocktails.

Sun exposure is a major risk factor for skin cancer, which is the most commonly occurring cancer in the U.S. Sitting in the sun subjects you to ultraviolet (UV) radiation, which can cause DNA damage to the cells in your skin and lead to cancer. To reduce UV exposure, wear protective clothing (long sleeves, long pants, a wide-brimmed hat, sunglasses with UV-absorbing lenses), use sunscreen, try to avoid exposure to the midday sun (10 am to 4 pm), and don’t use tanning booths or sunlamps, which also expose you to UV radiation.

Cigarettes, chewing tobacco, snuff and cigars contain dozens of chemicals called carcinogens that can cause genetic damage to cells and interfere with normal cell development and growth, leading to cancer. To avoid this risk, avoid tobacco products, including exposure to second hand smoke.

Heavy alcohol consumption has also been linked to a number of cancers. Alcohol can impair a cell’s ability to repair DNA, and can enhance the carcinogenic effects of other chemicals. Alcoholism may suppress the immune system, which attacks and eliminates abnormal cells. If you drink, limit your intake to no more than two drinks per day for men and one drink per day for women. A drink is defined as 12 ounces of beer, 5 ounces of wine, or 1.5 ounces of 80 proof distilled spirits.

**Decreasing Your Cancer Risk**

Exciting new findings are shedding light on many lifestyle habits you can adopt to help protect against cancer. Researchers are discovering that being physically active can help you reduce the risk of getting a number of different cancers. Regular exercise helps control levels of various chemicals such as growth factors and hormones that encourage cancer. It can boost the immune system, and it can help you maintain body weight, which is important since obesity is an important risk factor for cancer.

“Maintaining a healthy weight and avoiding weight gain during adult life, a very common occurrence in the US, may reduce risk for a number of cancers,” according to Dr. Rachel Ballard-Barbash of NIH’s National Cancer Institute (NCI). “Research suggests that avoiding excess weight may reduce cancer mortality in the population by 14% in men and 20% in women, and have an even greater benefit in reducing the initial occurrence of cancer.”

General recommendations for overall health are to strive for at least thirty minutes of moderate activity most days of the week. Emerging evidence suggests that up to an hour a day of physical activity may be needed to control obesity and some types of cancer. The activities should be vigorous enough to raise your heart rate, and may cause sweating. Exercises such as brisk walking, swimming, yoga, bicycling or dancing are all good choices. Routine activities such as taking the stairs, pushing a stroller, washing a car, and cleaning windows are beneficial as well.

In addition to exercise, one of the best things you can do to reduce your cancer risk is eat plenty of fruits and vegetables. They contain phytochemicals with names like lycopenes, flavanoids, and zeaxanthins, that may decrease cancer risk by protecting DNA from being damaged. The phytochemicals come conveniently packaged alongside vitamins and minerals to provide a one-stop buffet of cancer-fighters. Since many of the chemicals work best in combination, they offer an advantage you can’t get in many supplements. “In fact,” Dr. Ballard-Barbash says, “recent reviews have found no evidence of any benefit for cancer or heart disease prevention for several types of vitamin supplements.”

Eating lots of fruits and vegetables not only decreases your risk of some cancers, but also heart disease, diabetes, and hypertension. View the vegetable crisper in your refrigerator as a medicine cabinet full of disease fighting, age-defying nutrients.

**It’s Not That Hard**

Unfortunately, many of us don’t follow our mother’s advice to eat our veggies. This holds especially true for men, who typically eat only about four servings of fruits and vegetables a day. While the old ‘strive for five’ adage still applies, it’s now recommended that women try to aim for seven servings and teenage boys and men shoot for nine servings of fruits and vegetables.

![img](/assets/images/36Cancer-Ounce-of-Prevention-Worth-a-Pound-of-Cure.webp)

**Exactly what is a serving?** It’s smaller than most people think. For example: A small glass of 100% fruit or vegetable juice (3/4 cup or 6 oz), a medium-size piece of fruit (an orange, small banana, medium-size apple), one cup of raw salad greens, 1/2 cup of cooked vegetables (about the size of a baseball), 1/2 cup of cut-up fruit or vegetables, 1/4 cup of dried fruit (about the size of a golf ball), 1/2 cup of cooked beans or peas.

These numbers can seem daunting, but it’s actually easy to accomplish (see picture). What you may consider a ‘portion’ most likely counts for at least two servings, according to Valerie Green, nutrition program manager for the NCI’s 5 A Day for Better Health program. She suggests stocking up on frozen vegetables and then simply adding them to dishes such as pastas and soups. She further advises that frozen, as well as canned, fruits and vegetables won’t go bad, and can actually be as nutritious as fresh foods.

The thought of getting cancer is scary. While you can’t influence your genetics by choosing your parents, you can greatly influence your risk of developing cancer by some of the simple lifestyle choices you make. The bottom line is that cancer is not inevitable for most of us.

***A Word to the Wise...***

*Ten steps you can take to reduce your risk of getting cancer:*

- Don’t use tobacco products (cigarettes, cigars, pipes, chew)
- Don’t use sunlamps or tanning beds
- Don’t consume excessive alcohol
- Do limit exposure to second-hand smoke
- Do protect yourself from the sun
- Do eat 5-9 servings of fruits and vegetables a day
- Do choose foods with less fat and more fiber
- Do watch your weight
- Do exercise regularly
- Do visit your doctor for regular checkups and screenings
