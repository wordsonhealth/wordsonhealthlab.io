---
layout: post 
title: "Teen Brains Still Under Construction"
date: 2019-04-28
image: assets/images/teen-brains-still-under-construction.webp
excerpt: "Parents Play Important Role"
tags: Adolescent, NIMH, Teen Brain, Teen emotions
draft: false
---

Contrary to what most parents have thought at least once, “teens really do have brains,” quipped Dr. Jay Giedd, an NIH research scientist, in a recent lecture on the “Teen Brain under Construction.” In fact, researchers have not only confirmed that teens have brains; they are now beginning to understand the biological basis for their sometimes peculiar behavior.

![img](/assets/images/59-Teen-Brains-Still-Under-Construction.webp)

Giedd, who studies brain development at NIH’s National Institute of Mental Health, explained that scientists have only recently learned more about the path of brain growth. One important finding, he said, showed that the frontal cortex area—which governs judgment, decision-making and impulse control—doesn’t fully mature until around age 25.

“That really threw us,” Giedd said. “We used to joke about having to be 25 to rent a car, but tons of industry data show that 24-year-olds are costing insurance companies more than 44-year-olds are.”

But why is that? “It must be behavior and impulse control,” he said. “Whatever these changes are, the top bad things that happen to teens involve emotion and behavior.”

Medically, Giedd said, in terms of cancer, heart disease and other serious illnesses, people in their teens and early 20s are incredibly healthy. But with accidents as the leading cause of death in adolescents and suicide following close behind, “this isn’t a great time emotionally and psychologically. We call it the great paradox of adolescence: right at the time you should be on the top of your game, you’re not.”

The next step in Giedd’s research, he said, is to learn more about what influences brain growth, for good or bad. “Ultimately, we want to use these findings to treat illness and enhance development.”

Until then, parents need to know that the science shows they really can influence their children’s brain development.

“From imaging studies,” Giedd said, “one thing that seems especially intriguing is this notion of modeling . . . that the brain is pretty adept at learning by example. As parents, we teach a lot when we don’t even know we’re teaching, just by showing how we treat our spouses, how we treat other people, what we talk about in the car on the way home. . . . Things that a parent says in the car can stick with them for years. They’re listening,” he said, “even though it may appear they’re not.”

So, what can we do to change our kids? “Well, start with yourself in terms of what you show by example,” Giedd advised.

***A Word to the Wise...***

*For Guiding Teens:*

- Until age 25, the part of our brain that governs judgment, decision-making and impulse control is still under construction, so a parent’s job as “foreman” usually isn’t over when teens turn 18.
- Teen emotions and behavior can be unpredictable. Watch for emotional fall-out, wear a hard hat and choose battles wisely.
- Be a good model; teach young ones and teens respectfulness by showing respect—to them, to others in the family and in the community, and to yourself.
- Assume they’re always listening, observing and learning, even when it seems like they’re not.
