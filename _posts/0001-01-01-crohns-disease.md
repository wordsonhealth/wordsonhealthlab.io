---
layout: post 
title: "Crohns Disease"
date: 2019-02-18
image: assets/images/crohns-disease.webp
excerpt: "Symptoms, Diet, Causes, Treatment and Life Expectancy"
tags: Crohn's Disease, diet, Life Expectancy, Treatment
draft: false
---

Crohn’s disease didn’t slow Jane down at first. Prescribed drugs subdued her symptoms. A bright and energetic teenager, Jane excelled in high school and was accepted to Cornell. But during her college career the medication stopped working. Her pain grew worse. Her weight dropped to 89 pounds. Lacking the energy to study, she contemplated quitting school.

Jane’s story is typical of many people with Crohn’s disease, an autoimmune disorder of the bowels that affects about 500,000 Americans. “This condition frequently strikes people in their 20s who are just beginning their lives and careers,” says Dr. Warren Strober, an immunologist at NIH’s National Institute of Allergy and Infectious Diseases (NIAID).

Dr. Strober and his colleagues Drs. Ivan Fuss and Peter Mannon are working on a treatment for Crohn’s disease. A partnership with the company Genetics Institute is moving this treatment from the lab to the bedside: It is being tested in patients at the NIH Clinical Center and at other medical centers nationwide. The NIAID researchers hope their efforts may help people reclaim their lives from Crohn’s disease, especially patients who don’t respond to available drugs.

**Conventional Drugs Still Lacking**

In Crohn’s disease, the immune system appears to attack the digestive tract as if it were foreign tissue, causing abdominal pain, cramping, diarrhea, and rectal bleeding. The beleaguered digestive tract doesn’t absorb food properly, and those who suffer from the disease can lose weight and lack energy. In the worst cases, the immune system’s overly aggressive T cells — white blood cells that coordinate the body’s immune responses — bore holes in the digestive tract, necessitating surgery.

Although the roots of Crohn’s disease are likely genetic and environmental, the exact cause is unknown, so conventional drugs aim to curb symptoms rather than fix the underlying problem. Experiences with patients like Jane motivated the NIAID researchers to search for a better treatment. When Jane sought their help, Drs. Strober and Fuss didn’t have many options at their disposal. None of the usual steroids or immunosuppressive drugs worked.

The doctors finally tried Remicade, a drug used only in the most difficult cases. Remicade reduced Jane’s symptoms enough for the other drugs to handle them. Her pain lessened. She regained her lost energy and weight. She finished college and went on to graduate school. Despite her ordeal, Jane was lucky. Remicade works in only about two-thirds of patients.

“For some patients, there is no satisfactory way to deal with the condition,” says Dr. Strober. “They simply have to suffer with it.”

“Clearly, there is room for something better,” says Dr. Fuss.

**New Therapy on the Horizon**

That “something better” may be anti-interleukin-12 therapy, an approach developed by Drs. Strober, Fuss, and Mannon. Based upon decades of basic research into the immunological characteristics of Crohn’s disease, this treatment comes closer to eliminating the cause, rather than suppressing symptoms.

Studying mouse models of the disease, Drs. Strober and Fuss turned up a key piece of information: In the animal’s digestive tracts, the immune system overproduced a cytokine, or “messenger molecule,” called interleukin-12 (IL-12). Establishing a connection between IL-12 and Crohn’s disease was important because IL-12 is known to help stimulate inactive T cells to become an aggressive form of T cells known as Th1 cells. In Crohn’s disease, these Th1 cells damage the digestive tract. Drs. Strober and Fuss went on to show elevated IL-12 levels in people with Crohn’s disease.

The researchers reasoned that blocking the IL-12 signal might reduce the number of Th1 cells and prevent the harmful effects of Crohn’s disease. To that end, they experimented with a synthetic antibody (called anti-IL-12) that binds to and inactivates IL-12. The researchers treated mice with the antibody and found that not only did it prevent the disease from occurring, it also healed active disease.

“It completely healed the mice within days,” says Dr. Strober. “It was an amazing breakthrough.”

“We immediately recognized its therapeutic potential in humans,” adds Dr. Fuss.

The antibody didn’t just prevent inactive T cells from converting to Th1; it also eliminated the active Th1 cells already wreaking havoc in the digestive tract. The researchers discovered that once a Th1 cell is activated by IL-12, it needs a continuous supply of that chemical to survive. With natural IL-12 blocked, these cells die off. “This extra benefit of anti-IL-12 therapy, which strikes closer to the root of the disease, is what makes it especially promising,” says Dr. Fuss.

**From The Lab to the Clinic**

The National Institutes of Health (NIH) licensed a patent for anti-IL-12 therapy to the pharmaceutical company Genetics Institute. NIAID researchers have teamed with the company to investigate whether anti-IL-12 therapy will work in humans. In late 2000, the company began a Phase I/II safety and efficacy trial with the goal of enrolling 80 patients at 12 centers nationwide, including the NIH Clinical Center. The trial will measure the effect of two doses of anti-IL-12 on patients with severe-to-moderate Crohn’s disease.

The biggest challenge will be observing whether or not anti-IL-12 affects individuals differently, says Dr. Mannon, who is conducting the NIH trial. Laboratory mice are biologically and genetically similar to one another, but the people enrolled in the clinical trial will be much more diverse. “In animal models, the therapy looked extremely effective, almost like a magic bullet,” he says. “But in human beings, the results may vary. The challenge will be to identify any biological factors that predict a person’s response.”

***A Word to the Wise...***

*Living with and watching out for complications of Crohn’s disease*

The classic symptoms of Crohn’s disease are abdominal pain, diarrhea, and vomiting, but more subtle symptoms may appear years earlier, say Drs. Warren Strober and Ivan Fuss, scientists at NIH’s National Institute of Allergy and Infectious Diseases. If you experience prolonged and unexplained fever, joint pain, eye pain, mouth ulcers, or skin rashes, you may want to see a gastroenterologist who can check for Crohn’s.

The NIAID experts have suggestions for ways to help people with Crohn’s disease manage the day-to-day aspects of it. Furthermore, they point out the risk of certain complications associated with the disease or its treatment that are important to catch and treat early.

**Complications:**

- Intestinal cancer. People with Crohn’s disease have a higher risk of intestinal cancer, says Dr. Strober. That’s why it is important for regular cancer screening visits to a gastroenterologist. “Don’t neglect this,” Dr. Strober says. “This form of cancer is easily treatable if found early.”
- Bone loss. Prednisone, a steroid drug used to treat Crohn’s, can cause bones to lose calcium. In addition, the joint inflammation and lactose intolerance sometimes caused by Crohn’s can result in further loss of calcium from bones. To avoid a broken hip or vertebrae injury down the road, the NIAID doctors recommend going to an endocrinologist for a bone density scan. If necessary, vitamin D and calcium therapy can be started to help prevent further bone loss.
- Eye problems. Crohn’s disease can sometimes cause uveitis, an inflammation of the uvea, or the lining inside the eyeball. Symptoms include bloodshot eyes, pain, and blurred vision. In addition, taking prednisone can sometimes lead to cataracts or glaucoma. Drs. Strober and Fuss recommend a yearly eye exam by an ophthalmologist to catch and fix any potential problems early.
- Other steroid side effects. Steroids can cause weight gain as well as the accumulation of fat in places it usually doesn’t, like your cheeks and back. “However, these are not lifelong effects,” says Dr. Fuss. “As your disease comes under control, the amount of steroids you need to take will decrease and these effects will reverse and almost always go away.”

**Daily Life:**

- Diet. As far as diet, there’s no need to restrict what you eat as long as the foods don’t make your symptoms worse. “Eat whatever you can handle,” says Dr. Strober. He notes, however, that fatty foods, spicy foods, and milk products do worsen symptoms in many patients.
- Exercise. If you have Crohn’s, you should exercise regularly at mild to moderate levels, according to Dr. Fuss. Long-term use of steroids can weaken muscles and ligaments. Exercise can help correct this, reducing the risk of pulled muscles, sprains, or other injuries.
- Stress. Stress does not cause Crohn’s Disease, both Drs. Fuss and Strober emphasize. However, stress may worsen symptoms, or at least your perception of them. “Patients learn to live with a fair amount of discomfort, but stress can lower your tolerance,” says Dr. Strober. Still, stress is a part of everyday life, and the doctors advise facing challenges and tackling difficult situations rather than letting your condition get the better of you. Your physician can recommend stress management classes or social services to help deal with stress.
