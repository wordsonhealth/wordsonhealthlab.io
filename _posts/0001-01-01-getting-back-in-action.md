---
layout: post 
title: "Getting Back in Action"
date: 2019-05-28
image: assets/images/getting-back-in-action.webp
excerpt: "Participation is Key to Recovery"
tags: Arthritis, Heart Attack, NICHD, Recovery, Serious illness, Stroke
draft: false
---

Stroke. Heart attack. Arthritis. Serious illness or injury often leads to a long, slow return to health. Thoughts of making a comeback take a backseat to simple survival. Recovery can also mean learning how to live with a disability. Researchers supported by NIH are studying the many roads people take to recovery. No matter where you’ve been, their findings may help you find your way back.

Whatever the condition, one of the most important keys to recovery is participation, says Karen Lohmann Siegel, a physical therapist at NIH’s Clinical Center. “Many medical procedures and treatments are done to a patient by a health care provider, and the patient’s role in the treatment is small,” she explains. “That is not the case in rehabilitation. In rehab, the patients do a lot of the work themselves to get better.”

![img](/assets/images/74-Getting-Back-in-Action.webp)

Each year about 700,000 people in the U.S. have a stroke and 1.1 million suffer a heart attack. The moment you survive the critical stage of a cardiovascular illness or injury is when you start the recovery phase, says Dr. Michael Weinrich of NIH’s National Institute of Child Health and Human Development (NICHD).

“We have good data that people who engage in moderate activity improve their cardiovascular system,” Weinrich says. “That’s particularly true for people who have a stroke.”

Results from several studies stress the role of physical activity in rehabilitation. The good news is that it’s possible to help people get enough exercise to improve their chances of recovery. Researchers supported by NIH’s National Institute of Nursing Research, for example, have found that education and encouragement promoted exercise in a rehab program for people recovering from a heart attack or bypass surgery.

Physical activity speeds recovery in other areas, too. NICHD and NIH’s National Institute of Neurological Disorders and Stroke are funding a 5-year clinical study on improving the motor skills of stroke survivors. Called EXCITE (Extremity Constraint Induced Therapy Evaluation), the study is looking at how well patients recover function in an arm disabled by stroke. For a period of time, the patient avoids using the healthy arm to perform daily tasks. Instead, the arm weakened by stroke does all the work. Results so far have shown that the forced activity strengthens the weak arm and helps the stroke survivor recover motor skills faster.

Joint replacement surgery is another condition for which physical activity speeds recovery. As people live longer and want to stay active, operations to replace worn-out knees and hips with artificial joints have become more common. Recovering from this type of surgery means learning to walk again. Not long after your surgery, doctors and nurses want you right back on your feet. A physical therapist will plan exercises to help you retrain your muscles and adjust to using the artificial joint. You’ll probably feel some pain, but the movement will help you recover more quickly.

People who’ve had injury to their nervous system after disorders like stroke want to regain more than just basic movements. They want to get back to moving the way they did before they became sick. Scientists are now testing the idea that damaged muscles may recover better and faster with help from technology. NIH-funded researchers at Arizona State University have recently designed a lightweight robotic device called RUPERT (for Robotic Upper Extremity Repetitive Therapy) that helps stroke survivors regain some basic activity in their arms.

Dr. Jiping He, ASU professor of bioengineering and RUPERT research team leader, explains that the device has an advanced control system that detects the wearer’s intent to move. It can help them do the things we often take for granted, like reaching for a cup, eating or moving something from one place to another.

RUPERT is being developed by Kinetic Muscles, Inc., the same company that helped ASU produce a similar device for recovering hand function called the Hand Mentor. Power for these devices is supplied by “pneumatic muscles,” small instruments that use compressed air to mimic muscle movements.

Research teams funded by NICHD and NIH’s National Institute of Biomedical Imaging and Bioengineering (NIBIB) began testing the Hand Mentor last year in stroke survivors. Dr. He says RUPERT should be ready in a year or so to start testing in patients. Preliminary work is also under way for leg rehab devices.

Another interesting development in rehabilitation research is an innovative movement analysis lab at NIH’s Clinical Center. Specialized video cameras record the movements of reflective markers attached to the patient’s feet, legs and hips. Sensors go on the patient’s skin to record leg muscle activity. Special plates built into the lab’s floor measure the forces exerted by the patient’s body on the floor. Once all the signals are collected by computer, the rehab team uses that information to get a very detailed idea of how the person’s joints and muscles move. They can then design a tailored recovery plan for each patient’s unique situation.

“People are complex organisms—we adapt,” Weinrich says. “We still don’t know what optimal rehabilitation is, but we are learning.

”Surviving the initial phase of illness was your first success. Playing a major role in your recovery plan and adding some physical activity to your daily routine will help speed you along the way.

*Definitions:*

**Cardiovascular:**
The system of heart and vessels that circulates blood throughout the body.

**Clinical study:**
A research study with human volunteers that aims to answer specific health questions.

**Motor Skills:**
Skills that require muscle movements. They involve the brain, skeleton, joints and nervous system.

**Nervous System:**
System that receives and interprets sensations (like touches, sounds, and sights), and coordinates activities (like muscle movements) throughout the body.

**Rehabilitation:**
Exercise and other therapy designed to help you return to your normal activities after an illness or injury.

***A Word to the Wise...***

*You’re the Key Player in Your Recovery*

Rehabilitation is a team effort with one goal: getting you back to where you want to be. NIH Clinical Center Physical Therapist Karen Lohmann Siegel offers the following tips on being a good team player:

- **Set meaningful goals:** Both long-term and short-term goals are important. “Short-term goals are things you should reasonably be able to do in 1 to 2 weeks,” Siegel explains. They are the building blocks to get you to your ultimate long-term goals, and may be revised many times over the course of rehab.
- **Communicate with your rehab team:** Often there’s more than one way to achieve results. If one exercise is not working for you, let your team know. They may be able to recommend another approach.
- **Know your value to the team:** Be an active participant in your own recovery. In rehab, it is all about you!
