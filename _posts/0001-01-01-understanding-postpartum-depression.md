---
layout: post 
title: "Understanding Postpartum Depression"
date: 2019-05-10
image: assets/images/understanding-postpartum-depression.webp
excerpt: "Common but Treatable"
tags: Antidepressant, Childbirth, Depression, Postpartum Depression
draft: false
---

Many women have mood swings right after childbirth. They can be happy one minute and sad the next. Even when their baby is asleep, they may have difficulty sleeping, eating and feel a little depressed. If these symptoms begin a few days after delivery and go away after 7-10 days without treatment, they are in all likelihood the “baby blues,” a short-lasting condition that 50-80% of women feel and that usually doesn’t require medical intervention. Clinically diagnosed postpartum depression, however, is another story.

Actress Brooke Shields recently gave a compellingly candid public account of her experience with postpartum depression following the birth of her daughter. When actor Tom Cruise publicly criticized her course of treatment, psychiatrists seized the opportunity to offset the stigma and misconceptions associated with this illness.

Postpartum depression affects 10-15% of women any time from a month to a year after childbirth. Women with postpartum depression may feel restless, anxious, sad or depressed. They may have feelings of guilt, decreased energy and motivation, and a sense of worthlessness. They may also have sleep difficulties and undergo unexplained weight loss or gain. Some mothers may worry about hurting themselves or their baby. In extremely rare cases—less than 1% of new mothers—women may develop something called postpartum psychosis. It usually occurs within the first few weeks after delivery. Symptoms may include refusing to eat, frantic energy, sleep disturbance, paranoia and irrational thoughts. Women with postpartum psychosis usually need to be hospitalized.

![img](/assets/images/65-Understanding-Postpartum-Depression.webp)

Researchers aren’t sure what causes postpartum depression, but think that the dramatic shifts in hormone levels during pregnancy and immediately afterward may result in chemical changes in the brain leading to the condition. Childbirth is also a major life change that can create ongoing stress and contribute to depression. The new mom’s responsibility for the baby, the household and her work duties upon returning after maternity leave may affect her risk of getting postpartum depression.

The good news is that, like diabetes or heart disease, postpartum depression is an illness that can be successfully treated with medicine and therapy. Women treated with antidepressant medicines and talk therapy usually show marked improvement. Depending on the type of medication they’re using, they may be able to continue breast feeding.

Researchers are making progress in understanding how changing hormone levels and other factors affect the brain after childbirth. They hope to develop better medications for treating postpartum depression by targeting the chemical pathways they’re uncovering.

If you suspect that someone you know is suffering from postpartum depression, it’s important to show understanding and support during this stressful time to help the new mom avoid the stigma, shame and isolation often associated with postpartum depression.

Dr. Cathy Roca, chief of the Women’s Mental Health Program at NIH’s National Institute of Mental Health, stresses, “Postpartum depression is common and treatable. Having postpartum depression does not mean that you’re not a good mom.” Ask your health care provider to recommend a therapist or support groups that can help.

*Statistics:*

**Facts About Depression**

- Major depression is the leading cause of disability in the U.S. and worldwide.
- Nearly twice as many women (12%) as men (7%) are affected by a depressive disorder each year.
- Women with postpartum depression have a 50% chance of getting it after subsequent pregnancies and may be at an increased risk for future depression not associated with pregnancy.
- Women with a family history of depression and bipolar disorder are more likely to get postpartum depression.
