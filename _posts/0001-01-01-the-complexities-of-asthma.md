---
layout: post 
title: "The Complexities of Asthma"
date: 2019-06-06
image: assets/images/the-complexities-of-asthma.webp
excerpt: "Genes, Environmental Factors Both Involved"
tags: Asthma, chest tightness, coughing, NIEHS, trouble breathing, wheezing
draft: false
---

You probably know of at least one child with severe asthma. It’s just more common these days. Once considered a minor ailment affecting only a few, asthma is now the most common chronic disorder in childhood, affecting an estimated 6.2 million children under the age of 18. The trend has been particularly disturbing among inner-city children. Research is now uncovering the complex mix of genes and environmental factors that cause the disease.

Asthma is a disease that’s caused by swelling and inflammation of your airways. When your airways become narrower, less air can get through to your lungs. That’s what causes the wheezing (a whistling sound when you breathe), coughing, chest tightness and trouble breathing that we know as the symptoms of asthma.

![img](/assets/images/78-The-Complexities-of-Asthma.webp)

Despite improvements in diagnosis and management, the prevalence of asthma has increased over the past 15 years. In the U.S. alone, 20.5 million people—6.7% of adults and 8.5% of children—have been diagnosed with asthma.

Research shows that asthma runs in families. When one or both parents have asthma, a child is more likely to develop it, too. This is known as genetic susceptibility.

However, environmental factors also contribute. Allergies and asthma are closely intertwined, and allergy-causing substances called allergens in the environment are a major cause of asthma. Studies in the last 10 years have shows that indoor allergens from house dust mites, cockroaches, dogs, cats, rodents, molds and fungi are among the most important asthma triggers.

“Home carpeting can be a problem because children can develop asthmatic reactions to the house dust mites that live in the carpet,” says Dr. David Schwartz, director of the NIH’s National Institute of Environmental Health Sciences (NIEHS).

From 1998 to 2002, NIEHS scientists, along with researchers from the Department of Housing and Urban Development, conducted an extensive survey to assess how widespread these indoor allergens are in American homes. The results of this survey, the National Survey of Lead and Allergens in Housing, showed that more than 46% of the homes surveyed had levels of dust mite allergens high enough to produce allergic reactions, and nearly a quarter had levels high enough to trigger asthma symptoms in genetically susceptible people.

The survey also showed that nearly two-thirds of U.S. homes have detectable levels of cockroach allergens, with higher levels in high-rise apartments, urban settings, older homes and homes of low-income households. About 10% had cockroach allergen levels above the threshold for triggering asthma symptoms.

“One of the most surprising findings from the national survey was that 100% of homes had detectable levels of dog and cat allergen, even though dogs were present in only 32% of the surveyed homes, and cat ownership was reported in only 24%,” says Dr. Darryl Zeldin, a scientist with NIEHS and senior author of the study.

The researchers then collaborated with scientists from Harvard University and the University of Washington to evaluate some practical methods for reducing house dust mite allergens in the bedrooms of low-income Seattle homes.

The research showed that some simple steps—washing the bedding in hot water, putting allergen-impermeable covers on the pillows, box springs and mattresses, and vacuuming and steam-cleaning the carpets and upholstered furniture—can significantly reduce dust mite allergen levels,” Zeldin says.

They also conducted a 6-month trial to test a method for reducing cockroach allergen levels in low-income, urban homes. It included cockroach extermination, professional cleaning and in-home lessons on asthma management. At the end of 6 months, cockroach allergen levels were reduced by 84% on bedroom floors and in beds, and by 96% on kitchen floors. A follow-up study showed that these reductions could be maintained with continued cockroach control.

NIEHS also partnered with NIH’s National Institute of Allergy and Infectious Diseases to develop a cost-effective program aimed at reducing asthma severity among predominantly African-American and Hispanic children in low socioeconomic areas. The study, called the Inner-City Asthma Study, showed that a program to reduce allergens in the home can result in fewer asthma symptoms in children.

While much asthma research has focused on indoor allergens, scientists are realizing that outdoor pollutants also play a major role. NIEHS-funded researchers at the University of Southern California’s Keck School of Medicine studied air pollution levels in 10 Southern California cities and found that the closer children live to a freeway, the greater their chances of being diagnosed with asthma. They saw a relationship between higher levels of asthma and certain pollutants that come from the burning of fossil fuels (like the exhaust of a car or truck), as well as from emissions from industrial plants.

Armed with a better understanding of asthma’s environmental triggers, NIEHS researchers want to learn more about which genes make people susceptible to developing asthma when they encounter these triggers. Using a technique called gene expression profiling, researchers will screen thousands of genes at once to identify which genes are activated when a patient’s airways become obstructed or inflamed.

“Once we learn more about how these genes differ from one person to another, we may be able to explain why some people develop asthma while others remain unaffected,” Schwartz says.

A better understanding of how asthma develops will help researchers design more effective strategies for prevention and treatment. In the meantime, talk to your doctor or use the resources listed here to learn more about how you can prevent asthma symptoms.

*Definitions:*

**Allergen**:
A substance that can cause allergies.

**Asthma**:
A chronic disease that causes your airways, the tubes that carry air in and out of your lungs, to get narrower so that less air flows through. Symptoms include wheezing (a whistling sound when you breathe), coughing, chest tightness and trouble breathing.

***A Word to the Wise...***

*Know the Signs and Symptoms of Asthma*

Not all people have these asthma symptoms, and symptoms may vary from one asthma attack to another. Common symptoms include:

- Coughing. Coughing from asthma is often worse at night or early in the morning, making it hard to sleep.
- Wheezing, a whistling or squeaky sound when you breathe.
- Chest tightness. This can feel like something is squeezing or sitting on your chest.
- Shortness of breath. Some people say they can’t catch their breath, or they feel breathless or out of breath. You may feel like you can’t get enough air in or out of your lungs.
- Fast or noisy breathing.

With proper treatment most people with asthma can expect to have few or no symptoms. See your doctor if you suspect you might have asthma.

Source: National Heart, Lung, and Blood Institute, NIH
