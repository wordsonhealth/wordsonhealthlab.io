---
layout: post 
title: "Keys to Successful Aging"
date: 2019-02-26
image: assets/images/keys-to-successful-aging.webp
excerpt: "Good Habits and Positive Attitudes"
tags: Aging, Elderly, NIA, Older People, physical activity
draft: false
---

You can change how you think, feel and act when you are older simply by changing how you think, feel and act now, geriatrics experts say. Staying healthy, fit and active are the keys to successful aging.

Dr. Judy Salerno, deputy director of NIH’s National Institute on Aging (NIA), says, “When I entered the aging field many years ago, we didn’t talk about disease prevention. We simply characterized normal aging. Now we are seeing that people can age successfully in good health well into old age. Disease and disability are not inevitable consequences of aging.”

That is good news not only for individuals but also for communities and society in general. In the last two decades, census figures have shown that the world’s vital statistics are changing. In the U.S., the proportion of older adults went from roughly four percent in 1900 to 13 percent today, and will increase to more than 20 percent by 2030.

In the recently ended Diabetes Prevention Program study, sponsored by NIH’s National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK) and NIA with additional support by several other institutions, scientists found that moderate diet and exercise successfully delayed and possibly prevented participants from developing type 2 diabetes.

About 16 million people in the United States have diabetes, which is the main cause of kidney failure, limb amputations and new-onset blindness in adults, as well as a major cause of heart disease and stroke. Type 2 diabetes accounts for up to 95 percent of all diabetes cases.

“The most interesting aspect of the study in terms of aging was that diet and exercise worked better in those participants ages 60 and older,” Dr. Salerno said. She noted that lifestyle changes prevented the disease at better rates than did the oral diabetes drug metformin, which was also tested during the program.

Even before the diabetes prevention trial results, NIA researchers had realized the health benefits of regular physical activity by older people. NIA has a popular guide entitled Exercise: A Guide from the National Institute on Aging, which includes a video and a booklet that describe easy physical activities that can help older people stay fit and stave off certain disorders, particularly those illnesses associated with muscle strength and bone health.

Regular exercise and physical activity are important to the health and abilities of older people, according to research studies conducted by NIA. “In fact,” says the introduction to their exercise booklet, “studies suggest that not exercising is risky behavior.”

Dr. Pamela Peeke, a professor at the University of Maryland School of Medicine, cited research data at a “Successful Aging” seminar on the NIH campus showing that regular exercise – by people of any age – may improve daily functioning, strength, flexibility and endurance.

“Studies sponsored by NIA and other gerontology centers have found that people who have been normally sedentary and have lost a tremendous amount of muscle mass can recoup that very nicely and increase power and strength as well,” she said, “even through their eighties and into their nineties.”

![img](/assets/images/29-Keys-to-Successful-Aging.webp)

**Role of the Mind**

NIA researchers are also finding that the mind plays an important part in a person’s ability to age well and feel content at an elderly age.

“Many studies have shown that staying engaged and maintaining good social connections help older people retain cognitive function,” says Dr. Salerno, citing research from ACTIVE (Advanced Cognitive Training for Independent and Vital Elders), a clinical study cofunded by NIA and another NIH component, the National Institute of Nursing Research. Early results of ACTIVE look promising, and additional long-term followup studies are already underway.

University of Wisconsin professor Dr. Gloria Sarto also stressed at the “Successful Aging” seminar that the mind plays an important part in a person’s ability to age well. She listed several key attitude factors: having self-esteem, exerting control or autonomy, developing quality relationships with other people and “seeing life as meaningful.”

“One does not grow older without a certain amount of resilience,” she said. “Find something positive in the face of adversity. See an ordeal as an opportunity to learn.”

**Changing Perceptions**

“Our basic message to older people is that it’s never too late,” Dr. Salerno says. “How your grandmother aged is not necessarily how you will age. Genes are not necessarily your destiny. Genes are only part of the story.”

Dr. Salerno says perceptions about old age have undergone an almost total change in the last decade or so. What’s more, she said, insights on getting older are no longer useful only for people of a certain age.

“Habits that are established young in life may help determine how healthy you will be in old age,” she says. “Maintaining good habits and positive attitudes is what we should all be aiming for. Once a couch potato does not mean always a couch potato. It’s never too late to establish good habits, but it’s never too early either.”

***A Word to the Wise...***

*Want to Age Well? NIA Says, Start Living Well*

The secret to aging gracefully is to establish a healthy lifestyle early and stick to it as you grow older, according to Dr. Judy Salerno, deputy director of NIH’s National Institute on Aging (NIA). Recent results of research studies supported by NIA led the institute to offer the following ten Tips for Healthy Aging:

1. Eat a balanced diet.
2. Exercise regularly.
3. Get regular check-ups.
4. Don’t smoke. It’s never too late to quit.
5. Practice safety habits at home to prevent falls and fractures. Always wear your seatbelt when traveling by car.
6. Maintain contacts with family and friends, and stay active through work, recreation, and community.
7. Avoid overexposure to the sun and the cold.
8. If you drink, moderation is the key. When you drink, let someone else drive.
9. Keep personal and financial records in order to simplify budgeting and investing. Plan for long-term housing and financial needs.
10. Keep a positive attitude toward life. Do things that make you happy.
