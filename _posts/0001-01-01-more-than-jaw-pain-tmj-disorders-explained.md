---
layout: post 
title: "More Than Jaw Pain Tmj Disorders Explained"
date: 2022-01-03T00:45:56+01:00
image: assets/images/more-than-jaw-pain-tmj-disorders-explained.webp
excerpt: Find simple ways to tackle jaw pain and dysfunction.
draft: false
---

Your jaw works hard every day so you can laugh, talk, smile, and eat. When it’s working properly, you may not give it much thought. But if your jaw starts to hurt, it can take the joy out of simple, everyday things.

The jaw joint is one of the most complex joints in the human body. For most people, it moves effortlessly up and down, side to side, and in and out, transitioning from one movement to the next seamlessly. But, more than 10 million people in the U.S. live with jaw pain and dysfunction.

Doctors call these conditions temporo-mandibular disorders. They’re more commonly called temporomandibular joint (TMJ) disorders.

“Temporomandibular disorders—and how people respond to them—vary widely,” explains Dr. Dena Fischer, a dental health expert at NIH. “For example, some experience discomfort, others tension, and still others severe pain.”

Some people get symptoms in the muscles that move the jaw. For others, it’s in a disc within the jaw joint that’s damaged. You can also develop arthritis, or joint **inflammation**. You can even have more than one kind of disorder at the same time.

TMJ disorders sometimes start after an injury. But for most people, there’s no obvious cause. In addition to pain, other symptoms can include stiffness, limited jaw movement, painful clicking or popping in the joint, or changes in the way the teeth fit together.

If you have any of these symptoms, talk with your health care provider. To diagnose a TMJ disorder, they’ll ask you questions about your symptoms and examine your head, neck, face, and jaw. They’ll also check your dental and medical history. They may use imaging tests, like X-rays, too.

Experts recommend starting with simple, self-care practices for jaw pain (see the Wise Choices box for tips). “For a lot of people, the pain will resolve over time,” Fischer explains. “Your doctor may also recommend trying a bite guard. These are plastic splints that fit over the teeth.”

Sometimes, TMJ disorders can become chronic—causing pain or discomfort that lasts more than three months. Aggressive treatments include surgery, splints that change the bite, and even adjusting or removing teeth. But whether these treatments help hasn’t been scientifically studied, explains Fischer.

For some people, they may make things worse. “And once you have surgery, you can’t put things back the way they were before,” she says.

If you have symptoms that last more than three months, your dentist or health care provider may refer you to a specialist. Doctors who specialize in muscles and bones, arthritis, pain, and the nervous system may be able to help.

But better treatments are needed. NIH-funded researchers have been studying the role that **genes** play in who develops a TMJ disorder and how long it lasts. In a large study, researchers identified several genes that are more common in people who have severe jaw pain. They’re now testing whether early treatment can help people with certain genes lower their risk of developing a chronic disorder.

“We hope that having a better understanding of why temporomandibular disorders develop will ultimately help us prevent them and find new treatments,” Fischer says.

*Note*: The title of this article was changed after publication.

### Wise Choices

#### Tackling Jaw Pain

Simple, self-care practices to try:

- Avoid hard foods.
- Use ice packs on the joint.
- Try over-the-counter pain medications, like ibuprofen, for a short amount of time.
- Avoid extreme movements, like wide yawning and chewing gum.
- Learn relaxation and stress reduction techniques.
