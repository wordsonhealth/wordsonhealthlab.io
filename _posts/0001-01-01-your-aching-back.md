---
layout: post 
title: "Your Aching Back"
date: 2019-04-10
image: assets/images/your-aching-back.webp
excerpt: "Searching for Better Pain Relief"
tags: Aching Back, Bones, low back pain, NCCAM, Osteoporosis, spine
draft: false
---

If warmer spring weather lures you outside for heavy yard work, now’s a good time to learn about how to prevent and treat low back pain. Four out of every five people have had low back pain at one time or another. With symptoms ranging from a dull ache to absolute agony, low back pain can put your life on hold. In fact, it’s second only to the common cold in causing missed work days for adults under age 45.

Most low back pain clears up in a few days or weeks with a combination of rest, appropriate exercise, and over-the-counter pain medicines. But pain that persists for more than three months-chronic back pain-is more difficult to treat, in part because there are many different possible causes.

The lower, or lumbar, spine is a complex structure made up of powerful muscles, ligaments, bones, and joints. It provides the strength for standing, walking, lifting and other activities, and allows the body to turn, twist, and bend.

Back pain can be caused by a number of things, from sports injuries and other damage to simple wear and tear. If muscles are poorly conditioned or overworked, they are more easily strained. Someone who works all week at a desk, for example, can strain their back muscles doing heavy yard work on the weekend. Likewise, if the ligaments that help stabilize the low back are weak from inactivity or stiff from overuse, a sudden wrenching movement can cause a ligament sprain.

Aging can also bring low back pain. Bones lose strength over time. In someone with osteoporosis , the bones of the lumbar vertebrae can break or compress in a fall or even during some everyday activities. Arthritis can inflame joints, causing pain and stiffness. And “slipped disks,” in which the rubbery cartilage between disks bulge outward, can press against the spinal nerves to cause pain.

Keeping your back healthy is the best way to prevent low back injury. There are several practical things you can do:

- Don’t try to lift objects too heavy for you. Lift by bending your knees, not your back; keep your back straight and your head down and in line with your back. Keep the object close to your body, and don’t twist when lifting.
- Regular, low-impact exercises like walking, swimming, or stationary bike riding 30 minutes a day can increase muscle strength and flexibility. Yoga can also help stretch and strengthen muscles and improve posture. Always stretch before exercise or other strenuous physical activity to prevent back injury.
- Try to practice good posture. Your back supports your weight most easily when it is straight. Wear comfortable, low-heeled shoes. When standing, keep your weight balanced on both feet. Keep your shoulders back and don’t slouch.
- When sitting, try to use a chair with good lower back support. A pillow or rolled-up towel placed behind the small of your back might help. Make sure your work surfaces, like your keyboard, are at a comfortable height. If you have to sit for a long period of time, rest your feet on a low stool or a stack of books. Switch sitting positions often, and walk around the office and gently stretch your muscles every so often to relieve tension.
- Sleeping on a firm surface on your side helps your back.
- Eat a healthy diet with enough calcium, phosphorus, and vitamin D to help promote new bone growth.
- Keep extra weight off your waistline, where it can strain your lower back.
- If you smoke, quit; smoking reduces blood flow to the lower spine and causes the spinal discs to degenerate.

![img](/assets/images/50-Your-Aching-Back.webp)

Since not every back injury can be prevented, researchers are always looking for news ways to treat low back pain. For example, NIH’s National Center for Complementary and Alternative Medicine (NCCAM) is funding the study of “chiropractic,” an ancient method of adjusting and manipulating body parts, to see if it helps with low back pain. So far, NCCAM says that studies of chiropractic treatment are not conclusive. Chiropractic treatment and conventional medical treatments seem to be about equally helpful. NCCAM is continuing to study this and other alternative treatments.

Dr. Jon Levine, director of the NIH Pain Center at the University of California at San Francisco , is particularly interested in the first sensory nerve cell that begins the pain signal to the brain. Many patients with pain, including chronic low back pain, say that although their medication relieves pain, they don’t want to take it because of side effects like cloudy thinking, sleepiness, fatigue, or even addiction. Levine explains that many of the side effects from pain medications originate in the brain, not in the outlying areas of the nervous system that reach out to various parts of the body.

“If we can make drugs that target mechanisms outside the brains such as the first sensory nerve cell,” he explains, “we can prevent those side effects.” His continuing work is supported by funds from NIH’s National Institute of Neurological Disorders and Stroke.



*Definitions:*

**Ligaments**: Tough bands of tissue that attach bones to each other.

**Osteoporosis**: Condition where bones become thin and fragile and are more likely to break.

**Nerves (also called Neurons)**: Cells responsible for sending and receiving signals between the body and brain.

**Nervous system**: System that receives and interprets sensations (like touches, sounds, and sights), and coordinates activities (like muscle movements) throughout the body.

**Receptor**: Molecule on the outside of a cell that receives chemical signals from other parts of the body.

Another aspect of pain relief that Levine and his colleagues study is how men and women respond differently to pain medications. They discovered that one medication works at two different sites in the brain, a pain-relieving site and a pain-enhancing site, and that men have more of the pain-enhancing effect than women. They were able to find the pain-enhancing receptor and block it, reducing the pain-causing effect of the drug in men. This receptor-blocking technique also helped relieve pain in women at lower doses of the drug than before. Thanks to this research, a commercial biotechnology company is now doing further studies to see if the new drug proves to be safe and effective against pain.
Levine emphasizes that the cost of low back pain to society is immense. “Our ability to understand and control this problem-and prevent the recurrence of back pain-would be a major breakthrough for public health in this country,” he says.

**What Makes a Spine?**

![img](/assets/images/50-Spine.webp)

**Vertebrae** are the bones that surround and protect the spinal cord, which carries nerve signals to and from the brain. Between the vertebrae are **intervertebral disks**, pads of cartilage filled with a gel-like substance that act as spinal shock absorbers. **Facet joints** connect the vertebrae to each other and permit back motion. **Ligaments** are elastic bands of tissue that prevent the vertebrae from slipping out of line as the spine moves. **Nerves** branch out from the spinal cord to go to other parts of the body.

Source: National Institute of Arthritis and Musculoskeletal and Skin Diseases.



***A Word to the Wise...***

*If you’ve hurt your back:*

- **Apply ice and heat.** As soon as possible after the trauma, apply ice to the tender spot several times a day for up to 20 minutes. After two to three days of cold treatment, apply heat (such as a heating pad) for short periods to relax muscles and increase blood flow to the area.
- **Get exercise.** Back-healthy activities can include stretching, swimming and walking. Ask your health care provider for advice about gentle exercises.
- **Try bed rest**, but only for a day or two. Too much bed rest can make back pain worse, so get back to your regular activities as soon as possible. The best position is on one side with a pillow between your knees.
- **Take pain relievers.** Over-the-counter pain relievers can ease mild to moderate low back pain. For more than occasional use or more severe pain, be sure to talk to your doctor.
