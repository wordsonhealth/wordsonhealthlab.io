---
layout: post 
title: "Migraines Not in the Mind"
date: 2019-01-04
image: assets/images/migraines-not-mind.webp
excerpt: "Most people don’t realize how serious migraine can be"
tags: headaches, Migraine, migraine treatment, stresses
draft: false
---

Some people dismiss migraines as nothing more than a normal headache exaggerated in the telling. But Dr. Stephen Silberstein, director of the Jefferson Headache Center at Thomas Jefferson University in Philadelphia, recently told an NIH audience, “A migraine is more than a headache.”

Dr. Silberstein, speaking at a seminar sponsored by NIH’s Office of Research on Women’s Health, stressed that migraines have real physiological causes and effects well beyond those of the common tension headaches most people have. “Migraines are not in the mind,” he stressed.

Classic migraines begin 10-30 minutes before the arrival of the actual headache, with a phenomenon known as an aura. People experiencing an aura may feel pins and needles, have trouble speaking, have muscle weakness, and commonly have visual disturbances — seeing things such as flashing lights, zigzag lines, bright spots, or a loss of part of their field of vision. Common migraines aren’t preceded by an aura, but people may experience some mental fuzziness, mood changes, fatigue, and the unusual retention of fluids beforehand.

Both types of migraine bring intense pain, often along with an increased sensitivity to light. Migraine is also sometimes accompanied by diarrhea, increased urination, nausea and vomiting. The pain of a migraine usually lasts 1 to 2 days, but can last 3 or 4 days.

Researchers previously thought that migraines were brought about by blood flow changes to the brain, but Dr. Silberstein says that recent studies of blood flow don’t support this view. “Blood flow changes do occur in migraines,” he says, “but it’s not a disorder caused by blood flow.” One study last year found that the meninges, the thin membranes surrounding the brain, are inflamed during a migraine attack; scientists now believe that this is the source of migraine pain. Exactly how this inflammation occurs is unknown.

What is known is that migraines are triggered in susceptible people by certain environmental factors: stress and anxiety, diet, physical exertion, sleep extremes, head trauma, and hormonal changes. Not everyone gets migraines from these things, of course. Dr. Silberstein says that some people seem to have a genetic predisposition to getting migraines. This potential for getting migraines is then set off by something in their environment.

Migraines usually begin between the ages of 5 and 35, and their peak prevalence is between the ages of 35 and 45. Some people can get as many as 10 migraines a month, but the average is around 1.5 per month. Although athletic men such as basketball’s Kareem Abdul-Jabbar and football’s Terrell Davis have had migraines, they are more prevalent among women. Women start getting migraines more than men after their first menstrual period, highlighting the effect that hormones can have on the condition. According to Dr. Silberstein, migraine occurs in 4 percent of both boys and girls, 6 percent of men, and 18 percent of women. Menopause can reduce the incidence of migraines, but hormone replacement therapy, which many women undergo to counteract the effects of menopause, can increase their incidence.

**What to do?**

If you are a migraine sufferer, there are several things you can do to combat your migraines. First, try to maintain a regular routine, going to sleep and waking up at the same time every day. Try to keep your caffeine intake constant from day to day, including weekends. Exercise regularly, quit smoking if you smoke, and try to identify any environmental triggers that may bring on your headaches (see box). Riboflavin (vitamin B2) is an often overlooked remedy for migraine prevention, according to Dr. Silberstein. He says that 400 milligrams (mg) a day may begin to prevent migraines within a couple of months.

During a migraine attack, using cold packs or pressing on the bulging artery that is sometimes found in front of the ear may provide temporary relief for some. There are also several drugs that your doctor can recommend to prevent or treat migraines. Dr. Silberstein cautions against taking too much over-the-counter medicine for your headaches; that can actually add to the problem by increasing the frequency of your headaches. “If you have very frequent headaches,” he stresses, “you need help.” — a report from The NIH Word on Health, June 2000.

**Is it a Migraine?**

There are three general types of headaches:

|                      | **Migraine**                                                 | **Tension**        | **Cluster**                                                  |
| -------------------- | ------------------------------------------------------------ | ------------------ | ------------------------------------------------------------ |
| **Characteristics:** | throbbing pain, often preceded by an aura; more prevalent in women | steady ache        | severe, coming in groups over weeks or months; mainly attack men |
| **Pain in:**         | one or both sides of head                                    | both sides of head | one side of head, often centering around one eye             |
| **Severity:**        | mild, moderate or severe                                     | mild or moderate   | very severe                                                  |
| **Other symptoms:**  | may experience sensitivity to light, sound, and odors; nausea; vomiting; diarrhea; visual disturbances; fever, chills, aching, sweats | no                 | nasal congestion; drooping eyelid; watery, teary eye         |

Remember that headaches can also be caused by underlying medical problems such as tumors or meningitis. Make sure to see your health professional if your attacks:

- start after age 50
- are different or more severe than past attacks
- are triggered by coughing or bending
- are linked to a stiff neck and fever
- are accompanied by blurred vision or speech, or numbness, tingling or muscle weakness
- make it difficult for you to think or remember
- cause severe vomiting
- follow a severe head injury

***A Word to the Wise...***

***These triggers may bring on migraines in certain people:***

- Air pollutants
- Cigarette smoking
- Bright or flickering lights
- Dairy foods (for those with lactose intolerance)
- Monosodium glutamate (commonly found in Asian cuisine, snack foods)
- Nitrates (commonly found in pickled foods, cold cuts)
- Patterns such as stripes or zigzags
- Perfumes
- Physical exertion
- Red wine and other alcoholic drinks
- Stress and anxiety

In addition to these established factors, some people have unique environmental triggers they need to avoid. Whether their headaches are a direct result of exposure to the trigger or an indirect result of the stress and anxiety of being exposed to the trigger, their headaches are still very real, and so is their pain.
