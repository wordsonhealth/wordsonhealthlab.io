---
layout: post 
title: "Learn the Aids Link"
date: 2019-06-28
image: assets/images/learn-the-aids-link.webp
excerpt: "Drugs Can Give You More Than a High"
tags: AIDS, Drugs, Drugs Abuse, HIV, HIV/AIDS, Infections, NIDA
draft: false
---

Most people know that drug abuse is linked to AIDS. Less understood is why they’re linked. The “high” that people get from drugs may alter their judgment and lead them to take risks they normally would not take—including having unprotected sex.

HIV, or human immunodeficiency virus, attacks the body’s disease-fighting (immune) system. If the virus overwhelms the immune system, you can no longer fight off disease. When this happens, it’s called acquired immune deficiency syndrome, or AIDS. The only sure way to prevent AIDS is to avoid becoming infected with HIV.

![img](/assets/images/89-Learn-the-AIDS-Link.webp)

A large body of drug abuse research shows that drugs of abuse, including marijuana, cocaine, methamphetamine and “ecstasy,” affect areas of the brain that control decision-making. Engaging in risky behavior puts young people in real danger of becoming infected with HIV.

“In recent years, the number of young people in the U.S. diagnosed with AIDS rose substantially,” said Dr. Nora Volkow, director of NIH’s National Institute on Drug Abuse (NIDA). “And because drug use encourages risky behaviors that can promote HIV transmission, NIDA views drug abuse treatment as essential HIV prevention.”

Thousands of teenagers now have AIDS, according to the Centers for Disease Control and Prevention. And while in earlier years, most reported adolescent AIDS cases were in males, the number of female AIDS cases is now nearly equal with males. Among women diagnosed in 2004, roughly 7 out of 10 got the disease from having sex with infected male partners.

Minority populations are also at risk for drug abuse and HIV. For example, black and Hispanic women made up only 25% of all U.S. women in 2004, yet they accounted for around 80% of AIDS cases diagnosed in women that year. In fact, African Americans, who make up just 13% of the population, accounted for approximately half of the total AIDS cases diagnosed in 2004.

That’s why NIDA, together with its partners, is working to get teens, women and young adults to understand the link between drug abuse and HIV/AIDS. Learn the link and, if you have teenagers, talk to them about the link.

Seeking medical treatment as soon as possible after becoming infected with HIV is critical. If you think you may be at risk for HIV, through drug use or sexual behavior, get tested for the virus.

***A Word to the Wise...***

*Protect Yourself From HIV*

- Don’t share needles, syringes or other equipment used to inject drugs, steroids, vitamins or for tattooing or body piercing.
- Abstain from sexual intercourse or stay in a long-term relationship with a partner who has been tested and you know is not infected. The more sex partners you have, the greater your chances are of getting HIV or other diseases passed through sex.
- Correct and consistent use of the male latex condom can reduce the risk of disease transmission through sex. However, no protective method is 100% effective. Condom use cannot guarantee protection against sexually transmitted disease.
- Don’t share razors or toothbrushes because of the possibility of contact with blood.
- If you are pregnant or think you might be soon, talk to a doctor or your local health department about being tested for HIV. If you have the virus, drug treatments are available to help you reduce the chance of passing HIV to your baby.

—Adapted from material from the U.S. Centers for Disease Control and Prevention
