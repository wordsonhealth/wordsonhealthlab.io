---
layout: post 
title: "Get a Comprehensive Dilated Eye Exam"
date: 2019-01-30
image: assets/images/get-a-comprehensive-dilated-eye-exam.webp
excerpt: "Age-Related Eye Disease Doesn't Have to Steal Your Sight"
tags: AMD, Cataract, Diabetic Eye Disease, Eye exam, Glaucoma
draft: false
---

If there’s one disability associated with getting older that cannot be overlooked, then the eyes have it. It’s called age-related eye disease (ARED). According to an expert at the National Eye Institute (NEI), four major AREDs — glaucoma, cataract, age-related macular degeneration, and diabetic retinopathy — can creep up silently during your senior years, threatening your vision. People ages 65 and older are at greater risk for these diseases, said Dr. Robert Nussenblatt, scientific director of NEI, a part of the National Institutes of Health. However, careful monitoring of your eye health and regular eye examinations can help you see well into old age.

“As we get older in this society,” he said in a lecture at NIH, “we tend to value our independence more. We remain vibrant and active and, in doing so, perhaps our sense of sight becomes even more important.”

![img](/assets/images/15-Eye.webp)

The United States will spend between $20 million and $22 million dollars directly on eye problems, and almost an equal amount on indirect costs associated with delivering care to people with eye problems, he noted. Approximately 47,000 Americans becomes blind every year.

“Essentially,” he added, “that means that one American becomes blind, or has severe vision loss, every 11 minutes.” However, you can help preserve your eyesight by being aware of some common age-related eye diseases and learning how to recognize their often-subtle symptoms.

**What Is a Comprehensive Eye Exam?**

According to the National Eye Institute, a comprehensive eye exam should include at least the following three tests:

- **Visual acuity test**: The familiar eye chart measures how well you see at various distances.
- **Pupil dilation**: An eye care professional places drops into the eye to widen the pupil. This reveals more of the retina and other signs of disease. After the examination, close-up vision may remain blurred for several hours.
- **Tonometry**: A standard test that determines the fluid pressure inside the eye. There are many types of tonometry. One uses a purple light to measure pressure; another, an “air puff,” test, which measures the resistance of the eye to a puff of air. Elevated pressure is a possible sign of glaucoma.

In addition, several special examinations may be recommended to detect other eye disorders, including signs of age-related eye diseases:

- **Visual Field**: This test measures your side (peripheral) vision. It helps your eye care professional find out if you have lost sidevision, a sign of glaucoma.
- **Amsler Grid**: While conducting the examination, your eye care professional may ask you to look at an Amsler grid. It is a pattern that resembles a checkerboard. You will be asked to cover one eye and stare at a black dot in the center of the grid. While staring at the dot, if you notice that the straight lines in the pattern appear wavy, it may be a sign of wet age-related macular degeneration (AMD). If your eye care professional suspects you have wet AMD, you may need to have a test called fluorescein angiography. In this test, a special dye is injected into a vein in your arm. Pictures are then taken as the dye passes through the blood vessels in the retina. The photos help your eye care professional evaluate leaking blood vessels to determine whether they can be treated.
- **Ophthalmoscopy**: In this examination, the eye care professional looks through a device with a special magnifying lens that provides a narrow view of the retina, or (wearing a headset with a bright light) looks through a special magnifying glass and gains a wide view of the retina. Your eye care professional will look at your retina for early signs of retinal disease, such as: (1) leaking blood vessels, (2) retinal swelling, such as macular edema, (3) pale, fatty deposits on the retina — signs of leaking blood vessels, (4) damaged nerve tissue, and (5) any changes in the blood vessels.

![img](/assets/images/15-normal_vision_color.webp)

Above is a simulated photo as it might be seen by a person with normal vision. Below is the same scene as it might be viewed by people with an age-related eye disease.

 

![img](/assets/images/15-armd2.webp)

**Age-related Macular Degeneration (AMD)** 
AMD is a disease associated with aging that gradually destroys sharp, central vision. Central vision is needed for seeing objects clearly and for common daily tasks such as reading and driving.

  

![img](/assets/images/15-cataract2.webp)

**Cataract** 
A cataract is a clouding of the lens in the eye. Vision with cataract can appear cloudy or blurry, colors may seem faded and you may notice a lot of glare.

  

![img](/assets/images/15-diabetic2.webp)

**Diabetic Eye Disease** 
Diabetic eye disease is a complication of diabetes and a leading cause of blindness. The most common form is diabetic retinopathy which occurs when diabetes damages the tiny blood vessels inside the retina.

 

![img](/assets/images/15-glaucoma2.webp)

**Glaucoma** 
Glaucoma is a group of diseases that can damage the eye’s optic nerve and result in vision loss and blindness. It is usually associated with high pressure in the eye and affects side or peripheral vision.

 

***A Word to the Wise...***

- Encourage family members and friends who may be at high risk for glaucoma — Blacks over age 40 and everyone over age 60 — to have an eye examination through dilated pupils every two years.
- The sun’s ultraviolet rays can be dangerous to eyes. When purchasing sunglasses, ask for ones manufactured with a UV blocker. Be sure the blocker is designed to screen out both ultraviolet A and ultraviolet B rays.
- Quit smoking. It is not only bad for your heart and lungs, but also for your eyes.
- If you are pregnant and have diabetes, get an eye exam in which your eyes are dilated every trimester.
- If you have diabetes, get eye exams in which your eyes are dilated once a year.
- If you have diabetes, check with your doctor about how to maintain tight control of your blood sugar.
