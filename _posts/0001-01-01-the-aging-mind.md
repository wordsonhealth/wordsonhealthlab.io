---
layout: post 
title: "The Aging Mind"
date: 2019-05-14
image: assets/images/the-aging-mind.webp
excerpt: "Learning to Adjust to Natural Changes"
tags: Aging, Aging brain, Brain Imaging, Memory
draft: false
---

As our brains age, we’re less likely to think as quickly as we used to or remember things as well. But the knowledge we gain from life experience can sometimes compensate for other changes in our brains as we age. Older professionals, for example, are often better at their jobs than younger ones. Research is now revealing how the brain changes and adapts as we age. These insights are shedding light on real-life challenges, like how to remember things and how to avoid scams.

Dr. Denise C. Park, director of the Roybal Center for Healthy Minds at the University of Illinois, explains that knowledge and experience are protected as you age. “When you’re performing a complex task,” she says, “your memory may be less efficient, but your knowledge about how to do it may be better.” In most real-world experiences, older people already have previous knowledge that they can use to interpret new situations and decide how to respond.

Researchers can design tests that expose problems in the aging mind by creating tasks in which older adults can’t use their world knowledge. These tests reflect some real life situations. For instance, when an older adult gets an upsetting new medical diagnosis or a crafty scam artist pressures them for a quick answer, they may have trouble processing information quickly and making a sound decision.

![img](/assets/images/67-The-Aging-Mind.webp)

Park says that one key to dealing with situations like these is not to make rash decisions. Ask for further information and more time to consider. Discuss a new medical diagnosis or unfamiliar sales offers with friends or relatives to get more perspective.

Perhaps the most common change people face as they age is trouble remembering things. Park says it’s important to acknowledge that your memory is fallible. “For medicines, driving directions or other things with specific details, don’t rely on your memory,” she says. “That’s good advice for everybody, but especially for older adults.” If you need to remember something important, write it down on a pad you carry around with you or use an electronic device that lets you store notes and reminders.

You can also structure your routines to help you remember things. Try to take a medicine with a snack or a particular meal, for example, and always keep your keys and your wallet in the same place. “Because older adults tend to lead very structured lives, this technique works very well for them,” Park says.

Park also advises using your imagination. “If you imagine completing a future action,” she says, “you’re much more likely to perform it.” For example, imagine taking your medicine in as much detail as you can, paying attention to where, when and how.

Practicing for future events can also help prepare your mind for the real thing. Rehearse your response to a salesperson with a relative or visit somewhere new in advance to make sure you know how to get there.

Some evidence suggests that activities requiring mental effort such as playing board games, reading and playing a musical instrument can help stave off mental decline. Other studies suggest that physical activity might help.

Researchers supported by NIH’s National Institute on Aging continue to explore new ways to keep the brain healthy as we age. In the meantime, try to keep in good shape both mentally and physically, and use the tips in this article to help your aging mind keep working as well as it can.

***A Word to the Wise...***

*Tips for an Aging Mind*

Here are some strategies to help your brain as it changes with age:

- Don’t make rash decisions. Give yourself time to think.
- Write things down. Memory can be fallible for people of any age.
- Structure your day and your surroundings to help you remember things.
- Imagine a future action in as much detail as you can.
- Keep your brain active with activities that require mental effort, such as reading.
- Stay physically active.
