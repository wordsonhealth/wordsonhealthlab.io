---
layout: post 
title: "Anxiety Disorders"
date: 2019-03-06
image: assets/images/anxiety-disorders.webp
excerpt: "What are the types of anxiety disorders? Causes, Symptoms, Diagnosis and Treatment"
tags: Anxiety, Anxiety Disorders, NIMH, Psychotherapy
draft: false
---

Anxiety disorders like panic disorder and post-traumatic stress disorder are more common than most people realize. Over 19 million adults in America suffer from these chronic conditions, which can seriously interfere with work and personal relationships.

Dr. Dennis Charney, director of the Mood and Anxiety Disorders Program at the National Institute of Mental Health (NIMH) of the National Institutes of Health (NIH) wants you to know that there are effective ways to treat these conditions. “Treatment works as well as, if not better than, most treatments for other serious medical disorders,” he says.

**Types of Anxiety Disorders**

Anxiety disorders can become very serious if left untreated. It’s important to realize that they can also be accompanied by depression, eating disorders, substance abuse or another anxiety disorder, compounding the problem. There are several types of anxiety disorders. They include:

**Social Anxiety Disorder (SAD)**. SAD is the most common of the anxiety disorders, with 15 percent of the American population afflicted by it. It is characterized by a persistent fear of social or performance situations.

“In social situations,” Dr. Charney explains, “people with SAD become very nervous. They feel that people are looking at them, that they’re not saying the right things, that they don’t look right.” These people can become very shy and begin to avoid social situations. As a result, they don’t have as many friends as they could. It also affects their ability to perform at work because many jobs involve speaking in front of other people and being in group meetings where you are expected to make a contribution. So SAD can have a very broad effect on your life.

“This should be separated from shyness,” Dr. Charney stresses. “Shyness is a temperament. Some people are more shy than others. SAD produces impairment.”

**Post-traumatic stress disorder (PTSD)**. PTSD is a reaction to a terrifying event that keeps returning in frightening, intrusive memories. The traumatic event could be something you see or something that happens to you directly.

“PTSD produces an intense fear and a sense of helplessness,” Dr. Charney says. People with this disorder can become detached and emotionally numb. They may feel guilt for surviving. “The survivors wonder, why me?” he says. They also often have problems sleeping.

PTSD is fairly common. At some point in their lives, 40 to 80 percent of people are exposed to a very serious, traumatic event. At any given time, eight percent of the people in the U.S. have PTSD.

**Generalized anxiety disorder (GAD)**. Everyday events and decisions cause exaggerated worry and tension in people with GAD. “Patients with GAD are worrywarts,” Dr. Charney says. “They feel the world in general is not a safe place, that bad things happen to good people like themselves. They are always feeling distressed.” They become restless, fatigued, irritable, and tense.

“People with GAD have chronic, moderate levels of symptoms associated with lots of worrying,” Dr. Charney explains, “but they don’t have panic attacks that send them to the emergency room.” About six percent of the U.S. population suffers from GAD.

**Panic disorder**. People with panic disorder have recurrent, unexpected feelings of extreme fear and dread that strike for no apparent reason, causing their heart to race, rapid breathing, sweating, and shakiness. These “attacks” can send people to the hospital believing they are having a heart attack.

“It could come right out of the blue for no apparent reason,” Dr. Charney explains, “when you’re not in a situation that would normally make you feel stress or anxiety or fear.”

People with this condition often avoid places where they’ve had panic attacks, and in severe cases, may become housebound. Two to four percent of the people in America suffer from panic disorder.

**Obsessive-compulsive disorder (OCD)**. People who suffer from OCD become trapped in a pattern of repetitive thoughts and behaviors that are senseless and distressing but extremely difficult to stop. If severe and left untreated, OCD can destroy a person’s capacity to function at work, at school, or even in the home. OCD affects more than two percent of the country’s population.

**Why Me?**

Scientists aren’t quite sure why some people get anxiety disorders. Different people exposed to the same situation can react in very different ways. Part of this difference may be in the genes they have inherited.

“These disorders run in families,” Dr. Charney explains. So if a parent has an anxiety disorder, the chance of their children having one of these conditions is higher. That may be due to the genes they’ve inherited, but Dr. Charney points out, “The environment that a child is brought up in may be important too. Ultimately, it’s probably an interaction between genetic predisposition and environment.”

Scientists have recently been gaining insights into the development of anxiety disorders. “Children of parents with panic disorders have a higher incidence of behavioral disorders very early in life, before you would think major environmental impacts would occur,” Dr. Charney says.

A growing body of evidence shows that infants who tend to be shy, timid and constrained in social situations — even in the very first weeks of life — have higher rates of anxiety disorders when they get older. “We’re actively searching for other behavior manifestations that might relate to the development of other anxiety and mood disorders early in life,” Dr. Charney adds. Scientists hope that understanding the development of these disorders will lead them to better ways of preventing and treating them.

Researchers are taking several approaches to figure out the underlying scientific bases for anxiety disorders. Using advanced imaging techniques, they are mapping the brains of people with anxiety disorders to see how they differ from those without the disorders. Much current effort is focused on a region of the brain called the amygdala, which plays a central role in feelings of fear and anxiety.

**What Are the Treatments for Anxiety Disorders?**

Treatments for anxiety disorders are extremely effective and often combine medication and psychotherapy.

More medicines are available than ever before to effectively treat anxiety disorders. These include antidepressants and benzodiazepines. If one medication is not effective, you and your doctor can discuss others.

The two most effective forms of psychotherapy used to treat anxiety disorders are behavioral therapy and cognitive-behavioral therapy. Behavioral therapy tries to change actions through techniques such as breathing exercises or through gradual exposure to what is frightening. Cognitive-behavioral therapy teaches patients to understand their thinking patterns so they can react differently to the things that cause them anxiety.

Source: NIMH

Scientists are also trying to better understand how the body deals with the stress that accompanies anxiety, a complex reaction that involves another region of the brain called the hippocampus, as well as many other systems of the body. And they are studying the many different chemicals both in the brain and throughout the rest of the body that are involved in the experience of anxiety and fear.

“Anxiety and mood disorders are diseases of the brain and the body, not just the mind,” Dr. Charney stresses.

**Getting Help**

Scientists have found that adolescence is an important period for the diagnosis and treatment of anxiety disorders.

Dr. Charney says, “Of adolescents who have any one of the mood or anxiety disorders, 42 percent still have an anxiety or depressive disorder in adulthood.” In contrast, only five percent of adolescents who were healthy go on to develop one of the disorders.

“It is likely that if we aggressively treat adolescents who suffer from mood and anxiety disorders, we can prevent many of these disorders from becoming chronic,” Dr. Charney says. “We want to make these diagnoses as early as possible.”

But treatments can be effective at any age. If you think you may have an anxiety disorder, don’t hesitate to discuss it with your health care provider. There are many different types of treatments available, and these can be tailored to specific problems. In some cases, psychotherapy, or counseling, is sufficient. In other cases, medication alone can be very effective. Some people may need both. Researchers are now looking at ways to define at an early stage who will do well with which treatments.

“Probably in a majority, it’s a combination of both medication and psychotherapy that works,” Dr. Charney says. “Psychotherapy teaches you things that are very effective and helpful for the long run. But in many patients, that may not be enough because there’s also a biology that the psychotherapy may not be able to overcome. The therapy helps you learn better behaviors and the medications help treat the biologic or genetic disturbance.”

“The take home message is that treatment can work,” Dr. Charney says. “We’re working to make it better, and that’s why we’re doing research at places like NIMH. But there are treatments available now that can work. Patients have to believe in that, because it’s true.”

***A Word to the Wise...***

*How to Get Help for Anxiety Disorders*

If you, or someone you know, has symptoms of anxiety, a visit to the family physician is usually the best place to start. A physician can help determine whether the symptoms are due to an anxiety disorder, some other medical condition, or both. Frequently, the next step in getting treatment for an anxiety disorder is referral to a mental health professional.

Among the professionals who can help are psychiatrists, psychologists, social workers, and counselors. For some people, group therapy is a helpful part of treatment. It’s important that you feel comfortable with the therapy that the mental health professional suggests. If this is not the case, seek help elsewhere. However, if you’ve been taking a medication, it’s important not to stop using it abruptly. Certain drugs have to be tapered off under the supervision of your physician.

When you find a health care professional that you’re satisfied with, the two of you will be working together as a team. Together you will be able to develop a plan to treat your anxiety disorder.

You may be concerned about paying for treatment for an anxiety disorder. If you belong to a Health Maintenance Organization (HMO) or have some other kind of health insurance, the costs of your treatment may be fully or partially covered. Your doctor may also know of public mental health centers that charge people according to how much they are able to pay. If you are on public assistance, you may be able to get care through your state Medicaid plan.

Source: NIMH
