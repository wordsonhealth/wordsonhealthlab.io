---
layout: post 
title: "Stress and Disease"
date: 2019-01-16
image: assets/images/stress-and-disease.webp
excerpt: "People tend to talk about stress as if it’s all bad, It’s not"
tags: Immune System, Relaxation, Stress, Stress Control
draft: false
---

For thousands of years, people believed that stress made you sick. Up until the nineteenth century, the idea that the passions and emotions were intimately linked to disease held sway, and people were told by their doctors to go to spas or seaside resorts when they were ill. Gradually these ideas lost favor as more concrete causes and cures were found for illness after illness. But in the last decade, scientists like Dr. Esther Sternberg, director of the Integrative Neural Immune Program at NIH’s National Institute of Mental Health (NIMH), have been rediscovering the links between the brain and the immune system.

**The Immune System and the Brain**

When you have an infection or something else that causes inflammation such as a burn or injury, many different kinds of cells from the immune system stream to the site. Dr. Sternberg likens them to soldiers moving into battle, each kind with its own specialized function. Some are like garbage collectors, ingesting invaders. Some make antibodies, the “bullets” to fight the infectious agents; others kill invaders directly. All these types of immune cells must coordinate their actions, and the way they do that is by sending each other signals in the form of molecules that they make in factories inside the cell.

“It turns out that these molecules have many more effects than just being the walkie-talkie communicators between different kinds of immune cells,” Dr. Sternberg says. “They can also go through the bloodstream to signal the brain or activate nerves nearby that signal the brain.”

These immune molecules, Dr. Sternberg explains, cause the brain to change its functions. “They can induce a whole set of behaviors that we call sickness behavior. . . . You lose the desire or the ability to move, you lose your appetite, you lose interest in sex.” Scientists can only speculate about the purpose of these sickness behaviors, but Dr. Sternberg suggests that they might help us conserve energy when we’re sick so we can better use our energy to fight disease.

These signaling molecules from the immune system can also activate the part of the brain that controls the stress response, the hypothalamus. Through a cascade of hormones released from the pituitary and adrenal glands, the hypothalamus causes blood levels of the hormone cortisol to rise. Cortisol is the major steroid hormone produced by our bodies to help us get through stressful situations. The related compound known as cortisone is widely used as an anti-inflammatory drug in creams to treat rashes and in nasal sprays to treat sinusitis and asthma. But it wasn’t until very recently that scientists realized the brain also uses cortisol to suppress the immune system and tone down inflammation within the body.

![img](../assets/images/08-stress-disease.webp)

**Stress and the Immune System**

This complete communications cycle from the immune system to the brain and back again allows the immune system to talk to the brain, and the brain to then talk back and shut down the immune response when it’s no longer needed.

“When you think about this cross-talk, this two-way street,” Dr. Sternberg explains, “you can begin to understand the kinds of illnesses that might result if there is either too much or too little communication in either direction.”

According to Dr. Sternberg, if you’re chronically stressed, the part of the brain that controls the stress response is going to be constantly pumping out a lot of stress hormones. The immune cells are being bathed in molecules which are essentially telling them to stop fighting. And so in situations of chronic stress your immune cells are less able to respond to an invader like a bacteria or a virus.

This theory holds up in studies looking at high-levels of shorter term stress or chronic stress: in caregivers like those taking care of relatives with Alzheimer’s, medical students undergoing exam stress, Army Rangers undergoing extremely grueling physical stress, and couples with marital stress. People in these situations, Dr. Sternberg says, show a prolonged healing time, a decreased ability of their immune systems to respond to vaccination, and an increased susceptibility to viral infections like the common cold.

**Some Stress is Good**

People tend to talk about stress as if it’s all bad. It’s not.

“Some stress is good for you,” Dr. Sternberg says. “I have to get my stress response to a certain optimal level so I can perform in front of an audience when I give a talk.” Otherwise, she may come across as lethargic and listless.

But while some stress is good, too much is not good. “If you’re too stressed, your performance falls off,” Dr. Sternberg says. “The objective should be not to get rid of stress completely because you can’t get rid of stress — stress is life, life is stress. Rather, you need to be able to use your stress response optimally.”

The key is to learn to move yourself to that optimal peak point so that you’re not underperforming but you’re also not so stressed that you’re unable to perform. How much we’re able to do that is the challenge, Dr. Sternberg admits. This may not be possible in all situations, or for all people, because just as with the animals Dr. Sternberg studies, some people may have a more sensitive stress response than others.

“But your goal should be to try to learn to control your stress to make it work for you,” Dr. Sternberg says. “Don’t just think of getting rid of your stress; think of turning it to your advantage.”

**Controlling the Immune Response**

Problems between the brain and the immune system can go the other way, too. If for some reason you’re unable to make enough of these brain stress hormones, you won’t be able to turn off the immune cells once they’re no longer needed.

“There has to be an exit strategy for these battles that are being fought by the immune system, and the brain provides the exit strategy through stress hormones,” Dr. Sternberg says. “If your brain can’t make enough of these hormones to turn the immune system off when it doesn’t have to be active anymore, then it could go on unchecked and result in autoimmune diseases like rheumatoid arthritis, lupus, or other autoimmune diseases that people recognize as inflammation.”

Dr. Sternberg says that there are several factors involved in these autoimmune conditions. There are many different effects that the brain and its nervous system can have on the immune system, depending on the kinds of nerve chemicals that are being made, where they’re being made, what kind of nerves they come from, and whether they’re in the bloodstream or not. Still, at least part of the problem in these diseases seems to involve the brain’s hormonal stress response.

“So if you have too much stress hormone shutting down the immune response, you can’t fight off infection and you’re more susceptible to infection,” Dr. Sternberg concludes. “Too little stress hormones and the immune response goes on unchecked and you could get an inflammatory disease.”

**Pinpointing the Problems**

Why these miscommunications between the brain and the immune system come about is still largely unknown, and involves many genes and environmental factors. But by studying animals, scientists have finally been able to start understanding how the miscommunications occur.

Dr. Sternberg first started publishing work on the links between the brain and the immune system back in 1989 studying rats with immune problems. “In many of these cases it’s very hard to show the mechanism in humans,” Dr. Sternberg explains, “but you can show the mechanism in animals because you can manipulate all the different parts of the system and you can begin to understand which parts affect which other parts.” It has taken “a good ten years” to gather enough evidence in human studies to show that the principles her lab uncovered in rats were also relevant to human beings.

Drugs that have been tested in rats to correct brain/immune system problems have had unpredictable effects. That is because nothing happens in isolation when it comes to the brain and the immune system. Dr. Sternberg points out that our bodies are amazing machines which at every moment of the day are constantly responding to a myriad of different kinds of stimuli — chemical, psychological, and physical. “These molecules act in many different ways in different parts of the system,” she says. Understanding how the brain and the immune system work together in these different diseases should help scientists develop new kinds of drugs to treat them that would never have occurred to them before.

**Taking Control Now**

Dr. Sternberg thinks that one of the most hopeful aspects of this science is that it tells us it’s not all in our genes. A growing number of studies show that, to some degree, you can use your mind to help treat your body. Support groups, stress relief, and meditation may, by altering stress hormone levels, all help the immune system. For example, women in support groups for their breast cancer have longer life spans than women without such psychological support.

There are several components of stress to think about, including its duration, how strong it is, and how long it lasts. Every stress has some effect on the body, and you have to take into account the total additive effect on the body of all stressors when considering how to reduce stress.

Perhaps the most productive way to think about stress is in terms of control. Dr. Sternberg shows a slide of an F-14 jet flying sideways by the deck of an aircraft carrier, its wings completely vertical. “The Navy Commander who flew that jet told me that he was the only one in the photo who was not stressed, and that’s because he was the one in control. The officer sitting in the seat ten feet behind him was in the exact same physical situation but was not in control. Control is a very important part of whether or not we feel stressed.

So if you can learn to feel that you’re in control or actually take control of certain aspects of the situation that you’re in, you can reduce your stress response.” Studies show that gaining a sense of control can help patients cope with their illness, if not help the illness itself.

Until science has more solid answers, it can’t hurt to participate in support groups and seek ways to relieve stress, Dr. Sternberg says. But what you need to remember is if you do these things and you’re not successful in correcting whatever the underlying problem is, it’s not your fault because there’s a biology to the system. “You need to know the benefits of the system,” she says, “but its limitations as well.” In other words, try not to get too stressed about being stressed.

***A Word to the Wise...***

***Stress Control***

First try to identify the things in your life that cause you stress: marital problems, conflict at work, a death or illness in the family. Once you identify and understand how these stressors affect you, you can begin to figure out ways to change your environment and manage them.

If there’s a problem that can be solved, set about taking control and solving it. For example, you might decide to change jobs if problems at work are making you too stressed.

But some chronic stressors can’t be changed. For those, support groups, relaxation, meditation, and exercise are all tools you can use to manage your stress. If nothing you do seems to work for you, seek a health professional who can help. Also seek professional help if you find that you worry excessively about the small things in life.

Keep in mind that chronic stress can be associated with mental conditions like depression and anxiety disorders as well as physical problems. Seek professional help if you have:

- Difficulty sleeping
- Changes in appetite
- Panic attacks
- Muscle tenseness and soreness
- Frequent headaches
- Gastrointestinal problems
- Prolonged feelings of sadness or worthlessness
