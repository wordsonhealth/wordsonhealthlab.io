---
layout: post 
title: "Overweight How You Can Win at the Weight Control"
date: 2019-03-30
image: assets/images/overweight-how-you-can-win-at-the-weight-control.webp
excerpt: "The Food Guide Pyramid for overweight and obesity"
tags: Food Guide Pyramid, Food serving, obesity, Overweight, Weight Loss
draft: false
---

If you are or have ever been overweight, you know how hard it is to shed those extra pounds. But don’t give up! According to the Dietary Guidelines for Americans, if you weigh more than you should (see Height-for-Weight Chart), even a weight loss of 5 to 10 percent of your body weight can improve your health, for example, by lowering your blood pressure and cholesterol levels. If you have a family history of weight-related medical problems such as diabetes, heart disease, and high blood pressure, it’s even more important to keep your weight down.

The Weight-control Information Network (WIN), a service of the National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK), of the National Institutes of Health (NIH), can provide you with science-based, practical information on such topics as weight control, physical activity and binge eating disorders.

**Food Guide Pyramid**

One source for sound nutrition advice on cancer prevention and diet is the American Dietetic Association (ADA). As a reference for meal planning, the ADA recommends the Food Guide Pyramid and the Dietary Guidelines for Americans. Both of these support the “total diet approach” to eating. This means long-term eating habits are more important than what you eat at a single meal. In their words, “there are no good or bad foods, only good or bad diets or eating styles.”

![img](/assets/images/45-Food-Guide-Pyramid.webp)

The Food Guide Pyramid shows the types and serving sizes for the foods we should eat every day to stay healthy. Foods are placed in the pyramid in a way that shows how important they are to our health. We should eat more of the foods at the base than foods at the top.

For example, grains, fruits, and vegetable groups are at the base of the pyramid. Meat, dairy, and fat groups are toward the top. Though all groups are important, the plant foods are at the base so that we remember to eat more of those and less of the animal products and added fats.

The layout of the pyramid is also based on the fact that heart disease is the number one killer in America. Having the higher fat foods at the top tells us to eat those foods in moderation.

**Food serving sizes**

A serving size is smaller than you may think. Following are serving sizes for each of the food groups.

Bread, cereal, rice & pasta group (6-11 servings a day recommended)

A serving is:

1 slice of bread
1 oz of ready-to-eat cereal
1/2 cup cooked pasta
1/3 cup rice
1/2 cup cooked cereal (oatmeal, cream of wheat, grits)
4-6 crackers

Fruit group (2-4 servings a day recommended)

A serving of fruit is:

1 medium-sized fruit, such as an apple, orange, banana, or pear
1/2 cup of raw, cooked, canned, or frozen fruits
3/4 cup (6 oz) of 100 percent fruit juice
1/4 cup dried fruit, like raisins, apricots, or mango

Vegetable group (3-5 servings a day recommended)

A serving of vegetables is:

1 cup of raw (carrots, broccoli) or leafy vegetables (i.e., lettuce, spinach)
1/2 cup cooked garden vegetables
3/4 cup of vegetable juice

Milk, yogurt & cheese group (2-3 servings a day recommended)

For dairy products and recipes that use them, choose skim or 1 percent milk and soft cheeses such as cottage, ricotta, mozzarella, parmesan, and Neufchatel. These have less saturated fat, which can clog your arteries.

A serving of dairy foods is:

1 cup of milk
1 cup unflavored yogurt
1 1/2 oz of natural cheese or 2 oz processed cheese
1/2 cup of ice cream or ice milk

Meat, poultry, fish, dry beans, eggs & nuts group (2-3 servings or 6-9 oz a day recommended)

One serving of meat or meat substitute is 3 oz of chicken, beef, pork, fish, or veggie burger. An easy way to judge a meat portion is to remember that a 3-oz portion is the size of a full deck of cards or a woman’s palm.

If you eat red meat, choose lean cuts such as round, sirloin and flank. Because these have less fat, they are tough cuts and will benefit from being marinated or cooked with a liquid in a crockpot to make them more tender. Keep portions to the size of a deck of cards. Moderate portions of meat will leave more space on your plate for cancer fighting fruits, vegetables, and grains.

One ounce of meat or meat substitute is:

1/2 cup cooked or canned legumes (beans and peas)
1 egg
3/4 cup of nuts (small handful)
2 tablespoons of peanut butter
Fats, oils & sweets

(Fats and sweets should be eaten sparingly. They are high in calories and fat.)

A serving of fat is:

1 teaspoon of oil or regular mayonnaise
1 tablespoon of light mayonnaise
1 tablespoon of regular or 2 tablespoons of light salad dressing
2 tablespoons of sour cream
1 strip of bacon

A serving of sweets is:

1 3-inch cookie
1 plain doughnut
4 chocolate kisses
1 teaspoon of sugar or honey

***A Word to the Wise...***

- According to the new clinical practice guidelines* on overweight and obesity, a joint effort of the National Heart, Lung, and Blood Institute (NHLBI) and NIDDK, if you are among the 55 percent of Americans who are overweight or obese, you have an increased risk of dying from problems like diabetes, high blood pressure, stroke and heart disease.
- It’s always a good idea to talk with your doctor before beginning a weight-loss program.
- Fad or starvation diets offer temporary solutions, at best. At worst, they may jeopardize your health.
- Don’t think that just because you are eating low fat/low calorie foods that you can eat all you want. Those calories still add up and must be burned off regardless of what kind of food you eat. Balance is the key to successful dieting.
- Whether you are overweight or not, exercise at least three times a week to look and feel your best.
- Exercise increases lean body weight. You will appear slimmer as you develop your muscles because muscles give shape and form to your body.

**The Food Guide Pyramid pamphlets**

![img](/assets/images/45-Food-Guide-Pyramid-pamphlets.webp)

The Food Guide Pyramid was a recognizable nutrition tool that was introduced by the USDA in 1992. It was shaped like a pyramid to suggest that a person should eat more foods from the bottom of the pyramid and fewer foods and beverages from the top of the pyramid.

The Food Guide Pyramid displayed proportionality and variety in each of five groups of foods and beverages, which ascended in horizontal layers starting from the base and moving upward toward the tip: breads, cereals, pasta and rice; fruits and vegetables; dairy products; eggs, fish, legumes, meat and poultry; plus alcohol, fats and sugars.
