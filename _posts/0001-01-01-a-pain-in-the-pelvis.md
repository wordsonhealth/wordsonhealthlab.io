---
layout: post 
title: "A Pain in the Pelvis"
date: 2019-04-06
image: assets/images/a-pain-in-the-pelvis.webp
excerpt: "Spreading Cells Can Cause a Condition Called Endometriosis"
tags: Endo pain, Endometriosis, NICHD, Pelvis
draft: false
---

Pain can be the first sign that something’s wrong—cramping pain in the pelvis or pain during sex. But for women of childbearing age, difficulty getting pregnant is most often what leads them to see a doctor. The culprit, endometriosis, is difficult to diagnose and may require a surgical procedure.
Endometriosis, or “endo” for short, is caused by tissue that normally grows along the lining of the uterus that begins to grow elsewhere—on the ovaries, for example. This “unauthorized” growth may be what causes pain.

Researcher Dr. Pamela Stratton is chief of the gynecology consult service at NIH’s National Institute of Child Health and Human Development (NICHD). She says the really puzzling thing is that the degree of pain is not always related to the amount of endo. A woman feeling low levels of pain might have a number of large endo cell masses, yet a woman in a lot of pain might have only trace amounts.

Treatments may reduce her pain and improve fertility. The good news is that endo can often be treated while it is being diagnosed. There are two types of tests that can show images of unauthorized tissue growth—ultrasound and MRI—but only a surgical procedure called laparoscopy can confirm that the growth is endo. After masses are removed during the procedure, a lab determines if they are endo.

Ending pain permanently, however, can be more difficult, Stratton explains. For many women, the pain returns, signaling regrowth of the tissue. Because endo cells are fed by estrogen, some doctors prescribe estrogen blockers to deprive endo of its food source.

“Treating the pain by altering hormone levels gets much more tricky,” Stratton says. Lowering estrogen can limit endo growth, but it can also cause unpleasant side effects like hot flashes, unwanted hair growth and weight gain.

NIH scientists are working on new ways to treat endo. In one study, researchers are hoping an estrogen drug called raloxifene can limit endo growth. Stratton and her colleagues are also working with other specialists to better understand how the brain processes the pain caused by endo. Other factors such as chronic stress may make endo pain worse.

Researchers are continuing to work on a variety of new ways to help women with endo pain.



*Definitions*

**Ultrasound**:
An ultrasound machine sends out high-frequency sound waves that reflect off the organs within your body. A computer uses these reflected waves to create an image of your insides.

**MRI (magnetic resonance imaging)**:
Creates an image of your insides using a powerful magnet and radio waves.

**Laproscopy**:
Uses a tiny light, or laparoscope, through a small cut to look at the area in question and perform surgery.



Endometriosis (Statistics):

More than 5.5 million women in North America have endo. It affects about eight to 10 percent of women of childbearing age. Of women who are unable to conceive, untreated endo causes the infertility in about 30 to 40 percent.
