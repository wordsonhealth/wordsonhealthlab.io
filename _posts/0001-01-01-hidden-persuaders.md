---
layout: post 
title: "Hidden Persuaders"
date: 2019-04-18
image: assets/images/hidden-persuaders.webp
excerpt: "The Mindless Eating That Adds Pounds"
tags: Mindless Eating, obesity, Overweight, Weight Loss
draft: false
---

People seem to gain weight easily but have a hard time taking it off. Americans are continuing to get heavier, increasing the risk of getting Type 2 diabetes, heart disease, stroke, cancer and various other health conditions. Healthy eating can play an important role in helping you avoid excess weight. It can also increase the quality and length of your life. Interesting new research is revealing that part of the reason why it’s so difficult to eat healthy is that “hidden persuaders” can lead you to eat more than you think you’re eating.

Dr. Brian Wansink, director of the Cornell Food and Brand Lab, reviewed the latest research into these hidden persuaders in a recent talk at NIH. For example, the size and shape of containers, he said, can as much as double the amount of food you consume. In a field study at a Philadelphia movie theater, researchers gave participants free popcorn in large or extra large sizes. Unknown to the participants, they were randomly given popcorn that was either fresh or 10 days old. The researchers found that people eating from the extra-large popcorn containers ate 45-50% more than those eating from the large ones. Participants even ate 40-45% more stale popcorn when it was served in bigger containers.

![img](/assets/images/54-Mindless-Eating.webp)

Food descriptions affect your food intake as well. Wansink described how researchers were able to help a cafeteria boost its business. Using creative terms that appeal to the senses, “seafood filet” and “chocolate cake” became “succulent Italian seafood filet,” and “Belgium black forest double chocolate cake” on the menu. People making selections from these more descriptive menus were overwhelmingly more enthusiastic about the food they received. Those making their selections from non-descriptive menus were mostly disappointed. Research shows that this common advertising technique, called descriptive labeling, not only attracts customers to selected menu items, but also causes them to eat more.

Losing track of how much you’re eating also leads you to eat more. In one study, students at a Super Bowl party in a restaurant were given free all-you-can-eat chicken wings. Plates were bused from some of the tables while bones were left to pile high on others. Those whose plates were not bused ate less. Participants from the bused tables seemed to have a harder time judging how much food they were eating. The researchers concluded that those people who saw reminders of what they were eating consumed less in the end. This conclusion was confirmed by another study showing that people wound up eating less candy when they saw their empty wrappers pile up as they ate.

Another interesting finding Wansink described is that healthier food doesn’t always lead to healthier eating. Most people know that olive oil is a healthier fat than butter, but it’s not healthier if you eat a lot more of it. Researchers gave a group of diners at an Italian restaurant either butter or olive oil with their bread. Those with the olive oil consumed an average of 16% more fat with each slice of bread. However, they did eat 19% less bread. Wansink stressed the importance of focusing not only on the targeted food but also on the companion foods. Think about eating a healthier meal rather than focusing on separate parts of the meal.

Eating healthy begins with what you buy in the first place. Wansink explained that we are highly influenced by quantities listed in signs. Our minds tend to anchor on the numbers that are suggested to us, and we then adjust our purchase from there. That’s why signs often list items like “3 for $3.00” rather than just saying “$1.00 each.” These signs can end up as much as doubling how much we buy, because we tend to focus only on what to buy when we go shopping, not how much to buy. If you bring home more food than you need, you’ll be tempted to eat more.

“By encouraging healthy, mindful eating, we can decrease obesity,” Wansink said. A keen awareness of all these hidden persuaders is an important step in controlling the amount and quality of food you eat.

**Questions for dining out**
Can you please:

- remove the bread basket?
- serve fat-free (skim) milk rather than whole milk or cream?
- trim visible fat from poultry or meat?
- leave all butter, gravy or sauces off a dish?
- serve salad dressing on the side?
- accommodate special requests?
- use less cooking oil when cooking?

Source: National Heart, Lung, and Blood Institute, NIH

***A Word to the Wise...***

*Tips to Avoid Mindless Eating:*

- Be aware of the size and shape of containers. It’s the amount of food that counts, not what it looks like.
- Serve food on smaller plates and bowls. Empty plates and bowls cue some people to stop eating.
- Use nutrition labels, paying attention to the serving size listed. Consider the facts; don’t guess at how many calories you’re eating.
- Look past the packaging. A food’s package or the language on a menu can lead you to actually like a food better, increasing your chance of overeating.
- Keep visual reminders of how much you’re eating. Keep wrappers, empty containers, bones and other reminders of how much you’ve eaten nearby.
- Think about eating a healthier meal. Don’t just focus on separate parts of the meal.
- Control your purchases. Don’t let signs lead you to buy more than you need.
