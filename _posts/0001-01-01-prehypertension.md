---
layout: post 
title: "Prehypertension"
date: 2019-03-14
image: assets/images/prehypertension.webp
excerpt: "A Little Too Much Pressure, A Lot of Trouble"
tags: blood pressure, Hypertension, NHLBI, Prehypertension
draft: false
---

The advice for keeping a healthy blood pressure has long been to exercise, lose weight, eat healthy foods and cut back on salt. But what doctors consider to be a healthy range for blood pressure has now changed significantly, according to an expert panel assembled by NIH’s National Heart, Lung, and Blood Institute (NHLBI). A review of the latest evidence led the panel to establish a new category, “prehypertension”, to warn people whose blood pressure readings place them at higher risk for serious health problems. That’s why it’s more important than ever for people to have their blood pressure taken regularly and to understand the reading.

“The first step in preventing and/or controlling high blood pressure is to know your blood pressure reading in numbers, not just in words,” says Dr. Ed Roccella, coordinator of the National High Blood Pressure Education Program, a component of NHLBI. Knowing your numbers will help you assess what you need to do to lower your risk of developing future health problems. Dr. Roccella explains, “People must be aware that an elevated or rising blood pressure number is cause for action.”

**Reading the Numbers**
Blood pressure readings are given in two numbers — “systolic” over “diastolic”.Systolic pressure, the top number in a blood pressure reading, is the force of blood in the arteries as the heart beats. Diastolic pressure, the bottom number, is the force of blood in the arteries as the heart relaxes between beats. Both numbers are important to help your doctor determine your risk of health problems.

People with blood pressure 140/90 and over are said to have high blood pressure, or hypertension. Before now, most people with blood pressure readings lower than 140/90 were considered to be in the normal blood pressure range. However, in an extensive review of more than 30 medical studies worldwide during the last 6 years, a scientific panel learned a lot more about the risks associated with rising blood pressure.

The panel found that problems in the cardiovascular system, (the heart and blood vessel system that carries blood throughout the body) can begin at much lower blood pressure levels than previously believed. Studies have shown that the risk of death from heart disease or stroke can begin to rise when blood pressures increase past 115/75. In addition to heart attack and stroke, elevated blood pressure can lead to several other serious health conditions, including kidney disease. And the damage only gets worse as people age and their rising blood pressure becomes more difficult to treat.

That’s why the panel developed a new range — called “prehypertension” — for blood pressure readings between 120/80 and 139/89. People who have readings in this range are now encouraged to adopt lifestyle changes to help lower their blood pressure and hopefully prevent hypertension.

**Changing Your Lifestyle**
The main goal of establishing the new prehypertension category, Dr. Roccella says, is to alert people and their doctors that early action can prevent serious health consequences later.

According to the panel’s report, 122 million people in the United States are overweight or obese, which adds to the rise in blood pressure. Changing the way you eat and getting more exercise can make a big difference.

You can start by cutting back on the amount of sodium in your diet. That means not only resisting the salt shaker, but also reading food labels more carefully when shopping; many canned and packaged foods contain a lot of sodium.

Use the Dietary Approaches to Stop Hypertension (DASH) eating plan as a guide. DASH encourages you to eat more fresh fruits, vegetables and low fat dairy products, and to limit saturated fat and salt. The DASH eating plan can help you lose weight and maintain a healthier body. In fact, according to the report, sticking to the DASH eating plan can be as effective as some medications in lowering your blood pressure.

Reducing the amount of alcohol you drink is another good way to help lose weight and lower blood pressure. Yet another proven way to help lower your risk of hypertension is increasing your daily exercise.

If you already have high blood pressure, your doctor may prescribe medications to help control it. But even if you take medication regularly, the changes you make in your eating habits and exercise regimen can work with your medicine to help you maintain a healthy blood pressure.

Dr. Roccella says, “People with hypertension can work with their doctors to select an appropriate regimen of lifestyle changes and medications to control their high blood pressure.”

**The Silent Killer**
High blood pressure usually doesn’t cause symptoms, so many people pay little attention to their blood pressure until they become seriously ill. It’s important to know your blood pressure numbers so that you can take action to keep your numbers in a safer range.

“Changing one’s lifestyle — such as losing weight if overweight, increasing physical activity and reducing salt intake — can prevent the progressive rise in blood pressure and even lower it,” Dr. Roccella concludes. “Raising awareness among patients and the public is a key step to prevent and control high blood pressure, an important public health problem.”

***A Word to the Wise...***

High blood pressure (also known as “hypertension”) can almost always be prevented, according to NIH’s National Heart, Lung, and Blood Institute (NHLBI). The following steps are important even if you do not have high blood pressure:

- Maintain a healthy weight.
- Be physically active.
- Follow a healthy eating plan.
- Eat foods with less sodium (salt).
- Drink alcohol only in moderation.
- Take prescribed drugs as directed.
