---
layout: post 
title: "Understanding Obsessive Compulsive Disorder"
date: 2022-01-03T00:54:23+01:00
image: assets/images/understanding-obsessive-compulsive-disorder.webp
excerpt: Do you spend too much time thinking about things you don't want to?
tags: featured
draft: false
---

It’s common to worry about things like germs or to double check that the stove is turned off. But for people with obsessive-compulsive disorder (OCD), these thoughts and behaviors are so severe that they interfere with daily life.

OCD is a mental health condition that causes repeated unwanted thoughts, called obsessions. This can trigger compulsions—the urge to do things over and over to deal with the troubling thoughts. You don’t need to have both to have OCD.

Many people with OCD have a fear of germs or contamination. This can lead to obsessive thoughts about things being “dirty.” Some people may feel a need for things to be symmetrical or in a perfect order. Worries about harm to yourself or others are also common. In some cases, these unwanted thoughts can be violent or disturbing.

“An obsession is an intrusive, distressing thought that usually kids or adults with OCD are able to recognize as a fear that doesn’t make a ton of sense,” explains Dr. Kate Fitzgerald, an OCD expert at the University of Michigan. “But these intrusive thoughts tend to cause them much anxiety.”

People with OCD may develop rituals meant to relieve their anxiety from the thoughts. This could involve behaviors like excessive handwashing or cleaning, arranging things in a certain order, or compulsive counting.

Many of us are a little “obsessive.” So when is there cause for concern? The biggest sign is if these thoughts or habits are making it hard to function in your day-to-day life, explains Fitzgerald.

This can mean problems with family, work, or school. Spending more than one hour a day on thoughts or behaviors can indicate a problem. See the Wise Choices box for more signs and symptoms.

You may have heard someone say that they’re “OCD” about cleaning or organizing. But OCD is a debilitating disorder.

“Those terms are just kind of out there in the popular culture without recognizing that true OCD can paralyze people,” notes Fitzgerald. “There are people who can’t work, can’t go to school, can’t function because of the illness.”

People usually develop symptoms of OCD as a child or young adult. Most people are diagnosed by about age 19. But childhood onset of OCD often occurs as early as eight or nine years old.

Scientists aren’t sure what causes OCD. But genetics are likely to play a role. People with a parent or sibling with OCD are at a higher risk for developing the disorder themselves.

OCD is usually treated with antidepressant medication and psychotherapy. One of the most common forms of psychotherapy for OCD involves exposing people to their triggers. Therapists then help people overcome their compulsive thoughts or actions. This form of therapy is called exposure and response prevention.

Although psychotherapy helps many people with OCD, it isn’t effective for everyone. Fitzgerald and her team are trying to learn more about who responds to psychotherapy. Her studies suggest that people with certain patterns of brain activity are more likely to benefit. One day, brain scans or other testing might help match people with OCD to the best treatment.

### Wise Choices

#### Is it OCD?

Everyone double checks things sometimes. But a person with OCD generally:

- can’t control his or her thoughts or behaviors, even after recognizing that they’re excessive. 
- spends at least one hour a day on these thoughts or behaviors.
- doesn’t get pleasure when performing the behaviors or rituals, although they may bring brief relief from anxiety.
- experiences significant problems in his or her daily life due to these thoughts or behaviors.
