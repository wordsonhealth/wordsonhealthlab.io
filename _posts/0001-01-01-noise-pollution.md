---
layout: post 
title: "Noise Pollution"
date: 2019-06-12
image: assets/images/noise-pollution.webp
excerpt: "A Different Environmental Problem"
tags: hearing, Hearing Loss, Loud sounds, NIDCD, Noise Pollution, outdoor noise
draft: false
---

Your mother was right when she told you to turn down the volume. Too much noise not only pollutes the environment; it can permanently damage your hearing.

Some 22 million Americans between the ages of 20 and 69 have already permanently damaged their hearing by exposure to loud sounds. And research is finding that an ever-increasing number of young people have the hearing loss typically found in older adults.

It’s easier than you think to permanently damage your hearing. The blast of a firecracker at close range can do it in an instant. Repeated exposures to loud engines like motorcycles or long hours spent listening to loud MP3 and other portable music players can erode hearing more slowly.

![img](/assets/images/81-Noise-Pollution.webp)

If you’re a construction worker, farmer, factory worker or airline employee, harmful sounds may be a regular part of your job. Harmful noises at home include vacuum cleaners, gas-powered lawn mowers, leaf blowers and shop tools. Noisy recreational activities include target shooting and hunting, snowmobiling, riding go-carts, woodworking and other noisy hobbies. Even some children’s toys produce sounds in the danger zone.

How loud is too loud? Prolonged exposure to sounds louder than 85 decibels (dB) can cause gradual hearing loss. A normal conversation is about 60 dB. Many personal stereo systems at maximum level are over 100 dB. Rock concerts and firecrackers can be 140 dB and higher.

Noise-induced hearing loss usually happens slowly, with no pain. Right after exposure to noise, you may notice some “ringing” in your ears. You might have trouble hearing people talk. After several hours or even a few days, these symptoms may go away. However, when you are exposed to loud noise repeatedly, you could have hearing loss that lasts forever.

Exposure to loud sounds can damage or destroy the inner ear’s sensory hair cells. Once damaged, the hair cells don’t grow back. Scientists once believed that loud noises damage the hair cells by the pure force of the loud sound vibrations. Recent studies, however, have found that exposure to noise triggers the formation of molecules called free radicals that are known to kill off hair cells.

Scientists supported by NIH’s National Institute on Deafness and Other Communication Disorders (NIDCD) have shown that antioxidants such as aspirin and vitamin E, which can protect against damage caused by free radicals, can reduce hearing loss in guinea pigs when given as much as three days after noise exposure. Future studies will explore if this strategy works in humans.

One day, gene transfer may be used to help restore lost hearing. NIDCD-supported researchers transferred a gene involved in the regrowth of hair cells into deaf guinea pigs and restored hearing. This type of therapy, however, is still a long way from human use.

To protect your hearing, practice good hearing health in your everyday life. Turn down the volume on all household noise sources and wear hearing protection when you mow the lawn, vacuum, blow dry your hair or operate power tools. Encourage children to wear hearing protection in noisy environments and take the time to show them how to prevent hearing damage from MP3 and other portable music players. Make hearing health a part of your lifestyle.

*Definitions:*

**Decibel:**
A unit used to describe how loud something is.

***A Word to the Wise...***

*Protect Your Hearing!*

- Know which noises can cause hearing damage.
- Wear earplugs, earmuffs or other protective devices when involved in a loud activity.
- Teach your children to lower the volume on their portable music players and to limit listening time.
- Be alert to hazardous noise in the environment.
- Protect children who are too young to protect themselves.
- Tell family, friends and colleagues about the hazards of noise.
- If you think you have a hearing loss, see your doctor.
