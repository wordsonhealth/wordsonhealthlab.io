---
layout: post 
title: "Hiv and Aids Still Gaining Strength"
date: 2019-03-02
image: assets/images/hiv-and-aids-still-gaining-strength.webp
excerpt: "HIV/AIDS: Facts on Symptoms, Signs, Treatment, Prevention and More"
tags: AIDS, HIV, HIV/AIDS, Minorities, Women
draft: false
---

No matter who you are or where you live, there are three things you should know about Acquired Immune Deficiency Syndrome (AIDS), according to Dr. Victoria Cargill of the Office of AIDS Research at the National Institutes of Health (NIH). Know how it’s spread, know when and how to get help, and know that your life can go on productively even if you test positive.

Despite the success and availability of drug therapies that have sharply cut the death rate from AIDS in the U.S., the epidemic continues to gain strength in some groups such as women and minorities and in some areas such as urban and poor neighborhoods and in rural areas of the southeastern U.S. Racial and ethnic minorities — especially African Americans and Latinos — continue to account for a disproportionate number of the new cases of HIV/AIDS in the U.S. across all categories — women, men and adolescents. Almost 80 percent of the HIV/AIDS cases diagnosed in women are in African Americans and Latinas.

“There are several reasons why these groups are hardest hit,” Dr. Cargill says. “There continue to be knowledge gaps in racial and ethnic communities about how HIV, the virus that causes AIDS, is spread.”

The NIH Office of AIDS Research plans, coordinates, evaluates and funds all of the agency’s AIDS research. In addition to coordinating the office’s HIV/AIDS research program on racial and ethnic minorities, Dr. Cargill treats people who have HIV/AIDS in the Anacostia area of Washington, D.C.

Most people know that HIV can be spread in several ways: from mother to child at birth; via unprotected sex with an infected person; or by sharing needles with someone who is infected. “We have been very successful in lowering mother-to-child transmission rates in the U.S.,” she continues, “but that can only work if expectant mothers get prenatal care. We have to continue to tell pregnant women to see a doctor and get tested. We have to emphasize to everyone that any unprotected sexual contact can spread the virus. We have to stress that blood contamination can come not only via needles used for injecting drugs, but also needles used for piercing, as well as any drug paraphernalia — the cotton, the cooker, the spoon. Any of these can become contaminated with blood, and therefore any of them can become a vehicle for HIV infection.”

**Disparities in Survival Rates**

Research data point to important reasons for the disparity in survival rates among different populations with HIV/AIDS. African Americans experience the longest delays in starting the most powerful anti-HIV drug regimens and they are the least likely to use them consistently enough for maximum benefit, according to results of a recent study conducted among Medicaid recipients with AIDS by the New Jersey Medicaid program. Researchers there investigated patients’ use of antiretroviral AIDS treatments from 1996 to 1998. The study, funded by grants from NIH, revealed significant disparities along ethnic lines. African Americans delay the initiation of powerful anti-HIV therapy an average of eight months longer than whites. Both African Americans and Latinos are less likely than whites to remain on the drugs once therapy is started.

“Minorities tend to show up for HIV testing and begin treatment much later,” Dr. Cargill says. “By the time these patients present for HIV care, they tend to have more advanced disease. They have been sicker much longer. They also tend to have more difficulty with adherence to the treatment program. This is very important. HIV/AIDS is not like some disorders where you can miss a dose and your disease is still basically under control. People don’t realize that if they skip even one dose, HIV suppression decreases and the virus can begin to replicate at a rapid rate. Successful treatment requires commitment by both the patient and the provider.”

Other factors including poverty, racism, drug abuse, and unequal access to healthcare are also being studied as possible reasons for the growing burden of HIV/AIDS among minorities, particularly in inner cities.

**Testing, Prevention and Treatment**

Dr. Cargill concludes that the only way to improve survival rates for HIV/AIDS in all communities is to develop more diligent efforts toward prevention and more realistic attitudes toward treatment.

“HIV/AIDS can be treated, and the treatments are very effective,” she says. “The new generation of drug therapies continues to improve at delaying the onset of AIDS and allowing people to go back to full and productive lives. There’s a catch, though. Patients must get regular care and they must take the medications as prescribed. We could save so many more lives, if people only understood the benefit of treatment.”

Doctors who regularly treat people with HIV/AIDS highly recommend a test for anyone who suspects they might have been exposed to the virus — by having unprotected sex or sharing needles, for example — and begins to experience low-grade fever, night sweats, weakness, fatigue or a rash within six weeks or so.

“This six-week window is a very important period,” Dr. Cargill stresses. “During this early stage, sometimes called the acute retroviral syndrome or primary HIV infection, people produce millions of virus particles (called virions) and they are highly infectious. If we can get more people diagnosed during this early stage, we can use therapy to delay the onset of HIV-associated complications, as well as AIDS. More importantly, we can help prevent the spread of HIV infection to other people.”

Dr. Cargill thinks that even if you think you’re at very low risk, you should consider getting tested to make sure of your HIV status. She has a special message for people over age 50. “Think about getting tested,” she urges. “HIV did not forget you. HIV has not overlooked anyone — not Baby Boomers, not women who are nearing or past menopause, not older people. With the increased prescription of agents for erectile dysfunction in recent years, we’ve seen increases in sexual behavior among older adults. With that increase, we’ve also seen the number of HIV/AIDS cases in people over age 50 rise dramatically, particularly among racial and ethnic minorities.”

Anonymous, confidential testing and counseling sites are located all over the nation, Dr. Cargill points out. “There is no reason to avoid being tested.”

***A Word to the Wise...***

*Getting Tested for HIV*

Dr. Victoria Cargill of the Office of AIDS Research at the National Institutes of Health (NIH) thinks although it’s a good idea for everyone to know their HIV status, there are some people in particular who should consider getting tested:

- Anyone who has had a sexually transmitted disease, especially syphilis, in the last 10 years.
- Anyone who has had unprotected sex with an infected person.
- Anyone who has shared needles with someone who is infected.
- Anyone who has had sexual contact with someone who has used recreational drugs. “Even if it was not injection drug use,” Dr. Cargill warns. “Data shows that other illicit drugs such as cocaine have side effects, including hypersexuality, which can lead to indiscriminate behavior and ultimately to sexually transmitted infections such as HIV.”
- Anyone who has been incarcerated or had sexual contact with someone who has been incarcerated. “We know that prisons are major amplification centers for HIV infection and AIDS,” Dr. Cargill explains.
- Anyone who has exchanged sex for money, or sex for drugs.

In addition, there are some medical problems that can be indicators of HIV infection. For example, women who have repeated yeast infections within a year that are not related to diabetes or taking birth control pills. Or adults diagnosed with oral thrush who are not undergoing cancer chemotherapy or have not had organ transplantation. Or people under age 40 who develop shingles or herpes zoster and have no other associated medical conditions. In these cases, you should strongly consider taking an HIV test. If you have concerns, please talk with your doctor about getting tested.
