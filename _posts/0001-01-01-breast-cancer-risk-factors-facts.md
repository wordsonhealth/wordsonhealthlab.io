---
layout: post 
title: "Breast cancer: Risk factors & Facts"
date: 2019-01-20
image: assets/images/breast-cancer-risk-factors-facts.webp
excerpt: "Mammograms: Not Just Once, But for a Lifetime"
tags: breast cancer, breast disease, invasive breast cancer, mammograms
draft: false
---

Confused about when to start and how often you should have screening mammograms to detect breast cancer? You don’t have to be.

For most women, the National Cancer Institute recommends regular screening mammograms every one to two years starting in their forties or older. Women at increased risk may need to follow a different schedule based on their doctors’ recommendations.

Routine mammograms are the best way to detect breast cancer early, before a lump is felt and when treatment is most likely to be effective. Regular mammograms are especially important as women grow older, since most breast cancer occurs in women over 50.

Approximately 266,000 women in the United States were diagnosed with breast cancer, and about 40,000 died from the disease. Detecting breast cancer at the earliest possible stage is the best thing you can do to increase your chances of surviving the disease.

The absolute risk of developing breast cancer during a particular decade of life is lower than 1 in 8. The younger you are, the lower the risk. For example:

- If your current age is 20, the probability of developing invasive breast cancer in the next 10 years is .06%, or 1 in 1,732. This means that 1 in 1,732 women in this age group can expect to develop breast cancer. Put another way, your odds of developing breast cancer if you are in this age range are 1 in 1,732.
- If your current age is 30, the probability of developing invasive breast cancer in the next 10 years is .44%, or 1 in 228.
- If your current age is 40, the probability of developing invasive breast cancer in the next 10 years is 1.45%, or 1 in 69.
- If your current age is 50, the probability of developing invasive breast cancer in the next 10 years is 2.31%, or 1 in 43.
- If your current age is 60, the probability of developing invasive breast cancer in the next 10 years is 3.49%, or 1 in 29.
- If your current age is 70, the probability of developing invasive breast cancer in the next 10 years is 3.84%, or 1 in 26.

As you can see, the older you are, the higher your absolute risk of breast cancer. Keep in mind that these numbers and percentages are averages for the whole population. Your individual breast cancer risk may be higher or lower, depending on a number of factors.

**Risk Factors for Breast Cancer:**

One or more of the following conditions puts a woman at higher than “average risk” for breast cancer:

- You’ve had breast cancer before.
- Laboratory evidence has confirmed that you’re carrying a specific mutation or genetic change that increases your susceptibility to breast cancer (BRCA1/BRCA2 mutations).
- Your mother, sister, daughter or two or more close relatives, such as cousins, have had breast cancer (especially if diagnosed at a young age).
- You’ve been diagnosed with another type of breast disease (not cancer, but a condition–atypical hyperplasia–that may predispose you to cancer), or you’ve had two or more breast biopsies for benign disease, even if no abnormal cells were found.
- You are 45 years of age of older and have at least 75 percent dense breast tissue on a mammogram.
- You had your first child at age 30 or older, or have never had children.
- You’ve received chest irradiation for conditions such as Hodgkin’s disease at age 30 or younger

To help women get the facts, NCI has developed information and materials about mammograms, including information about the risk factors for breast cancer and screening recommendations for women of different ages. Consider the following facts about breast cancer:

**FACT**: Except for skin cancer, breast cancer is the most frequently diagnosed cancer in women in this country. Breast cancer is second only to lung cancer in cancer-related deaths.

**FACT**: A woman’s risk for breast cancer increases with age and continues to increase over her lifetime.

**FACT**: Most women who get breast cancer have no known risk factors, such as family history of the disease.

**FACT**: Scientific studies have shown that regular screening mammograms can help decrease the chance of dying from breast cancer. The benefits are greater for women over the age of 50, although women in their forties who have regular mammograms also may have a reduced risk of dying from breast cancer.

Women no longer need to be confused about following the best recommendations for detecting early breast cancer. Increasing your chances of survival from breast cancer could be as simple as “getting the facts.”

***A Word to the Wise...***

- If you are in your 40s or older, get a mammogram on a regular basis, every 1 to 2 years.
- Talk with your doctor or health care professional about planning your personal schedule for screening mammograms and breast exams and stick to it.
- Obtain as much information as you can about your family history of cancer, breast cancer and screening mammograms.
- Talk to your doctor or health care professional about the known risk factors for breast cancer.
- For women eligible for Medicare benefits, Medicare will help pay for one screening mammogram every year.
