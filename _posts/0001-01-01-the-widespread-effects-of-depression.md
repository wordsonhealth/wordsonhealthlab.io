---
layout: post 
title: "The Widespread Effects of Depression"
date: 2019-04-16
image: assets/images/the-widespread-effects-of-depression.webp
excerpt: "Tips for Coping With Depression"
tags: Depression, Mental health, Mental illness, NIMH
draft: false
---

Depression is one of the leading causes of disability worldwide. That’s the word from NIH’s National Institute of Mental Health (NIMH), the component of the federal government that studies depression and other mental illnesses.

You probably know depression as a medical condition that primarily affects the brain. Its symptoms include a persistent sad, anxious or “empty” mood, feelings of hopelessness, pessimism and worthlessness, and a loss of interest in hobbies and activities once enjoyed. The “Symptoms of Depression” side box contains a more complete description.

But according to Dr. Husseini Manji, chief of NIMH’s Laboratory of Pathophysiology, the psychological symptoms of depression are just the “tip of the iceberg.” Because the brain is the body’s “control center,” the effects of depression spread throughout the body, often resulting in problems with sleep, appetite, energy level, motivation, memory, and concentration. Performing everyday activities can be an enormous challenge for people who are depressed.

**A Devastating Illness**

“Depression needs to be recognized as a devastating illness,” Dr. Manji explains. “It can occur with other diseases, but it is a very real medical condition in its own right.”

![img](/assets/images/53-The-Widespread-Effects-of-Depression.webp)

Research shows that depression increases the risk of death for people of all ages. For those with other illnesses such as type 2 diabetes and certain infections, depression can make their symptoms worse. Elderly people with depression may be at higher risk for Alzheimer’s disease, and depression may increase their chance of being admitted into a nursing home.

No one knows better the ravages of depression than the estimated 20 million Americans of every age who suffer from depression. Although women and older people seem to have higher rates of depression, depression can strike anyone at any time. Those who have recently experienced a traumatic event, such as a divorce, job loss or sudden death of a loved one, may be at higher risk.

**More Than Stressed Out**

Dr. Manji emphasizes that depression is not a character flaw, a lack of willpower or a sign of emotional weakness. “You can’t simply wish or will depression away,” he says.

People who are “stressed out” may think that their current situation is to blame, but a prolonged case of the blues that interferes with normal functioning is usually the result of a chemical imbalance in the brain,” he explains.

That’s why treatment is so important — and the sooner the better. There are a variety of treatments that work, including medications and psychotherapy. NIMH researchers and others are constantly looking at new ways to treat and prevent depression.

If you think you may be depressed, seek professional help (see “Where to Get Help”) and learn ways to cope to help you feel better (see “Tips for Coping With Depression”). Don’t let depression keep you down.

**Where To Get Help**

- Family doctors
- Mental health specialists, such as psychiatrists, psychologists, social workers, or mental health counselors
- Health maintenance organizations
- Community mental health centers
- Hospital psychiatry departments and outpatient clinics
- University – or medical school – affiliated programs
- State hospital outpatient clinics
- Family service, social agencies, or clergy
- Private clinics and facilities
- Employee assistance programs
- Local medical and/or psychiatric societies

Source: National Institutes of Mental Health

**Symptoms of Depression**

Symptoms vary from person to person and vary over time. Not everyone who is depressed has every symptom. Some people have a few, and some have many.

- Persistent sad, anxious or “empty” mood
- Feelings of hopelessness, pessimism
- Feelings of guilt, worthlessness, helplessness
- Loss of interest or pleasure in hobbies and activities that were once enjoyed, including sex
- Decreased energy, fatigue
- Difficulty concentrating, remembering and making decisions
- Insomnia, early-morning awakening, or oversleeping
- Appetite and/or weight loss or overeating and weight gain
- Thoughts of death or suicide, suicide attempts
- Restlessness, irritability
- Persistent physical symptoms that do not respond to treatment, such as headaches, digestive disorders, and chronic pain

Source: National Institutes of Mental Health

***A Word to the Wise...***

*Tips for Coping With Depression*

Depression can make you feel exhausted, worthless, helpless, and hopeless. Negative thoughts and feelings can make some people feel like giving up. It is important to realize that these negative views are part of the depression and typically do not reflect the actual circumstances. Negative thinking begins to fade as treatment takes effect. In the meantime:

- Break large tasks into small ones, set some priorities, and do what you can.
- Try to be with other people and confide in someone; it is usually better than being alone.
- Participate in activities that make you feel better. Mild exercise, going to a movie, a ball game, or participating in religious, social, or other activities may help.
- Expect your mood to improve gradually. Feeling better takes time. People rarely “snap out of” depression, but they can feel a little better day-to-day.
- Postpone important decisions until the depression has lifted. Before deciding to make a significant decision, such as changing jobs, getting married or divorced, discuss it with others who know you well and have a more objective view of your situation.
- Remember, as your depression responds to treatment, positive thinking will replace the negative thinking that is part of the depression.
- Let your family and friends help you.
