---
layout: post 
title: "Sleep Deprivation"
date: 2019-01-22
image: assets/images/sleep-deprivation.webp
excerpt: "A Good Night's Sleep? Merely a Dream for Millions"
tags: Insomnia, problem sleepiness, sleep deprivation, Sleep Disorders
draft: false
---

“To sleep–perchance to dream,” wrote Shakespeare in his masterpiece play, Hamlet. It’s a nice concept. However for many men, women and children, the elusive road to slumberland is anything but a dream.
For many, the road is paved with obstacles–often a sleep disorder, ranging from insomnia to restless legs syndrome (RLS) to sleep apnea–where individuals usually snore, experience fitful sleep, and may stop breathing for short periods, in some instances hundreds of times a night. The consequences of sleep deprivation, specifically the “problem sleepiness” during the day that normally follows, can have extremely serious, even life-threatening consequences.

Considering we spend nearly one-third of our lives tucked under the sheets, you would think we would know how to get a good night’s rest. Not so for many. If you have sleep difficulties, you’re not yawning alone–chances are some of your family members, coworkers, and neighbors also have a “sleep debt,” the cumulative effect of not getting the quantity or quality of sleep that one needs. As many as forty million Americans are afflicted with more than 70 different types of sleep-related problems.

While some sleep disturbances may be linked to biological changes associated with aging or certain physical diseases, especially those that cause pain, others may be associated with a mental health disorder such as depression or anxiety. Poor sleep may also stem from “bad” habits such as napping too long or too late in the day, or doing shift work, which applies to nearly one quarter of the population, according to the National Center on Sleep Disorders Research. The Center is part of the National Heart, Lung, and Blood Institute, a unit of the National Institutes of Health (NIH). On the other hand, you may simply not be giving yourself the opportunity to acquire ample shuteye.

“What we can say with certainty is that there is a pervasive nature of sleep deprivation out there–no question about it. It’s part of how our society functions,” noted James P. Kiley, Ph.D., Director of the National Center on Sleep Disorders Research.

Why isn’t America getting a better night’s rest? “It’s a two-part problem,” the NIH scientist explained. “First, we have a society that’s on a 24-hour cycle–with multiple jobs in many cases and multiple responsibilities both at work and home. When you’re pushed for time, as many people are, the first thing that usually goes is sleep.” However, when you sacrifice hours this way, you frequently end up paying for it in terms of decreased productivity and an increased risk for errors in judgment and accidents, according to Dr. Kiley.

He said that the second part of the problem relates to actual sleep disorders. Insomnia–the inability to fall asleep and remain there–affects many millions of people. “For sleep apnea, easily another 10 to 15 million. Narcolepsy (falling asleep uncontrollably during the day), perhaps 250,000. We don’t even know how many people have restless legs syndrome (RLS). In general, society is not well rested, and looking at these numbers and their causes, you begin to see why,” Dr. Kiley explained.

While people of any age may be affected, there seems to be a large prevalence of sleep disturbances among elderly men and women. Sleep studies reveal that they get less REM (deep) sleep over time. With aging, sleep becomes more fragile, that is, it doesn’t take much disturbance to awaken the individual. Women may first notice this during menopause.

Lack of sleep and its link to accidents–automobile and on-the-job–now appears to be a problem of far greater magnitude than previously believed. Fatigue leads to diminished mental alertness and concentration. According to Dr. Kiley, it’s the resultant “near miss” (in a motor vehicle or otherwise) that sometimes makes people recognize they have a problem and need to seek professional help. He says there could be as many as 1,500 fatalities and one hundred thousand sleep-related automobile accidents annually in the United States. Shift workers are especially prone to this problem. “Their biological clock is ticking at the wrong time, and they typically drive home after work when they’re extremely tired. Young males under 25 also have a disproportionate number of auto accidents related to sleepiness. We want to target them through education; in fact, we’re currently working on a program with the U.S. Department of Transportation that we hope will be very effective in this area,” said Dr. Kiley.

What about napping? In some countries, a siesta or short daytime rest is a respected, time-honored daily ritual. Dr. Kiley also indicated it may have an important role. “With older people in particular, napping is a good practice. Because their sleep is fragmented and they get less of it at night, they typically make up for it with naps during the day. Napping works, it definitely has a role,” he explained, adding that it can increase productivity and help restore your ability to think.

What about waking up too early, like before the birds’ first chirp? While such “early morning awakenings” may be a sign of depression or other treatable emotional disorder, the passage of time may be the culprit. “As you age, your biological clock ticks at a slightly different rate. Because of this, you run into people with an advanced sleep syndrome–they go to bed early and then wake up too early,” said Dr. Kiley. “Again, sleep is very fragile with age and we really don’t know why.” In some cases, going to bed a bit later may help reset your biological clock and allow you to sleep later.

How many hours per night should you sleep? NIH sleep experts believe you should be obtaining somewhere in the range of 7 to 8 hours of sleep a night. This figure varies considerably across the age span and from person to person. Still, if you’re getting less than 6 hours of sleep per night regularly, chances are you’re building up your “sleep debt,” and may be compromising your health and welfare, sleep authorities contend.

If you’re having chronic sleep difficulties, should you merely lie there and take it? No. Dr. Kiley suggests you practice sensible sleep habits (see “A Word to the Wise”). However, if you’ve done all you can and still aren’t getting good, quality sleep, talk with your family doctor. If you need additional help, ask for a referral to a sleep specialist. This may be needed, in particular, for more complex conditions such as narcolepsy. While this disease is not curable, it is treatable, though the regimen with carefully prescribed medications is complicated, and best handled by a sleep specialist. On the other hand, “we’ve made great strides in the sleep apnea area. General practitioners now do a pretty good job of diagnosing this condition. In the next 5 to 10 years we would like to have every physician as familiar with the other sleep disorders as they now are with apnea,” Dr. Kiley concluded.

The Greek philosopher Sophocles once remarked that “sleep is the only medicine that gives ease.” As researchers seek to unravel the remaining mysteries surrounding sleep, many more men, women, and children should soon find a night in the bed a more pleasant pill to take–rest assured.

***A Word to the Wise...***

There is no surefire method that guarantees a good night’s sleep–not even a warm bath or a warm glass of milk. However, experts now recognize that there are some factors that may either help promote or discourage a peaceful night’s sleep. If your sleep problems persist, be sure to see your family physician or a sleep expert. Most problems can be treated with good results.

**DO:**

- Exercise regularly, but not within 4 to 6 hours of going to bed.
- Take a short nap during the day if you’re tired, but keep it brief. And do it early in the day.
- Take a rest break if you feel tired while driving. Fresh air or loud music won’t keep you alert, but a short nap may do the trick, especially if you combine it with caffeine, says Dr. James Kiley of the National Center on Sleep Disorders Research.
- Try to relax and let nature take its course. If that doesn’t help you fall asleep, get up and move around. Go to another room and read, watch television or listen to music. Don’t take sleeplessness lying down.
- Go to sleep and get up at the same time each day, even on weekends and holidays.
- See a doctor for any medical conditions affecting your quality or quantity of sleep, such as arthritis, bronchitis, asthma, certain heart problems, hormone disorders such as hyperthyroidism, and sleep-related disorders like RLS and sleep apnea.

**DON’T:**

- Use alcohol for at least 2 hours before bedtime. It may make your drowsy at first, but after a few hours it can disrupt sleep.
- Overdo your consumption of caffeinated beverages or over-the-counter medicines during the day. Some pain-relieving medications contain caffeine–check the labels. Be aware that certain prescription medicines, such as those for asthma, some antidepressants, and anti-anxiety drugs may cause insomnia.
- Use the bedroom to pay bills, watch television, or discuss the problems of the day. The bedroom should be used only for sex and sleep.
- Rely on over-the-counter herbs, or other sleep-promoting substances whose safety and effectiveness have not been determined.
- Smoke, especially before bedtime. Nicotine can disrupt sleep and reduce total hours slept. Smokers report far greater sleepiness and minor accidents than do nonsmokers.
- Think that sleep problems cannot be treated. In most instances, there are effective treatments, even for older men and women or those with health problems.
