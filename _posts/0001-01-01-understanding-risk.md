---
layout: post 
title: "Understanding Risk"
date: 2019-03-22
image: assets/images/understanding-risk.webp
excerpt: "What Do Those Headlines Really Mean?"
tags: Clinical Trial, Medical research, New medical findings, NIA, Scientific Results
draft: false
---

Lately it seems to happen almost every day — you hear about a new result of medical research on television or read about it in the paper. Often it’s about your “risk” or chance of having a disease or health problem. After reading the story did you worry because the numbers made your chance of getting sick seem high? Or, did the story confuse you because you remember a news item not too long ago about the same health problem, but with a very different finding? What does it all mean? Which stories can you trust?

A new publication from NIH’s National Institute on Aging (NIA) might be able to help. Understanding Risk: What Do Those Headlines Really Mean? explains different types of scientific studies, different types of risk, and suggests some questions you can ask yourself when reading or listening to the next story about research results.

**Types of Studies**

The first step in understanding medical research reports is to know the difference between types of research studies. Some projects involve laboratory studies using microscopic cells or creatures such as yeasts, viruses, or bacteria. Some of these studies may not immediately seem relevant to humans, but very often studying other organisms helps us to understand our own bodies and what can go wrong with them to cause diseases and disorders.

Some studies involve animals like mice which can serve as a “model system” for how the human body works. Investigators looking at human diseases, though, eventually need to turn to studies in people.

When studying people, scientists often use observational studies. In these, researchers keep track of a group of people for several years without trying to change their lives or provide special treatment. This can help scientists find out who develops a disease, what those people have in common, and how they differ from the group that did not get sick. What they learn can suggest a path for more research. However, observational studies have certain weaknesses. Sometimes differences between groups are caused by something the investigators are not aware of. Only further research can prove for sure whether their finding is the actual cause of illness or not.

A randomized controlled clinical trial (RCT) is considered the best way to learn whether a certain treatment works or not. A clinical trial can involve thousands of human volunteers. They are assigned to two or more study groups by chance (randomly). One of the groups, the control group, receives a preparation such as a pill that looks just like the treatment or drug being tested, but actually does nothing (a placebo). This is the only way to really compare and see if the treatment is effective.

Clinical trials are usually conducted in phases. The trials at each phase have a different purpose and help scientists answer different questions: In Phase I trials, researchers test a new drug or treatment in a small group of people (usually 20-80) for the first time to evaluate its safety, determine a safe dosage range, and identify possible side effects. In Phase II trials, the study drug or treatment is given to a larger group of people (often 100-300) to see if it is effective and to further evaluate its safety. In Phase III trials, the study drug or treatment is given to even larger groups of people to confirm its effectiveness, monitor side effects, compare it to commonly used treatments, and collect information that will allow the drug or treatment to be used safely. In Phase IV trials, post-marketing studies gather additional information about the drug’s risks, benefits, and optimal use.

**How a Clinical Trial Works**

First, the scientists sign up volunteers. These volunteers are randomly divided into two groups. One receives the test drug, and the other, the control group, gets a placebo. The study is also masked, meaning that neither the doctors nor the volunteers know who is getting the test treatment or the placebo.

Then the investigators keep track of each group over time. They watch for side effects of the drug. At the end of the study period, everyone learns which group was getting the test drug and which was on placebo, and the results are analyzed.

For more information, or if you are interested in participating in a clinical trial, visit https://clinicaltrials.gov/.

A clinical trial, especially a larger one, sometimes yields different results than earlier studies in laboratories, in animals, or even in people. But, the clinical trial results will be more meaningful to you because the research involved people and because of the way these studies are designed.

**Understanding Scientific Results**

How do you understand the results you read in a news story? Different types of numbers can be used to explain the same facts, and the way these numbers are presented can change how you feel about the results. For example, let’s say you heard that only one percent of people in the U.S. had a disease. You might not think that was very much until you realize that that’s almost three million people! When you hear numbers like these, it’s important to try to put them in context to understand how important they really are.

When investigators report their findings, especially from large clinical trials, they often talk about risk. They might mention relative risk, absolute risk, or both. These are different ways to explain how likely someone is to have a certain health problem. Relative risk is usually shown as a ratio or a percent. An absolute risk is nothing more than a number found by subtraction. How these numbers are presented to you can sway how you feel about the finding and affect whether you change your behavior.

Consider an imaginary new drug. After testing this fictitious drug in a large clinical trial, the investigators learn that the relative risk of getting a certain side effect from the medicine is 2.0, which means the risk is increased by 100%. Sounds serious. But that doesn’t mean that all of the people using the drug will have that side effect. It means that twice as many people on the drug get the symptom as those not taking the drug (news reports often phrase it this way, too). So, if six people out of every 10,000 normally have a symptom in one year, then twelve people out of 10,000 using the medicine might have it. The absolute risk of this side effect is twelve minus six, or six more people out of every 10,000 using the medicine. Many people who really need a new medicine might not consider six in 10,000 a large enough risk to prevent them from taking it, even if the side effect is quite serious.

It’s important to be critical when reading or listening to reports of new medical findings. The next time you read or hear about a medical study in the news, ask yourself the questions in the accompanying list to see how important the finding may be to you. If you’re not sure, turn to your health care provider for help.

***A Word to the Wise...***

*When you learn about a new medical finding, ask yourself:*

- Was it a study in the laboratory, in animals, or in people? The results of research in people are more likely to be meaningful for you.
- Does the study include enough people like you? You should check to see if the people in the study were the same age, sex, education level, income group, and ethnic background as yourself and had the same health concerns.
- Was it a randomized controlled clinical trial involving thousands of people? They are the most expensive to do, but they also give scientists the most reliable results.
- Where was the research done? Scientists at a medical school or large hospital, for example, might be better equipped to conduct complex experiments or have more experience with the topic. Many large clinical trials involve several institutions, but the results may be reported by one coordinating group.
- Are the results presented in an easy-to-understand way? They should use absolute risk, relative risk, or some other easy-to-understand number.
- If a new treatment was being tested, were there side effects? Sometimes the side effects are almost as serious as the disease. Or, they could mean that the drug could worsen a different health problem.
- Who paid for the research? Do those providing support stand to gain financially from positive or negative results? Sometimes the Federal government or a large foundation contributes funding towards research costs. This means they looked at the plans for the project and decided it was worthy of funding, but they will not make money as a result. If a drug is being tested, the study might be partly or fully paid for by the company that will make and sell the drug.
- Who is reporting the results? Is the newspaper, magazine, or radio or television station a reliable source of medical news? Some large publications and broadcast stations have special science reporters on staff who are trained to interpret medical findings. You might want to talk to your health care provider to help you judge how correct the reports are.

The bottom line is: Talk to your doctor. He or she can help you understand the results and what they could mean for your health. Remember that progress in medical research takes many years. The results of one study often need to be duplicated by other scientists at different locations before they are accepted as general medical practice. Every step along the research path provides a clue to the final answer-and probably sparks some new questions also.
