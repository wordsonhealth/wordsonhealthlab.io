---
layout: post 
title: "Maintain Your Muscle Strength Training at Any Age"
date: 2022-01-02T19:22:47+01:00
image: assets/images/maintain-your-muscle-strength-training-at-any-age.webp
excerpt: Building muscle can keep your body working properly. Find out how to get started.
tags:
draft: false
---

You’ve likely heard that exercise can help you live a longer, healthier life. When you hear the word ‘exercise,’ you might think of going for a run or hopping on a bicycle. Or maybe playing soccer with your kids or basketball with your friends after work. But these activities don’t include all the types of movements that are important for your health.

The examples above are endurance exercise. Also called cardiovascular exercise, activities like these increase your breathing and heart rate. They can keep your heart and lungs in good shape and help prevent many chronic diseases. But exercises to maintain flexibility, balance, and strength are also important.

Stretching gives you more freedom of movement and makes daily activities more comfortable. Balance practice helps prevent falls, which become a concern as you get older.

Strength training, also called resistance training or weight training, is particularly important. It brings many benefits. First, it makes your muscles stronger. That can help you keep up the activities you enjoy—at any stage of your life.

It’s not about getting big muscles, explains Dr. Wendy Kohrt, an aging expert at the University of Colorado. In fact, most people who do strength training don’t see much of a change in muscle size.

But at all stages of life, she says, “maintaining muscle mass and muscle function is really important for quality of life.”

### Building Up Benefits

Building muscle can do more than make you stronger. Some types of strength training keep your bones healthy, too. Strength training can also improve the way your body processes food to help prevent diabetes and related diseases.

“And like endurance activity, regular strength training is associated with lower risk of cardiovascular disease and other chronic diseases,” says Dr. Joseph Ciccolo, an exercise researcher at Columbia University.

But the main benefit of strength training, as the name suggests, is that it makes your muscle cells stronger. “That benefit is unique to strength training,” says Dr. Roger Fielding, who studies the benefits of exercise at Tufts University.

Experts recommend that children and teens do muscle-strengthening activities at least three days a week. For adults, they encourage strength training for the major muscle groups on two or more days a week.

The benefits of strength training increase as you get older, says Fielding. Maintaining strength is essential for healthy aging.

“Loss of muscle with aging can limit people’s ability to function in their home environment and live independently,” Kohrt says. “Just being able to get up out of a chair or go up and down stairs requires a fair amount of muscle strength.”

In a recent study, Fielding and other researchers tested a three-month weight-lifting program in older adults who already had difficulty walking. At the end of the study, participants who lifted weights improved at tasks like repeatedly bending their knees. Such movements are essential for activities of daily living. In contrast, study participants who only stretched at home did not see similar improvements in strength.

“As we age, I think it’s even more important to consider incorporating some strength training into our physical activity routines,” says Fielding. “We can either slow down the progression of age-related muscle loss or prevent it.”

### Mind and Body

Research is starting to show that strength training isn’t just good for physical health—it can be good for mental health.

Ciccolo is studying the effects of strength training on anxiety, depression, and related conditions. His team recently found that strength training could reduce some symptoms of post-traumatic stress disorder (PTSD) in both women and men.

Endurance exercise may also help people with these problems, says Ciccolo. But some people might be more interested in strength training than aerobic activity. “We want to get people to engage in activities that they find enjoyable,” he says.

How strength training may benefit mental health is still under study. It might help lower certain hormones in the body associated with stress and depression, Ciccolo explains.

In addition, helping people get stronger may boost self-esteem and their sense of control over their lives. “You can feel that you’re being successful and accomplishing something,” he says.

Ciccolo is currently running a study to see if strength training can help relieve symptoms of depression in African American men.

“There’s huge stigma among black men with respect to counseling for mental illness,” he says. “We’re hoping this could be a nontraditional way to get at depression.”

### Getting Started

If you want to get started with strengthening exercises, what should you do? Strength training may seem intimidating if you’ve never tried it.

“People naturally learn to walk as part of growing up. But you don’t necessarily learn how to lift weights,” Ciccolo says.

If it’s feasible for you, booking a few sessions with a personal trainer is a good way to get started, says Kohrt. “That can get you introduced to the types of exercises you could do,” she explains.

There are also many low- or no-cost classes available. Look for them at local gyms, recreation centers, senior centers, and community centers.

Like with any new activity, to make strength training stick, “you have to find something that you really like to do,” says Fielding. “Some people will want to exercise in a group, in a community setting. Others will be happy doing all their exercises in their home, by themselves.”

If you’ve never lifted weights before, talk with your health care provider before you start any home-based strength training routine.

Whatever you choose to do, “start slowly and build up very gradually,” says Kohrt. See the Wise Choices box for more tips on getting started safely.

### Wise Choices

#### Lifting Weights Safely

Get started building muscle safely by following these tips:

- **Start slowly, especially if you haven’t been active for a long time.** Little by little, build up your activities and how hard you work at them.
- **Pay attention to your body.** Exhaustion, sore joints, or muscle pain mean you’re overdoing it.
- **Use small amounts of weight to start.** Focus on your form, and add more weight slowly, over time.
- **Use smooth, steady movements to lift weights into position.** Don’t jerk or thrust weights.
- **Avoid “locking” your arm and leg joints in a straight position.**
- **Don’t hold your breath during strength exercises.** That could cause changes in your blood pressure. Breathe out as you lift the weights and breathe in as you relax.
- **Ask for help.** To get started, schedule a session or two with a personal trainer, or look for a group class at a local gym, recreation center, or senior center.
