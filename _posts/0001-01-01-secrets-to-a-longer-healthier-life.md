---
layout: post 
title: "Secrets to a Longer Healthier Life"
date: 2019-06-18
image: assets/images/secrets-to-a-longer-healthier-life.webp
excerpt: "Studying Exceptionally Long-Lived Families"
tags: Elderly, Genetics, Healthy, LLFS, Long Life, Longevity, NIA
draft: false
---

People who live in good health for 100 years and longer aren’t just lucky. Researchers have found that those who live an exceptionally long and healthy life often have company… in their very own families. Scientists are now aiming to better understand the genes, lifestyle or other factors that make these people so unique. Hopefully, we can all benefit from their findings.

Recent studies have revealed that, as a group, people who lived to be 100 years or more (centenarians) were healthier at younger ages than their peers. The findings suggest that unique “protective” factors against disease and disability may have been at work throughout their lives, not just at very old ages. If the factors that lead to exceptional survival begin working much earlier in life, and if they could be found, they might point the way toward interventions to lengthen healthy lives. So what exactly is it that protects these people and contributes to their extraordinary survival?

![img](/assets/images/84-Secrets-to-a-Longer-Healthier-Life.webp)

Genetics may play a role. Studies of very old people and their families in specific populations—such as those in Iceland and Okinawa, and in Ashkenazi Jewish, Mormon and Amish communities—have shown that exceptionally long life runs in families. Other studies show that centenarians’ siblings have a mortality risk at any age throughout their adult life of about half that of the general population. Centenarians’ children tend to be healthier than their peers, too. They have lower mortality rates from cancer and heart disease, and fewer age-related diseases such as heart disease, hypertension, diabetes and stroke.

More specific findings support the idea that genes contribute to exceptional survival. For example, one form of a gene called apolipoprotein E seems to be associated with longer life, while another form has been linked to an increased risk of heart disease, stroke and Alzheimer’s disease.

While genes likely play a role in exceptional survival, non-genetic factors that tend to run in families, such as lifestyles, can also contribute. For example, a family’s eating and exercise habits, smoking habits and other factors have an effect on how long family members live.

NIH’s National Institute on Aging (NIA) is funding a variety of studies to better understand the factors that lead to exceptional longevity. A major research effort called the Longevity Consortium was begun by NIA in late 2000. It brings together leading scientists from more than 30 institutions to exchange ideas about longevity research and develop new collaborations. Some members are trying to identifying longevity-related genes and pathways in animal models. Others continue to study special populations of people in the search for genes associated with longevity.

Another ongoing study, the Comprehensive Assessment of Long-term Effects of Reducing Intake of Energy (CALERIE), aims to understand the effects of eating fewer calories over time. Several animal studies have shown that restricting calories can extend the average life span of some laboratory animals and delay age-related problems. Whether a similar effect holds true in people remains to be seen. This study is still in an early phase.

Given family patterns of long life and good health, NIA has begun a major study to collect information on long-lived families. Winifred K. Rossi, deputy director of NIA’s Geriatrics and Clinical Gerontology Program, said, “We want to learn why these exceptional families age so well.”

Rossi is the program official for NIA’s new five-year, $18 million effort to learn more about the factors that contribute to a long, healthy life. It’s called the Long Life Family Study (LLFS), and it’s now recruiting families to participate. Usually, studies look at health problems. The LLFS is unique, Rossi said, because by following exceptional families over time, the researchers are focusing on what protects against disease and disability.

“We want to understand more about their health, lifestyle and their genes,” Rossi explained. “Right now we don’t understand all of the things that contribute to healthy longevity in these unique families. We hope that the LLFS will identify factors that can help other people live as healthy as possible as long as possible.”

*Definitions:*

**Calories:**
A measure of the amount of energy stored in food.

**Genes:**
Stretches of DNA, a substance you inherit from your parents, that define characteristics like height, eye color and how likely you are to get certain diseases.

**Genetics:**
Having to do with your genes.

**Longevity:**
Long life.

***A Word to the Wise...***

*Tips for a Healthy, Long Life*

The Long Life Family Study aims to reveal the factors that lead to exceptional survival and teach us ways to lengthen healthy lives. In the meantime, we already know about some things that can help your chances of living a healthy, long life. You’re probably already familiar with them from when you were a kid:

- Eat your fruits and vegetables.
- Don’t smoke.
- Rest enough.
- Exercise several times a week.
- Monitor your health.
- See your doctor regularly.
