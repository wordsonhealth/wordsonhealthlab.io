---
layout: post 
title: "Disrupted Speech Why Do We Stutter"
date: 2022-01-02T19:04:17+01:00
image: assets/images/disrupted-speech-why-do-we-stutter.webp
excerpt: Stuttering affects more people than you might think. Find out what causes people to stutter.
draft: false
---

Stuttering affects more people than you might think. Roughly 3 million Americans have this speech disorder that makes speaking smoothly difficult. Scientists are learning about what causes people to stutter, and **genes** tell a big part of the story.

“People with stuttering know exactly what they want to say. They’re just unable to say it at the rate they would like,” says Dr. Dennis Drayna, an NIH expert on the genetics of communication disorders.

Stuttering often involves speech sounds that are repeated or held for too long—often when starting words or sentences. It affects about 1 in 20 children. Most will outgrow the disorder on their own or with the help of a professional called a speech-language pathologist.

“However, about 20–25% of children who stutter will continue into adulthood,” says Drayna. This condition is known as persistent developmental stuttering. Overall, about 1% of adults stutter, and it’s much more common in men than women.

For those who stutter, communicating with others can be difficult. It can cause anxiety about speaking and lead them to avoid talking. This, in turn, can affect relationships, self-esteem, and quality of life.

It’s common for people who stutter to be able to speak without stuttering when in a low-stress environment. They may have no problem speaking fluently with a pet or baby, for example. Singing or speaking together in a group can also lessen stuttering. But stuttering often gets worse if they’re feeling tired or anxious.

Researchers are still working to fully understand what causes stuttering. But they do know that it often runs in families. “It’s 15 times more likely that a sibling of a person who stutters will stutter than a random person in the population,” explains Drayna.

By studying families with multiple people who stutter, Drayna has identified several genes that can cause stuttering. **Mutations** in these genes have now been found in people around the world who stutter. These studies suggest that genes likely play a role for many people who stutter.

All the genes identified so far are involved in a process inside the cell called intracellular trafficking. This process helps direct things in the cell to their proper locations. Problems with intracellular trafficking have recently been recognized in other neurological disorders, like Parkinson’s and Alzheimer’s disease. But more research is needed to understand how it impacts speech and stuttering.

Scientists are also using brain imaging scans to better understand brain activity in people who stutter. This may help show why some children outgrow stuttering and hopefully lead to better treatments one day.

For now, treatment for stuttering involves therapy with a speech-language pathologist. Many of the current therapies aim to make speech smoother. Some work to change the thoughts that can bring on or worsen stuttering. Electronic devices are also available to help those who stutter manage their speech.

While stuttering can be an obstacle, there are many resources to help overcome it. See the Wise Choices box for tips on helping your child.

### Wise Choices

#### Does Your Child Stutter?

- Be patient and focus on what he or she is saying.
- Listen attentively when your child speaks and wait for him or her to say the intended word. Try not to finish sentences or fill in words.
- Avoid telling your child to “relax” or “slow down.”
- Speak at a relaxed pace with your child and pause often. This can help reduce time pressures the child may be experiencing.
- Set aside some time each day to talk with your child when he or she has your undivided attention.
- Contact a speech pathologist if stuttering lasts over six months.
