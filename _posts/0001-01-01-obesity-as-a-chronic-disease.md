---
layout: post 
title: "Obesity as a Chronic Disease"
date: 2019-02-02
image: assets/images/obesity-as-a-chronic-disease.webp
excerpt: "When it Comes to Weight Loss, Stick with the Basics"
tags: chronic disease, obesity, Overweight, Weight Loss
draft: false
---

More Americans than ever are losing “the battle of the bulge.”
The number of obese men and women in this country has nearly doubled in the last thirty years, and a record 55 percent of Americans are either overweight or obese, according to the June 1998 guidelines on the evaluation and treatment of overweight and obesity from NIH’s National Heart, Lung, and Blood Institute (NHLBI) and the National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK).

If you’re struggling to keep your waistline under control, NHLBI can provide a few simple guidelines that may help.

“Many people are confused by the messages they hear everyday about weight loss and maintenance, and they disregard the basics,” says Karen Donato, coordinator of the NHLBI’s Obesity Education Initiative.

And it’s easy to fall into habits that contribute to weight gain. Consider the following reasons why more Americans are likely to become overweight:

- Americans are consuming about 300 more calories per day than 20 years ago.
- More of us are eating out more often.
- Portion sizes are larger.
- Low fat products are everywhere, and many people believe that low fat is healthy and therefore can be consumed freely without regard to quantity.
- Many Americans ignore calories and fail to get enough exercise.

**Did You Know That…**

- Obesity is a chronic disease and a risk factor for diabetes and coronary artery disease, the cause of heart attacks.
- After smoking, obesity is the second most preventable cause of death in the United States.
- The most successful weight loss program combines calorie reduction, increased physical activity, and when appropriate, behavior therapy to improve eating and exercise habits.
- Obesity is more prevalent in some minority populations than among white Americans. These groups include African Americans, especially women, Mexican and Puerto-Rican Americans, some other Hispanic populations, many Native American groups, and those of Pacific Island ancestry.

Another possible cause for the increase in obesity is Americans’ obsession with fat in the diet and attempting to eliminate or reduce it by overeating low fat foods. However, many low-fat foods are actually higher in calories than regular fat versions. The NHLBI urges consumers to read food labels and compare products, because although fat matters, calories count.

Physical activity counts too. Nearly a third of Americans don’t get enough physical activity. Instead we spend too much time sitting at our computers, driving the car, watching television, and taking the elevator instead of the stairs. The NHLBI guidelines recommend that all Americans get at least 30 minutes of moderate exercise, such as brisk walking or biking on most, if not all, days of the week.

By sticking with the basics, weight loss and maintenance can be achieved by adding a few simple changes to your lifestyle. “There is no quick fix. Losing weight and keeping it off is not easy for many people,” adds Donato. “The common sense approach — to eat less and move more — is the most effective.”

***A Word to the Wise...***

- Consult your physician or health care provider before starting a weight loss program.
- Increase physical activity gradually, working up to 30 minutes of moderate physical activity. Physical activity is important for weight maintenance, too.
- Reducing just fat in the diet without reducing calories will not produce weight loss. Cutting back on fat can help reduce calories and is good for the heart.
- When you shop for groceries, pay special attention to serving size and total calories, in addition to fat.
