---
layout: post 
title: "Speech and Language Disorders"
date: 2019-05-30
image: assets/images/speech-and-language-disorders.webp
excerpt: "Coping With Hearing and Speech Problems"
tags: Communication Disorders, Hearing Loss, NIDCD, Speech Problems
draft: false
---

Did you know that almost 42 million Americans suffer from a communication disorder—a problem with their speech, voice, language or hearing? That number includes people in all stages of life, but those most likely to be affected are the most vulnerable in our society: older adults and the very young. May is Better Hearing and Speech Month, and NIH’s National Institute on Deafness and Other Communication Disorders (NIDCD) wants you to know that you can do something about communication disorders.

Each year, approximately 2 or 3 out of every 1,000 babies born in the U.S. have a detectable hearing loss, a problem that can affect their speech, language, social and intellectual development. Hearing loss increases with age, too; almost half of those 75 years old or older have hearing loss.

![img](/assets/images/75-Speech-and-Language-Disorders.webp)

Early detection of hearing loss in newborns is especially critical. Results from NIDCD-funded research show that if children with hearing loss are identified by 6 months of age and receive help early, they can make bigger strides in developing language skills than children whose hearing loss is identified later.

Treatment for hearing loss may include hearing aids or cochlear implants. Adults and children who are deaf and hard-of-hearing can also learn to communicate using American Sign Language or cued speech, a system that uses hand shapes along with natural mouth movements to represent speech sounds.

If you think you are losing your hearing, see an audiologist, a professional who can measure your hearing, or an otolaryngologist, a doctor who specializes in diseases of the ear, nose and throat, to have your hearing checked.

People can also have difficulty speaking, problems with their voice or trouble understanding and using language. One language disorder called aphasia is often the result of a stroke that damages portions of the brain. Speech and language disorders such as this can make it difficult to succeed in school or work, and can cause social problems as well.

The good news is that most people with speech and language problems can be helped. A speech-language pathologist is a health professional trained to evaluate and treat people with voice, speech and language disorders. A speech-language pathologist can help you improve or regain your capacity to speak and understand language.

Even if the problem can’t be eliminated, people with language and speech problems can learn communication strategies that help them cope better and achieve a better quality of life.

*Definitions:*

**Cochlear Implant:**
A small electronic device that can provide a sense of sound to a person who is profoundly deaf or severely hard of hearing. Hearing aids amplify sound, but cochlear implants make up for damaged or non-working parts of the inner ear.

***A Word to the Wise...***

*Signs of Possible Hearing Loss:*

- Do you have a problem hearing on the telephone?
- Do you have trouble following a conversation when two or more people are talking at the same time?
- Do people complain that you turn the TV, radio or stereo volume up too high?
- Do you have to strain to understand conversation?
- Do you have trouble hearing when there’s a lot of noise in the background?
- Do you find yourself asking others to repeat themselves?
- Do many people seem to mumble or not speak clearly?
- Do you misunderstand what others are saying and respond inappropriately?
- Do you have trouble understanding the speech of women and children?
- Do people get annoyed because you misunderstand what they say?

If you answered “yes” to three or more of these questions, talk to your doctor about getting a hearing evaluation.
