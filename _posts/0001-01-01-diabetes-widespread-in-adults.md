---
layout: post 
title: "Diabetes Widespread in Adults"
date: 2019-06-08
image: assets/images/diabetes-widespread-in-adults.webp
excerpt: "One-Third Still Don't Know They Have It"
tags: CDC, Diabetes, Diabetes type 2, Pre-diabetes, Type 2 Diabetes
draft: false
---

In a new analysis of national survey data, researchers found that the prevalence of diabetes in U.S. adults is continuing to rise. And despite efforts to raise awareness of the problem, about a third of adults with diabetes still don’t know they have it.

Diabetes is a group of diseases marked by high levels of glucose in the blood. Persistent high levels can lead to blindness, kidney failure, amputations, heart disease and stroke.

![img](/assets/images/79-Diabetes-Widespread-in-Adults.webp)

Researchers at NIH and the Centers for Disease Control and Prevention (CDC) analyzed data from a national sample of U.S. adults 20 years old and over who took part in the National Health and Nutrition Examination Survey. Participants were interviewed in their homes and given a physical exam with a blood test, including a glucose reading taken after an overnight fast.

The researchers found that the prevalence of diagnosed diabetes in U.S. adults rose from about 5.1% in the years 1988-1994 to 6.5% in 1999-2002. About 2.8% of U.S. adults, a third of those who have diabetes, don’t even know they have it.

The study also found that about a quarter of U.S. adults have impaired fasting glucose, a form of pre-diabetes. People with pre-diabetes have an increased risk for developing type 2 diabetes, the most common form of diabetes, and for heart disease and stroke.

Knowing whether you have pre-diabetes or type 2 diabetes is an important step. If you have pre-diabetes, you may be able to prevent or delay type 2 diabetes by cutting calories and increasing your physical activity to lose a modest amount of weight. A major study of people with pre-diabetes showed that lifestyle changes leading to a 5-7% weight loss lowered diabetes onset by 58%.

If you have diabetes, controlling your blood glucose, blood pressure and cholesterol will prevent or delay the complications of diabetes. Be sure to talk to your health care professional about your risk.

*Definitions:*

**Glucose**:
A type of sugar. When the glucose level in your blood gets too high, it can damage your tissues and organs.

**Pre-diabetes**:
A condition in which your blood glucose level is higher than normal but not high enough for a diagnosis of diabetes. It puts you at increased risk for developing type 2 diabetes, heart disease and stroke.

**Type 2 Diabetes**:
Formerly called adult-onset diabetes, this is the most common form of diabetes. People can develop it at any age. Being overweight and inactive increase the chances of developing type 2 diabetes.

***A Word to the Wise...***

*At Risk for Diabetes?*

If you’re over 45, ask your health care provider about testing for pre-diabetes or diabetes. You should also ask about testing if you’re younger than 45, overweight, and have another risk factor, such as you:

- are 45 or older
- have a family history of diabetes
- are overweight
- have an inactive lifestyle (exercise less than three times a week)
- are a member of a high-risk ethnic population such as African American, Hispanic/Latino American, American Indian and Alaska Native, Asian American or Pacific Islander
- have high blood pressure
- have a low HDL cholesterol level or a high triglyceride level
- have had diabetes that developed during pregnancy (gestational diabetes) or have given birth to a baby weighing more than nine pounds
- have polycystic ovary syndrome, a disorder that affects the female reproductive system
- have acanthosis nigricans (dark, thick skin near neck or armpits)
- have a history of disease of the blood vessels to the heart, brain or legs
- have had an impaired glucose reading in a previous test
