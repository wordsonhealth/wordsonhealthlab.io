---
layout: post 
title: "Healing From Depression"
date: 2019-05-08
image: assets/images/healing-from-depression.webp
excerpt: "Research Points the Way to Recovery"
tags: Antidepressant, Depression, lifestyle, NIMH
draft: false
---

Depression can be treated with medications or talk therapy, and both have supporters with countless studies to prove how effective they are. But researchers now know that all depression is not equal, and that different people need different approaches to get better. For someone suffering from depression, the bottom line is that depression can usually be treated effectively with one or a combination of treatments. The trick is figuring out which treatments work best for which people. Researchers are making progress toward the day they can do that. They’re also developing innovative new therapies that may change the way depression is treated in the future.

Depression is more than the blues or the blahs. Everybody gets sad or feels down sometimes, but most people with the blues can lift their mood by exercising, socializing or other activities. Those with major depression can’t, and their symptoms can last weeks, months or even years. These symptoms can include:

• A persistent sad, anxious or “empty” mood
• Feelings of hopelessness, pessimism, guilt, worthlessness or helplessness
• Loss of interest or pleasure in hobbies and activities you once enjoyed
• Decreased energy and fatigue
• Difficulty concentrating, remembering or making decisions
• Insomnia, early-morning awakening or oversleeping
• Appetite and/or weight loss or overeating and weight gain
• Thoughts of death or suicide; suicide attempts
• Restlessness, irritability
• Persistent physical symptoms such as headaches, digestive problems and chronic pain

![img](/assets/images/64-Healing-From-Depression.webp)

Some people think depression is some kind of a personal weakness, something you can will away. Research has proven otherwise. Dr. Husseini Manji, director of the Mood and Anxiety Disorders Program at NIH’s National Institute of Mental Health (NIMH), says, “If you’ve got this illness, your brain chemistry is not the way it should be. Lifestyle changes can help, but you can only do so much. You have to get treatment.”

In fact, depression takes a physical toll that doctors can measure. “We’re learning that depression is associated with a number of medical consequences,” Manji said. It raises the risk of heart disease, high blood cholesterol and high blood pressure. The chance of someone dying after a heart attack is 4 times greater if they’re depressed.

Antidepressant medications and talk therapy are the most common treatments for depression. Your doctor might have to try several treatments before finding the combination that’s right for you. For those who don’t respond to conventional treatments, electroconvulsive therapy (ECT) has been the treatment of last resort. Electrodes are placed on the head to deliver electrical impulses and cause seizures within the brain. ECT is very effective, but it has serious drawbacks: It’s costly, requires multiple hospital visits and can result in memory loss.

A newer method for treating depression is called vagus nerve stimulation (VNS). First used for depression by Dr. Mark George of the Medical University of South Carolina and just approved by the U.S. Food and Drug Administration, VNS serves as a sort of “pacemaker” for the brain. A surgically implanted device periodically sends small electric pulses up the vagus nerve in the neck and into the brain. George says researchers don’t fully understand why VNS works, but it does. However, it does require invasive surgery.

Advanced brain imaging techniques showing activity within the brain are allowing researchers to design newer methods for treating depression that target particular areas of the brain. Dr. Helen Mayberg at Emory University’s School of Medicine, for example, has used a technique called Deep Brain Stimulation, in which thin wires are surgically implanted into a particular area of the brain. A small current run through the wires improved depression in 4 of 6 patients. Such surgery may be impractical for large numbers of people, but Mayberg says her study proves the principle that a small electric current in this area of the brain can help treat depression. “This research may help point the way to other effective treatments,” she said.

George’s research group and others are pursuing a technique that doesn’t require surgery called Transcranial Magnetic Stimulation (TMS). In TMS, a small electromagnet on the scalp induces a current in the brain. The device can be fairly well focused in the brain and doesn’t seem to cause side effects. TMS has been promising in small studies, and a large-scale NIMH-funded study is now under way to test it more rigorously.

Another approach researchers are taking is to try to develop better medications to treat depression. Many scientists have been intrigued by the fact it often takes people days or weeks to get better with antidepressant medications even though the drugs work very quickly to affect the molecules that brain cells use to communicate with each other. Manji explained that we now know these medications are starting a process inside cells to turn certain genes on and off, and that those genes, in turn, are the ones responsible for people getting better. “There’s been a lot of research into trying to find what those genes are,” Manji said.

The genes researchers are uncovering seem to be involved in helping nerve cells grow and survive. While nerve cells in the brain don’t seem to die with depression, they do sort of “shrivel up,” as Manji put it.

“It’s good news because maybe we can do something about it,” Manji said. Several drugs targeting these new pathways are now being developed and tested, and Manji is optimistic that new medications will be available within the next few years.

Depression researchers hope that understanding the genes involved in depression will ultimately help doctors make better treatment decisions as well. Manji believes that as few as 4 or 5 genes might enable doctors to predict, with a simple blood test, which treatments will work best for which people.

None of these developments changes the fact that current treatments for depression still work for most people. “Most people bounce back and get totally back on their game if they get treated,” George stressed. If you or someone you know is depressed, get treatment as soon as you can.

*Definitions*

**Antidepressant:**
Something used to treat depression.

**Depression:**
An illness that brings a persistent sad, anxious or “empty” mood, feelings of hopelessness and pessimism, and other symptoms that interfere with the ability to work, study, sleep, eat and enjoy once pleasurable activities.

***A Word to the Wise...***

*Helping Someone Who May Be Depressed:*

- Tell the person that you are concerned about him or her.
- Talk to the person about seeing a doctor.
- Take the person to a doctor.
- “Be there” for the person after he or she starts treatment.
- Take any comments about suicide or wishing to die seriously. If you think someone you know might be suicidal, don’t leave them alone. Try to get them to seek help immediately from an emergency room, physician or mental health professional. Call the toll-free, 24-hour hotline of the National Suicide Prevention Lifeline to be connected to a trained counselor at a suicide crisis center.
