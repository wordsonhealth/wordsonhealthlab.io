---
layout: post 
title: "Fighting Seasonal Allergies"
date: 2019-02-22
image: assets/images/fighting-seasonal-allergies.webp
excerpt: "How to control its symptoms to prevent it from serious conditions?"
tags: Allergy, coughing, NIAID, runny nose, sneezing
draft: false
---

Sometimes it’s hard to know whether you’re suffering from allergies or a string of colds. Their symptoms – like sneezing, coughing and a runny nose – are similar. If you have these symptoms every spring or fall, or all through the growing season, it’s a good bet you’ve got seasonal allergies, or hay fever. Doctors call this type of allergy “seasonal allergic rhinitis”. Whatever its name, it can make you absolutely miserable.

The National Institute of Allergy and Infectious Diseases (NIAID) estimates that the number of people suffering from allergic rhinitis may be as high as 35 million. Allergic rhinitis may not seem dangerous in itself, but it can play a role in other diseases like asthma, a chronic and potentially fatal lung disease affecting approximately 15 million Americans, and sinusitis, an infection in the hollow air spaces of the skull surrounding the nose that affects approximately 38 million Americans.

There is no cure for allergy, but there are ways to control its symptoms and, hopefully, to prevent it from triggering more serious conditions like asthma and sinusitis.

**The Allergic Response**

Whether allergic rhinitis strikes by season or lasts all year long, it is essentially the same disease. Seasonal allergic rhinitis is frequently caused by pollen released into the air by trees, grasses and weeds as part of their reproductive cycle. Molds can cause similar allergic symptoms. They release tiny spores that ride on the wind to establish the mold in new places. Whether pollens or spores, when these airborne allergens (the name for allergy-causing compounds) get into your nose and your eyes, they can cause allergies.

But what exactly is an allergy? Everybody is exposed to these pollens and spores, but only some develop allergies to them. An allergy comes about when the immune system, which is there to protect us from microbial invaders like viruses and bacteria, reacts to a normally harmless substance. Specialized cells of the immune system – known as B cells – manufacture molecules called antibodies to bind very specifically to different foreign invaders (see figure). There are five different classes of antibodies, each with its own function. IgA antibodies, for example, are found in saliva, nasal secretions and tears. IgG antibodies are the major type in the blood. Another group, IgE, causes the symptoms of allergies.

Scientists aren’t actually sure what protective role IgE antibodies normally have. Laboratory mice without IgE seem perfectly normal. So do people with little detectable IgE. “What else does IgE do in the system?” Dr. Dean Metcalfe, Chief of NIAID’s Laboratory of Allergic Diseases, asks. “I don’t think anybody knows. The speculation is that IgE may be involved in host defenses against parasites.” But since most clinical trials are conducted in countries where there are few diseases caused by parasites, it’s been difficult for scientists to test this theory.

What scientists do know is that IgE antibodies attach to the surfaces of two types of immune system cells: mast cells and basophils. When these IgE encounter the allergen they were manufactured to recognize – for example, on a pollen grain or a mold spore – they attach like a key in a lock and trigger the mast cells and basophils to release the little packets of chemicals they contain. Those chemicals include histamine and other compounds that cause local inflammation, leading to the symptoms that you know as an allergy: sneezing, coughing, a runny or clogged nose, postnasal drip, and itchy eyes and throat.

“There is nothing intrinsically wrong with mast cells or basophils,” Dr. Metcalfe explains. But when B cells make IgE to some environmental antigen that most people don’t respond to, the mast cells and basophils are activated when you don’t want them to be, and you have an allergy.

Scientists think that some people simply have a genetic tendency to develop allergies. “The genes themselves have not yet been identified,” Dr. Marshall Plaut, Chief of NIAID’s Allergic Mechanisms Section, says. “But there’s an impressive amount of work that’s been done to identify regions in chromosomes where there appear to be genes associated with allergy.”

**Recognizing an Allergy**

Sometimes it’s obvious you have allergic rhinitis. If you have symptoms like a runny nose and itchy eyes all year round, it’s a good bet your condition is caused by allergies. The culprit may be something indoors like dust mites in your bedding or carpet, or mold in the shower.

Many allergies are more acute and temporary. “Maybe you walk into somebody’s house, they have three cats and suddenly your eyes itch, or your nose itches or your nose runs or you start to wheeze,” Dr. Metcalfe says. “Pretty clearly, that’s an allergy. The exposure to something that triggers these allergic reactions – and that’s reproducible – is often the first sign to people they have an allergy.”

But it’s not always easy to tell if you have allergies. Dr. Metcalfe says, “Let’s say that somebody’s sitting around and it’s, say, February, and they say, ‘Gee I think I’m getting a cold.’ But maybe they’re developing a tree allergy, because the tree pollen may be coming out in February or March in their area. Well, sometimes it’s pretty hard for someone to determine whether it’s a cold or a transient seasonal allergy.”

Your doctor can help you sort out whether you have allergies, but it’s difficult for him or her to make a diagnosis without your help. You can play an important role by keeping track of when and where you get symptoms. “The mucus membranes look a little different, but it’s not enough to make a perfectly clear diagnosis,” Dr. Plaut says. “The history of your symptoms is usually much stronger evidence than simply looking. It’s often the duration of symptoms; people do not have colds that last more than two weeks.”

Look at your local pollen and mold counts and see if high numbers match up with when you get your symptoms. Peak seasons for pollens and molds differ depending on where you live, but many local newspapers, TV and radio stations now track levels of molds and pollens daily. The National Allergy Bureau™ of the American Academy of Allergy, Asthma and Immunology (not associated with NIH), which reports current pollen and mold spore levels to the media. As a general guide, tree pollen allergies strike in late winter to early spring, grass allergies can strike from spring through summer, and ragweed typically strikes in the fall.

Ultimately, your doctor may send you to an allergy specialist, or allergist, for blood or skin tests to see if your body is making IgE against particular allergens. But keep in mind that even these tests can’t be used alone for a diagnosis. Dr. Metcalfe says, “You cannot send your blood away to some laboratory in another state, get a readout of what you have IgE to, and then say you’re allergic to the following twelve things. Those are not absolute diagnostic tests. They’re just used to show that you have the potential and if the history agrees, then the physician and the patient may together reach the diagnosis.”

That’s because IgE antibodies are necessary for an allergy to develop, but they’re not the whole story. “The basic abnormality in allergic diseases is that people make IgE antibodies to specific allergens, and if they didn’t make those IgE antibodies they wouldn’t be allergic,” Dr. Plaut explains. “But that’s not to say that everybody who makes IgE antibody has major symptoms.” In common allergies, he says, there is a wide range of IgE antibody levels.

So even with such sophisticated tools, you still play an important role in helping your doctor figure out if you have allergies, and what is causing them.

![img](/assets/images/27-Fighting-Seasonal-Allergies.webp)

Allergy comes about when the immune system, which is there to protect us from harmful invaders like viruses and bacteria, reacts to a normally harmless substance like pollen. The immune system makes IgE antibodies against that specific allergy-producing substance, or “allergen”. These IgE antibodies attach to the surfaces of two types of immune system cells: mast cells (shown here) and basophils. When these IgE antibodies encounter the allergen they were manufactured to recognize – like the pollen grain – they attach to it and trigger the cells to release the packets of chemicals they contain. Those chemicals include histamine and other compounds that cause the symptoms like runny nose, itchy eyes and sneezing that you know as an allergy.

**Prevention and Treatment**

If you do have allergic rhinitis, you and your doctor have many options. The first, and most obvious, is to avoid the allergens you react to. This is not as simple as it might seem. Some people think that they can just move to a place where the offending pollen doesn’t grow. This drastic measure may provide temporary relief, but people who have an allergy to one thing tend to develop allergies to others as well, and may soon find themselves in the same situation.

There are some simple things you can do. Sunny, dry, windy days can be especially troublesome for people with pollen allergies, so try to stay indoors on those days with the windows closed and the air conditioner on. Some people take vacations at the height of the expected pollination period and choose a location where they won’t get much exposure; the seashore, for example, is a good place for many people with pollen allergies. See the side box for more ideas from NIAID about avoiding pollen exposure. There are also ways to avoid exposure to indoor dust mites, molds and other common allergens that cause allergic rhinitis. No matter what your allergy, your doctor should be able to provide guidance for you.

If avoidance doesn’t work for you, allergy symptoms can often be controlled with medications. Antihistamines are usually the first line of defense. As you might expect from the name, an antihistamine counters the effects of histamine, which is one of the chemicals released by mast cells and basophils that cause allergy symptoms. Some antihistamines can cause drowsiness and loss of alertness and coordination. During the last few years, however, antihistamines that cause fewer of these side effects have become available.

Along with an antihistamine, your doctor might recommend oral or nasal decongestants to reduce congestion and swelling of the nasal passages. Keep in mind that these nose drops and sprays should not be used for more than a few days or they might backfire and lead to even more congestion.

“If antihistamines are not judged to be sufficient,” Dr. Metcalfe says, “then the next line of therapy is almost always a steroid nasal spray to decrease inflammation.” Topical nasal steroids reduce mucus secretion and nasal swelling. Although nasal steroids can have side effects, they are generally safe when used at recommended doses. NIAID says that the combination of antihistamines and nasal steroids is a very effective way to treat allergic rhinitis, especially in people with moderate or severe allergic rhinitis.

Another option you might have heard about is cromolyn sodium, a nasal spray which inhibits the release of chemicals like histamine from mast cells. It’s very safe, Dr. Metcalfe says, and for some people, it might be very effective. But cromolyn sodium takes several days to begin working, so to be effective it has to be started before the allergic reaction begins. For many, this is simply not practical. Dr. Metcalfe says, “The other thing is, if you have strong allergies, it tends to break through cromolyn.” For these reasons, cromolyn sodium has largely been replaced by nasal steroids, but it may still prove useful for many allergy sufferers.

Lastly, immunotherapy, or a series of allergy shots, is an option if you and your doctor know which allergens are causing you trouble. “Allergy shots are very effective in reducing symptoms,” Dr. Plaut says. The therapy involves several injections of increasing amounts of the allergens to which you are sensitive. This is a long process that many people are not willing to go through, but it can bring a significant reduction in symptoms and the need for medication.

Scientists actually aren’t exactly sure how immunotherapy works. Somehow, the series of injections gets the immune system to tone down its response to the antigen over time, a phenomenon scientists call “tolerance”. Dr. Plaut explains, “Tolerance is a reduction in the immune response to antigen that persists after therapy is stopped. Tolerance is probably due to a long-lasting change in the immune system.”

But whatever immunotherapy’s mechanism of action, Dr. Metcalfe says, “The trials are very clear. It does work.”

**Research Directions**

The concept of tolerance is a hot topic in the field of allergy research. Dr. Plaut says, “People have been doing immunotherapy since around 1920, we still don’t know all the ways that it works, and we’re trying to pin it down.” By understanding how immunotherapy works, scientists hope to find better ways to cause long-lasting changes in the immune system.

The ability to better induce tolerance could lead to new treatments for a variety of medical conditions, not just allergies. In recognition of this, NIAID, along with NIH’s National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK) and the Juvenile Diabetes Research Foundation International, has funded the Immune Tolerance Network. The Network supports research and clinical trials of tolerance not only in asthma and allergy, but also in graft rejections and in autoimmune disorders such as rheumatoid arthritis, lupus, multiple sclerosis, psoriasis, and inflammatory bowel disease.

There are several other approaches scientists are taking in allergy research. “Anti-IgE is one of the more promising drugs that appears to be in the pipeline,” Dr. Plaut says. Still being studied in clinical trials and therefore not available to the general public, this compound reduces the amount of IgE circulating in the body by binding to it at the same site where IgE usually attaches to mast cells. If IgE cannot bind to mast cells, it cannot activate them to release chemicals like histamine that cause allergic symptoms.

Recently, NIAID-supported researchers developed another promising molecule called GE2 that binds to the surfaces of mast cells and basophils in a way that interferes with the allergic reaction. In laboratory tests on human mast cells and basophils, the higher the dose of GE2, the less histamine the cells released when stimulated by an allergen. In tests on mice, GE2 significantly reduced allergic skin reactions. While promising, this compound is far from being ready for the general public. Still, it represents one of the many ways our deepening knowledge of the immune system is leading to new approaches to treating allergy.

Dr. Plaut, like many scientists, is excited by all the new research, but he stops and checks himself. “I should say that standard treatment works for most people,” he points out. If you suspect you have allergies, go see your doctor. Current therapies can provide immediate relief.

***A Word to the Wise...***

*Avoiding Pollen*

If you are allergic to pollen, avoiding exposure to it is the best way to prevent allergic symptoms. There are several things you can do to minimize your exposure:

- Stay indoors in the morning when outdoor pollen levels are highest.
- Wear face masks designed to filter out pollen if you must be outdoors.
- Keep windows closed and use the air conditioner if possible in the house and car.
- Do not dry clothes outdoors.
- Avoid unnecessary exposure to other environmental irritants such as insect sprays, tobacco smoke, air pollution, and fresh tar or paint.
- Avoid mowing the grass or doing other yard work, if possible.

Source: NIAID
