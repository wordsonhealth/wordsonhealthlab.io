---
layout: post 
title: "Discoveries in Basic Science a Perfectly Imperfect Process"
date: 2022-01-03T00:48:49+01:00
image: assets/images/discoveries-in-basic-science-a-perfectly-imperfect-process.webp
excerpt: Science is a slow process. A study usually only looks at one piece of a much larger puzzle. That can make it seem like science is always changing.
draft: false
---

Have you ever wondered why science takes so long? Maybe you haven’t thought about it much. But waiting around to hear more about COVID-19 may have you frustrated with the process.

Science can be slow and unpredictable. Each research advance builds on past discoveries, often in unexpected ways. It can take many years to build up enough basic knowledge to apply what scientists learn to improve human health.

“You really can’t understand how a disease occurs if you don’t understand how the basic biological processes work in the first place,” says Dr. Jon Lorsch, director of NIH’s National Institute of General Medical Sciences. “And of course, if you don’t understand how the underlying processes work, you don’t have any hope of actually fixing them and curing those diseases.”

Basic research asks fundamental questions about how life works. Scientists study cells, genes, proteins, and other building blocks of life. What they find can lead to better ways to predict, prevent, diagnose, and treat disease.

### How Basic Research Works

When scientists are interested in a topic, they first read previous studies to find out what’s known. This lets them figure out what questions still need to be asked.

Using what they learn, scientists design new experiments to answer important unresolved questions. They collect and analyze data, and evaluate what the findings might mean.

The type of experiment depends on the question and the field of science. A lot of what we know about basic biology so far has come from studying organisms other than people.

“If one wants to delve into the intricate details of how cells work or how the molecules inside the cells work together to make processes happen, it can be very difficult to study them in humans,” Lorsch explains. “But you can study them in a less complicated life form.”

These are called research organisms. The basic biology of these organisms can be similar to ours, and much is already known about their genetic makeup. They can include yeast, fruit flies, worms, zebrafish, and mice.

Computers can also help answer basic science questions. “You can use computers to look for patterns and to try to understand how the different data you’ve collected can fit together,” Lorsch says.

But computer models have limits. They often rely on what’s already known about a process or disease. So it’s important that the models include the most up-to-date information. Scientists usually have more confidence in predictions when different computer models come up with similar answers.

This is true for other types of studies, too. One study usually only uncovers a piece of a much larger puzzle. It takes a lot of data from many different scientists to start piecing the puzzle together.

### Building Together

Science is a collective effort. Researchers often work together and communicate with each other regularly. They chat with other scientists about their work, both in their lab and beyond. They present their findings at national and international conferences. Networking with their peers lets them get feedback from other experts while doing their research.

Once they’ve collected enough evidence to support their idea, researchers go through a more formal peer-review process. They write a paper summarizing their study and try to get it published in a scientific journal. After they submit their study to a journal, editors review it and decide whether to send it to other scientists for peer review.

“Peer review keeps us all informed of each other’s work, makes sure we’re staying on the cutting-edge with our techniques, and maintains a level of integrity and honesty in science,” says Dr. Windy Boyd, a senior science editor who oversees the peer-review process at NIH’s scientific journal of environmental health research and news.

Different experts evaluate the quality of the research. They look at the methods and how the results were gathered.

“Peer reviewers can all be looking at slightly different parts of the work,” Boyd explains. “One reviewer might be an expert in one specific method, where another reviewer might be more of an expert in the type of study design, and someone else might be more focused on the disease itself.”

Peer reviewers may see problems with the experiments or think different experiments are needed. They might offer new ways to interpret the data. They can also reject the paper because of poor quality, a lack of new information, or other reasons. But if the research passes this peer review process, the study is published.

Just because a study is published doesn’t mean its interpretation of the data is “right.” Other studies may support a different hypothesis.

Scientists work to develop different explanations, or models, for the various findings. They usually favor the model that can explain the most data that’s available.

“At some point, the weight of the evidence from different research groups points strongly to an answer being the most likely,” Lorsch explains. “You should be able to use that model to make predictions that are testable, which further strengthens the likelihood that that answer is the correct one.”

### An Ever-Changing Process

Science is always a work in progress. It takes many studies to figure out the “most accurate” model—which doesn’t mean the “right” model.

It’s a self-correcting process. Sometimes experiments can give different results when they’re repeated. Other times, when the results are combined with later studies, the current model no longer can explain all the data and needs to be updated.

“Science is constantly evolving; new tools are being discovered,” Boyd says. “So our understanding can also change over time as we use these different tools.”

Science looks at a question from many different angles with many different techniques. Stories you may see or read about a new study may not explain how it fits into the bigger picture.

“It can seem like, at times, studies contradict each other,” Boyd explains. “But the studies could have different designs and often ask different questions.”

The details of how studies are different aren’t always explained in stories in the media. Only over time does enough evidence accumulate to point toward an explanation of all the different findings on a topic.

“The storybook version of science is that the scientist is doing something, and there’s this eureka moment where everything is revealed,” Lorsch says. “But that’s really not how it happens. Everything is done one increment at a time.”

### Wise Choices

#### Research in the News

When you read or hear about a new study, ask:

- What question is the study trying to answer?
- How do the findings fit in with previous studies on the topic? 
- Has the study been peer reviewed?
- Are there other studies that have confirmed the idea, or is it new and untested?
- Was the study done in people or a research organism?
- Who do the findings apply to? Was the study done in certain groups of people?
- What questions still need to be answered?
