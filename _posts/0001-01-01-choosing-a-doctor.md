---
layout: post 
title: "Choosing a Doctor"
date: 2019-04-08
image: assets/images/choosing-a-doctor.webp
excerpt: "Things to think about when choosing a doctor"
tags: Choosing a Doctor, health care, Medical decisions, Specialists
draft: false
---

Choosing a doctor is a very important decision, yet most people choose doctors based on little more than a recommendation from a friend. “Most of us do more research and spend more time figuring out which refrigerator we want to buy, rather than what doctor we want to take care of us,” according to Dr. Judith Salerno, deputy director of the National Institute on Aging, a component of NIH.

It is important to have a doctor who knows you and your health problems and who understands your special health care needs. A personal physician, or primary care physician, can help you make medical decisions and oversee the care you get from other, more specialized physicians.

Choosing a doctor is a decision that is probably best made while you are healthy and have some time to think about a number of possibilities. If you don’t have a doctor or are thinking about changing doctors, now may be the best time to look for one.

**What Should You Look For?**

Many personal likes and dislikes are involved in choosing a doctor. In general, you want a doctor who is well trained and competent. You also want a doctor who cares about you, who will listen carefully to your concerns, who can explain things clearly and fully, and who can anticipate your health problems. Other things that might affect your choice of a doctor include the type of health insurance you have, the hospital where the doctor treats patients, and the languages the doctor speaks.

There are different types of doctors. You might choose a family practitioner, an internist, or a geriatrician. Family practitioners provide health care to all family members, regardless of age. An internist is a doctor for adults. Some internists take additional training to become specialists; for example, cardiologists are internists who specialize in diseases of the heart. Geriatricians specialize in the care of older adults; they first train in family practice or internal medicine and then undergo additional training in caring for older people.

Dr. Salerno says a good first step is to make a list of the things that matter to you. Then, go back over your list and decide which are most important. See the accompanying side box for some suggestions of things you might want to think about.

**Who Can Help You Find a Doctor?**

After you have a general sense of what you are looking for in a doctor, ask relatives, friends, coworkers, and other health professionals for recommendations. Ask about the person’s experiences with the doctor. For example, “What do you like about Dr. Smith?” A doctor whose name comes up often might be a strong possibility.

It may be helpful to have several names to choose from, in case the doctor you select is not currently taking new patients or does not participate in your health insurance plan. You can usually get a list of participating doctors from your plan’s membership services office. Depending on what insurance plan you have, your choices may be limited to those doctors affiliated with the plan. If a doctor you are interested in is not on the list, however, check with their office; provider lists are not always up to date.

**Making an Informed Choice**

Once you have chosen two or three doctors, it’s a good idea to call their offices. The office staff are often a good source of information about the doctor’s education and training, office policies, and payment procedures. Try make an appointment to talk to the doctor, if you can. Dr. Salerno says, “You’ll probably have to pay for that appointment to come and sit and meet them. But if you choose to do that, it may give you helpful information. You may find that there’s good rapport, or that there’s not with the particular physician who came up on the top of your list.”

Board certification is another way to tell about a doctor’s expertise. Doctors who are board certified have had training after medical school and have passed an exam certifying them as specialists in certain fields of medicine. These include the primary care fields of family practice, internal medicine, and geriatrics.

Dr. Salerno says, “You can go to some libraries and look up the credentials of your physician. There are directories of medical providers and there are also a number of web sites that can provide you information about the specific credentials — for instance, where a physician went to medical school, whether they have other degrees, and if they’re Board Certified.”

Remember, once you have found a doctor you are happy with, your job is not finished. A good doctor/patient relationship is a partnership, with both you and your doctor working together to solve your medical problems and maintain your good health. Make sure that you feel comfortable working with your doctor.

***A Word to the Wise...***

*Things to think about when choosing a doctor:*

- Do you prefer a male or female doctor?
- Do you prefer a younger doctor or an older doctor?
- Where is the location of the doctor’s office? Is it easy for you to get to?
- Is the doctor part of a group or is he a solo practitioner?
- If he or she is a solo practitioner, who covers for them when they’re away?
- Does he or she accept your health insurance?
- What hospital(s) is the doctor affiliated with?
- How comfortable do you feel talking about your health problems with them?
