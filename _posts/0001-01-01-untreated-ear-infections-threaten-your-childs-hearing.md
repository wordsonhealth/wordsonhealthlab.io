---
layout: post 
title: "Untreated Ear Infections Threaten Your Child’s Hearing"
date: 2019-04-02
image: assets/images/untreated-ear-infections-threaten-your-childs-hearing.webp
excerpt: "Can you tell if a child has an ear infection?"
tags: Baby's Hearing, Ear Infection, hearing, NIDCD, Otitis
draft: false
---

Earlier in the week, your 2-year-old had a cold and a sore throat. Now you find he’s more irritable than usual, isn’t sleeping well and keeps tugging at his ear. These are the first signs of a common health problem in infants and toddlers–ear infection. Inflammation of the middle ear, or otitis media as it is known medically, occurs when infections that cause colds or respiratory and breathing problems spread to the middle ear. About 75 percent of children have at least one ear infection by the time they are 3 years old. Left untreated, ear infections not only can cause severe pain, but also can result in serious complications–even hearing loss, according to the National Institute of Deafness and Other Communication Disorders (NIDCD), one component of the National Institutes of Health.

Although adults can develop ear infections, infants and small children seem to experience otitis media more often. Researchers think one reason for this is because the passageway, called the eustachian tube, that connects the upper part of the throat to the inner ear is much shorter and straighter in children than it is in adults. When the tube gets blocked with mucus or by swelling due to a cold or bacterial infection, the middle ear cannot be properly ventilated. Fluid that would ordinarily drain begins to collect in the middle ear, causing pain, hearing difficulty and loss of balance. An untreated infection can spread from the middle ear to other parts of the head, including the brain. In addition, the usually temporary hearing loss from an ear infection can become permanent, if the infection is left untreated.

Because infants and small children have not developed sufficient speech and language skills to tell someone what’s bothering them, detecting a child’s ear infection is often difficult for parents. However, there are some common signs to watch. (see sidebar)

Physicians, on the other hand, can diagnose the condition more easily by examining the child’s ears with a special light called an otoscope. The doctor will look at the outer ear and the eardrum, searching for inflammation, which indicates an infection. If it is found, most physicians will recommend an antibiotic and a pain reliever, if needed.

If fluid remains in the middle ear for more than three months and is associated with loss of hearing, many doctors suggest an operation to insert “drainage tubes” in the affected ears. These tubes usually stay in the eardrum for 6 to 12 months, after which they usually work their way out.

If your child shows signs of an ear infection, consult your doctor as soon as possible.

***A Word to the Wise...***

*Can You Tell If a Young Child Has an Ear Infection?*

Most children affected by otitis media (an infection of the middle ear) do not yet have sufficient speech and language skills to tell someone what is bothering them. That often makes the disorder hard to detect. Parents and caregivers should watch for these common signs:

![img](/assets/images/46-Untreated-Ear-Infections-Threaten-Your-Childs-Hearing.webp)

- Unusual irritability
- Difficulty sleeping
- Tugging or pulling at one or both ears
- Fever
- Fluid draining from the ear
- Loss of balance
- Unresponsiveness to quiet sounds or other signs of hearing difficulty such as sitting too close to the television or being inattentive

If you notice any of these in a child, consult the doctor. The doctor can diagnose an ear infection by examining the ears with a special light instrument. Prompt treatment can relieve pain, cure the infection and prevent permanent hearing loss.

**How Do We Hear?**

The ear consists of three major parts: the outer ear, the middle ear and the inner ear. The outer ear includes the pinna, the visible part of the ear, and the ear canal. The outer ear extends to the tympanic membrane or eardrum, which separates the outer ear from the middle ear. The middle ear is an air-filled space that is located behind the eardrum. The middle ear contains three tiny bones, the malleus (hammer), incus (anvil) and stapes (stirrup), which transmit sound from the

eardrum to the inner ear. The inner ear contains the hearing and balance organs. The cochlea is the hearing organ that converts sound waves into electrical signals that are sent as nerve impulses to the brain where they are interpreted.

![img](/assets/images/46-Middle-Ear.webp)

**What is Middle Ear Dysfunction?**

The middle ear begins at the eardrum. Behind the eardrum is the middle ear space, which is normally filled with air. This space contains the three tiny bones of the middle ear, named for their shapes: the hammer, the anvil, and the stirrup. This space also contains the opening of the eustachian tube. This tube connects the middle ear space with the upper part of the throat.
