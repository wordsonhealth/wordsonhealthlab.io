---
layout: post 
title: "Avoiding Adolescent Alcohol Abuse"
date: 2019-01-18
image: assets/images/avoiding-adolescent-alcohol-abuse.webp
excerpt: "An Open, Trusting Line of Communication is Key"
tags: Adolescent, Alcohol, Alcohol Abuse, dangerous behavior
draft: false
---

You’ve tamed the tantrums, prevailed through Pampers® and potty training, and took on the role of tooth fairy many times, but that was a few years ago. Now you’re faced with a new challenge: dealing with your child’s early adolescence (ages 10-14) and the possibility that he or she might begin using alcohol.

Dramatic physical and emotional changes affect most 10- to 14-year-olds, and often lead to feelings of awkwardness and self-consciousness. Add the effects of peer pressure and this becomes a very impressionable time, a period where children are likely to experiment with alcohol and other harmful substances. But take heart: Study after study has shown that even during the teen years, parents have enormous influence on their children’s behavior.

**SIX WAYS TO SAY NO TO A DRINK**

At some point, your child will be offered alcohol. To resist such pressure, teens say they prefer quick “one-liners” that allow them to dodge a drink without making a big scene. It will probably work best for your teen to take the lead in thinking up comebacks to drink offers so that he or she will feel comfortable saying them. But to get the brainstorming started, here are some simple pressure-busters, from the mildest to the most assertive.

1.  No thanks.
2.  I don’t feel like it — do you have any soda?
3.  Alcohol’s NOT my thing.
4.  Are you talking to me? FORGET it.
5.  Why do you keep pressuring me when I’ve said NO.
6.  Back off!

The best way to influence your adolescent to avoid drinking is to have a strong, trusting relationship with him or her. It is especially important to let your child know that in your eyes, he or she does measure up and that you care for him or her very deeply. When children have a strong bond with a parent, they are more likely to feel good about themselves and less prone to cave in to peer pressure. Having a positive relationship with you may also influence your child to live up to your expectations because he or she wants to keep a close tie with you.

Talking with your child is the best way to help him or her avoid the risks of dangerous behavior. Many parents may feel uneasy bringing up the subject of alcohol, and your young teen may try to dodge the discussion. You should take the time to think about the issues you want to discuss before your talk, as well as how you will answer any questions your child may have. This discussion should be just the first part of an ongoing, comfortable, and open conversation.

Always remember: You can make a difference in the choices your child makes, and now is the time to act.

***A Word to the Wise...***

- Learn about your child’s likes and dislikes and take an active role in what interests him or her.
- Volunteer in your child’s classroom or go along on special outings as a chaperone. This not only helps your child’s schoolteachers, but is also a good way to get to know his or her friends better.
- Try to set aside some time each day, without distractions, to discuss his or her activities that day and don’t be afraid to show your changing child that what goes on in his or her life matters to you.
- Try to ask open-ended questions — avoid ones that can be answered by just a “yes” or “no”. Encourage your child to tell you how he or she thinks and feels about the issue you’re discussing.
- Don’t turn your conversation into a lecture. If you show respect for your child’s viewpoint, he or she will be more likely to listen to and respect yours.
- Let your child know that you appreciate his or her efforts as well as accomplishments, and avoid harmful criticism.
- Understand that your child is growing up and, as you guide his or her behavior, make an effort to respect your child’s need for independence and privacy.

 
