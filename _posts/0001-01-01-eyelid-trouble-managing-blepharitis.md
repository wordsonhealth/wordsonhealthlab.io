---
layout: post 
title: "Eyelid Trouble Managing Blepharitis"
date: 2022-01-03T00:58:48+01:00
image: assets/images/eyelid-trouble-managing-blepharitis.webp
excerpt: Are your eyelids red, swollen, or itchy? This condition may be the cause.
draft: false
---

You probably don’t give your eyelids much thought. But many conditions can irritate them.

One of the most common issues is called blepharitis. Blepharitis is an **inflammation** of the eyelid. It can affect the inside or the outside of the skin that lines the eyes.

The condition can make your eyelids red, swollen, irritated, and itchy. It can also cause crusty dandruff-like flakes to form on your eyelashes. Though rarely dangerous, blepharitis may cause discomfort and pain.

The main cause of blepharitis is extra growth of the normal bacteria found on your skin. Other conditions, including allergies, **rosacea**, certain mites, dandruff, or oily skin can increase the risk of this bacterial overgrowth.

Blepharitis can lead to other eye problems. Common ones include a stye, which is a red, painful bump on the eyelid caused by a blocked oil gland. A chalazion is like a stye, but doesn’t hurt, though it can make your eyelid swell and turn red. Very rarely, blepharitis can cause damage to the cornea—the clear outer layer at the front of your eye.

Blepharitis often contributes to another common eye problem called dry eye. In this condition, oil and flakes alter the thin layer of tears that sits across the surface of your eye. This can make your eyes feel dry.

But some people’s eyes instead feel watery or teary because their tears aren’t working correctly. That’s because of inflammation on the eye’s surface.

“Patients with dry eye tell me that their eyes water all the time, especially in windy environments,” explains Dr. Jason Nichols, an eye doctor who studies dry eye diseases at the University of Alabama at Birmingham.

Once someone develops blepharitis, it never totally goes away. But flare ups can be managed and prevented.

Most people can keep the condition in check with good eyelid hygiene. See the Wise Choices box for easy tips on eyelid care.

“But people have to be consistent and clean their eyes daily,” says Nichols.

Some people with blepharitis may be prescribed antibiotics. Others need medications to reduce inflammation or keep their eyes moist.

If you have recurring irritation of your eyes or your eyelids, Nichols says, “see an eye care provider, and make sure you get an accurate diagnosis.”

Nichols’ research team is working on developing imaging and other methods to look closely at the surface of our tears and oil glands in the eyes. This may help them better understand what happens when the eyelids get irritated.

“We often take our eyes for granted, but when things go wrong, it really does have an impact on quality of life,” Nichols says.

### Wise Choices

#### Eyelid Care

Steps for cleaning your eyelids when you have blepharitis:

- Wash your hands with soap and water.
- Mix warm water with a gentle cleanser on a soft washcloth.
- Press the cloth against your closed eye for a few minutes to loosen crusts. This can also help keep your oil glands from clogging.
- Gently rub the cloth back and forth, focusing on the area where your eyelashes meet your eyelids.
- Rinse your eye with clean water.
- Commercially available eyelid cleaning wipes and non-allergenic makeup removal wipes are also available.
