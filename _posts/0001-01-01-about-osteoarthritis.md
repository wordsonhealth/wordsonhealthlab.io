---
layout: post 
title: "Signs and symptoms of osteoarthritis"
date: 2019-01-06
image: assets/images/about-osteoarthritis.webp
excerpt: "Older people aren't the only ones who get Osteoarthritis"
tags: Arthritis, Bones, Joints, Osteoarthritis
draft: false
---

It may start as a little morning stiffness and for some people it may never get any worse. But for millions of other Americans aged 65 or older (though symptoms can occur before age 45), osteoarthritis — a degenerative joint disease — can become disabling and even crippling.

Osteoarthritis is a painful disease that mostly affects the cartilage in a joint causing it to wear away. Cartilage is the slippery tissue that covers the ends of bones and allows them to glide over each other and acts much like a shock absorber. Although it is not known what causes osteoarthritis, scientists suspect a combination of factors is responsible, including excess weight, joint injury, stresses on joints from certain jobs and a family history of osteoarthritis. Unlike other forms of arthritis, the disease only affects the joints, usually the ends of the fingers, thumbs, neck, lower back, knees and hips.

In the early stages of osteoarthritis, joints may simply ache a little after physical work or exercise. But over time, as more and more of the cartilage wears down, the bare bone ends begin to rub against each other, causing steady or intermittent pain, swelling, tenderness, and loss of motion in the joint. You may even experience a crunching feeling or the sound of bone rubbing on bone. Bits of bone or cartilage may break off and float inside the joint space causing even greater pain and damage.

If you suspect you have osteoarthritis and your doctor confirms that you do, he or she can help you establish a treatment program to fit your needs, lifestyle, and health. Treatment approaches include exercise, rest and proper care of the joint, weight control, and medicines.

**A Healthy Joint**

![img](../assets/images/joint01.webp)

In a healthy joint, the ends of boes are encased in smooth cartilage. Together, they are protected by a joint capsule lined with a synovial membrane that produces synovial fluid. The capsule and fluid protect the cartilage, muscles, and connective tissues.

**A Joint With Osteoarthritis**

![img](../assets/images/joint02.webp)

With osteoarthritis, the cartilage becomes worn away. Spurs grow out from the edge of the bone, and synovial fluid increases. Altogether, the joint feels stiff and sore.

**What should you do?**

- Ask your doctor or physical therapist what exercises are best for you and for guidelines on exercising when you’re sore or have swelling.
- Learn to slow down and rest when your body tells you to. Too much activity or overexercising can cause more pain.
- Learn how to protect your joints and take pressure off them.
- If you are overweight or obese, losing weight can reduce stress on joints and slow down further injury. Regular exercise and a healthy diet can help you get rid of those extra pounds.
- Ask your doctor if you should take medicines to stop or reduce pain so you can function better. Common over-the-counter drugs for treating osteoarthritis include ibuprofen (Advil or Motrin), naprosyn ( Aleve) and acetaminophen (Tylenol). Among prescription drugs for this disease are two new ones, Vioxx and Celebrex. Just be sure to find out from your doctor which medicine is best suited to your needs. Most of the drugs can cause serious side effects like bleeding ulcers or kidney problems. They should be used with caution by people over 65 or those with a history of ulcers or gastrointestinal bleeding.

***A Word to the Wise...***

***If you have osteoarthritis:***

- Break down activities into small tasks that you can more easily manage.
- Develop a daily routine that allows you to balance rest with activity.
- Ask your doctor about joining a self-help and education program. These programs teach about osteoarthritis, its treatments, exercise and relaxation, and how to communicate with your doctor and solve problems.
- Develop a support system of family, friends, and health caregivers who will be there to give a helping hand if you need it to manage your osteoarthritis.
- Focus on your abilities instead of disabilities, your strengths instead of weaknesses, and keep a positive attitude.
