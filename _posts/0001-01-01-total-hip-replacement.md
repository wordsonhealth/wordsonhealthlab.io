---
layout: post 
title: "Total Hip Replacement"
date: 2019-02-04
image: assets/images/total-hip-replacement.webp
excerpt: "Artificial Hips Relieve Pain and Renew Lives"
tags: Artificial Hips, Hip Replacement, Hip surgery, The hip joint
draft: false
---

Are walking and bending over and other daily activities becoming too painful because of osteoarthritis in your hip? If so, then hip replacement might be something to consider. It is one of the most successful surgical procedures of this century. A fact sheet entitled “Questions and Answers about Hip Replacement” from the National Institute of Arthritis and Musculoskeletal and Skin Diseases, a component of the National Institutes of Health, can provide you with useful information. Some of the topics covered are:

**Who Should Have Hip Replacement Surgery?**

In the past, hip replacement surgery was limited mainly to people over age 60. Although younger individuals may have needed this type of surgery, their higher level of activity after surgery caused the artificial parts to wear out faster. But new technology has improved the durability of artificial hips, and now this surgery can be successful in carefully selected younger people as well.

**What Are Alternatives to Hip Replacement?**

Before considering this surgery, doctors will usually try other methods of treatment such as pain medication, physical therapy, and walking aids. An exercise program can strengthen the muscles in the hip joint and sometimes improve positioning of the hip and relieve pain.

**What Does Hip Replacement Surgery Involve?**

There are several different procedures; each one has advantages and disadvantages. The doctor must determine which procedure is best for each patient. The surgery usually lasts two to three hours.

![img](/assets/images/17-Total-Hip-Replacement.webp)

The hip joint consists of a “ball and socket” arrangement. The “ball” is at the top or head of the femur, or thighbone, and fits into the “socket,” or acetabulum, which is a cup-like structure in the pelvis, or hip bone. During a total hip replacement, the surgeon removes diseased bone tissue and cartilage from the hip joint and replaces the head of the femur and the acetabulum with artificial parts. The artificial “ball” is made of metal or ceramic with a stem that goes into the femur, and the “socket” is a strong, moldable plastic. These materials allow a natural, gliding motion of the joint.

**What Can You Expect After Surgery?**

Within a day after surgery, you may begin doing exercises such as contracting and relaxing certain muscles to strengthen the hip. You will also learn how to prevent injury to your hip during simple daily activities. Most patients can leave the hospital in less than 10 days. Complications following hip replacement surgery have been greatly reduced in recent years. The most common problems are loosening and dislocation.

**What Types of Exercises Are Best for Someone With a Total Hip Replacement?**

After you have had a hip replacement, you should talk to your doctor or his or her staff about setting up an exercise program. Recommended activities include walking, swimming, and stationary bicycling to reduce joint pain and increase flexibility and muscle strength. But high-impact activities, such as basketball, jogging, and tennis, should be avoided so you won’t stress or jar the new joint.

***A Word to the Wise...***

**How to Prepare for Surgery and Recovery**

People can do many things before and after they have surgery to make everyday tasks easier and help speed their recovery.

**Before Surgery**

- Learn what to expect before, during, and after surgery. Request information written for patients from the doctor.
- Arrange for someone to help you around the house for a week or two after coming home from the hospital.
- Arrange for transportation to and from the hospital.
- Set up a “recovery station” at home. Place the television remote control, radio, telephone, medicine, tissues, waste basket, and pitcher and glass next to the spot where you will spend the most time while you recover.
- Place items you use every day at arm level to avoid reaching up or bending down.
- Stock up on kitchen staples and prepare food in advance, such as frozen casseroles or soups that can be reheated and served easily.

**After Surgery**

- Follow the doctor’s instructions.
- Work with a physical therapist or other health care professional to rehabilitate your hip.
- Wear an apron for carrying things around the house. This leaves hands and arms free for balance or to use crutches.
- Use a long-handled “reacher” to turn on lights or grab things that are beyond arm’s length. Hospital personnel may provide one of these or suggest where to buy one.
