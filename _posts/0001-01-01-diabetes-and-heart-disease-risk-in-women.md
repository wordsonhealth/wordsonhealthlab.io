---
layout: post 
title: "Diabetes and Heart Disease Risk in Women"
date: 2019-01-28
image: assets/images/diabetes-and-heart-disease-risk-in-women.webp
excerpt: "Women with Diabetes Face Greater Risk of Heart Disease"
tags: Diabetes, Diabetes type 2, Heart Disease, Women with Diabetes
draft: false
---

If you are overweight, you are at risk for diabetes. And if you are a woman, you should know that diabetes can affect you differently than a man, particularly your heart.

Diabetes is on the rise, both in men and women, young and old. “There is an epidemic of diabetes in this country,” said Barbara V. Howard, Ph.D., President of MedStar Research Institute in Washington, D.C. She was speaking at a recent seminar on women and diabetes sponsored by NIH’s Office of Research on Women’s Health.

Some 16 million Americans have diabetes, about one-third of whom do not know it. More people than ever before are developing type 2 diabetes, mainly because of obesity and inactivity, the two major risk factors for this type of diabetes. Most women with diabetes have type 2 diabetes, which is more common in older people. Women who develop diabetes during pregnancy, known as gestational diabetes, are at increased risk of developing type 2 diabetes later in life.

Diabetes can lead to blindness, kidney failure, nerve damage that can result in foot or leg amputation, heart disease and stroke. Special attention must be paid to this public health problem, particularly in women, Dr. Howard stressed.

The reason? “Type 2 diabetes is a major risk factor for heart disease in women,” she said. Many studies have shown that women with diabetes have more than three times the risk of developing heart disease — the number one killer of all women — than women without diabetes (men with diabetes have a 1.7 higher risk), she said. Even younger women with diabetes have an increased risk of heart disease.

**Did You Know That…**

- About 90 percent of diabetes in the U.S. is the type 2 form, which occurs in most people after the age of 40.
- Type 2 diabetes is increasing in young people, especially among those who are overweight, physically inactive, or have a family history of diabetes.
- Obesity is a major risk factor for diabetes. Upper-body obesity is a stronger risk factor for type 2 diabetes than excess weight below the waist.
- Regular physical activity can protect against type 2 diabetes, while a lack of exercise is a risk factor for developing diabetes.

Why do women with diabetes face an increased risk of heart disease? When women develop diabetes, they have more adverse changes that add to heart disease risk, Dr. Howard stated. Their blood pressure rises, HDL (good) cholesterol levels fall, and abdominal fat increases. Similar changes occur in men with diabetes, but not to the same extent as in women.

According to recent statistics, diabetes is increasing among women of all races. Hardest hit are African American, Hispanic, and Native American women. Rates of diabetes are generally higher among American Indians but vary considerably among tribes. Over the age of 55, more than 50 percent of women from some American Indian tribes have diabetes.

Diabetes occurs about equally among men and women, according to NIH’s National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK). Unlike men, women rarely develop heart disease before age 45. However, women catch up with men after age 65, when diabetes is more prevalent in both sexes.

Although diabetes cannot be cured, it can be controlled. The take-home message, says Dr. Howard, is that doctors need to be more aggressive in treating risk factors for heart disease in women with diabetes. And if you are a woman with diabetes, you should discuss with your doctor what you should do to prevent heart disease and control diabetes.

***A Word to the Wise...***

If you are a woman, there are things you can do to reduce your chances of developing diabetes, or, if you have diabetes, to help prevent long-term complications:

- Maintain a healthy lifestyle by eating the right foods and getting regular exercise. Some diabetes may be prevented with weight control and physical activity.
- Find out if you have any risk factors for diabetes, such as: being older than 45, being overweight, having a close family member (parent, sister, or brother) who has had diabetes, having had diabetes when you were pregnant, and being African American, Hispanic/Latino, Asian American or Pacific Islander, or Native American.
- Avoid cigarette smoking. Although it is not directly related to diabetes, smoking increases the risks for a number of diseases, such as heart disease.
- If you are at risk for diabetes, have regular check-ups and speak to your health care provider about what you can do to prevent diabetes.
- If you have diabetes, find out what you should do to control it.
