---
layout: post 
title: "Discussing Drinking"
date: 2019-06-14
image: assets/images/discussing-drinking.webp
excerpt: "A Back-to-School Conversation You Need to Have"
tags: Alcohol, Alcohol Poisoning, College Drinking, Drinking, NIAAA
draft: false
---

As college students arrive on campus this fall, it’s a time of new experiences, new friendships and making memories that will last a lifetime. Unfortunately for many, it can also be a time of excessive drinking and dealing with its aftermath-vandalism, violence, sexual aggression and even death. For those beginning their college experience, a rapid increase in heavy drinking over a relatively short period of time can cause serious problems with the transition to college. Alcohol abuse can also be a problem for high school students. Fall semester is a good time to sit down with your child to have a frank discussion about drinking.
We go through many changes in our teen years. Relationships change as our bodies and brains mature. Recent research has shown that the human brain continues to develop into a person’s early 20’s.

This period is also marked by taking risks. This can include risky drinking. And early drinking is associated with other risky behavior, such as academic failure, unsafe sexual behavior and drug use. Over the long-term, early drinking is associated with an increased risk of developing an alcohol use disorder at some time during the life span.

![img](/assets/images/82-Discussing-Drinking.webp)

The consequences of excessive drinking by young people are more significant, more destructive and more costly than many parents realize. According to the College Drinking Task Force report to NIH’s National Institute on Alcohol Abuse and Alcoholism (NIAAA), drinking by 18- to 24-year old college students contributes to an estimated 1,700 student deaths, 599,000 injuries and 97,000 cases of sexual assault or date rape each year.

Students form their expectations about alcohol from their environment and from each other. As they face the insecurity and stresses of establishing themselves in a new social setting, environmental and peer influences combine to create a culture of drinking. This culture actively-or at least passively -promotes drinking through tolerance, or even unspoken approval, of college drinking as a rite of passage.

The transition to college can be difficult, with about 1 of 3 first-year students failing to enroll for their second year. Anecdotal evidence suggests that the first 6 weeks of the first semester are critical to a first-year student’s academic success. Many students begin drinking heavily during these early days of college, and this can interfere with their successful adaptation to campus life.

But parents can still play a major role in preventing alcohol problems. The time to start is before your child leaves for college. As the fall semester begins, prepare your college-age children by talking with them about the consequences of drinking. Stay involved during the crucial early weeks of college. Inquire about campus alcohol policies, and ask your children about their roommates and living arrangements.

High school students can also come under pressure to drink from their peers at school. It’s important to talk to your high school students about peer pressure and how to resist it. They need to know that alcohol can harm their judgment, coordination and reflexes. It can cause them to lose control, take chances and do things they never would do otherwise. In fact, alcohol is linked with an estimated 5,000 deaths in people under age 21 each year-more than all illegal drugs combined.

When you sit down to talk with your child about the consequences of drinking, discuss the penalties for underage drinking as well as how alcohol use can lead to date rape, violence and academic failure. Underage drinking has also been linked with deaths and injuries from burns, falls, alcohol poisoning and suicide.

Discuss drinking and driving. Motor vehicle crashes are the leading cause of death in people aged 15 to 20. Deadly crashes involving alcohol are twice as common in teens compared with people 21 and older.

Now’s the time to talk to your children about the dangers of alcohol. Help prevent them from doing something that they-and you-might regret for the rest of their life.

*Statistics:*

- More than three-fourths of 12th graders and two in five 8th graders have tried alcohol.
- Of the people who began drinking before age 14, 47% became alcohol dependent at some point later in their lives, compared with 9% of those who began drinking at age 21 or older.
- Drinking by 18- to 24-year old college students contributes to an estimated 1,700 student deaths, 599,000 injuries and 97,000 cases of sexual assault or date rape each year.

Information supplied by NIAAA, from various sources.

*Definitions:*

**Anecdotal evidence:**
Evidence that’s based not on carefully controlled scientific experiments, but on things that people notice around them.

**Peer pressure:**
The feeling that someone your own age is pushing you toward making a certain choice.

***A Word to the Wise...***

*Alcohol Poisoning*

When you sit down to talk to your children about the dangers of alcohol abuse, take a few minutes to help them recognize the signs of alcohol poisoning. It could mean the difference between life and death for them or one of their friends.

**What Is Alcohol Poisoning?** Too much alcohol can shut down the parts of the brain that control breathing and the gag reflex, which prevents choking. Someone who drinks a fatal dose of alcohol will eventually stop breathing. Even if someone survives an alcohol overdose, the experience can cause irreversible brain damage. Rapid binge drinking is especially dangerous because victims can continue drinking beyond a fatal dose before they lose consciousness.

A person who appears to be sleeping it off may still be in real danger. Blood alcohol levels can continue to rise even after someone’s passed out, since alcohol in the stomach and intestine can continue to enter the bloodstream and circulate throughout the body.

**What Should I Look For?** Critical signs of alcohol poisoning include mental confusion, unconsciousness, vomiting, seizures, slow (fewer than 8 breaths per minute) or irregular (10 seconds or more between breaths) breathing and hypothermia (low body temperature, bluish skin color and paleness).

**What Should I Do?** Know the danger signals. If you suspect someone has alcohol poisoning, don’t wait for all the critical signs to be present. If you suspect an alcohol overdose, call 911 immediately for help.
