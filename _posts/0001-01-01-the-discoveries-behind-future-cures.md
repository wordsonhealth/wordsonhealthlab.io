---
layout: post 
title: "The Discoveries Behind Future Cures"
date: 2019-06-24
image: assets/images/the-discoveries-behind-future-cures.webp
excerpt: "Nobel Prize Honors Basic Research Breakthroughs"
tags: Biology, Future Cures, Nobel Prize, Research
draft: false
---

The Nobel Prize, which is given every year, recognizes great achievements that have improved our lives. Two of this year’s prizes honor advances in basic biology that have already opened up new avenues for diagnosing and treating a host of human diseases.

The prize winners get a medal and $1.4 million. We all get a future of better health.

Both prizes involve a process central to life: how instructions encoded in our DNA are used to make proteins, molecules that perform critical tasks in the body. When a cell needs a protein, it first copies the genetic instructions for that protein from the DNA into a related molecule called RNA. This copying process is known as transcription. Like a courier, the RNA then delivers the instructions to the cell’s protein-making factories.

![img](/assets/images/87-The-Discoveries-Behind-Future-Cures.webp)

Without transcription, all creatures would die because their cells no longer would make proteins. Many human illnesses, like heart disease and uncontrolled inflammation, have been linked to transcription errors.

Dr. Roger D. Kornberg won the Nobel Prize in chemistry this year for figuring out a key step in transcription. He follows in the footsteps of his father, who won a Nobel Prize in 1959.

To better understand how the process works and what can cause it to go awry, Kornberg used high-tech tools to capture the first image of the protein central to transcription in organisms ranging from yeast to humans. That snapshot is now helping researchers interested in transcription-related diseases to identify potential new medicines.

Doctors are already testing this year’s Nobel Prize-winning discovery in physiology or medicine as a treatment for asthma, diabetes, brain diseases and age-related vision loss. In 1998, researchers Dr. Craig C. Mello and Dr. Andrew Z. Fire identified a process that could turn off or “silence” genes, segments of DNA that help determine our appearance, behavior and health.

Other researchers had previously noticed a puzzling phenomenon in petunias. When they gave the flowers an extra “purple” gene to brighten their color, the petals unexpectedly became stark white. It turns out that a special type of RNA prompts cells to destroy their own matching RNAs, effectively silencing genes.

Fire and Mello found that a similar thing happened in worms. By giving pieces of this RNA to the worms, the scientists could target and turn off specific genes. They called the process “RNA interference,” or RNAi. Within a year, RNAi was documented in several other organisms.

RNAi has now become a powerful tool for scientists. Many researchers predict that gene silencing might one day lead to new medical treatments for a number of human diseases, such as cancer and viral infections.

“These discoveries in basic science have changed our view of biology and they’re already beginning to change how we treat disease,” says Dr. Jeremy M. Berg, director of NIH’s National Institute of General Medical Sciences, which has supported the research of Fire, Mello and Kornberg for many years.

Researchers around the world, many of them funded by NIH, continue to make new breakthroughs in basic science that will one day lead to the medical techniques of the future.

Berg asserts, “More breakthroughs that lay the foundation for improvements in health and medicine are on the way.”
