---
layout: post 
title: "Gut Health and Gluten Intolerance"
date: 2019-06-22
image: assets/images/gut-health-and-gluten-intolerance.webp
excerpt: "In Celiac Disease, the Two Don’t Mix"
tags: Celiac Disease, Gluten, Intestine, Malnutrition, NIDDK
draft: false
---

What if you couldn’t eat bread? Or pasta? Or cookies? What if you couldn’t eat anything containing wheat, rye and barley because of gluten, a protein found in these grains? You would be among the millions of American who get gas, diarrhea and other symptoms whenever they eat foods with gluten. The condition is called celiac disease, and many people who have it don’t even realize it.

Celiac disease shouldn’t be ignored, though. It can lead to malnutrition, osteoporosis and other serious problems. The only treatment is to eliminate gluten from your diet.

![img](/assets/images/86-Gut-health-and-gluten-intolerance.webp)

When people with celiac disease eat things with gluten, their immune system attacks their small intestine. This attack damages and can even destroy the tiny finger-like structures called villi that line the small intestine. Without healthy villi, which normally allow nutrients from food to be absorbed into your bloodstream, your body can’t get enough nutrients no matter how much food you eat. The resulting malnutrition can be a serious problem—particularly for children, who need adequate nutrition to grow and develop properly.

The symptoms of celiac disease can vary. Dr. Stephen P. James of NIH’s National Institute of Diabetes and Digestive and Kidney Diseases (NIDDK) explains, “One of the challenges with celiac disease is the vast array of symptoms associated with the disease.” Symptoms range from gas, diarrhea and belly pain to delayed growth, certain skin rashes, infertility and osteoporosis.

The symptoms can be similar to those of several other diseases, so diagnosing celiac disease is sometimes difficult. Some people with the disease don’t even have any symptoms. With or without symptoms, people with celiac disease are at risk for the complications of the disease. The longer a person with celiac disease goes undiagnosed and untreated, the greater their chance of developing malnutrition and other serious problems.

“We now know that celiac disease is more prevalent than previously thought—affecting nearly 1 percent of the U.S. population—and remains under-diagnosed,” said Dr. Griffin Rodgers, acting director of NIDDK.

To heighten awareness of celiac disease among both health professionals and the public, NIDDK, along with other organizations, recently launched a campaign offering a variety of resources about celiac disease.

James said, “We are hoping to educate health professionals and the public that celiac disease is not only a gastrointestinal disease.”

If you suspect you have celiac disease, see your doctor for a diagnosis as soon as you can. They can draw blood and measure the levels of molecules made by your immune system that attack your small intestine. Before being tested, continue to eat a regular diet that includes foods with gluten. If you stop eating these foods beore being tested, the results may be negative for celiac disease even if you have it.

If your tests and symptoms suggest celiac disease, your doctor will likely perform a small-bowel biopsy, during which a tiny piece of tissue from the small intestine is removed to check for damage to the villi.

The good news if you’re diagnosed with celiac disease is that following a gluten-free diet will stop symptoms, heal existing intestinal damage and prevent further damage for most people with the illness. Improvements usually begin within days of eliminating gluten. The small intestine usually heals completely within months in children and young adults, and within two years for older adults.

A gluten-free lifestyle can be challenging. To start, you’ll need to adopt a completely new approach to eating. If you’re diagnosed with celiac disease, your doctor will probably ask you to work with a dietitian on a gluten-free diet plan. People with celiac disease have to be very careful about what they buy for lunch at school or work, what they buy at the grocery store, what they eat at restaurants or parties and what they grab for a snack. The dietitian can teach you how to read ingredient lists and identify foods that contain gluten.

People with celiac disease need to eliminate gluten for the rest of their lives, not just until they’re healed. Eating any gluten, no matter how little, can damage your small intestine again, whether or not you have noticeable symptoms. Newly diagnosed people and their families may find support groups helpful as they all learn to adjust to this new way of life. With practice, looking for gluten becomes second nature.

Unfortunately, some people’s intestines are so severely damaged by the time they’re diagnosed that, despite a strictly gluten-free diet, they can’t heal. Because their intestines are not absorbing enough nutrients, they might need to receive nutrients directly into their bloodstream through a vein (intravenously). Researchers are now evaluating drug treatments for such “unresponsive” celiac disease.

If you suspect you have celiac disease (see the list in the box below for reasons to suspect celiac disease), don’t hesitate to see your doctor. If you wait too long, you may find yourself dealing with much more serious complications than an altered diet.

*Definitions:*

**Dietitian:**
A health care professional who specializes in food and nutrition.

**Immune System:**
The system that protects your body from invading viruses, bacteria and other microscopic threats.

**Malnutrition:**
When your body doesn’t get the right balance of nutrients it needs.

**Osteoporosis:**
A disease in which your bone becomes thin and fragile and can break easily.

***A Word to the Wise...***

*How to Recognize Celiac Disease*

Celiac disease affects people differently. One person might have diarrhea and belly pain, while another may be irritable or depressed. In fact, irritability is one of the most common symptoms in children. Symptoms of celiac disease may include one or more of the following:

- gas
- recurring bloating, belly pain
- chronic diarrhea
- pale, foul-smelling or fatty stool
- weight loss or weight gain
- fatigue
- unexplained anemia (lower red blood cell counts causing fatigue)
- bone or joint pain
- bone loss or weakening
- behavioral changes
- tingling numbness in the legs (from nerve damage)
- muscle cramps
- seizures
- missed menstrual periods (often because of excessive weight loss)
- infertility, recurrent miscarriage
- delayed growth
- failure to thrive (in infants)
- pale sores inside the mouth
- tooth discoloration or loss of enamel
- itchy skin rash called dermatitis herpetiformis
