---
layout: post 
title: "Heart Disease After Bypass Surgery"
date: 2019-01-08
image: assets/images/heart-disease-after-bypass-surgery.webp
excerpt: "Women Are Not Reducing Their Risk Factors for Heart Disease after Bypass Surgery"
tags: bypass surgery, Coronary artery, Coronary heart disease
draft: false
---

One year after undergoing coronary artery bypass surgery, many women still have risk factors for heart disease, according to research funded by NIH’s National Institute of Nursing Research. The good news is that many of these risk factors can be controlled.

Coronary heart disease is the leading cause of death among women. One in ten American women ages 45 to 64 has some form of heart disease, and this increases to one in five women over 65.

Certain risk factors — some of which can be controlled — increase a woman’s chances of developing heart disease. These include smoking, high blood pressure, high blood cholesterol, obesity, diabetes, and physical inactivity. Other risk factors cannot be controlled, such as being age 55 or older or having a family history of early heart disease.

Jerilyn Allen, Sc.D., R.N., a nurse researcher and associate professor at The Johns Hopkins University School of Nursing, studied 130 women between the ages of 55 and 75 with heart disease who had undergone bypass surgery a year earlier. Before the operation, 17 percent of the women in the study smoked cigarettes; a year later, 10 percent were still smokers. Furthermore, over half of the women were still obese; 54 percent had high blood pressure; and 92 percent still had high levels of “bad” cholesterol (LDL or low density lipoproteins).

There are several possible explanations for the continued presence of risk factors, says Dr. Allen. “After surgery, some of these women may view themselves as no longer at risk for heart disease. Others are not being properly informed about the risk factors that lead to subsequent episodes of heart disease. Furthermore, some women may find it difficult to change long-standing habits, and require additional support.”

The presence of risk factors increases a woman’s chance of further heart disease. But there are steps that can be taken by both health care providers and patients to lower the risk.

Health care providers need to inform their patients about their risk for future disease, explains Dr. Allen. This is especially important, because most patients do not spontaneously change their behavior after bypass surgery.

In addition, health care providers should encourage patients with heart disease to incorporate healthy habits, such as maintaining a proper weight, stopping smoking, following a low-fat diet and increasing physical activity. Patients also need to take an active role in their health care by asking questions and seeking help when they need it.

Dr. Allen also is studying the effectiveness of a model in which nurses help cardiac patients manage their risk factors after surgery. “Nurses know how to help patients make difficult lifestyle changes and manage risk factors,” adds Dr. Allen. “Nurses could be hired as case managers as part of a team to prevent future heart problems in patients.”

Since most bypass patients are only hospitalized for four days after surgery, Dr. Allen explains, there is little time for developing a comprehensive treatment plan that would include developing strategies for modifying behavior. Nurse case managers can assist in this process by acting as an ongoing resource for patients and their families and by providing important follow-up services to help patients manage their risk factors after surgery.

***A Word to the Wise...***

- Take an active role in your health care after surgery.
- Speak with your health care providers about what to do to lower your risk of future heart disease, including participating in a cardiac rehabilitation program.
- Know your numbers. Blood pressure numbers and cholesterol levels are important indicators of risk of future heart disease. Make sure your health care providers are testing for them, and if the numbers are high, ask how to lower them.
- Make necessary lifestyle changes, such as stopping smoking, eating a low-fat diet and exercising. If you are having difficulties making changes, ask your health care providers for help.
