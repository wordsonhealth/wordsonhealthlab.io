---
layout: post 
title: "Safer Fun in the Summer Sun"
date: 2019-04-22
image: assets/images/safer-fun-in-the-summer-sun.webp
excerpt: "The ABCDEs of Skin Cancer"
tags: cancer, Melanoma, Skin Cancer, Summer Sun, Ultraviolet radiation
draft: false
---

Any diagnosis of cancer can be frightening—including skin cancer, the most commonly diagnosed cancer in the United States. However, skin cancer accounts for less than 1 percent of all cancer deaths; 85% to 95% of all cases are cured. Prevention and early detection are the most important weapons in the battle against skin cancer. Continuing research is making them ever more effective.

Skin cancer is strongly associated with exposure to ultraviolet (UV) radiation, part of the energy that comes from the sun (it also can come from artificial sources like sun lamps and tanning booths). UV radiation is made up of two types of rays, called UVA and UVB rays. UVB rays are more likely than UVA rays to cause sunburn, but UVA rays pass more deeply into the skin. Scientists have long thought that UVB radiation causes the skin damage that can lead to skin cancer and premature aging. They now think that UVA radiation may have these consequences, too.

![img](/assets/images/56-Safer-Fun-in-the-Summer-Sun.webp)

According to NIH’s National Cancer Institute, the cure rate for skin cancers could be nearly 100% if they were all brought to a doctor’s attention before they had a chance to spread. There are three different types of skin cancer—melanoma, basal cell carcinoma and squamous cell carcinoma. It is particularly important to diagnose and treat melanoma early. Melanoma is the deadliest form of skin cancer, with 55,100 new cases and 7,910 deaths expected this year in the U.S. alone.

*Definitions:*

**Basal Cells:** Small, round cells in the lower part (base) of the epidermis, the outer layer of skin.
**Carcinoma:** Cancer that begins in the skin or in tissues that line or cover internal organs.
**Dermatologist:** Doctor with special training in diseases and conditions of the skin, hair and nails.
**Melanoma:** A form of skin cancer that begins in melanocytes (cells that make pigment). Usually begins as a mole.
**Squamous Cells:** Thin, flat cells in the surface of skin, the lining of hollow organs in the body and the passages of the respiratory and digestive systems.

Melanoma usually begins as a mole. Twenty years ago, dermatologists noted that the typical warning signs of early melanoma follow an easy-to-remember formula:

- Asymmetry—the mole is not a circle, but lopsided
- Borders—the mole has uneven or ragged edges
- Color—the mole is not a uniform brown, but a mix of brown, black, red, blue, and white
- Diameter—the mole is wider than a pencil eraser

The dermatologists who devised that list now suggest adding “E,” for “Evolving.”

“An evolving lesion is one that changes size, shape or symptoms, such as itching or tenderness,” Dr. David Polsky of New York University’s Department of Dermatology explained.

The “E” captures a particular type of melanoma, called nodular, which often does not follow the original ABCs, Polsky said. Nodular melanoma is the most aggressive type of melanoma and accounts for 10% to 15% of all melanomas.

Polsky led a group suggesting the alphabetical expansion in a recent review published in the Journal of the American Medical Association. They cited a study of 125 patients with nodular melanoma in which 78% had noticed a significant change in their mole’s appearance. Other studies support the idea that moles that change shape, color, or size are more likely to be melanoma.

While melanoma may be the most deadly type of skin cancer, both basal cell carcinoma and squamous cell carcinoma are far more common. Researchers estimate that 40% to 50% of people who live to age 65 will be diagnosed with one of these skin cancers. They can occur anywhere but are typically on the head, face, neck, hands, and arms. They grow more slowly than melanoma and rarely spread to other areas, but need to be treated as well.

Carcinomas can appear as small, smooth, shiny, pale or waxy lumps, or sometimes as a firm red lump. Some people develop a precancerous condition called actinic keratosis, a rough, red or brown scaly patch on the skin that may develop into squamous cell carcinoma. It usually occurs in areas that have been exposed to the sun, such as the face, the back of the hands and the lower lip.

Your overall chance of developing a skin cancer is related to your lifetime exposure to UV radiation. While most skin cancers appear after age 50, the sun’s damaging effects begin at an early age. It’s important to start sun protection in childhood to prevent skin cancer later in life. Check yourself regularly for new growths or other changes in your skin, and report any unusual growths to a doctor.

**Questions for your doctor**
If you are diagnosed with skin cancer, here are some questions to ask your doctor:

- Exactly what kind of cancer do I have?
- What types of treatment are available?
- Are there any risks or side effects of treatment?
- Will there be a scar?
- Will I have to change my normal activities?
- How can I protect myself from getting skin cancer again?
- How often will I need a checkup?

Source: National Cancer Institute

***A Word to the Wise...***

*Preventing Skin Cancer:*

- Avoid exposure to the midday sun, when your shadow is shortest (from 10 a.m. to 2 p.m. standard time or 11 a.m. to 3 p.m. daylight savings time).
- Wear protective clothing, such as sun hats and long sleeves, to block out the sun’s harmful rays.
- Use sunscreen with a sun protection factor (SPF) of at least 15, reapplying frequently. But be careful not to let sunscreen encourage you to spend even more time in the sun.
- Do a simple skin self-exam regularly for anything unusual, like a change in the size, texture, or color of a mole, or a sore that does not heal.
