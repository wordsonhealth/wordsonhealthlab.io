---
layout: post 
title: "It S Never Too Late to Get in Shape"
date: 2019-02-10
image: assets/images/it-s-never-too-late-to-get-in-shape.webp
excerpt: "Regular exercise could make the difference between an individual"
tags: Activities, Exercise, Get in Shape, Strength, Training
draft: false
---

In 1900, an American could expect to live until age 47. Today, 71 million Americans are age 50 or older and, this year alone, millions more “baby boomers” are entering their 50’s and expecting to live until their mid- or late 70’s, if not longer.
Most Americans want to stay healthy as they age so they can continue to live in their own homes and do things on their own. The key to being able to do this, according to a growing body of research, is regular physical activity and exercise. Older Americans who stay active throughout their later years can maintain or increase their endurance, strength, balance, and flexibility. In some cases, staying active or exercising regularly on a long-term basis can actually improve the health of older people who already have diseases and disabilities.

To help us stay fit as we age, the National Institute on Aging (NIA), a component of the National Institutes of Health, along with astronaut and Senator John Glenn, has launched a national education campaign for keeping fit after 50. They have a new 100-page illustrated booklet, “Exercise: A Guide from the National Institute on Aging,” that shows easy ways to increase your physical activity in order to improve your health and vitality as you age.

“Regular exercise could make the difference between an individual—or a society—that ages well or one that doesn’t,” said Senator Glenn. “This guide gives people the tools they need to start an exercise habit and stick with it.” He said his participation in last October’s shuttle mission at age 77 would not have been possible without attention to exercise and diet. An important part of his regimen has always involved exercises anyone can do, such as jogging and brisk walking, rather than relying on high-tech equipment or training.

**What Can Exercise Do for Me?**

- **Endurance exercises**, which increase breathing and heart rate, can improve one’s stamina for those tasks you need to do in order to be able to live on your own and be independent, like climbing stairs and shopping for groceries.
- **Strength exercises** not only build muscles so you can do more but also help you keep your weight down and your blood sugar in check.
- **Balance exercises** can help prevent falls, which in older people are a major cause of broken hips and other injuries that often lead to going into a nursing home.
- **Flexibility exercises** or stretching help keep your body limber. Such exercises may help people recover from injuries and may help prevent injuries from happening in the first place. Flexibility may also play a part in preventing falls and may help in reducing pain.

**Examples of Endurance Activities/Exercises**

Examples of activities that are moderate for the average older adult are listed below. Some older
adults, especially those that have been inactive for a long time, will need to work up to these
activitites gradually.

  **Moderate:**

- Swimming
- Bicycling
- Cycling on a stationary bicycle
- Gardening (mowing, raking)
- Walking briskly on a level surface
- Mopping or scrubbing floor
- Golf, without a cart
- Tennis (doubles)
- Volleyball
- Rowing
- Dancing

Men over 40, women over 50, people who have been inactive for a long time or anyone who has
or may be at risk for a chronic disease should check with their doctor before engaging in activities
such as the following:

  **Vigorous:**

- Climbing stairs or hills
- Shoveling snow
- Brisk bicycling up hills
- Digging holes
- Tennis (singles)
- Swimming laps
- Cross-country skiing
- Downhill skiing
- Hiking
- Jogging

**Example of Strength/Balance Exercises**

Plantar Flexion:

1. Stand straight, holding onto a table or chair for balance.
2. Slowly stand on tip toe, as high as possible.
3. Hold position.
4. Slowly lower heels all the way back down.
5. Repeat 8 to 15 times.
6. Rest a minute, then do another 8 to 15 repetitions.

As strength increases, do the exercise on one leg only, alternating legs.
Or, hold onto the chair with one hand, then one fingertip, then no hands; then do exercise with eyes closed, if you are steady.
