---
layout: post 
title: "Low Vision Education"
date: 2019-01-10
image: assets/images/low-vision-education.webp
excerpt: "We should not accept that nothing can be done about low vision"
tags: Eye disease, lifestyle, Low Vision, Vision Education
draft: false
---

For about 14 million Americans — one in every 20 — the inability to see well makes doing things difficult. They have trouble recognizing the faces of friends, seeing the TV, and reading price tags. Walking around the neighborhood presents a challenge. What can be done?

Plenty! The National Eye Institute, a component of the National Institutes of Health, recently launched a Low Vision Education Program that outlines steps people can take to make the most of their remaining vision. The new program brings the message that information and help are available to people with low vision and their families.

“People should not accept the statement that nothing can be done about their low vision,” says Dr. Carl Kupfer, NEI director. “They can improve their quality of life through vision rehabilitation services that can teach them how to use their remaining vision more effectively. Using a variety of visual and adaptive devices may bring back or help them keep their independence.”

People with low vision typically have visual impairment that interferes with their ability to carry out routine tasks. Low vision is not usually correctable with standard glasses, contact lenses, medicine or surgery. Low vision is often the result of eye diseases such as cataracts, glaucoma, diabetic retinopathy, and age-related macular degeneration. Early detection, diagnosis and proper treatment of these diseases can help preserve a person’s remaining vision.

Low vision particularly affects people age 65 and older, and Hispanics and African Americans, who are more likely to develop eye disease at an earlier age. More than $22 billion is spent each year on care and services for people who are blind or have visual impairments.

NEI’s Low Vision Education Program informs people about resources and lifestyle changes that can help them improve their quality of life. For instance, people with low vision do not have to give up reading and writing simply because they are having trouble seeing. They can use low vision aids such as large-print publications, magnifying devices, computers with large print, audio recordings, and writing with large felt-tip markers on tablets with bold lines.

In addition, there are a wide range of devices available to help people with low vision see better. These devices include reading glasses with high-powered lenses, and telescopes and telescopic spectacles. It is important to note that these devices must be prescribed by eye care professionals, and patients must be trained to use them properly.

According to eye disease experts, another way for people with substantial vision loss to stay independent is to incorporate assistive technology and other adaptive devices into all areas of life. For example, a person may have a computer with software that allows for speech output and screen magnification, but without magnifying lenses, cannot see the crosswalk sign on the corner. High-tech tools are often used in conjunction with more traditional devices to provide maximum effectiveness. Selecting the right device and receiving the proper training on how to use it is crucial.

There are simple safety measures that can help prevent accidents around the house, including:

- placing colored tape on the edges of steps
- placing large-print labels on stoves, microwaves, and fuse boxes
- using dark-colored electrical outlets and light switches on light-colored walls for better contrast

To help provide important information to people with low vision and their families and caregivers, NEI has introduced a Low Vision Traveling Exhibit that will make its way to shopping malls across the United States during the next few years. The exhibit contains an interactive multimedia touch screen program that provides a self-assessment to help people determine if they or someone they know may have low vision; short videos that provide “hands-on” advice; and panels that highlight local resources on low vision. The exhibit also displays aids and devices used to help people with low vision.

***A Word to the Wise...***

***Do You Have Low Vision? Early Diagnosis Is Key to Saving Eyesight***

According to the National Eye Institute, there are several signs that can alert you to vision loss. The questions below can help you determine if you are having problems seeing, or whether you should have a comprehensive eye examination. Even with your glasses, do you have difficulty:

- Recognizing faces of friends and relatives?
- Doing things that require you to see well up close, like reading, cooking, sewing, or fixing things around the house?
- Picking out and matching the color of your clothes?
- Doing things at work or home because lights seem dimmer than they used to?
- Reading street and bus signs or names of stores?

Vision changes like these could be early warning signs of eye disease. Usually, the earlier your problem is diagnosed, the better the chance of successful treatment and keeping your remaining vision. Although policies vary by state, generally Medicare will cover low vision examinations performed by eye care professionals.
