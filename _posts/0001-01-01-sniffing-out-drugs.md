---
layout: post 
title: "Sniffing Out Drugs"
date: 2019-04-26
image: assets/images/sniffing-out-drugs.webp
excerpt: "Abuse of Household Products Rising"
tags: Chemical odors, Drug Abuse, Euphoria, Inhalant Abuse, NIDA
draft: false
---

When most people think of drug abuse, they think of illegal substances like heroin, cocaine and LSD. Would it surprise you to know that some of the most toxic substances abused by children and teens can be found in the home? Certain household and office products, including glue, shoe polish, gasoline and cleaning fluids can cause intoxication when their vapors are inhaled. Called “inhalants,” these vapors can have devastating side effects. They pose a particularly significant problem because they are readily accessible, legal and inexpensive. In a 2003 study, 12.7% of 10th graders and 11.2% of 12th graders said they had abused inhalants at least once.

When the chemical vapors released by inhalants are breathed in by nose or mouth, they are absorbed by the lungs and travel rapidly through the blood to the brain and other organs. In minutes, the user feels alcohol-like effects such as slurred speech, clumsy movements, dizziness and euphoria. These effects usually last only a few minutes, but the user can extend them for hours by inhaling the vapors repeatedly. Successive inhalations can also break down inhibitions and self-control. Inhalants also have serious side effects, from headaches, nausea and vomiting to unconsciousness or even death.

![img](/assets/images/58-Sniffing-Out-Drugs.webp)

How inhalants cause their effects is a topic currently being investigated by NIH’s National Institute on Drug Abuse (NIDA). The vapors in inhalants contain chemicals that change the way the brain works, causing the user to feel happy for a short time. But these vapors often contain more than one chemical. Some may leave the body quickly, but others are absorbed into fatty substances in the brain and nervous system, where they can stay for a long time.

*Definitions:*

**Nerve Cells:** Cells responsible for sending and receiving signals between the body and brain.
**Nervous System :** System that receives and interprets sensations (like smells, sounds and sights), and coordinates activities (like muscle movements) throughout the body.

One of these fatty substances is myelin—a protective cover that surrounds many of the body’s nerve cells. Nerve cells in your brain and spinal cord are sort of like “Command Central” for your body. They send and receive messages that control just about everything you think and do. If nerve cells are your body’s electrical wiring, then myelin is the rubber insulation that protects the electrical cords. The chemicals in inhalants can break down myelin. If myelin breaks down, nerve cells may not be able to transmit messages as effectively.

As a result, people taking inhalants may have trouble solving complex problems and planning ahead. They might start losing control over their movement and coordination, making them slow or clumsy. They also may lose the ability to learn new things or have a hard time keeping track of simple conversations.

Regular abuse of inhalants can also cause serious damage to major organs, including the brain, liver, heart, kidneys and lungs. A single session of repeated inhalations can lead to cardiac arrest and death by altering normal heart rhythms or by preventing oxygen from entering the lungs, causing suffocation.

There are three general types of inhalants. Solvents include paint thinners or removers, degreasers, dry-cleaning fluids, gasoline, glue, correction fluids and felt tip marker fluid. Gas inhalants can be found in butane lighters and propane tanks, whipped cream aerosols or dispensers (whippets), spray paints, hair or deodorant sprays and fabric protector sprays. The third type, nitrites, are commonly known as “poppers.” Most poppers contain the chemicals isobutyl nitrite or butyl nitrite. They are available illegally and come in small brown bottles, sometimes labeled as “video head cleaner,” room odorizer or liquid aroma.

It’s difficult to know how many emergency room visits and deaths inhalants cause. There are probably many more emergency room admissions due to inhalants than we know about. Inhalant use is easily hidden, and they leave the body quickly so they are long gone by the time someone gets to the emergency room.

NIDA continues to support new research on the prevention and treatment of inhalant abuse, but early identification and intervention remain the best ways to stop inhalant abuse before it causes serious health consequences. Parents should store household products carefully to prevent accidental inhalation by very young children. They should also remain aware of the temptations that these dangerous substances pose to children and teens in their homes.

*Statistics:*

**Inhalant Abuse**

- In 2002, the nation’s hospital emergency departments reported almost 1,500 mentions of inhalant abuse by patients.
- Most inhalant abusers are younger than 25. Data suggest that inhalant abuse reaches its peak during the seventh through ninth grades.
- A NIDA survey of drug use by 8th-12th graders shows that lifetime inhalant use for 8th graders has increased significantly in the
  past year.

Source: National Institute on Drug Abuse,

***A Word to the Wise...***

*How to Recognize Inhalant Abuse:*

Parents, educators, family physicians and other health care practitioners should be alert to the following signs of a serious inhalant abuse problem:

- Chemical odors in breath or clothing
- Paint or other stains on face, hands or clothes
- Hidden empty spray paint or solvent containers and chemical-soaked rags or clothing
- Drunk or disoriented appearance
- Slurred speech
- Nausea or loss of appetite
- Inattentiveness, lack of coordination, irritability and depression
