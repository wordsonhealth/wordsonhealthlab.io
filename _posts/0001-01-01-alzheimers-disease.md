---
layout: post 
title: "Alzheimer’s Disease"
date: 2019-03-18
image: assets/images/alzheimers-disease.webp
excerpt: "Potential for Prevention, Earlier Detection"
tags: Alzheimer, Alzheimer's, Alzheimer's disease, NIA, NIMH
draft: false
---

An estimated 4.5 million older people currently have Alzheimer’s disease, and researchers predict that by 2050 the number could nearly triple to 13.2 million. But several promising recent developments in the study of Alzheimer’s disease may one day lead to new methods of diagnosing, preventing and slowing the disease’s progress. These include a new way to look inside the brains of people with the disease as well as new methods for preventing buildup of a protein, called amyloid, which forms plaques that scientists believe may be involved in causing Alzheimer’s symptoms.

Detecting Alzheimer’s disease at an earlier stage is a major research goal. In one compelling study that may lead to crucial earlier diagnosis and prevention efforts, scientists created a compound that allows them to use positron emission tomography (PET, an imaging tool used to view body function and diagnose disease) to look at early signs of the abnormal clumps of amyloid proteins (called plaques) that form in the brains of people who have Alzheimer’s. Called Pittsburgh Compound B, the substance was developed by researchers at the University of Pittsburgh School of Medicine and Sweden’s Uppsala University. Their work — aimed at being able to intervene sooner in people with Alzheimer’s, possibly as the plaques begin to form — was supported in part by a grant from the National Institute on Aging (NIA), part of NIH.

“We are moving toward earlier and earlier detection of Alzheimer’s disease,” notes Dr. Neil Buckholtz of NIA’s Neuroscience and Neuropsychology of Aging Program. “Research at Pittsburgh and elsewhere is bringing us closer to the day when we can look in the brain, see if the characteristics of the disease are there, and then develop therapies that hopefully can change the progression of the disease and its clinical course.” Buckholtz heads a new effort in which NIA is bringing together government, academic and industry scientists to accelerate the use of imaging in research and, ultimately, in the diagnosis and treatment of Alzheimer’s disease.

**Stopping Alzheimer’s Plaques**
Understanding the formation and activity of beta amyloid, the primary component of Alzheimer’s plaques, is a major focus of research. Investigators continue to work intensely to figure out the process by which amyloid precursor protein (APP) is snipped by enzymes to release the beta amyloid fragments that clump together to form the sticky plaques. One of these enzymes is called beta secretase. In a mouse study at Johns Hopkins University School of Medicine, scientists developed a transgenic mouse in which the gene for BACE1, a beta secretase enzyme, is eliminated, or “knocked out.” The scientists found that with BACE1 eliminated, beta amyloid protein fragments were no longer produced in brain cell cultures of the knockout mice. This finding has resulted in a focus on the design of drugs that could inhibit BACE1 activity.

Earlier NIH-supported studies also discovered potential mechanisms to clear amyloid plaques. Researchers at the Salk Institute and their colleagues elsewhere found that gene transfer of an enzyme may help clear out plaques. The human enzyme, neprilysin, was injected into the brains of transgenic mice with human amyloid plaques. After the treatment, the enzyme appeared to help degrade existing plaques and help reduce growth of new plaques.

Teams of scientists in New York and California have suggested that astrocytes, one of the three principal types of brain cells, can break down amyloid and eliminate it from the brain. Astrocytes nourish and protect neurons in the brain and are located close to brain plaques. Researchers have a theory that some astrocytes may be impaired, making them unable to, in Alzheimer’s case, completely clear amyloid from the brain.

“Our studies on the biology of Alzheimer’s disease give us an increasingly clear picture of what happens in the brain and how the communications among cells are compromised,” says Dr. Stephen Snyder of NIA’s Etiology of Alzheimer’s Section. “With this knowledge, scientists are now able to demonstrate ways in which the culprits involved in Alzheimer’s might be interfered with and even eliminated.”

**Possible Treatments Under Study**
Some research is turning to old drugs for their potential promise in treating Alzheimer’s disease. In one study at Massachusetts General Hospital, scientists funded by NIA and NIH’s National Institute of Mental Health (NIMH) examined a rarely used antibiotic that seemed to dissolve the plaques. The drug, clioquinol, was chosen because it draws zinc and copper from the body. Some scientists believe that when amyloid is present, it attracts zinc and copper, paving the way for destructive brain plaques to form. The team found that treatment with clioquinol reversed the deposition of amyloid in the brains of mice with AD. Despite these encouraging findings, researchers need to proceed very cautiously when investigating this compound for human use, as small amounts of these metals are necessary for many chemical reactions in the body.

Other widely used medications are currently being tested. One population study published two years ago found that people with elevated levels of a compound called homocysteine in their blood had nearly double the risk of developing Alzheimer’s. Blood levels of homocysteine can be reduced by increasing intake of folic acid (or folate) and vitamins B6 and B12. Researchers are now examining whether lowering homocysteine levels in the blood with B vitamins and folate might reduce the risk of Alzheimer’s.

Researchers are also studying simvastatin, a cholesterol-lowering drug, in people who already have Alzheimer’s disease to see if the drug might slow down the rate of Alzheimer’s progression. Earlier studies have associated the use of cholesterol-lowering drugs in this class with a positive impact on brain function and reduced risk of Alzheimer’s.

***Alzheimer’s Impact Growing; Caretakers Need Love, Too***

The most dramatic increase in the number of people with Alzheimer’s disease will be in the population age 85 and older, when about 8 million cases of the disease are expected in this group by 2050. With the impact of Alzheimer’s disease growing as more of the U.S. population heads into their senior years, the National Institute on Aging (NIA), part of NIH, is conducting research aimed at making sure that people who take care of those with Alzheimer’s get help, too.

Not only does a diagnosis of Alzheimer’s have an enormous effect on the person with the disease, but the progressive nature of the disease also places a tremendous strain on the person’s family, friends and especially the primary caretaker. Depression and high levels of stress are seen commonly in family caregivers of people who have Alzheimer’s, according to a multi-site study called REACH, or Resources for Enhancing Alzheimer’s Caregiver Health. REACH is funded by NIA and the National Institute of Nursing Research (NINR), another component of NIH.

In a separate report, study investigators found that Alzheimer’s caregivers could benefit from bereavement and counseling services before death of the family member, because of the long periods of steady decline associated with the disease. Education on pain control, coping and other social support could also help reduce the burden of caregiving and depression, scientists say.
“As we search for ways to treat and ultimately prevent Alzheimer’s disease, we must continue to provide information and strategies to help caregivers and policymakers cope with the social and behavioral aspects of the disease,” says Dr. Sidney Stahl of NIA’s Behavioral and Social Research Program.

**Where Research Is Headed**
In addition to looking for ways to diagnose and treat Alzheimer’s earlier, researchers are examining lifestyle and genetic factors that might affect a person’s chances of developing the disease.

Diet and exercise, for example, are under close study. Another interesting avenue being explored is the influence of formal education on a person’s memory and learning skills. Researchers are trying to determine if the amount of formal education a person has somehow affects the brain so that it facilitates a person’s potential to work around or reduce the effect of the brain abnormalities associated with Alzheimer’s.

To examine how heredity and genes play a role in Alzheimer’s, NIA recently stepped up its Alzheimer’s Disease Genetics Initiative. The initiative teams up NIA, academic researchers at Indiana University and Columbia University, the NIA-supported network of 29 Alzheimer’s Disease Centers around the U.S., and the Alzheimer’s Association. Scientists hope to create a large bank of data and blood cells containing genetic material from 1,000 families in which at least two living siblings have been diagnosed with late-onset Alzheimer’s disease. By studying families with the late-onset form of Alzheimer’s, the most common form of the disease, scientists hope to understand more about how Alzheimer’s develops as well as new methods of prevention and treatment. To find out how to participate in the study, families should contact the National Cell Repository for Alzheimer’s Disease (NCRAD) visit the website www.ncrad.org.

“Finding the additional risk factor genes for late-onset AD is critically important to move our understanding forward,” says Dr. Creighton Phelps, director of the program that supports the NIA Alzheimer’s Centers. “Working intensively with a large number of families will, we hope, allow scientists to find these genes sooner rather than later.”

Scientists still need to learn a lot more about what causes Alzheimer’s disease. By studying genetics, education, diet, environment, and other factors to learn what role they might play in the development of this disease, researchers hope they will find ways to diagnose, prevent or even treat this devastating illness.

***A Word to the Wise...***

Alzheimer’s disease begins slowly. At first, the only symptom may be mild forgetfulness. People with Alzheimer’s may have trouble remembering recent events, activities, or the names of familiar people or things. Simple math problems may become hard to solve. Such difficulties may be a bother, but usually they are not serious enough to cause alarm.

However, as the disease goes on, symptoms are more easily noticed and affect a person’s ability to do everyday tasks, eventually becoming serious enough to cause people with Alzheimer’s disease or their family members to seek medical help. For example, people with the disease get to the point where they forget how to do simple tasks, like brushing their teeth or combing their hair. They can no longer think clearly. They have problems speaking, understanding, reading, or writing. Later on, some people with Alzheimer’s disease may become anxious or aggressive, or wander away from home. Eventually, patients need total care.

The seven warning signs of Alzheimer’s disease are:

1. Asking the same question over and over again.
2. Repeating the same story, word for word, again and again.
3. Forgetting how to cook, or how to make repairs, or how to play cards – activities that were previously done with ease and regularity.
4. Losing the ability to pay bills or balance the checkbook.
5. Getting lost in familiar surroundings, or misplacing household objects.
6. Neglecting to bathe, or wearing the same clothes over and over again, while insisting that they have taken a bath or that their clothes are still clean.
7. Relying on someone else, such as a spouse, to make decisions or answer questions they previously would have handled themselves.

Source: Alzheimer’s Disease Education & Referral Center, a service of NIH’s National Institute on Aging
