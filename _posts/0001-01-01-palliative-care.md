---
layout: post 
title: "Palliative Care"
date: 2019-03-26
image: assets/images/palliative-care.webp
excerpt: "Improving Quality of Life on the Way to Death"
tags: end-of-life care, Hospice Programs, NIA, NINR, Palliative Care
draft: false
---

It was the summer of her fourteenth year and an impressionable one at that. Her beloved grandfather was dying of bladder cancer. Distressed at the ineffectual way her family dealt with his illness and subsequent death, the young teen painfully learned how the effects of an incurable disease can bounce back and forth among family members and the patient. Dr. Ann Berger learned lessons that summer that propelled her through her subsequent training as a nurse, through medical school, and into her current position as the head of the pain and palliative care section at NIH’s Warren Grant Magnuson Clinical Center.

Dr. Berger has a unique rapport with her patients, a sentiment reflected in her office, which displays a variety of floppy straw hats and a tea cart loaded with cups and saucers. The hats and tea cart are used when the patient or family members need their spirits lifted. Inspired by her colleague, Dr. Joann Lynn, a geriatrician, Dr. Berger steadfastly believes, “the end of life is really about living with a disease that is going to kill you — about ‘good’ living on the way to death.”

A recent study conducted at the University of Pittsburgh shows that people are beginning to value a good death as much as they do a long life. “People care a great deal about the quality of the death experience,” the study concluded. “On average, interviewees would have been willing to trade seven months of healthy life to ensure a better quality of care in the final month of life.”

**The Need for Palliative Care**

Palliative care aims to improve the quality of life for patients near the end of their lives. It involves not only medications to relieve pain, but also a team approach to provide comfort and support that involves family, friends and health care providers.

Dr. Berger and other advocates say that palliative care needs to expand beyond its traditional focus on terminally ill cancer patients. Those with a wide range of debilitating or life-threatening chronic illnesses such as diabetes, emphysema, multiple sclerosis and cardiovascular disease can also benefit from palliative care. For example, people with advanced heart disease can experience such severe shortness of breath they cannot walk to their next-door neighbor’s house. They may become as severely depressed as a terminally ill patient, and thus may require this type of individualized treatment.

“Palliative care begins,” Dr. Berger says, “at diagnosis, and should be administered throughout the course of the disease.” Results of an epidemiological study led by Dr. June Lunney of NIH’s National Institute of Nursing Research (NINR) echoes Dr. Berger’s beliefs. “Because of the different ways people die, palliative care should start earlier for those who need it, and should involve health and social services that are adjusted to fit the anticipated pattern of death,” Dr. Lunney reported.

NINR and another NIH component, the National Institute on Aging (NIA), examined four “major pathways to death.” The pathways range from people who died suddenly to those lingering, expected deaths associated with frailty in old age. The study concluded that palliative care should have a more extensive and far-reaching focus. Further, because of the different ways that people die, it should be flexible enough to accommodate those who need it earlier and adjusted to fit the anticipated patterns of decline.

**Hospice Programs**

Hospice care helps terminally ill patients and their families get through the hard times. Hospice care focuses on caring, not curing, and stresses quality of life: peace, comfort, and dignity. Patients’ families are an important focus of hospice care, and these programs are designed to provide them with the assistance and support they need as well. You can find hospice programs in freestanding hospice centers, hospitals, nursing homes and other long-term care facilities.

Palliative care for Alzheimer’s patients, for example, can help both those with advanced Alzheimer’s disease and their families. This disease is marked by a progressive irreversible loss of mental ability, personality changes, and subsequent physical decline. If there is any merciful point in the progression of this disease, it is when the patient begins to forget what he or she has forgotten, and no longer has insight into his or her behavior. As the patient journeys through the various stages of illness, family members also experience a series of losses. While some families are able to tackle the issues that arise from terminal disease, others cannot even think about what will happen to their loved one.

**The Future of Care**

NIH, primarily through NINR, is currently supporting a broad portfolio of research into improving palliative care. For example, researchers are developing and testing models of palliative care to clarify when it should begin and how it should be structured. Other studies are looking at management of both the physical and psychological aspects of symptoms at the end of life. Researchers are trying to understand the importance and use of spirituality and other psychological influences to enhance quality of life at the end of life. Yet another research focus is to gain a better understanding of family support and the bereavement process.

At NIH’s own medical center, while the doctor and patient are intensely immersed in curative medicine, they also recognize and respect the need for palliative care and acknowledge that although the physician’s goal is curing disease, they must relieve and comfort always.

***A Word to the Wise...***

*Advanced Medical Directives*

It’s hard to face losing a loved one, especially during the long, steady decline of a disease like Alzheimer’s. But, says Dr. Judith Salerno, deputy director of NIA, “If we start planning for illness before it occurs, we will take some stigma away from how we choose to live with the disease.”

Many people complete an advance medical directive to state their wishes for end-of-life care, in case they become unable to make these decisions themselves. With this document, an individual can designate someone to take appropriate actions and make decisions, and guide family members in the event that they must choose, for example, whether or not to withdraw life support in a hopeless situation.

Dr. Virginia Tilden, in a study funded by NIH’s National Institute of Nursing Research at the Oregon Health Sciences University, found that end-of-life decisions are especially difficult and stressful for family members in the absence of guidance from the patient. Having an advance directive in place helps family members and other caregivers lower their level of stress and achieve a sense of acceptance and calm from “doing the right thing.”

Patients in the early stages of Alzheimer’s disease or facing a terminal illness should consider placing an advance directive in their records while they can still think and communicate clearly. However, only about 50% of Alzheimer’s disease patients have an advance directive. Without one, the healthcare team must often intervene to help family members arrive at a consensus, preserve the patient’s dignity and quality of life, and begin the healing for those left behind. By providing a way for patients to express their wishes, advance directives can ease the stress and anxiety surrounding the sensitive decisions of end-of-life care.
