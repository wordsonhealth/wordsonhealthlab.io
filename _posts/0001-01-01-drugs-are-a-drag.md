---
layout: post 
title: "Drugs Are a Drag"
date: 2019-06-16
image: assets/images/drugs-are-a-drag.webp
excerpt: "Help Your Child Avoid Drugs of Abuse"
tags: Addiction, Drug abuse for students, Drugs, Drugs Abuse, NIDA
draft: false
---

Kids heading back to school need to keep healthy if they’re going to do well in the classroom and on the playing field. That means sleeping well, eating right, getting exercise and avoiding drugs of abuse.
Drugs work by altering the chemistry of the brain. Drugs of abuse, such as nicotine, cocaine and marijuana, work by affecting the brain’s signaling system. This system is made up of billions of cells called neurons, which send and receive messages to and from other neurons. Messages travel through neurons as electrical signals, which cause the release of a type of chemical at the neuron’s end called a neurotransmitter to pass the signal on to other neurons.

Some drugs are similar in size and shape to neurotransmitters. This similarity allows them to attach to neurons and start a chemical chain reaction that leads to the release of large amounts of a neurotransmitter called dopamine. Others can block neurons from reabsorbing dopamine for re-use, allowing too much of the chemical to pool in the signaling area between neurons. Too much dopamine is what leads to the powerful high or “rush” that drugs can produce.

![img](/assets/images/83-Drugs-are-a-Drag.webp)

The first time someone abuses a drug, he or she may feel intense pleasure. Of course, drugs have other effects, too; a first-time smoker may cough and feel nauseated, while a first-time cocaine abuser might even have a heart attack or stroke.

The unusual amount of neurotransmitters in the brain begins to change the brain in a number of ways. For example, the brain may compensate by producing fewer neurotransmitters. Neurons may become less sensitive to signals from other neurons. Some neurons may even die from the drug’s toxicity.

Continued drug abuse also disrupts the brain circuits involved in reward, motivation and control, leading to the disease of addiction. No one knows how quickly this happens, but after enough exposure, an addicted person craves the drug like people crave food, water or air, linking it to their very survival. Eventually, natural “rewards” like friends and family lose out to compulsive drug-seeking and abuse.

That’s why it’s critical to prevent drug use before it ever starts. NIH’s National Institute on Drug Abuse (NIDA) is testing many approaches designed to prevent drug abuse among different populations, especially young people. But prevention must start at home, with parents. One of NIDA’s goals is to help parents understand the causes of drug abuse so they can help prevent it.

Explore NIDA’s science-based materials on drug abuse for students, teachers and parents, and use them to help your children do their best—in school and in life.

*Definitions:*

**Neuron:**
Nerve cells that send and receive signals throughout the brain, creating our thoughts, actions and personalities.

**Neurotransmitter:**
A chemical released by a neuron to signal other neurons.

***A Word to the Wise...***

*Is Your Child Abusing Drugs?*

Signs that your child is abusing drugs may be obvious—like he or she is dizzy or has trouble walking; seems silly and giggly for no reason; has red, bloodshot eyes; or has a hard time remembering things that just happened. But you should also be aware of the following changes in your child’s behavior, bearing in mind that these signs may reflect problems other than drug abuse:

- Withdrawal
- Depression
- Fatigue
- Hostility
- Carelessness with grooming
- Deteriorating relationships with family members and friends
- Changes in school or sports performance
- Increased absenteeism or truancy
- Lost interest in sports or other favorite activities
- Changes in eating or sleeping habits
- Unusual odor on clothes and in the bedroom
- Use of incense and air fresheners or room deodorizers
