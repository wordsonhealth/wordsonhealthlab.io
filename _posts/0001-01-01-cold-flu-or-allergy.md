---
layout: post 
title: "Cold, Flu, or Allergy?"
date: 2019-05-02
image: assets/images/cold-flu-or-allergy.webp
excerpt: "Figuring Out What's Ailing You"
tags: Airborne Allergy, Allergy, Cold, Flu
draft: false
---

Each year, millions of school and work days are missed because of colds and flu, and we’re heading into their prime season now. How you can best treat your condition depends on whether it’s a cold or flu that’s ailing you. Since these diseases share many of the same symptoms, they’re sometimes hard to tell apart.

![img](/assets/images/61-Cold-Flu-or-Allergy.webp)

Complicating the problem, many people get bad allergies in the fall, when ragweed pollen is at its peak in many areas, and winter can bring its own allergies as people spend more time indoors around pet dander and house dust mites. When the sniffles, stuffy nose and coughing begin, this chart can help you decide how to handle your symptoms.

| **Symptoms**         | **Airborne Allergy**                                         | **Cold**                                                     | **Flu**                                                      |
| -------------------- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| Fever                | Never                                                        | Rare                                                         | Usual, high (100-102°F), sometimes higher-especially in young children); lasts 3-4 days |
| Headache             | Rare                                                         | Rare                                                         | Common                                                       |
| General Aches, Pains | Never                                                        | Slight                                                       | Usual; often severe                                          |
| Fatigue, Weakness    | Sometimes                                                    | Sometimes                                                    | Usual, can last up to 3 weeks                                |
| Extreme Exhaustion   | Never                                                        | Never                                                        | Usual, at the beginning of the illness                       |
| Stuffy, Runny Nose   | Common                                                       | Common                                                       | Sometimes                                                    |
| Sneezing             | Usual                                                        | Usual                                                        | Sometimes                                                    |
| Sore Throat          | Sometimes                                                    | Common                                                       | Sometimes                                                    |
| Cough                | Sometimes                                                    | Common, hacking                                              | Common, can become severe                                    |
| Chest Discomfort     | Rare                                                         | Mild to moderate                                             | Common                                                       |
| **Treatment**        | -Antihistamines-Nasal steroids (prescription only)-Decongestants | -Antihistamines-Decongestants-Aspirin, acetaminophen (such as Tylenol) or ibuprofen for aches and pains | -Aspirin, acetaminophen or ibuprofen for aches, pains and fever-Antiviral medicines (see your doctor) |
| **Prevention**       | Avoid those things that you are allergic to, such as pollen, house dust mites, mold, pet dander, cockroaches | -Wash your hands often-Avoid close contact with anyone with a cold | -Wash your hands often-Avoid close contact with anyone with the flu-Annual vaccination-Antiviral medicines (see your doctor) |
| **Complications**    | Sinus infection, asthma                                      | Sinus congestion, middle ear infection, asthma               | Bronchitis, pneumonia; can be life-threatening               |
