---
layout: post 
title: "Protecting Against Hpv Common Viruses Can Lead to Cancer"
date: 2022-01-02T19:27:17+01:00
image: assets/images/protecting-against-hpv-common-viruses-can-lead-to-cancer.webp
excerpt: HPV is one of the most common sexually transmitted infections. Fortunately, there's a vaccine that protects against the most harmful forms.
tags: featured
draft: false
---

Human papillomavirus (HPV) is one of the most common sexually transmitted infections. There are many types of HPV. Most are fairly harmless. Some can cause genital warts, and others certain cancers.

Anyone who is sexually active can get one of these harmful types of HPV. Most people don’t know they have the virus.

“Over a lifetime, up to 80% of people will be exposed to this virus,” says Dr. Aimée Kreimer, an NIH expert on cancer and HPV. For most, the infection goes away on its own. The body’s **immune system** controls the virus.

But some people can’t successfully fight off HPV. It stays in their body and can cause health problems. Those can include genital warts or cancer in the parts of the body it infects.

Cancers related to HPV can take many years to develop after exposure. Worldwide, there are over 570,000 cases of cervical cancer each year and 311,000 deaths. HPV also causes cancer of the vulva, vagina, penis, anus, and back of the throat.

Fortunately, there are vaccines that can prevent HPV infection. Getting the HPV vaccine could protect against over 90% of the cancers caused by the virus.

“We now have more than a decade of evidence that this vaccine works incredibly well to protect against HPV infections,” says Kreimer.

Gardasil 9 is the HPV vaccine now used in the U.S. It protects against nine types of disease-causing HPV. When first introduced, the vaccine only protected against four HPV types.

For the HPV vaccine to be most effective, it needs to be given before exposure to the virus. The CDC recommends that both boys and girls get the HPV vaccine at age 11 or 12. At this age, the vaccine is given in two doses.

“If you start the vaccine early, you provide protection starting from that age onward,” says Dr. Carolyn Deal, an NIH expert on sexually transmitted diseases.

People who start the HPV vaccine at age 15 or older will need three doses. The vaccine is recommended for everyone up to 26 years of age.

The HPV vaccine has also been recently approved for those 27 to 45 years old. However, HPV vaccination of people in this age range provides less benefit, as more have been already exposed to HPV.

Since the vaccine was introduced, HPV infection rates have fallen dramatically. Among teen girls, infections with the types of HPV that cause genital warts and cancer have dropped by 86%.

“It’s a highly effective vaccine,” Deal says. “Cervical cancer is a devastating disease and, through vaccination, we have the potential to reduce it or even eliminate it down the road.”

In addition to getting vaccinated, if you’re sexually active, use condoms every time you have sex. This reduces your chances of getting HPV and other sexually transmitted infections.

Women ages 21 to 65 should also get screened for cervical cancer routinely.

### Wise Choices

#### Who should get the HPV vaccine?

- All boys and girls ages 11 to 12.
- Everyone through age 26 years, if you’re not already vaccinated.
- Some adults aged 27 through 45 years, but benefits in this age range tend to be minimal. Those concerned about their risk of infection should consult with their health care provider.
