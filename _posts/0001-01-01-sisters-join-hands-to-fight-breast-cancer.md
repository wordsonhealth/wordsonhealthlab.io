---
layout: post 
title: "Sisters Join Hands to Fight Breast Cancer"
date: 2019-04-04
image: assets/images/sisters-join-hands-to-fight-breast-cancer.webp
excerpt: "New Study to Discover Cause"
tags: breast cancer, cancer, NIEHS, Sister Study
draft: false
---

Sisters share a lot more than laughter and secrets. They also share genes and many lifestyle choices. Scientists already know that if a woman has a sister with breast cancer, she is twice as likely to develop it, too. But what causes this link? To help scientists figure that out, many women with sisters who have had breast cancer are taking part in a new project called the Sister Study.

The study, which is organized by NIH’s National Institute of Environmental Health Sciences (NIEHS), is looking for 50,000 women to participate who do not have breast cancer but have a sister who was diagnosed with the disease. Researchers will follow these women for at least 10 years, providing a huge set of facts to use in the search for the causes of breast cancer.

“I joined the Sister Study because I want to do my part in getting the word out about this dreadful disease and to find a cure for it,” a woman who identified herself as Cruz explained. “As a Latina I feel even more responsibility to participate because I want the risk factors for breast cancer in my specific Latina community to be studied.” Five of the six Cruz sisters are participating in the Sister Study. “All of us want to help prevent future generations from going through what [our sister] did,” she said.

There don’t seem to be quick answers to questions about breast cancer. Some think that ingredients in common products like gasoline, pesticides, paint remover, glue, and plastic might interfere with hormones that the body makes. They might also damage breast tissue in ways that lead to breast cancer. But past studies haven’t been able to find strong links between anything in the environment and breast cancer. Researchers in the Sister Study hope that all the information they gather will shed some light on the things that put women at higher risk.

**Hormones:** Chemicals made by living cells in the body that can travel in blood and other body fluids. They affect other cells, some far away from those that originally made them. Example: two female hormones made by cells in the ovaries, estrogen and progesterone, control the menstrual cycle.

The researchers want to get as much information as they can. They will collect blood samples and specimens of urine, toenails and house dust. They’ll ask study participants about any diseases they’ve had, their life habits, jobs and living spaces.

What makes the Sister Study unique is that all this information is being collected only on healthy sisters of women with breast cancer. Researchers hope this will help them separate out the differences that might affect their chances of developing breast cancer.

Knowledge about breast cancer grows stronger woman by woman, sister by sister. You’re eligible for the Sister Study if you’re a woman living in the U.S. between 35 and 74 years old who has never had breast cancer but have a sister related to you by blood who has. If you’re 60 or older, it’s especially important that you consider joining; breast cancer rates rise as women age, particularly between 50 and 75.

![img](/assets/images/47-Breast-Cancer-statistics.webp)

**Number of New Cases and Deaths per 100,000**: The number of new cases of female breast cancer was 127.5 per 100,000 women per year. The number of deaths was 20.6 per 100,000 women per year. These rates are age-adjusted and based on 2012-2016 cases and deaths.

**Lifetime Risk of Developing Cancer**: Approximately 12.8 percent of women will be diagnosed with female breast cancer at some point during their lifetime, based on 2014-2016 data.

**Prevalence of This Cancer**: In 2016, there were an estimated 3,477,866 women living with female breast cancer in the United States.

Source: National Cancer Institute.

***A Word to the Wise...***

*How can I help the Sister Study?*

There are 3 ways you can help the Sister Study:

- Join the Sister Study if you are eligible (see the story).
- Spread the word by telling other women to find out if they can join the Sister Study.
- Become a Sister Study volunteer and help make sure that all the women in your community know about the study
