---
layout: post 
title: "Rash Decisions How to Deal With Itchy Red Skin"
date: 2022-01-02T18:54:30+01:00
image: assets/images/rash-decisions-how-to-deal-with-itchy-red-skin.webp
excerpt: A rash could have many causes. Find ways to make it go away.
draft: false
---

You’ve broken out in a red, itchy rash. You’re likely wondering where it came from—and, most importantly, how to make it go away.

Unfortunately, your mystery rash could have many causes. Maybe you switched to a new detergent or accidentally brushed against poison ivy. Maybe it came from a virus or fungus. It can be hard to pinpoint the cause.

Red, uncomfortable skin rashes are called dermatitis. “When we use the term dermatitis, all we really mean is skin **inflammation**,” says Dr. Brian Kim, a dermatologist at Washington University in St. Louis.

Our skin is a barrier, serving as the first line of defense against the outside world. It is filled with immune cells. These cells actively fight against viruses, bacteria, and other invaders. If a foreign substance is detected, immune cells start a chain reaction to neutralize the threat. This, in turn, causes inflammation.

### Eczema

Many people are familiar with atopic dermatitis—the red, itchy rash commonly called eczema. “It affects up to 30% of people, particularly in childhood, and it tends to run in families,” explains Dr. Heidi Kong, a dermatologist at NIH. While some children may outgrow eczema, others will have the disease for life.

The cause of eczema is unknown. But skin microbes—bacteria, fungi, and viruses—may play a role. Kong is working to understand how the millions of microbes that live on your skin may contribute to eczema. Kong and her team have shown that certain types of bacteria are more commonly found on people with eczema. When applied to the skin of mice, these bacteria cause an immune response in the skin.

“We’re trying to understand the bacteria on a deeper level,” says Kong. “We see that they are associated with flares.” Flares are periods when eczema gets worse.

Treatment using bacteria normally found on healthy skin may help. In a small preliminary study, children and adults with eczema improved after using a spray that contained a specific kind of healthy bacteria. Research like this may one day lead to new treatments.

Scientists are also interested in understanding whether eczema can be prevented before it starts.

“I tell my patients that you were probably programmed from birth to be at risk for developing atopic dermatitis,” says Dr. Eric Simpson, a dermatologist at Oregon Health & Science University. Scientists estimate that the risk for eczema may be up to 60–70% genetic. But environmental causes also play a role.

What if a simple act, like moisturizing daily, could keep children from developing the disease? Doctors often prescribe moisturizing creams for people with eczema. These creams help to stop itching and restore the skin. Studies have shown that using moisturizers, sometimes called emollients, can prevent eczema flares.

In Simpson’s new study, parents will moisturize their baby’s entire body each day using one of several emollients. “Our idea was to repair the skin barrier that’s dysfunctional as early in life as you can,” says Simpson.

Over several years, the team will be able to see whether emollient treatment helped prevent eczema. They are also looking at whether regular moisturizing could impact allergies and asthma, which are common for children with eczema.

How do you choose a moisturizing product if you have eczema? “It’s a bit of trial and error,” Kong says. While some people prefer petroleum jelly-based products, other people find they have better results with cream-based lotions.

“But because of the tendency for a lot of these patients to have sensitive skin, we do recommend they avoid products that have a lot of fragrances or preservatives,” she says.

### Other Rashes

Not all rashes are mysterious. Some have a very clear cause. If your skin touches something you’re allergic to, it’s bound to get red and itchy. Common triggers include fragrances in soaps, lotions, and cosmetics. Some people are sensitive to nickel, which is often found in jewelry.

Your doctor may recommend a “patch test.” Small amounts of different substances are applied to your skin to see if you have a reaction.

“People are very familiar with allergic contact dermatitis by way of poison ivy,” Kim says. “But there are many, many environmental allergens that can cause contact dermatitis.” Luckily, hydrocortisone cream may be all you need if the rash only covers a small area.

Psoriasis is another common form of dermatitis. It causes thick, red, scaly skin on the elbows, knees, scalp, and other parts of the body. It’s caused by an overactive immune system. Skin ointments and medications that quiet the immune system can help. A doctor may also prescribe light therapy, where the skin is regularly exposed to ultraviolet light.

Rashes can also be triggered by bacterial and viral infections. And certain drugs may cause an itchy skin rash if you’re allergic.

“That’s a few of probably hundreds of rashes you can get,” Kim says. But common to all of them? “A five-year-old child could tell you that whenever something’s rashy, it itches. Itch is probably the most common symptom in the field of dermatology.”

### Itch Relief

For a long time, people questioned whether itch was even its own unique sensation. They thought it might be a mild form of pain. But scientists have discovered that itch has its own pathway in the brain.

That was the starting point for developing new treatments for chronic itch. Now researchers are focusing on finding drugs to help fight itch.

If you’re a little itchy, it’s okay to start with over-the-counter creams. Moisturizers and creams work well for dry, itchy skin. Mentholated moisturizers can cool the skin and provide relief. But some perfumes or dyes added to skin lotions may make things worse. For more tips on soothing a mild rash, see the “Wise Choices” box.

If you have a severe itch that’s been going on for a long time, you should see a doctor. For these types of severe itch, Kim says, trying to use over-the-counter treatments is “like trying to put out a fire with a garden hose.”

For an itchy rash, it’s important to understand the cause. Call your doctor if your rash is so uncomfortable that it interferes with your sleep or daily activities. You should also seek medical help if you break out in a rash after taking a new medication.

An itchy rash can be trying, but with help, you can calm your skin and feel better.

### Wise Choices

#### Mild Rash Relief

These tips may help soothe some rashes:

- Use moisturizer to calm your skin.
- Consider using over-the-counter medications like hydrocortisone if the rash covers a small area. 
- Take a lukewarm bath.
- Avoid rubbing and scratching.
- Wear cotton clothing so the rash doesn’t worsen.
- Get medical help if your itch is severe or has been going on for a long time.
