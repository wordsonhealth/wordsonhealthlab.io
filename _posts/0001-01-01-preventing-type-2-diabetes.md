---
layout: post 
title: "Preventing Type 2 Diabetes"
date: 2019-05-04
image: assets/images/preventing-type-2-diabetes.webp
excerpt: "Small Steps Yield Big Rewards"
tags: Diabetes, Diabetes Prevention Program, Diabetes type 2, Prediabetes
draft: false
---

Bob Huber doesn’t dwell on the time, about 10 years ago, when he found out he was likely to get type 2 diabetes. At 5’ 11”, he weighed 216 pounds, rarely exercised and had little energy.
“In a sitting position, I had a hard time taking a deep breath,” he recalls.

Then, at a health fair in Washington, DC, he happened by a booth where he had a screening test for blood glucose. It came back high. More testing confirmed he had a condition called prediabetes.

![img](/assets/images/62-Preventing-Type-2-Diabetes-2.webp)

Prediabetes causes no symptoms, but it is still a serious condition.
Many people with prediabetes develop type 2 diabetes within the next 10 years. People with prediabetes also have a 50% higher risk of having a heart attack and stroke than those who have normal blood glucose levels.

Huber, along with more than 3,000 other adults with prediabetes, decided to volunteer for the Diabetes Prevention Program (DPP), an NIH-funded study taking place at 27 clinical centers around the country.

The study found that overweight people with prediabetes can often prevent or delay diabetes by losing 5 to 7% of their body weight. They can do this through cutting fat and calories and increasing physical activity, such as walking 30 minutes a day 5 days a week. The modest weight loss from diet and increased physical activity lowered diabetes onset by nearly 60 percent in study participants. Treatment with metformin, an oral diabetes medication, lowered the risk of developing diabetes by 31 percent in the study.

Lifestyle changes worked just as well for men and women and all ethnic groups. Nearly half the DPP participants were from minority groups who suffer disproportionately from type 2 diabetes: African Americans, Hispanic Americans, American Indians, and Asian Americans and Pacific Islanders. In the study, people age 60 and older who made the lifestyle changes lowered their chances of developing diabetes by 71 percent.

When these results were announced, they surpassed even researchers’ expectations. Study chair Dr. David Nathan of Massachusetts General Hospital said, “They came as close to qualifying for ‘the gold’ as any diabetes finding of the last decade.”

Diabetes is the sixth leading cause of death in the United States. Statistics just released by the Centers for Disease Control and Prevention show that 20.8 million people—7% of the population—have diabetes, and over 6 million of them don’t even know they have it. Another 41 million people have prediabetes.

Bob Huber was 65 years old when he learned he had prediabetes. He’s 75 now and feels better than ever. After losing 20 pounds and keeping it off, he credits his energy to the healthy habits he learned as a DPP participant. But you don’t have to be a study participant to adopt these healthy habits. As Huber’s story shows, the power to control this condition is in your hands.

*Definitions*

**Diabetes:**
A disease in which the body has problems producing or using insulin, a hormone needed to convert sugar, starches and other food into energy. In time, diabetes can lead to serious problems including heart disease, blindness, kidney failure and nerve damage.

**Glucose:**
A type of sugar. When the glucose level in your blood gets too high, it can damage your tissues and organs.

**Prediabetes:**
A condition in which your blood glucose level is higher than normal but not high enough for a diagnosis of diabetes. People with prediabetes are at increased risk for developing type 2 diabetes and for heart disease and stroke.

**Type 2 Diabetes:**
Formerly called adult-onset diabetes, this is the most common form of diabetes. People can develop it at any age. Being overweight and inactive increase the chances of developing type 2 diabetes.

*Statistics*

**About Diabetes**

About 20.8 million people in the United States have diabetes.

Type 2 diabetes, by far the most common form, is closely linked to obesity and is increasing in all age and ethnic groups in the United States.

Prediabetes, a condition that often leads to type 2 diabetes, affects 40% of Americans ages 40 to 74, or 41 million people.

***A Word to the Wise...***

*Small Steps to Diabetes Prevention:*

Recent studies have proven that people at high risk for type 2 diabetes can often prevent or delay the onset of diabetes with 30 minutes of physical activity 5 days a week and by losing 5 to 7% of their body weight. In other words, you don’t have to knock yourself out to prevent diabetes. The key is: small steps lead to big rewards. Here are some tips that might help.

**EASY STEPS to increase activity:** 

- Put away the TV remote control and get up to change the channel.
- Try walking around the house while you talk on the phone.
- Park the car farther away from stores, movie theaters or your office.
- Get off the bus one stop early, if you are in a safe place to walk.
- Visit museums, the zoo or an aquarium. These are great ways to be active with your family.

**EAT RIGHT. Try some of these tips to get started:**

- You don’t have to cut out the foods you love to eat. Just cut down on the amount you eat, and eat them less often.
- Try to keep meat, poultry and fish servings down to three ounces – that’s about the size of a deck of cards.
- Try to eat three sensible meals at regular times throughout the day.
- Eat more fresh fruit, veggies, nuts and whole grains.
- Limit fried foods. Baking and broiling are healthier ways to eat meat, chicken and fish.
- When eating out, share large portions.
