---
layout: post 
title: "Keep It Cool in Hot Weather"
date: 2019-04-24
image: assets/images/keep-it-cool-in-hot-weather.webp
excerpt: "Advice for Older People Applies to All"
tags: Elderly, Heat illness, Hot Weather, Hyperthermia
draft: false
---

Older people are at high risk for developing heat-related illness because the body’s ability to respond to summer heat can become less efficient with advancing years. Fortunately, the summer can remain safe and enjoyable if you use sound judgment.

Heat stress, heat fatigue, heat syncope (sudden dizziness after exercising in the heat), heat cramps and heat exhaustion are all forms of “hyperthermia,” the general name given to a variety of heat-related illnesses. Symptoms may include headache, nausea, muscle spasms and fatigue after exposure to heat. If you suspect someone is suffering from a heat-related illness:

- Get the person out of the sun and into a cool place.
- Offer fluids like water, fruit and vegetable juices.
- Urge the person to lie down and rest, preferably in a cool place.
- Encourage them to shower, bathe or sponge off with cool water.

![img](/assets/images/57-Keep-it-Cool-in-Hot-Weather.webp)

Heat stroke is especially dangerous and requires emergency medical attention. A person with heat stroke has a body temperature above 104°F and may have symptoms such as confusion, combativeness, bizarre behavior, faintness, staggering, strong rapid pulse, dry flushed skin, lack of sweating or coma.

Both lifestyle and general health can affect a person’s chance of developing heat-related illness. Lifestyle factors that can increase risk include an extremely hot home, lack of transportation, overdressing and visiting overcrowded places. Health factors include:

- Age-related changes to the skin such as poor blood circulation and inefficient sweat glands.
- Heart, lung and kidney diseases, and any illness that causes general weakness or fever.
- High blood pressure or other conditions that require changes in diet (for instance, salt-restricted diets).
- Certain medications—including heart and blood pressure drugs, sedatives and tranquilizers—and combinations of medications. Continue taking prescribed medications and consult a doctor.
- Being substantially overweight or underweight.

To avoid heat illness, pay attention to weather reports. Older people, particularly those at special risk, should stay in an air-conditioned place on hot, humid days, especially when there’s an air pollution alert in effect. Don’t exercise or do a lot of activities when it’s hot.

Make sure to dress for the weather. Natural fabrics like cotton can be cooler than synthetic ones. Light colors also reflect the sun and heat better than dark ones.

Remember to drink plenty of liquids on hot, humid days—mostly water or fruit and vegetable juices. Avoid drinks with caffeine or alcohol, which make you lose more fluids.

***A Word to the Wise...***

If You Don’t Have Air Conditioning:

- Take a cool shower or bath.
- Create cross-ventilation by opening windows on two sides of the building.
- Keep windows open at night.
- Keep curtains, shades or blinds drawn during the hottest part of the day.
- Cover windows when they are in direct sunlight.
- Electric fans may help, but when the temperature reaches the high 90s, fans won’t prevent heat-related illness.
- Go somewhere that’s air-conditioned like the shopping mall, the movies, the library, a senior center or a friend’s house. If you don’t have a car or no longer drive, ask a friend or relative to drive you. Many towns or counties, area agencies, religious groups and senior citizen centers provide such services. If necessary, take a taxi. Don’t stand outside waiting for a bus.
