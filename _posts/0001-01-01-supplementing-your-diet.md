---
layout: post 
title: "Supplementing Your Diet"
date: 2019-05-20
image: assets/images/supplementing-your-diet.webp
excerpt: "Vitamins, Minerals and Beyond"
tags: Black cohosh, Dietary Supplements, Ginkgo biloba, Glucosamine, ODS, Palmetto
draft: false
---

The world of dietary supplements is getting more and more complicated. People aren’t just taking vitamins and minerals anymore. Now, things like glucosamine, saw palmetto, black cohosh and ginkgo biloba are crowding onto shelves beside old standbys like vitamin C, calcium and iron. How do you sort through it all?
Dietary supplements include a broad range of vitamins, minerals, herbs and other substances meant to improve upon your diet. They can come as pills, capsules, powders and liquids.

The U.S. Food and Drug Administration, which regulates dietary supplements, treats them more like foods than like drugs. Dr. Paul M. Coates, director of NIH’s Office of Dietary Supplements (ODS), says, “Dietary supplements are generally regarded as safe based on a long history of human use, unless proven otherwise. By contrast, drugs are not assumed to be safe until extensive testing has been done to prove their safety.”

![img](/assets/images/70-Supplementing-Your-Diet.webp)

Supplements can play an important role in your health. Some doctors advise patients to take a multivitamin-mineral supplement to make sure they’re getting enough of all the nutrients they need. While this may provide some insurance, Carol Haggans, a consultant with ODS, cautions, “People shouldn’t feel they can make up for an unhealthy diet by taking a multivitamin-mineral supplement.” A combination of all the vitamins and minerals together in foods provide the greatest health benefit, she says. “In general, if you eat a healthy diet, you shouldn’t need to supplement it with extra nutrients.”

However, some people might need more of certain nutrients. Doctors often advise women of child-bearing age to take folic acid, for example. Many people don’t get enough calcium. According to some surveys, 44% of boys and 58% of girls age 6-11 don’t get enough-and the numbers get even higher as people age. It’s probably best to eat 2-3 servings per day of calcium-rich foods like dairy products. But if you have trouble eating dairy products because they upset your stomach and you don’t get enough calcium in other foods, a supplement might help.

Since some supplements may help you, it’s easy to go a step farther and think that taking more would be even better. This can cost a lot and may not provide the benefit you expect. It can also be risky.

“Almost all of the nutrients have tolerable upper intake levels-the amount it’s recommended you stay under each day,” Haggans says. Amounts above these levels can be toxic. Too much vitamin A, for instance, can cause birth defects, liver problems, weak bones and nervous system disorders. Too much calcium can cause kidney problems and block your ability to use other minerals in your diet.

NIH has several studies under way to look at whether high doses of certain supplements can prevent disease. For example, NIH’s National Cancer Institute is funding the Selenium and Vitamin E Cancer Prevention Trial (SELECT) to see if selenium and vitamin E can help prevent prostate cancer. But this isn’t an area you should experiment in by yourself.

Dr. Coates explains that, for the most part, supplement “megadoses” haven’t been tested. “Absence of evidence of harm isn’t the same as evidence of absence of harm,” he says. “In many cases we just don’t know.”

It’s not difficult to get high doses of certain nutrients, either. Breakfast cereals have long been fortified with vitamins and minerals. Now, many other fortified products are crowding onto grocery shelves as consumers buy into the idea that more is better. Look at the foods and supplements you’re eating together to make sure that your total intake of any one nutrient isn’t too high. If you’re concerned, talk to a health care provider such as a doctor, pharmacist or registered dietitian or check the nutrient recommendation information at the ODS web site.

Dietary supplements beyond traditional vitamins and minerals have also become popular. In one study, about 19%, or 1 out of every 5 people surveyed, used natural products such as echinacea, ginseng, glucosamine and ginkgo biloba. But since they’re regulated more like foods than drugs, in a lot of cases we don’t know how or even if these supplements work as their supporters claim.

“Be prepared to ask questions,” Dr. Coates advises. “These products are available on drug store shelves, supermarket shelves and vitamin store shelves in packaging that makes them look like drugs, but they aren’t regulated like drugs. Consumers have to realize that the drug rules don’t apply.”

Haggans adds, “People assume if it’s on the shelf it must be safe and we must know a lot about it, but that’s not necessarily the case.”

The National Center for Complementary and Alternative Medicine (NCCAM) takes the lead at NIH in funding studies of supplements beyond traditional vitamins and minerals. They now have dozens of studies under way to test their safety and effectiveness.

In the meantime, if you’re considering taking a supplement, consult with your health care provider. Some supplements can interfere with other medications, so have a list ready of all the medications and supplements you’re taking or considering.

If you decide that a particular dietary supplement is right for you, make sure you’re buying a reliable brand. There are independent laboratories that test supplement products for quality and purity. “There are companies whose products are made to very high standards,” Dr. Coates says, “but that’s not always the case.”

***A Word to the Wise...***

*Before Using Dietary Supplements*

Here’s what you can do to protect your kidneys:

- **Safety first.** Some products can be harmful when taken in high amounts, for a long time or along with certain other substances. Don’t use a dietary supplement along with, or instead of, a prescription medicine without first consulting your health care provider.
- **Don’t chase the latest headline.** Sound health advice is based on research over time, not a single study touted in the media.
- **Learn to spot false claims.** If something sounds too good to be true, it probably is.
- **“Natural” doesn’t mean safe.** Natural ingredients may interact with medicines, be dangerous for people with certain health conditions or be harmful in high doses. For example, peppermint tea is generally safe to drink, but you can get a toxic dose of oil extracted from peppermint leaves.
- **Does it work?** Resist pressure to buy something on the spot. Ask a health care professional for advice or check credible sources like the web sites listed in this article to find out if the product is safe and does what it says it does.

– Adapted from material provided by NIH’s Office of Dietary Supplements
