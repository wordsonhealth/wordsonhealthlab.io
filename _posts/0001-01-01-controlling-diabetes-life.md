---
layout: post 
title: "Controlling Diabetes for Life"
date: 2019-01-12
image: assets/images/controlling-diabetes-life.webp
excerpt: "You'll live better longer with less risk of problems from diabetes"
tags: Control blood sugar, Diabetes, Insulin
draft: false
---

About 6 percent of the population, 16 million Americans, have diabetes. One third of them don’t even know it. And many of those with type 2 (formerly called adult-onset) diabetes have dangerously high blood sugar levels, putting them at increased risk for diabetes-related complications like blindness, kidney failure, and foot and leg amputations as well as heart disease and stroke. Yet diabetes can be controlled.

The National Diabetes Education Program (NDEP), a joint program of the National Institutes of Health (NIH) and the Centers for Disease Control and Prevention (CDC), is now reinforcing its “Control Your Diabetes. For Life.” awareness campaign with new materials encouraging people with diabetes to take control of their disease to live healthier lives.

Diabetes is a disease in which the body does not produce or properly use insulin, a hormone that is needed to convert sugars, starches, and other food into energy. It is characterized by high blood levels of the sugar glucose. Diabetes is the sixth leading cause of death by disease in the United States, and costs the country about $98.2 billion annually.

The “Control Your Diabetes. For Life.” campaign focuses on controlling diabetes by eating healthy foods to control blood sugar, getting regular physical activity, taking prescribed diabetes medicines, and monitoring blood sugar regularly. People taking these steps can feel better, stay healthier, have more energy, and prevent the signs and symptoms of high blood sugar such as thirst and fatigue, frequent urination, excessive weight loss, blurred vision, and the slower healing of cuts and bruises. They can also lower their chance of the more serious long-term consequences of diabetes: eye disease, kidney disease, and nerve damage.

Minority populations are disproportionately affected by diabetes, and they experience higher rates of complications associated with the disease. To address this problem, in addition to its campaign for general audiences, the NDEP has launched five media campaigns tailored to those people disproportionately affected by diabetes: African Americans, Hispanic Americans, Asian Americans and Pacific Islanders, Native Americans, and senior citizens. Specific television, radio, and print public service advertisements are designed to reach each of these diverse populations.

***A Word to the Wise...***

***To take good care of your diabetes for life, be sure to follow these 7 principles:***

- Find out what type of diabetes you have.
- Get regular care for your diabetes.
- Learn how to control your diabetes.
- Treat high blood sugar.
- Monitor your blood sugar level.
- Prevent and diagnose long-term diabetes problems.
- Get checked for long-term problems and treat them.

People who have diabetes and keep their blood sugar levels under control can expect to live a long and active life.
