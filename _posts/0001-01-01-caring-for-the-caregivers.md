---
layout: post 
title: "Caring for the Caregivers"
date: 2019-06-10
image: assets/images/caring-for-the-caregivers.webp
excerpt: "The Hidden Victims of Long-Term Illness"
tags: Caregiver, Caregiving, Daily living, Elderly, NINR
draft: false
---

People caring for a sick child, spouse or parent can find the emotional, physical and financial strains overwhelming. Other people thrive in the role of caregiver and feel a sense of well-being and greater meaning in life. Every situation is different, but research can help us better understand the causes of stress for caregivers and how best to help caregivers as they care for others.

The ranks of caregivers in our country are swelling. Americans are living longer, and the population is getting older as the baby boomer generation ages. Medical breakthroughs have also turned once fatal diseases into chronic illnesses that require a great deal of care. More than 50 million people—over 16% of the population—provide care for a chronically ill, disabled or aged family member or friend, according to the National Family Caregivers Association. Many of them have full-time jobs and other responsibilities on top of their caretaking duties.

![img](/assets/images/80-Caring-for-the-Caregivers.webp)

NIH’s Office of Research on Women’s Health recently held a seminar to focus attention on the caregivers. Family caregivers, who are often older themselves and mostly female, tend to neglect their own health and emotional needs under the stress. They must cope with many uncertainties about the future, along with their own sadness about what is happening to their loved one.

Researchers are now developing a better understanding of the scope of the problem. One study funded by NIH’s National Institute of Nursing Research (NINR) at Oregon Health & Science University, for example, found that many caregivers felt emotionally or physically drained and financially stressed, and 2 out of 3 had problems sleeping.

Dr. Anne Wilkinson of the RAND Corporation explained that having more people living longer with serious chronic illnesses and facing the end of their lives has created a new reality that the healthcare system is struggling to deal with. “We’re all charting new territory,” she said, “sort of like cartographers in the 1600s.”

A literature review of interventions to address caregiver burdens, funded by NINR, found that no single type of program appears to work for all caregivers. Caregiving is complex and multifaceted. In turn, Wilkinson said, “Multifaceted, long-term interventions tend to work better than one-shot deals and one particular kind of intervention.”

She added, however, that the literature shows high levels of psychological distress and unmet need among caregivers. More work clearly needs to be done to understand the needs of modern caregivers.

Dr. Sharon L. Lewis of the University of Texas Health Science Center in San Antonio referred to caregivers in her talk as “hidden victims.” She explained that caregiving affects different people in different ways. Her group’s study of how people respond to their caregiving roles divided people into 8 groups by gender, age and ethnicity. The groups that are most stressed by their caregiving roles, they found, are white adult daughters and Mexican American wives. White male spouses, in contrast, were the least stressed.

Men, Lewis explained, seem to approach caregiving differently. “They actually get the most help in their caregiving role,” she said. “They approach caregiving as a business. They see it as a task to be done.”

Lewis’s group, with funding from NINR and the Department of Veterans Affairs, has developed a multifaceted program to help caregivers called the Stress-Busting Program for Caregivers. Small groups of caregivers meet every week for an hour and a half over eight weeks and focus on topics such as dealing with challenging behaviors, coping skills in changing relationships, grief, loss, depression and positive thinking. At each session, they also learn some type of relaxation therapy.

Using several outcome measures, including perceived stress, depression and levels of natural killer cells (an important type of cell in the immune system), the study has proven effective in helping caregivers of all races and genders. However, Lewis said, “There’s definitely gender and culture issues that need to be addressed when strategies are implemented for caregivers.”

Caregivers for children with disabilities face many of the same problems as caregivers for adults, and some face very long periods of providing care. Dr. Peter Rosenbaum, co-director of the CanChild Centre for Disability Research at McMaster University in Canada, described their study of the families of 468 children with cerebral palsy between the ages of 6 and 16. The amount of stress the caregivers reported was very high. Almost a third had 3 or more chronic physical health problems.

“Everything we looked at,” Rosenbaum said, “from allergies to ulcers, was reported significantly more often by the caregivers than by comparable Canadian adults.” He stressed, “We need to make a much broader attempt to help families as well as kids.”

Another study by Rosenbaum’s group showed that family functioning and social support affect parents’ overall satisfaction, stress levels and emotional well-being.

He explained that their center now operates with some simple but important principles based on their research. Effective programs to help children and their families, he said, recognize that parents know their children best and want the best for them. They understand that families are different and unique. And they acknowledge that a supportive family and community are important for the whole family, not just the child.

Research is showing how important it is to help caregivers as well as the people they’re caring for. It’s also revealing how best to provide support for caregivers. If you’re caring for someone in your family, see the sidebox for some tips and visit the links to find other resources for help.

*Definitions:*

**Caregiver**:
Someone providing care for an elderly, ill or disabled family member or friend in the home.

**Multifaceted**:
Having many different aspects or components

***A Word to the Wise...***

*Self Care for the Caregiver*

Speaking at a recent seminar on caregiving at NIH, Chloe JonPaul, the Maryland state representative for the National Family Caregivers Association, said that self care is not a luxury for caretakers; “It is your right as a human being.” She shared these tips:

- Reward yourself with mini-breaks.
- Exercise.
- Attack the problem, not the person.
- Don’t be afraid to ask for help. It shows that you are problem-solving.
- Use every tool you can find-local groups, web-based support networks, reading materials and anything else that can help.
- Block out negative thoughts. Think “want to,” not “have to.”
- Your goal is to never say, “I should have” or “I would have.” Make sure you’ll be able to say, “I did it.”
