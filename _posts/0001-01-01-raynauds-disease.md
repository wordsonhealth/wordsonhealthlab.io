---
layout: post 
title: "Raynaud’s disease"
date: 2019-02-14
image: assets/images/raynauds-disease.webp
excerpt: "Guard Your Fingers And Toes From The Threat Of Raynaud's"
tags: Fingers, Raynaud's, Raynaud's disease, Toes
draft: false
---

Hypothermia, frostbite, that pesky winter cold that seems to hang on until spring — these are typical health hazards when it’s cold. But have you heard of Raynaud’s phenomenon? Less well-known, it also strikes susceptible people in cold temperatures.

Some five to ten percent of Americans — particularly women — are affected by Raynaud’s (“Ray Nodes”) phenomenon. It can deprive the fingers and toes of blood — and sometimes the nose, lips and ear lobes as well. When they get cold, all these parts can become numb and the skin can turn white or even blue. Raynaud’s can lead to more serious health problems like skin ulcers and gangrene.

When people with Raynaud’s are exposed to the cold, the small blood vessels in their fingers or toes go into spasmodic contractions to preserve heat. Sometimes the arteries that feed the skin collapse and blood flow to their extremities is greatly reduced. Once they warm their fingers or toes, blood flow increases, causing the skin to turn red and feeling returns. Episodes can last anywhere from a minute to several hours.

Stress may also play a role in Raynaud’s. During stressful conditions the body releases chemicals that can trigger symptoms typically seen in the disorder. In most people, doctors don’t know the underlying cause of Raynaud’s phenomenon. However, people with certain connective tissue diseases like lupus or Sjögren’s syndrome, or conditions such as diabetes, thyroid disease, or peripheral vascular disease may be more at risk.

**Attacks Can Come Often**

The frequency of Raynaud’s attacks varies from patient to patient. “Some folks get them as often as daily or several times a week,” explains Gregory Dennis, M.D., a rheumatologist and Director of Clinical Programs and Training with NIH’s National Institute of Arthritis and Musculoskeletal and Skin Diseases (NIAMS).

The period of exposure to the cold is extremely critical. “It only takes about 20 to 30 minutes of exposure to the cold to cause potentially serious problems, such as tissue damage,” Dr. Dennis says. “This can lead to ulcers on the fingertips and left untreated, even gangrene.” Bone damage may also ensue, he says.

So are residents of Florida or the typically warmer climates completely safe? Not necessarily. People with Raynaud’s who live in milder climates may have attacks during periods of colder weather. “It doesn’t always take real cold temperatures or long periods of exposure to bring on an attack,” Dr. Dennis notes. Attacks can occur whenever temperatures dip below 60 degrees. For some people, sticking their hand in the frozen section at a grocery store for just a few seconds could be enough to trigger an episode.

If you believe you have Raynaud’s phenomenon, your family doctor should be able to help you; if not, he or she may refer you to a vascular surgeon or a rheumatologist. There are medicines available that can decrease both the frequency and the severity of attacks.

However, there are things you should do in the meantime. Limit your exposure to the cold. Get out of the cold as soon as you feel an attack beginning, if not sooner. If you are in the midst of an attack, Dr. Dennis says, “you may want to whip your arms around in a windmill fashion. Or just make a fist and keep closing and opening it or keep shaking your hands.” These exercises drive blood back through the arteries and can help increase blood flow to your hands.

Once you get inside, run your hands or toes under lukewarm water. Or soak them in a bowl of warm water. Using a heating pad on a medium warm setting for about 15 minutes is also safe and effective. However, don’t put your hands or any part of your body on a radiator or other object that can scald you.

If stress triggers an episode, get out of the stressful situation and relax. Even better, learn to recognize and avoid stressful situations. Biofeedback training or other relaxation management techniques may work to minimize symptoms or lessen attacks.

The bottom line is that both self-help and medical treatments are available for this potentially dangerous condition. Talk to your doctor if you think you may have Raynaud’s phenomenon.

***A Word to the Wise...***

*Preventing Raynaud’s attacks*

- Keep warm — avoid chilling the extremities as well as the rest of the body.
- Always wear gloves and thick socks when you go outside during cold weather. You may want to purchase special gloves and socks that have battery-powered warmers inside them.
- Wear a hat in cold weather that covers your ears, or wear ear muffs.
- Don’t smoke. Smoking impairs your circulation. The nicotine in cigarettes can also cause your skin temperature to drop, which could bring on an attack.
- Recognize that air conditioning can trigger an episode. Don’t turn it up too high, and bring along something to keep you warm if you are going to be in a highly air-conditioned room for a long time, like in a movie theater.
- If you’re sensitive to cold, use insulated drinking glasses and wear gloves before putting your hands inside the freezer compartment at home or at your grocery store.
- Control stress. Recognizing and avoiding stressful situations may help control the number of attacks.
- Get regular exercise. Exercise is good for circulation and may help prevent attacks. Check with your doctor before you begin exercising.
