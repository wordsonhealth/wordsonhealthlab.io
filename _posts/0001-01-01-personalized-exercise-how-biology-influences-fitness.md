---
layout: post 
title: "Personalized Exercise How Biology Influences Fitness"
date: 2022-01-03T00:33:22+01:00
image: assets/images/personalized-exercise-how-biology-influences-fitness.webp
excerpt: Researchers are looking for ways to find out which exercises may best suit your body.
tags: featured
draft: false
---

Getting enough physical activity can make both your body and mind feel better. It can also help prevent or delay health problems. Now, researchers are looking for ways to find out which exercises may best suit your body.

Different types of exercise can bring different health benefits. You can strengthen your bones with weights. Increase your flexibility with stretching. Or, improve your heart health with aerobic activity. 

But people’s bodies are built differently. Some people have more of the type of muscle that provides strength. Others have more of the type that provides endurance, which keeps you moving for a long period of time. This is one reason why people may be naturally suited to different sports.

But this idea doesn’t just apply to athletes. It affects people getting physical activity for fitness, too.

“There are a variety of reasons why different people might adapt better to different types of exercise training,” says Dr. Marcas Bamman, an exercise researcher at the University of Alabama at Birmingham. “And an important factor that we’re starting to learn more about is our **genes**.”

Researchers are studying how genes influence our bodies’ responses to physical activity. They’re also looking at how exercise affects people’s bodies differently. They’re even exploring how it affects your **microbes**.

“The end goal is to be able to provide an exercise “prescription” that is optimal for each person, so they can gain the most benefit,” Bamman says.

### Influenced by Genes

Scientists know that different types of exercise have different effects on health, explains Dr. William Kraus, who studies heart disease prevention at Duke University. “The benefits vary by type, intensity, and amount of exercise,” he says.

For example, his lab has observed that long bouts of moderate-intensity exercise, like brisk walking, may be especially good at lowering blood sugar levels. This can be important for people trying to prevent diabetes.

But maybe you want to reduce the levels of “bad” cholesterol in your blood to help prevent a heart attack. For that, a lot of high-intensity exercise to get your heart pounding may help the most, Kraus adds.

His team has observed these effects across ages and for both men and women. But when you look at individuals within those groups, he says, not everyone gets the same benefit from the same workout. 

“We want to understand how your genetic background determines your response to exercise,” he says. His research team has identified a set of genes that predict who will get the biggest improvements in heart health from aerobic exercise, like jogging or cycling. 

Bamman’s team has found a set of genes that may help predict who would gain the most muscle from a strength training program. But, he explains, just because you may not get the same benefits as someone else from one type of exercise doesn’t mean you don’t get any.

“Everybody responds to exercise in a positive way,” Bamman says. “For example, people who couldn’t gain muscle as well as other people still gained strength in our study. They still improved walking ability and a lot of other important aspects of health.” 

Researchers are looking for other genes that may predict how exercise affects different aspects of health, like blood sugar control. In one study, Kraus and his team tested an exercise program to reduce the risk of diabetes in a large group of people. 

“Some people got a great improvement controlling their blood sugar, and some people got none, even though they did all the exercise,” he says.

Knowing who’s most likely to benefit from specific exercises may help health care providers better tailor their recommendations for people, says Kraus. 

But these studies are still in the early stages. If you have a health condition, talk with your provider about the types and amounts of physical activity that are safe for you. 

### Learning From Athletes

“There really is almost no health intervention as potent and as broad in its benefit as physical activity,” says Dr. Euan Ashley, who studies exercise and the heart at Stanford University. 

Ashley, Bamman, and Kraus are involved in [a large NIH-funded program](https://motrpac.org/join/recruitmentHome.cfm) looking at how exercise affects different molecules in the body. They’re also exploring how this differs between people. The team is studying both people who have previously not exercised regularly and active athletes. 

Studying the abilities of elite athletes has the potential to help us understand the upper limits of the human body, Ashley explains. 

“For an athlete to perform at the absolutely highest level, everything has to work perfectly,” he says. This includes the muscles, heart, blood cells, and more. Studies of athletes, such as runners and skiers, have found genetic differences that have positive effects on their bodies’ performance, Ashley says.

“By studying athletes, we can learn more about the extremes of each of these body systems. And by understanding the extremes, we can understand fundamental aspects of those systems. That could help us treat people with diseases in those systems,” explains Ashley.

### The Role of Microbes

It’s not only your biology that can influence how exercise affects your body. Scientists are discovering more and more about the role of your microbiome. That’s the collection of microbes that live in and on your body. 

In a recent study, researchers found changes in a certain type of gut bacteria in marathon runners. They transferred those bacteria into mice. The mice given the bacteria were able to run longer.

Scientists are only beginning to study the microbiome’s role in fitness. Such studies are difficult, because things like diet, sleep, and even the people you live with can affect your microbiome, Kraus says.

Researchers continue to learn more about biology and physical activity. But no matter who you are, how much activity you get can make a difference for your health. Experts recommend getting at least 150 minutes of moderate to vigorous physical activity a week. And at least two days a week, do strength training exercises. 

“There are huge benefits from exercise for both mental and physical health,” says Ashley.

### Wise Choices

#### Personalize Your Workout

- **Know your goals.** Try to set specific short- and long-term goals. Then talk with your health care provider about the types and amount of exercise you can safely do.
- **Do what you enjoy.** Don’t struggle with a workout you hate. Build in activities that make both your body and brain feel good.
- **Find a partner.** Ask a family member or friend to be active with you. Activity may be more fun with someone.
- **Track your progress.** You might not feel as though you’re making progress, but you may be pleasantly surprised if you look back at where you started. 
- **Review your goals.** Did you meet your goals? If not, why? Are they doable? Brainstorm some options of what to do differently. 
