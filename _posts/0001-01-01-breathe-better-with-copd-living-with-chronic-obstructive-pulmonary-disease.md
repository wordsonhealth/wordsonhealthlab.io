---
layout: post 
title: "Breathe Better With Copd Living With Chronic Obstructive Pulmonary Disease"
date: 2022-01-03T00:53:06+01:00
image: assets/images/breathe-better-with-copd-living-with-chronic-obstructive-pulmonary-disease.webp
excerpt: Many things can improve your quality of life with COPD. Find tips for managing your symptoms.
draft: false
---

Maybe you’ve noticed that you get out of breath doing light activities lately. Or have a cough that won’t go away. You might think it’s temporary, or just part of getting older.

But these issues can also be signs of chronic obstructive pulmonary disease, more commonly called COPD. With COPD, your lungs can no longer take in all the air you need.

More than 16 million people in the U.S. are living with COPD. Millions more likely have the disease but don’t know it. Symptoms can be mild at first and get worse over time.

“Some people with COPD get very short of breath when they try to walk, or do any kind of physical activity,” says Dr. Janet Larson, who studies COPD at the University of Michigan. “Some are bothered by persistent coughing.”

People may also feel tightness in their chest, experience wheezing, or a whistling or squeaky sound when breathing. They can also feel extremely tired, or fatigued.

“But many people don’t recognize the symptoms, or don’t know they can be from a disease,” says Dr. Prescott Woodruff, a lung specialist at the University of California, San Francisco.

COPD includes two main conditions. In one, called emphysema, tissue inside the lungs breaks down. In the other, called chronic bronchitis, the airways are irritated and show signs of **inflammation**. Many people with COPD have both.

“There are many things we can do for COPD,” Woodruff says. “Most of them can improve your quality of life. And some of them can improve life expectancy. So we want people to recognize the symptoms and seek help.”

### Lessening Lung Irritation

Smoking is the main risk factor for the disease. But up to a quarter of people who develop COPD have never smoked.

The number of people in the U.S. who smoke has dropped over the last few decades. “But the amount of chronic lung disease has not declined as fast as we would have expected, given the decrease in smoking,” says Dr. Joel Kaufman, who studies how the environment impacts chronic diseases at the University of Washington.

Other factors can also play a role. Second-hand smoke—smoke in the air from other people smoking—is a risk factor. Others include your age, gender, and where you live and work.

COPD is more common in women and in people over the age of 40. People with a rare genetic disorder, called AAT deficiency, are also at increased risk.

COPD is also about twice as common in rural communities compared with urban ones. That’s largely because these communities have higher smoking rates.

Long-term exposures to other substances, including some types of chemical fumes and dust, may also increase the risk of COPD.

In a recent study, Kaufman and his team found that people who lived in areas with high air pollution had more emphysema-like changes in their lungs over time. This highlights additional risk factors that can potentially be lowered for whole communities, Kaufman explains.

“The more we understand about these risks, the more we can effectively target air quality improvements going forward,” says Kaufman.

### Boosting Quality of Life

People who currently smoke or smoked in the past may feel stigmatized and embarrassed to talk with their doctor, when they shouldn’t, Woodruff says. “That’s a huge population that may not be getting the attention they need,” he says.

If you have symptoms of COPD, talk with your health care provider. They can refer you for testing. The main test used to diagnose COPD is called spirometry. In this test, you blow into a tube connected to a small machine. The machine measures how well your lungs are working.

Some people may also need imaging or blood tests. These tests can help determine whether their COPD is severe. They may also rule out other possible causes of symptoms.

Treatments for COPD can’t yet cure the disease. But they can slow the rate at which it gets worse, and make you feel better. Medications can help some people breathe more easily. These include drugs that help open the airways or reduce inflammation.

People with severe COPD may need oxygen therapy. Some may eventually have surgery. But usually only as a last resort if they don’t improve from taking medicines.

Pulmonary rehabilitation can also be used to treat the condition. These programs help you learn new breathing strategies, improve your body’s endurance and strength, and move in ways that preserve your energy. They may also include mental health care and nutrition counseling.

“People with COPD tend to be some of the least physically active people,” says Larson. Many feel embarrassed that they can’t keep up with other people, she explains. This can lead them to avoid activities they used to enjoy.

Larson and her team are testing a program to build light physical activity into the daily lives of people with COPD. Light physical activity—such as walking and using bands for strength exercises—can improve their health and energy levels, she explains.

By making exercise more accessible, “we hope that if they have a flare-up of their disease, and have to quit being active for a while, that they’ll be able to get right back to it on their own,” Larson says.

In an early study of the program, people with COPD increased their activity levels by more than half an hour a day. Talk with your health care provider to learn more about rehab programs.

### Personalizing Treatments

Better treatments are still needed for COPD. Scientists first need to learn more about how the disease differs between people.

“By identifying the different subtypes of COPD, we can then target the specific underlying problems with new therapies,” Woodruff says.

In a recent study, Woodruff’s team identified one such subtype. They found that many current or former smokers had normal results on a spirometry test, but still had other symptoms of COPD, like a chronic cough or trouble breathing.

The researchers are now testing whether COPD drugs can help people with this subtype feel better and prevent them from developing full-blown COPD.

The team also found that some people with COPD have a type of inflammation in their lungs that looks like asthma. This may be another subtype of COPD. Now they’re testing whether asthma medications can help reduce symptoms in these patients.

For now, lifestyle changes and medications can help people with COPD manage symptoms. See the Wise Choices box for tips to help you breathe better.

### Wise Choices

#### Managing COPD

- **Quit smoking and keep your home smoke-free.** Get free help at smokefree.gov, 1-800-QUIT-NOW (1-800-784-8669), or by texting QUIT to 47848.
- **Visit your doctor regularly.** Let them know if any of your symptoms change over time.
- **Protect yourself from germs that can affect the lungs.** Talk with your doctor about which vaccines to get, including for flu (influenza) and pneumonia.
- **Prepare for disease flare-ups.** Know when and where to seek help for your symptoms. Get emergency care if you have severe symptoms, such as trouble catching your breath or talking.
