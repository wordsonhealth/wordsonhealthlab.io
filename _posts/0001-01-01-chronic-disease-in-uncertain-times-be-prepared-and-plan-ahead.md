---
layout: post 
title: "Chronic Disease in Uncertain Times Be Prepared and Plan Ahead"
date: 2022-01-03T00:41:12+01:00
image: assets/images/chronic-disease-in-uncertain-times-be-prepared-and-plan-ahead.webp
excerpt: Planning ahead for emergencies can help make things more manageable for people with long-term health conditions.
draft: false
---

Coping with emergencies is challenging in the best of situations. During the corona-virus pandemic, many of us are just trying to get by each day. For people with chronic (long-term) health conditions—like diabetes and chronic kidney disease—the challenges can be even greater. But with planning, you can prepare what you’ll need to make things more manageable.

“Thankfully, people with chronic medical conditions have tools to help maintain their health, even during difficult times,” says Dr. Griffin P. Rodgers, director of NIH’s National Institute of Diabetes and Digestive and Kidney Diseases. Managing these conditions well can help lower your risk for complications and other diseases.

Keeping on top of health problems can take extra effort during uncertain times. First, be sure to follow the [CDC’s latest public health guidance](https://www.cdc.gov/coronavirus/2019-nCoV/index.html). That awareness is especially important in a pandemic. As we’ve seen with COVID-19, information about new diseases can quickly change.

Rodgers also says it’s important to keep in touch with your health care providers. They can help you to adapt and maintain your normal disease management plans.

Be sure to eat well and safely participate in physical activity as much as possible during these difficult times. That can help you prevent or delay health problems.

Some people with chronic conditions need to follow a special nutrition plan. For example, people with diabetes should follow a healthy eating plan prescribed for blood sugar control. Talk with your health care provider about your physical activity and eating routines.

Eating healthy and staying active can also help lower stress. Coping with uncertainty is stressful for anyone. People with health conditions may feel more stress when their normal routine and health care are disrupted.

There are many ways to lower stress and relax. Ideas include deep breathing, taking a walk, meditating, listening to music, or doing a hobby. Getting enough sleep (seven to eight hours each night) can have tremendous health benefits, including helping to reduce stress and control weight.

Your health care provider can help you find ways to lower your stress and screen for anxiety and depression. Depression is common among people with a chronic illness. And it can get in the way of managing the condition.

“Ask for help if you feel down or need help managing stress,” Rodgers says. “It’s always important to learn ways to lower stress and improve health.”

Preparing for the unexpected will help you manage a chronic health condition during a crisis. Consider packing a specialized “go-kit” for emergencies. See the Wise Choices box for what to include.

Maintaining your health doesn’t erase the risk for getting other diseases. But each healthy day is a day closer to better treatments for diseases. NIH is making a coordinated effort to help advance research on preventing, diagnosing, and treating COVID-19.

Contact your health care provider with any questions or concerns about how to prepare for natural disasters and emergencies.

### Wise Choices

#### Preparing For Disasters With a Chronic Disease

Create a “go-kit” for emergencies:

- At least one week’s worth of medical supplies and equipment. 
- Contact information for health care providers and emergency contacts.
- A medication list with doses and dosing schedules.
- A list of your allergies.
- Information about any medical devices you use.
- At least a three-day supply of any foods needed to manage your condition.
- Copies of your insurance card and photo ID.
- Copies of recent lab work you might need.
