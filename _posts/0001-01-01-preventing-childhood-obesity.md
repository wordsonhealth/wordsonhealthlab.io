---
layout: post 
title: "Preventing Childhood Obesity"
date: 2019-04-20
image: assets/images/preventing-childhood-obesity.webp
excerpt: "New Tools to Promote Healthy Habits"
tags: Childhood, Exercise, obesity, Overweight, physical activity, We Can program, Weight Loss
draft: false
---

More than twice as many children are overweight now than three decades ago. The problem with being overweight is more than just size; it puts kids at risk for developing chronic illnesses like heart disease, diabetes, and asthma. All parents want their child to be as healthy as possible, but many wonder where to start. A new national education program called **We Can**!—**W**ays to **E**nhance **C**hildren’s **A**ctivities and **N**utrition—provides guidance for parents, caregivers, teachers and others who want to help children ages 8-13 maintain a healthy weight.

Research shows that kids are eating too much food that’s high in calories and low in nutritional value. They’re also spending too much time with television and computers and not enough time moving around. The science-based **We Can**! program is a one-stop resource for parents who want to make healthier choices for their families.

The program focuses on three key behaviors that families can work on together: healthy eating, increasing physical activity and reducing recreational “screen time”—time spent watching TV or playing video games or computer games. Making small, easy changes over time—like serving fresh fruit for snacks, replacing regular sodas with water, fat-free or low-fat milk, and taking a walk after dinner instead of turning on the TV—can make a difference in a child’s health.

**We Can**! helps guide food choices, too, by categorizing items into “go” foods that can be eaten almost anytime, “slow” foods that can be eaten at most several times per week, and “whoa” foods that are meant for eating once in a while or on special occasions.

![img](/assets/images/55-Preventing-childhood-obesity.webp)

The **We Can**! program is a collaboration between 4 NIH components: the National Heart, Lung, and Blood Institute, the National Institute of Diabetes and Digestive and Kidney Diseases, the National Institute of Child Health and Human Development, and the National Cancer Institute. The resources available include a parents’ handbook in Spanish or English as well as a tool kit with lesson plans for parents and children.

The parent handbook, Families Finding the Balance, provides realistic tips for adopting healthy habits and making those behaviors stick. It helps parents teach their children to:

- Eat a sufficient amount and variety of fruits and vegetables per day;
- Choose small portions at home and at restaurants;
- Eat fewer high-fat and energy-dense foods that are low in nutrient value such as french fries, bacon, and doughnuts;
- Get at least 60 minutes of moderate physical activity on most, preferably all, days of the week;
- Reduce recreational screen time to no more than two hours per day.

“It’s all about energy in and energy out,” says Dr. Elizabeth G. Nabel, director of the National Heart, Lung, and Blood Institute. “To maintain a healthy weight, we need to strike a balance between the amount and types of food we eat, and the energy we burn up with activity.”

NIH director Dr. Elias Zerhouni says, “Our research shows that the main driver of whether or not you become obese later in life depends on how you were raised and the habits you acquire when you’re a child. That is why it is so important to intervene early.”

The **We Can**! program is designed so that parents or any local civic groups, parent groups, religious groups or other organizations can use the information to begin addressing the problem of overweight children in their community. Organizations such as parks and recreation departments, health departments, and hospitals and health systems in more than 35 communities across the country are already using the lessons and activities to encourage healthy nutrition and physical activity.
