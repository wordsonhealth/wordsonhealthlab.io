---
layout: post 
title: "Safe Driving Protecting Yourself Behind the Wheel"
date: 2022-01-02T19:31:11+01:00
image: assets/images/safe-driving-protecting-yourself-behind-the-wheel.webp
excerpt: Whether you’re a new driver or have been driving for decades, it’s important to think about what keeps you safe.
tags: featured
draft: false
---

Driving a car can give you freedom. But it’s also one of riskiest things you do every day. More than 36,000 people died in car accidents in the U.S. in 2018. Millions more are injured each year.

Many things can make driving risky. Speeding, not paying full attention to the road, and driving while tired all increase your chances of a crash. Drinking or using drugs can be especially dangerous.

Fortunately, there are things you can do to keep yourself and others safe while in the car. Researchers are working to understand what causes crashes and how to prevent them.

### Distracted Driving

You’ve likely seen ads reminding you to keep your eyes on the road. With cell phones and screens everywhere, distracted driving has become a major problem.

“Because we’re so phone driven, the tendency is when somebody calls us or texts us, we want to respond immediately,” says Dr. Bruce Simons-Morton, an NIH expert on teen driving. To drive safely, we have to overcome that powerful impulse, he explains.

Texting can take your eyes off the road for seconds at a time. In just five seconds, you travel the entire length of a football field at 55 miles per hour.

Distraction isn’t limited to phones. It’s anything that takes attention away from driving the car. Eating, playing with the radio, and adjusting your navigation system all distract from safe driving.

“Reaching for objects is also a big problem,” says Simons-Morton. You may take your eyes off the road when you reach for your sunglasses or something in the seat next to you.

People of any age can give in to distractions while driving. Many adults admit to texting, answering calls, and other dangerous behaviors. That’s a problem because teens are modeling their parents’ actions as they learn to drive.

### Teen Drivers

Studies show that teen drivers are at greatest risk for crashes. Crashes are higher among 16- to 19-year-olds than any other age group. That’s because some driving skills get better with experience. Teens are also prone to distraction, especially with friends in the car.

“The first six months of driving on their own is the most dangerous,” says Dr. Ginger Yang, a teen driving expert at Nationwide Children’s Hospital and The Ohio State University. The risk of getting in an accident remains high until at least their early 20s.

To help keep new drivers safe, all states now use a graduated licensing system. The system has three stages. Teens first get their learner’s permit, where they are supervised by an adult driver. After passing their driving test, they receive a license that comes with restrictions, like limits on nighttime driving and passengers. After maintaining a safe driving record for a set period, they can get a standard driver’s license.

Yang explains that parents can sometimes become less engaged when their teens first start driving independently. But even after being handed the keys, teens are still looking to their parents.

“Parents need to be good role models, because teens are still watching and learning from how parents behave,” Yang says. She is currently researching how parents can communicate with their teens to help improve their driving.

Yang and her colleagues have been studying ways to help teens who already have a traffic violation. Her team teaches parents ways to strengthen their child’s motivation to drive more safely. Parents learn to use open-ended questions and be active listeners. This technique is called motivational interviewing.

Yang advises parents that “conversations about safe driving need to be small topics each time but be brought up multiple times.”

Timing is important. Both parent and teen need to be calm for conversations to be effective.

Her past studies suggest parents can make a difference. By motivating their teens to engage in safe driving behaviors early on, parents can help teens establish safe driving habits that they carry into adulthood.

### Older Drivers

Younger drivers aren’t the only group at greater risk of crashes. As you age, physical and mental changes can make driving more dangerous.

“There’s a number of changes that happen in our vision as we grow older,” says Dr. Cynthia Owsley, who studies the impact of aging on vision at the University of Alabama at Birmingham.

Eye diseases, such as glaucoma, naturally get worse with age. Older adults are also more likely to have certain eye conditions that affect sight, like cataracts and age-related macular degeneration.

Problems distinguishing an object from its background, called contrast sensitivity, are also common. “Think of looking through a dirty windshield: Everything looks kind of washed out,” Owsley says.

Vision problems can also affect your ability to see to the side, or peripheral vision. This can make it harder to see cars in the lanes next to you.

For older adults, changes in the brain can make driving riskier, too. Owsley and others have shown that cognitive decline—problems with memory and other brain functions—increases the likelihood of a car crash.

Changes in physical ability, such as strength and reflexes, can also make driving more dangerous as you age. But getting older doesn’t necessarily mean you have to stop driving.

“I think the public worries about older drivers, but actually most older drivers are quite safe,” Owsley says. It’s older drivers with visual and cognitive impairments that are at greatest risk.

If you’re concerned about an older person’s driving, it’s important to start a conversation with them. Experts advise watching for the signs that driving is getting unsafe, like getting lost on familiar routes, experiencing a near-miss, or receiving a traffic ticket.

You can contact a driving assessment clinic as well. These clinics can provide a professional evaluation of a person’s driving ability. If driving is no longer safe, work with the older adult to develop a plan for getting around without a car.

### Safer Driving

Whether you’re a new driver or have been driving for decades, it’s important to think about safe driving.

The good news is that advances in car design and safety technology are helping protect you behind the wheel. You can also do several things to reduce your risk of an accident. Always stay alert. Resist the urge to text or talk on the phone. For more safe driving tips, see the Wise Choices box.

### Wise Choices

#### Safe Driving Tips

- Always keep your eyes on the road and your hands on the wheel.
- Don’t multi-task, like talking or texting, eating and drinking, or fiddling with the stereo, entertainment, or navigation system.
- Wear your safety belt.
- Drive at the speed limit. It’s unsafe to drive too fast or too slowly.
- Obey all traffic signs.
- Keep enough distance between you and the car in front of you to avoid a crash.
- Don’t drink and drive.
- When you take a new medicine, ask your doctor or pharmacist about side effects. Some can affect your driving.
- If you have glasses or contact lenses, make sure you have a current prescription and wear them while driving. 
- Don’t wear sunglasses or tinted lenses at night.
