---
layout: post 
title: "Going Under a Closer Look at Anesthesia"
date: 2022-01-02T18:57:23+01:00
image: assets/images/going-under-a-closer-look-at-anesthesia.webp
excerpt: Advances in how patients are monitored are helping to make administering anesthesia safer.
draft: false
---

Every day, people undergo surgery. Some operations require going deep into the body. Some can take many hours to finish.

Many of these procedures are possible only because of anesthesia. Different types of anesthesia may be used depending on the procedure. 

Local anesthesia numbs a small part of the body, such as a tooth. Regional anesthesia is used for larger areas of the body such as an arm, a leg, or everything below the waist. People remain awake during local and regional anesthesia, but pain is reduced or blocked.

General anesthesia uses drugs that make people unconscious and unable to feel pain or move. It’s used for long or invasive procedures. Every year, millions of people in the nation undergo general anesthesia.

“Doctors and nurses have used general anesthesia for over 170 years,” explains Dr. George Mashour, an NIH-funded anesthesia expert at the University of Michigan. 

Since that time, new drugs and advances in how patients are monitored have helped make administering anesthesia safer. We know a lot about how general anesthesia affects the heart, lungs, and many other organs. Clinicians can use machines to track how these organs are functioning while people are unconscious. This lets them precisely adjust anesthesia doses to keep people as safe as possible.

But how general anesthesia affects the brain is still not fully understood. NIH-funded researchers are studying ways to track brain activity during anesthesia. This may help them better understand anesthesia’s effects on the brain in general. It may also help them develop safer and more effective drugs.

General anesthesia does come with some risks. A rare complication is awareness under anesthesia. This is when a person gains some awareness during surgery, when they should be unconscious. Most often, this awareness is only of sounds. But sometimes it can include pain.

A more common unwanted effect of surgery and anesthesia is delirium. In delirium, a person is confused and often unclear about what is happening to them. Many people over the age of 60 experience delirium in the days following surgery. It’s also common in children when they awake from anesthesia.

“Ideally, we want to ensure that we are providing enough anesthesia to the brain to suppress consciousness and memory. But we don’t want to provide so much that we induce unwanted effects on brain function after surgery,” says Mashour.

Mashour and his team recently used thin wires placed on the scalp to measure electrical activity in the brain during anesthesia. They saw several shifts in the way areas of the brain communicated while people were unconscious. 

Different shifts were seen during different stages of anesthesia, such as going under and waking up. Understanding such brain activity patterns may help scientists develop a brain monitor for anesthesia, Mashour says. Studying anesthesia could also yield insight into consciousness itself and conditions where consciousness is impaired, such as sleep or coma. 

If you’re afraid of having general anesthesia, talk with your doctor. They can give you more information about the drugs used. Some procedures can also be done with types of anesthesia where you remain awake.

See the Ask Your Doctor box for questions you can ask before going under. 

### Ask Your Doctor

Before going under anesthesia, ask:

- Which type of anesthesia will I have? Do I have more than one option?
- Do I need to stop taking any of my medications before anesthesia?
- Do I need to do anything special before I have anesthesia?
- Will I be awake or aware of what is happening during anesthesia? 
- Will I feel any pain?
- How soon will I wake up after anesthesia? 
- Will I have any side effects from the drugs used?
- How long will I need to stay in the hospital afterward?
