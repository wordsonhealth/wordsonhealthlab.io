---
layout: post 
title: "How to Stop an Outbreak"
date: 2019-04-30
image: assets/images/how-to-stop-an-outbreak.webp
excerpt: "Computers Help Plan for a Deadly Flu"
tags: Bird Flu, Flu, Flu vaccine, Influenza, NIGMS
draft: false
---

As you stand in line waiting to roll up your sleeve for that annual flu shot, you may wonder why you need the influenza vaccine every year when other types of vaccines offer a lifetime of protection. The reason is that the virus that causes the flu constantly changes over time, so the vaccine you got last year may not protect you against the bug this year. Each year, researchers try to predict how the flu virus will change and develop a new vaccine before flu season starts.
But what if a different, deadlier type of flu started to quickly spread? One bug now has some researchers worried, and they are using computer technology to figure out how best to fight a potential outbreak.

In 1997, a type of flu found in poultry infected 18 people in Hong Kong and 6 of them died. As this bird flu spread to other fowl and moved into other areas of Southeast Asia, more human cases appeared. To date, 112 people have caught bird flu and more than half of them have died. This number may seem small compared to the thousands who die annually from flu-related complications, but for some health officials the bird flu deaths may be an early warning for a deadly worldwide outbreak.

Fortunately, the bird flu doesn’t currently spread easily from person to person, but a change in the virus could soon make this possible. If this happens, an outbreak could quickly spread across the globe to become a pandemic. Because most people haven’t been exposed to bird flu and have no natural protection against it, millions could get sick and even die. The two most recent pandemics caused by a combination of bird and human flu viruses, in 1957 and 1968, killed more than 100,000 Americans. The 1918 flu pandemic caused more than half a million deaths in the U.S. and up to 50 million worldwide.

![img](/assets/images/60-How-To-Stop-an-Outbreak.webp)

Researchers can’t predict when or even if the bird flu in Southeast Asia could turn into a pandemic. But to be better prepared in case an outbreak begins, scientists are working to understand the virus, its impact and what can be done to prevent its spread.

“The pressing questions are if and how we can contain an outbreak of [bird] flu at the source before it becomes a pandemic,” said Dr. Ira Longini, Jr. of Emory University, a member of a research group supported by NIH’s National Institute of General Medical Sciences (NIGMS) that’s using computers to investigate what we can do to stop an outbreak at the source.

Longini and his colleagues created computer models, virtual laboratories where they could study different situations based on actual data. These models, built on census statistics collected by the Thai government and information about the infectiousness of previous flu viruses, let the researchers model mock bird flu outbreaks in Southeast Asian communities.

With each scenario the researchers developed, they watched what happened as the virus spread across the virtual map. Did vaccinating people, even if the vaccine didn’t work very well against the new virus, slow the flu’s spread? Did antiviral drugs, which can lessen flu symptoms and prevent new infections, help contain the infection? Was quarantining people effective?

The researchers found that a combination of measures, like giving antiviral medications plus quarantining everyone near an infected person, could stop the virus in its tracks if started early enough. Additional strategies, including vaccination, were needed when the virus was more contagious.

The scientists will adjust the computer models in response to new information about the bird flu and human cases. “As these modeling approaches develop, they will offer policymakers and researchers powerful tools to use in strategic planning,” said NIGMS director Dr. Jeremy M. Berg.

These computer models are just one way that NIH-supported scientists are working toward understanding and preparing for a bird flu pandemic. Researchers funded by NIH’s National Institute of Allergy and Infectious Diseases (NIAID) have tested and shown that one of the antiviral drugs currently used to treat the symptoms of seasonal flu also could work for bird flu. Other NIAID researchers are testing a bird flu vaccine and expect results by the end of the year.

Each year in the United States, more than 100,000 people are hospitalized and about 36,000 people die from the flu and its complications, the Centers for Disease Control and Prevention estimates. Understanding how flu spreads and how the tools we have affect its spread are crucial to fighting future outbreaks.

*Definitions*

**Influenza:**
The flu, an illness caused by viruses that attack the respiratory system. Symptoms include fever, chills, body aches, runny nose, sore throat, headache and extreme exhaustion.

**Vaccine:**
A product that is injected, swallowed or inhaled to activate the body’s defenses and prevent future infection.

**Pandemic:**
A disease outbreak that spreads to people in different parts of the world.

**Computer model:**
A computer program that predicts the results of a series of complex events.

**Antiviral drug:**
Medication used to destroy or weaken a virus after infection.

*Statistics*

**Bird Flu**

Poultry infected with bird flu have been found in Indonesia, Vietnam, Thailand, Cambodia, China, Japan, Korea, Russia and Kazakhstan.
Most human cases of bird flu have been traced back to direct contact with diseased or dead poultry in rural areas of Southeast Asia.
Among the 112 people who have caught bird flu, 57 have died.

Source: World Health Organization

***A Word to the Wise...***

*How to Avoid The Flu:*

- Get a yearly flu vaccine if your doctor recommends it. It’s the best way to keep from catching the flu viruses that go around each year. Keep in mind, though, that the vaccines currently available to the public won’t yet protect you from bird flu.
- Stay home when you’re sick and avoid contact with others who are sick. These simple measures can help stop the spread of influenza and other contagious illnesses.
- If you travel to Southeast Asia, use caution when handling and cooking poultry that may be infected with bird flu.
