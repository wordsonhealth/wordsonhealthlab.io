---
layout: post 
title: "Preventing College Age Alcohol Abuse"
date: 2019-03-28
image: assets/images/preventing-college-age-alcohol-abuse.webp
excerpt: "Warning Signs, and Prevention Tips"
tags: Alcohol, Alcohol Abuse, College Drinking, NIAAA
draft: false
---

Young adults going off to college can expect to be exposed to many new experiences. Unfortunately, one of them may be heavy drinking. Alcohol abuse is now a widespread problem on the nation’s college campuses. In the short run, it puts students at risk for car accidents, date rape, and academic, medical, and legal problems. In the long run, it may establish a pattern of drinking that can lead to alcoholism and serious health problems.

Studies show that four out of five college students drink alcohol. Two out of five report binge drinking (defined as five or more drinks for men and four or more for women in one sitting). One in five students reports three or more binge episodes in the prior two weeks.

“For many students drinking is seen as a rite of passage, as part of having fun, of lowering social inhibitions,” says Dr. Vivian Faden, acting associate director of the Division of Epidemiology and Prevention Research at NIH’s National Institute on Alcohol Abuse and Alcoholism (NIAAA). “Other kids may self-medicate with alcohol to reduce stress or cope with depression.”

Whether a student drinks heavily depends on a host of factors, including a family history of alcohol abuse, the student’s experience with drinking in high school, and psychological problems such as depression and anxiety.

**Problems with College Drinking**

“People are not aware of the size of the problems that occur with college drinking,” says Dr. Mark Goldman, associate director of NIAAA. “They hear about an unfortunate accident, but they think of it as something that happens rarely. The point is, it doesn’t happen rarely.”

An extensive three-year investigation by the Task Force on College Drinking, commissioned by the NIAAA, produced the 2002 landmark report A Call to Action: Changing the Culture of Drinking at U.S. Colleges. The report estimated that 1,400 college students between the ages of 18 and 24 die each year from alcohol use, either because of drinking and driving or from the toxic effects of alcohol itself. Over 70,000 students are the victims of alcohol-related sexual assault or date rape. Another 500,000 are unintentionally injured in alcohol-related incidents, and 600,000 are assaulted by another student who has been drinking.

“If one puts these numbers together and keeps in mind that there are roughly eight million college students in the United States at any one time,” says Dr. Goldman, “this means that a large percentage of students is suffering the ill effects of college-age drinking.”

There are still more consequences to college student alcohol abuse. Students who binge drink are more likely to have unprotected sex, and 100,000 reported having been too intoxicated to know if they even consented to sex. Not surprisingly, drinking also affects academics, contributing to students missing classes, doing poorly on exams and papers, and receiving low grades.

Alcohol abuse among students also leads to property damage on campus and in surrounding communities. In addition, drinking affects non-drinking students, as noisy parties disrupt their study time, make it hard to sleep in dorms, and force them to deal with intoxicated students.

**Prevention on Campus**

Researchers have found that the most important step in stopping alcohol abuse is changing the culture of college drinking. More and more colleges and universities are dealing with college-age drinking through prevention education and counseling services. Strategies that have proven the most effective include addressing students’ alcohol-related attitudes and behaviors. This can involve challenging student beliefs about the likely effects of alcohol (that it will loosen inhibitions, for example) and their often-erroneous perception that their peers drink more than they do.

**How Do You Know if You Drink Too Much?**

If you are drinking too much alcohol, you can improve your life and health by cutting down. But how do you know if you drink too much? Read these questions and answer “yes” or “no”:

- Do you drink alone when you feel angry or sad?
- Does your drinking ever make you late for work?
- Does your drinking worry your family?
- Do you ever drink after telling yourself you won’t?
- Do you ever forget what you did while you were drinking?
- Do you get headaches or have a hang-over after you have been drinking?

If you answered “yes” to any of these questions, you may have a drinking problem. Check with your doctor to be sure. Your doctor will be able to tell you whether you should cut down or abstain. If you are alcoholic or have other medical problems, you should not just cut down on your drinking you should stop drinking completely. Your doctor will advise you about what is right for you.

Giving students nonjudgmental advice about their drinking has also helped. NIAAA research shows that college students who get even one counseling session offering feedback on their drinking habits will reduce their alcohol consumption.

**What Parents Can Do**

Parents should get involved while their children are still looking at colleges, according to Dr. Faden. “When parents visit colleges, it’s important to ask questions about alcohol policies and the campus culture,” she says. Dr. Goldman adds, “I know of schools that have changed their drinking policies [due to the influence of parents], and as a result have upgraded their student body.”

Dr. Goldman advises that “parents should not accept the idea that this is a college student rite of passage, but rather that this is a serious issue and pressure needs to be put on the schools to pay attention to this.” Parents can also help by encouraging their sons and daughters to take responsibility to make healthy choices with their lives, and by staying involved in their children’s lives even when they’re away at college. Says Dr. Faden, “It’s important for parents to talk to their kids, and often.”

***A Word to the Wise...***

*Staying Involved When Your Child Goes to College*

Make sure to ask about school alcohol policies when you are looking at potential colleges with your child. A list of many college alcohol policies can be found at www.collegedrinkingprevention.gov/
policies. Once your child begins college, pay special attention to his or her experiences and activities during the crucial first six weeks on campus. Many students initiate heavy drinking during these early days of college, and excessive alcohol consumption can interfere with their successful adaptation to campus life. About one-third of first-year students fail to enroll for their second year. Some things you can do:

- Find out if there is a program during orientation that educates students about campus policies related to alcohol use. If there is one, attend with your son or daughter, or at least be familiar with the name of the person who is responsible for campus counseling programs.
- Inquire about and make certain you understand the college’s “parental notification” policy.
- Call your son or daughter frequently during the first six weeks of college.
- Inquire about their roommates, the roommates’ behavior, and how disagreements are settled or disruptive behavior dealt with.
- Make sure that your son or daughter understands the penalties for underage drinking, public drunkenness, using a fake ID, driving under the influence, assault, and other alcohol-related offenses.
- Make certain that they understand how alcohol use can lead to date rape, violence, and academic failure.

Adapted from the NIAAA brochure What Parents Need to Know About College Drinking.
