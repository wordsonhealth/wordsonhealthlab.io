---
layout: post 
title: "Childhood Obesity on the Rise"
date: 2019-02-28
image: assets/images/childhood-obesity-on-the-rise.webp
excerpt: "Preventing Obesity in Children, Causes of Child Obesity, and More"
tags: Childhood, obesity, Overweight, physical activity, Weight Loss
draft: false
---

Open your window on a sunny afternoon, and what do you hear? The chirping of singing birds? The yelling of playing children? Odds are these days that you’ll hear the birds but not the children. As kids spend more time in front of TV, Video Games, and the Internet, their physical activity levels have decreased. And their body weights have increased.

Obesity in kids is now epidemic in the United States. The number of children who are overweight has doubled in the last two to three decades; currently one child in five is overweight. The increase is in both children and adolescents, and in all age, race and gender groups.

Obese children now have diseases like type 2 diabetes that used to only occur in adults. And overweight kids tend to become overweight adults, continuing to put them at greater risk for heart disease, high blood pressure and stroke. But perhaps more devastating to an overweight child than the health problems is the social discrimination. Children who are teased a lot can develop low self-esteem and depression.

There are many causes of obesity. While there’s no doubt genetics plays a role, genes alone can’t account for the huge increase in rates over the past few decades. The main culprits are the same as those for adult obesity: eating too much and moving around too little. Almost half of children aged 8-16 years watch three to five hours of television a day. Kids who watch the most hours of television have the highest incidence of obesity.

**Did You Know That…**

- Obese children and adolescents have shown an alarming increase in the incidence of type 2 diabetes, also known as adult-onset diabetes.
- Many obese children have high cholesterol and blood pressure levels, which are risk factors for heart disease.
- One of the most severe problems for obese children is sleep apnea (interrupted breathing while sleeping). In some cases this can lead to problems with learning and memory.
- Obese children have a high incidence of orthopedic problems, liver disease, and asthma.
- Overweight adolescents have a 70 percent chance of becoming overweight or obese adults.

If you’re concerned your child may be overweight, talk with their doctor. A health care professional can measure your child’s height and weight and calculate a ratio known as body mass index (BMI). This number is compared to a growth chart for children of your kid’s age and gender to determine whether his or her weight is in a healthy range.

**Encourage Activity**

You can help your children maintain a healthy body weight by encouraging them to be active. Try taking them to a park. According to the National Recreation and Park Association (NRPA), 75 percent of Americans live within a two-mile walking distance of a public park.

The National Heart, Lung, and Blood Institute (NHLBI) of the National Institutes of Health has teamed up with NRPA to offer a nationwide program called Hearts N’ Parks. Park and recreation departments and other community-based organizations receive assistance from NHLBI on providing activities for kids and adults that encourage healthy lifestyle choices. The goals are to reduce obesity and the risk of heart disease by encouraging nutritious eating habits and regular physical activity. Kids may go on field trips to local grocery stores and restaurants to learn how to make healthy selections and read food labels. They might participate in soccer, tennis, basketball, bowling, swimming, or hiking.

Karen Donato, coordinator of NHLBI’s Obesity Education Initiative, says that the program emphasizes non-competitive activities where everyone joins in the fun. “There shouldn’t be kids sitting on the sidelines,” she says.

An increasing number of schools are also encouraging healthy lifestyle behaviors. More nutritious choices in cafeterias and vending machines, such as salad bars and baked food rather than fried, encourage kids to try items other than sodas, candy bars and french fries. Some schools offer opportunities for increased physical activity through intramural sports programs and good-old-fashioned recess. A recent report from the U.S. Surgeon General’s office calls on schools to provide daily physical education (PE) for all grades. In schools where PE classes are offered, kids are now engaging in more activities that emphasize personal fitness and aerobic conditioning, rather than the competitive dodge-ball games you may recall from childhood.

Parents can get involved by making sure that their schools have healthy food options and provide PE. PTAs are a good place to speak out and take an active role.

While children can play ball at the local park and choose healthier foods in school, at the end of the day family support is what really counts. You are a role model for your kids. Children form habits from parents, and usually maintain them into adulthood. If your children see you reach for a banana instead of a brownie, they are likely to do the same. If they see you go for a walk or wash the car, they may join in.

When was the last time you biked or shot some hoops? According to Donato, sometimes we all need to be reminded to have fun and appreciate the value of play. Instead of opening your window to listen to the sounds of the season, open your door, go outside with your children and play.

***A Word to the Wise...***

*Help your children maintain a healthy body weight*

- Be supportive. Children know if they are overweight and don’t need to be reminded or singled out. They need acceptance, encouragement and love.
- Set guidelines for the amount of time your children can spend watching television or playing video games.
- Plan family activities that involve exercise. Instead of watching TV, go hiking or biking, wash the car, or walk around a mall. Offer choices and let your children decide.
- Be sensitive. Find activities your children will enjoy that aren’t difficult or could cause embarrassment.
- Eat meals together as a family and eat at the table, not in front of a television. Eat slowly and enjoy the food.
- Don’t use food as a reward or punishment. Children should not be placed on restrictive diets, unless done so by a doctor (for medical reasons). Children need food for growth, development and energy.
- Involve your children in meal planning and grocery shopping. This helps them learn and gives them a role in the decision making.
- Keep healthy snacks on hand. Good options include fresh, frozen, or canned fruits and vegetables; low-fat cheese, yogurt or ice cream; frozen fruit juice bars; and cookies such as fig bars, graham crackers, gingersnaps or vanilla wafers.
- Focus on small, gradual changes in eating and activity patterns. This helps form habits that can last a lifetime.
